import ROOT
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-f","--file"     , type=str           , default=''	, help="specify root file")  
parser.add_argument("-t","--nTrigger"     , type=int           , default=500	, help="number of triggers from analog Scan")  
args = parser.parse_args()

trigger = args.nTrigger

f = ROOT.TFile(args.file)
pPulsed_h2 = f.Get("pulsedPixels")
nHits_h2 = f.Get("Hits")

can = ROOT.TCanvas()
nHits_h2.SetMinimum(0)
nHits_h2.Draw("colz")




nPix = 0
n0=0 
nFewer=0 
nMore=0  
nClose=0  
nCloser=0 
nPerfect = 0
nNoisy = 0
nLoud =0
nQuiet = 0
nQuieter = 0
nAlive = 0
nUseless = 0 
  
for i in range (1, 513):
	  for j in range (1, 513):
		  
		  if (pPulsed_h2.GetBinContent(i,j)==0): continue
		  nPix += 1
		  n = nHits_h2.GetBinContent(i,j)
		  
		  if (n<trigger * 0.9 ): nFewer+=1
		  if (n>trigger *1.1):  nMore+=1
		  if ((abs(n- trigger) <= 0.1*trigger) and abs(n- trigger) > 0.05*trigger  ): nClose+=1
		  if (abs(n- trigger) <= 0.05*trigger ): nCloser+=1
		  
		  if (n==0): n0+=1
		  if (n >= trigger -1 and n <= trigger +1 ):  nPerfect+=1
		  if (n > trigger + 1 and n < trigger *1.2  ): nNoisy+=1
		  if (n >= trigger *1.2  ): nLoud+=1
		  if (n < trigger -1 and n >= trigger*0.95  ): nQuiet +=1
		  if (n < trigger * 0.95 and n >= trigger *0.9): nQuieter+=1
		  if (n < trigger * 0.9 and n >= trigger *0.5): nAlive+=1
		  if (n < trigger * 0.5 and n > 0): nUseless+=1
   
print "statistics for pulsing front-end of ",nPix,"Pixels:"
print ""
print "First set:"   
print "# of pixels with 0 hits: ",n0 
print "# of pixels with  # of hits < 90%  nTrigger: ",nFewer
print "# of pixels with # of hits > 110% nTrigger: ",nMore 
print "# of pixels # of hits within 10% of nTriggger: ",nClose 
print "# of pixels # of hits within 5% of nTriggger: ",nCloser 
print ""
print "Second set:"
print "# of pixels with exactly nTrigger hits: ",nPerfect
print "# of pixels with 100% to 120% of nTrigger: ",nNoisy
print "# of pixels with more than 120% of nTrigger: ",nLoud
print "# of pixels with 95% to 100% of nTrigger: ",nQuiet
print "# of pixels with 90% to 95% of nTrigger: ",nQuieter
print "# of pixels with 50% to 90% of nTrigger: ",nAlive
print "# of pixels with 0% to 50% of nTrigger: ",nUseless
print "# of pixels with 0 hits: ",n0 

raw_input("press any key")
