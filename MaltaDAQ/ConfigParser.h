#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H

#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <stdint.h>
#include "ReadoutConfig.h"

/**
 * ConfigParser is a tool that reads configuration files for MaltaMultiDAQ.
 * The configuration files are opened and parsed with ConfigParser::Open 
 * into ReadoutConfig objects, where each ReadoutConfig object represents
 * a Plane. The number of Planes is obtained with ConfigParser::GetN,
 * and the ReadoutConfig objects are accessible with the methods
 * ConfigParser::GetConfig and ConfigParser::GetConfigBySample.
 * The most representative parameters for the Plane are the type 
 * (MaltaBase, MiniMalta), and the sample name (W4R10).
 * An additional ReadoutConfig object can be added to the configuragion 
 * with the method ConfigParser::Add for testing purposes, but the intended 
 * operation is to load Planes exclusively through configuration files.
 * The configuration files follow the following format:
 * 
 * @verbatim
   [PLANE 1]
   type: MALTA
   wafer: W4R10
   address: 192.168.200.10
   
   [PLANE 2]
   type: MALTA
   wafer: W4R11
   address: 192.168.200.11
   @endverbatim
 *
 * The configuration loaded with ConfigParser::Open is persistent, and can 
 * be extracted at any time as a standard map of a map of strings with
 * ConfigParser::ConfigForROOTFile, or written into a new file with 
 * ConfigParser::WriteConfigFile.
 *
 * To include another file from the main configuration file, the statement 
 * (\c include: \c file) is allowed, where \c file must be the 
 * path to the file to be included. This statement will have
 * precedence over any other statements, and the contents of the included file
 * will be inserted in place of the statement. This is done before parsing, 
 * and can be used to include any sequence of statements. An example is the 
 * following:
 * 
 * @verbatim
   [PLANE 1]
   address: 192.168.200.10
   include: MALTA_W4R10.txt
   
   [PLANE 2]
   address: 192.168.200.11
   include: MALTA_W4R11.txt
   @endverbatim
 *
 *  
 * @brief Parse plain text configuration files into ReadoutConfig objects 
 * @author Carlos.Solans@cern.ch
 **/

class ConfigParser{

 public:
  
  /**
   * @brief Empty constructor
   **/
  ConfigParser();

  /**
   * @brief Clear the memory, delete the ReadoutConfig objects, close the file
   **/
  ~ConfigParser();

  /**
   * ConfigParser::Open first loads the contents of the file, 
   * and then parses its contents.
   * The loading expands the \c include statements and
   * inserts the contents of the included files into the 
   * array of strings to be parsed. 
   * The parsing interprets all other statements and
   * accordingly creates the ReadoutConfig objects that are stored
   * in memory.
   *
   * @brief Parse a file
   * @param file path to the file to parse
   * @return true if configuation was parsed properly
   **/
  bool Open(std::string file);

  /**
   * @brief Print the parsed configuration
   **/
  void Print();

  /**
   * @brief Get number of ReadoutConfig objects parsed.
   * @return number of ReadoutConfig objects
   **/
  uint32_t GetN();

  /**
   * This method allows to get the ReadoutConfig objects from the ConfigParser.
   * The return value will be NULL if the provided index is greater than the 
   * number of ReadoutConfig objects available in memory.
   * The returned objects is not a copy, and there is no need to delete it.
   * The ConfigParser will delete the ReadoutConfig objects in memory, when a new
   * file is opened or when a the ConfigParser is deleted.
   *
   * @brief Get the ReadoutConfig object from the list of objects at a given index.
   * @param module The index of the object to be returned from the list.
   * @return The ReadoutConfig object
   **/
  ReadoutConfig * GetConfig(uint32_t module);

  /**
   * @brief Add a new ReadoutConfig object for tests.
   * @param config a pointer to a ReadoutConfig object
   **/
  void Add(ReadoutConfig * config);

  /**
   * @brief Provides the configuration for a given sample name (testing phase)
   * @param sample name, e.g.:"W1R2"
   * @return sample configuration
   **/
  ReadoutConfig * GetConfigBySample(std::string sample);

  /**
   * @brief Enable verbose mode
   * @param enable bool
   **/
  void SetVerbose(bool enable);

  /**
   * @brief Writes the configuration file in memory to file.
   * @param filename Path to the file to write the memory to.
   **/
  void WriteConfigFile(std::string filename);

  /**
   * @brief Writes the configuration file in memory to a ROOT file, 
   * the format of the map is map[chipname][key]=val where chipname, key and val are std::string
   **/
  std::map<std::string,std::map<std::string,std::string>> ConfigForROOTFile();


 private:

  void Clear();

  std::vector<ReadoutConfig*> m_configs;

  std::string Trim(std::string str);
  
  std::string ToLower(std::string str);

  std::vector<std::string> Load(std::string filename);
  
  ReadoutConfig* FindConfig(std::string name);

  bool Parse(std::vector<std::string> &raw);

  bool m_verbose;

};

#endif
