#ifndef MALTASOCKETDAQ_H
#define MALTASOCKETDAQ_H

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <vector>
#include "MaltaDAQ/ReadoutModule.h"
#include <thread>



class MaltaSocketDAQ{

  public:
    ~MaltaSocketDAQ();
    int m_sockfd;
    std::vector<ReadoutModule*> m_maltas;
    std::thread m_thread_socket;
    void Stop();
    void ConfigureSocket(std::vector<ReadoutModule*> maltas);  
    void ReadSocket();//std::vector<ReadoutModule*> maltas); 

  private:
    bool m_cont=true; 
};
#endif
