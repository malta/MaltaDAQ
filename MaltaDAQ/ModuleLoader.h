#ifndef MODULELOADER_H
#define MODULELOADER_H

// #################################
// # ModuleLoader
// # Carlos.Solans@cern.ch
// # August 2017
// ################################

#include <string>
#include "MaltaDAQ/ReadoutModule.h"

typedef ReadoutModule* (*ReadoutModuleBuilder) (std::string name, std::string address, std::string outdir);

/**
 * ModuleLoader loads a predefined list of classes from a library at runtime. 
 * The newly created object must be deleted by the user.
 * 
 *  * @verbatim

   ReadoutModule * malta = ModuleLoader::getInstance()->newReadoutModule("MaltaDAQ","MaltaModule",
                                                                         "PLANE_1","192.168.200.10","");
																		 
   @endverbatim
 * 
 *
 * @brief Tool to dynamically load from a ReadoutModule from a library.
 * @author Carlos.Solans@cern.ch
 * @date Sometime in 2019
 */
class ModuleLoader {

public:

  /**
   * ModuleLoader destructor
   * @brief Unload any loaded module
   */
  ~ModuleLoader();

  /**
   * @brief Guess the Library Name from the class type
   * @param type class type.
   * @return string with the library name extracted from the front of the class type.
   * Try to find what is the library name for a given class based on the case.
   * Library is the string up to the second time lower to upper case is found.
   */
  static std::string getLibName(std::string type);

  /**
   * @brief Get ModuleLoader singleton
   * @return ModuleLoader pointer to ModuleLoader instance
   */
  static ModuleLoader * getInstance();

  /**
   * @brief Create a new ReadoutModule object
   * @param library name of the library that contains the class
   * @param type class name of the object to create
   * @param name friendly name for the module to create
   * @param address address of the module to create
   * @param outdir output directory
   * @return pointer to new ReadoutModule object 
   */
  //static ReadoutModule * newReadoutModule(std::string library, std::string type);
  static ReadoutModule * newReadoutModule(std::string library, std::string type, 
					  std::string name, std::string address, std::string outdir);

  
private:
  ModuleLoader();                        //! Default constructor not accessible
  static ModuleLoader * m_me;            //! Singleton ModuleLoader object
  std::map<std::string,void*> m_libs;    //! Map of libraries loaded
  void * getHandle(std::string library); //! Getter for library handle
};

#endif
