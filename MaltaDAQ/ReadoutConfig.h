#ifndef READOUTCONFIG_H
#define READOUTCONFIG_H

#include <string>
#include <vector>
#include <utility>
#include <map>

/**
 * ReadoutConfig holds the configuration for a generic pixel detector Plane.
 * The basic attributes are the \c name (ReadoutConfig::GetName), \c wafer
 * (ReadoutConfig::GetWafer), \c type (ReadoutConfig::GetType), and \c address
 * that correspond to the given name of the plane (\c PLANE_1), the wafer
 * identifier (W4R10), the type of detector being used (MALTA, MiniMALTA, or else),
 * and the ipbus connection string (ReadoutConfig::GetAddress).
 * Other attributes that are relevant to MALTA are the \c DUT (ReadoutConfig::IsDUT),
 * and the \c configuration \c voltage (ReadoutConfig::ConfigVoltage).
 * Additional attributes are available to define the name of the class that handles
 * the configuration and read-out of the plane (ReadoutConfig::GetClassname), and
 * the library that contains it (ReadoutConfig::GetLibrary).
 * The ReadoutConfig object also stores any other configuration statement
 * as keys, that are accessible as a vector with ReadoutConfig::GetKeys.
 * Finally, the whole configuration stored in the ReadoutConfig object
 * can be extracted as a map of strings with ReadoutConfig::ConfigForROOTFile.
 *
 * ReadoutConfig objects are created by the ConfigParser class from configuration files.
 * However, a new ReadoutConfig object can be created programatically like this:
 * @verbatim
   ReadoutConfig rc;
   rc.SetName("PLANE_1");
   rc.SetAddress("upd://192.168.200.10");
   rc.SetWafer("W4R10");
   rc.SetType("MALTA");
   @endverbatim
 *
 * The configuration loaded with ConfigParser::Open is persistent, and can
 * be extracted at any time as a standard map of a map of strings with
 * ConfigParser::ConfigForROOTFile.
 *
 * @brief Generic plane configuration class
 * @author Carlos.Solans@cern.ch
 *         Valerio.Dao@cern.ch
 **/
class ReadoutConfig{

 public:

  /**
   * @brief Create an empty ReadoutConfig
   **/
  ReadoutConfig();

  /**
   * @brief Delete the configuration
   **/
  ~ReadoutConfig();

  /**
   * @brief Set the name
   * @param name Desired name
   **/
  void SetName(std::string name);

  /**
   * @brief Set the address
   * @param addr Desired address
   **/
  void SetAddress(std::string addr);

  /**
   * @brief Set the type
   * @param typ Desired type
   **/
  void SetType(std::string typ);

  /**
   * @brief Set the class name
   * @param cname Desired class name
   **/
  void SetClassname(std::string cname);

  /**
   * @brief Set the library
   * @param library Desired library
   **/
  void SetLibrary(std::string library);

  /**
   * @brief Set the wafer
   * @param wafer Desired wafer
   **/
  void SetWafer(std::string wafer);

  /**
   * @brief Set the DUT
   * @param dut Desired DUT setting (True or False)
   **/
  void SetDUT(std::string dut);

  /**
   * @brief Set the configuration voltage
   * @param value Desired configuration voltage as a string
   **/
  void SetConfigVoltage(std::string value);

  /**
   * @brief Set the module size (1 chip per default), i.e. the number of MALTA chips on the readout board
   * @param number of chips (1 is default, other options are 2 or 4)
   **/
  void SetModuleSize(int32_t modulesize = 1);

  /**
   * @brief Add a mask string
   * @param mask Desired mask string
   **/
  void AddMaskString(std::string mask);

  /**
   * @brief Add a mask pixel
   * @param pixx The X position of the masked pixel
   * @param pixy The Y position of the masked pixel
   **/
  void AddMask(int32_t pixx, int32_t pixy);

  /**
   * @brief Add an additional key
   * @param key The additional key
   **/
  void AddKey(std::string key);

  /**
   * @brief Get the name
   * @return The given name
   **/
  std::string GetName();

  /**
   * @brief Get the ipbus address
   * @return The ipbus address
   **/
  std::string GetAddress();

  /**
   * @brief Get the type
   * @return The type
   **/
  std::string GetType();

  /**
   * @brief Get the library
   * @return The library
   **/
  std::string GetLibrary();

  /**
   * @brief Get the class name
   * @return The class name
   **/
  std::string GetClassname();

  /**
   * @brief Get the wafer identifier
   * @return The wafer identifier
   **/
  std::string GetWafer();

  /**
   * @brief Get the sample name
   * @return The sample name
   **/
  std::string GetSample();

  /**
   * @brief Get the dut attribute
   * @return The dut attribute
   **/
  std::string GetDUT();

  /**
   * @brief Get the module size, i.e. the number of chips in the module
   * @return The number of chip. Default is 1, optional is 2 or 4
   **/
  int32_t GetModuleSize();

  /**
   * @brief Check if DUT is true or false.
   * @return The value of DUT casted as an integer.
   **/
  int32_t IsDUT();

  /**
   * @brief Get the configuration voltage
   * @return The configuration voltage as a string
   **/
  std::string GetConfigVoltage();

  /**
   * @brief Get the configuration voltage as a float.
   * @param defValue The default value in case the config voltage is not set or cannot be parsed.
   * @return The configuration voltage as a float.
   **/
  float ConfigVoltage(float defValue);

  /**
   * @brief Get additional keys
   * @return A vector of strings containing the additional keys
   **/
  std::vector<std::string> GetKeys();

  /**
   * @brief Get mask strings
   * @return A vector of strings containing the mask strings
   **/
  std::vector<std::string> GetMaskStrings();

  /**
   * @brief Get the masked pixels
   * @return A vector of pairs containing the position of the masked pixels
   **/
  std::vector<std::pair<int32_t,int32_t> > GetMasks();

  /**
   * @brief Get the masked pixels
   * @return A vector of pairs containing the position of the masked pixels
   **/
  std::map<std::string,std::string> ConfigForROOTFile();

  /**
   * @brief Get additional keys as a map that is computed on the fly
   * @return A map of strings to strings
   **/
  std::map<std::string,std::string>& GetKeyMap();

 private:

  std::string m_name;
  std::string m_address;
  std::string m_type;
  std::string m_wafer;
  std::string m_dut;
  std::string m_library;
  std::string m_classname;
  std::string m_configvoltage;
  std::vector<std::string> m_keys;
  std::vector<std::string> m_masks;
  std::vector<std::pair<int32_t,int32_t> > m_pixmasks;
  std::map<std::string,std::string> m_keymap;
  int32_t str2int(std::string str);
  float str2float(std::string str, float def);
  int32_t m_modulesize;

};

#endif
