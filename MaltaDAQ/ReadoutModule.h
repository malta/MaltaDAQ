#ifndef READOUTMODULE_H
#define READOUTMODULE_H

#include <string>
#include <thread>
#include "ReadoutConfig.h"

class TH1F;
class TH1D;
class TH2D;
class TCanvas;

#define DECLARE_READOUTMODULE(TF) \
  extern "C" ReadoutModule * create##TF (std::string name, std::string address, std::string outdir){ \
    return new TF (name,address,outdir); \
  }

/**
 * The ReadoutModule is an abstract class to interface the planes used by 
 * the test-beam data-taking application (MaltaMultiDAQ).
 * Each plane type should extend this class.
 *
 * Planes are loaded dynamically by MaltaMultiDAQ through the 
 * ModuleLoader that returns them as ReadoutModule objects.
 *
 * The methods implemented in ReadoutModule are the following:
 * * ReadoutModule::SetConfig stores the ReadoutConfig for this plane.
 * * ReadoutModule::SetVerbose enables or disables the verbosity of the plane.
 * * ReadoutModule::SetName defines a name for this plane.
 * * ReadoutModule::SetMaxEvents defines a maximum number of events.
 * * ReadoutModule::SetSampling defines the sampling ratio (1 out of N events) for the monitoring.
 * * ReadoutModule::IsRunning check if the Run thread is running.
 * * ReadoutModule::GetName return the name of the module.
 * * ReadoutModule::GetTimingHisto return the timing histogram.
 * * ReadoutModule::GetNHitHisto return the number of hits per event histogram.
 * * ReadoutModule::GetHitMapHisto return the number of hit map histogram.
 *
 * The methods that need to be implemented in the extended class are the following:
 * * ReadoutModule::SetInternalTrigger should configure the plane for internal triggering.
 * * ReadoutModule::Configure should configure the plane for data taking. 
 * * ReadoutModule::Start should start the data taking on the plane by spawning the Run method.
 * * ReadoutModule::Stop should stop the data taking on the plane by stopping the Run thread.
 * * ReadoutModule::EnableFastSignal enable the trigger signal out from the plane if any.
 * * ReadoutModule::DisableFastSignal disable the trigger signal out from the plane if any.
 *
 * @brief Abstract class to control the planes in MaltaMultiDAQ
 * @author Carlos.Solans@cern.ch
 * @author Valerio.Dao@cern.ch
 **/
class ReadoutModule{
  
 public:
  
  /**
   * @brief create a new readout module
   * @param name User friendly name for the module
   * @param address IPbus address for the module
   * @param outdir The output directory
   **/
  ReadoutModule(std::string name, std::string address, std::string outdir);

  /**
   * Must be overloaded by extended class.
   * @brief virtual destructor
   **/
  virtual ~ReadoutModule();
  
  /**
   * @brief Set the ReadoutConfig
   **/
  void SetConfig(ReadoutConfig *config);

  /**
   * @brief Get the ReadoutConfig
   **/
  ReadoutConfig * GetConfig();

  /**
   * Must be overloaded by extended class.
   * @brief Configure the module for data taking
   **/
  virtual void Configure()=0;
  
  /**
   * Must be overloaded by extended class.
   * @brief Start data acquisition thread
   * @param run The run number as a string
   * @param usePath Prepend the outputPath defined in the constructor to the file name
   **/
  virtual void Start(std::string run, bool usePath = false)=0;

  /**
   * Must be overloaded by extended class.
   * @brief implementation of the data acquisition thread
   **/
  virtual void Run()=0;

  /**
   * Must be overloaded by extended class.
   * @brief Stop the data acquisition thread
   **/
  virtual void Stop()=0;

  /**
   * @brief Enable the verbose mode
   * @param enable Enable verbose mode if true
   **/
  void SetVerbose(bool enable);
  
  /**
   * @brief Set the name of the module
   * @param name The name for the module
   **/
  void SetName(std::string name);
  
  /**
   * @brief Set the InternalTrigger flag
   * @param enable Enable ReadoutModule::m_internalTrigger if true
   **/
  void SetInternalTrigger(bool enable);

  /**
   * @brief Set the maximum number of events
   * @param maxEvents maximum number of events
   **/
  void SetMaxEvents(uint32_t maxEvents);

  /**
   * @brief Set the sampling
   * @param sampling 
   **/
  void SetSampling(uint32_t sampling);

  /**
   * @brief Check if the run thread is running
   * @return true if the run thread is running
   **/
  bool IsRunning();

  /**
   * @brief Get the name of the module
   * @return The name of the module
   **/
  std::string GetName();
  
  /**
   * @brief Get the timing histogram
   * @return The timing histogram
   **/
  TH1D* GetTimingHisto();

  /**
   * @brief Get the number of hits histogram
   * @return The number of hits histogram
   **/
  TH1D* GetNHitHisto();

  /**
   * @brief Get the hit map histogram
   * @return The hit map histogram
   **/
  TH2D* GetHitMapHisto();

  TH2D* GetSwappedHitMapHisto();
  TH2D* GetSymmetricHitMapHisto();

  /**
   * @brief Get the extended L1id
   * @return the extended L1id
   **/
  virtual uint32_t GetNTriggers()=0;

  /**
   * @brief enable fast signal generation
   **/
  virtual void EnableFastSignal()=0;

  /**
   * @brief disable fast signal generation
   **/
  virtual void DisableFastSignal()=0;

  /**
   * @brief Abstract method to update a TCanvas with realtime information
   * @param c TCanvas to update
   **/ 
  virtual void PlotCanvasRealTime(TCanvas *c);
  
  /**
   * @brief reset the L1A counter 
   **/
  virtual void ResetL1Counter();

  /**
   * @brief Retrieve the L1 reset flag
   **/
  virtual bool RetrieveL1ResetFlag();
  
  /**
   * @brief Get the projection X
   * @return The X projection from the module
   **/
  TH1F* GetProjectionX();

  /**
   * @brief Get the projection Y
   * @return The Y projection from the module
   **/
  TH1F* GetProjectionY();

 protected:
      
  bool m_cont;
  bool m_verbose;
  bool m_debug;
  bool m_internalTrigger;
  bool m_running;
  bool m_resetL1Counters;
  uint32_t m_sampling;
  uint32_t m_maxEvt;
  uint32_t m_extL1id;
  
  std::string m_name;
  std::string m_outdir;
  std::thread m_thread;

  ReadoutConfig * m_config;

  TH1D* m_Htime;
  TH1D* m_Hsize;
  TH2D* m_Hmap;
  TH1F* m_pX;
  TH1F* m_pY;

  TH2D* m_Hmap_swapped;
  TH2D* m_Hmap_symmetric;
  
};

#endif

