//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Dec  5 15:16:42 2019 by ROOT version 6.10/08
// from TTree Thres/DataSeream
// found on file: test_andrea.root
//////////////////////////////////////////////////////////

#ifndef ThScan_h
#define ThScan_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TH1F.h"
#include "TH2F.h"
#include "TLine.h"
#include "TPaveStats.h"
#include "TF1.h"
#include <cmath>


// Header file for the classes stored in the TTree if any.

class ThScan {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          pixX;
   UInt_t          pixY;
   UInt_t          DoF;
   Float_t         chi2;
   Float_t         scale;
   Float_t         thres;
   Float_t         sigma;
   Float_t         scantime;
   UInt_t          PN1st;

   // List of branches
   TBranch        *b_pixX;   //!
   TBranch        *b_pixY;   //!
   TBranch        *b_DoF;   //!
   TBranch        *b_chi2;   //!
   TBranch        *b_Scale;   //!
   TBranch        *b_Vh_Vl;   //!
   TBranch        *b_sigma;   //!
   TBranch        *b_scantime;   //!
   TBranch        *b_PN1st;   //!
   
   TH1F *m_pix_not_sel;

   TH1F *m_noise_1D;
   TH2F *m_noise_2D;
   
   TH1F *m_thres_1D;
   TH2F *m_thres_2D;

   TH1F *m_thres_1D_sector_array[8];//MALTA sectors
   TH1F *m_noise_1D_sector_array[8];//MALTA sectors

   
   TH1F *m_thres_1D_array[32];//group of 16 columns sharing the same DACs
   TH1F *m_noise_1D_array[32];//group of 16 columns sharing the same DACs

   TH1F *m_DACGroups_1D;
   TH1F *m_DACGroups_1D_perSectorArray[8];//group of 16 columns sharing the same DACs,  per MALTA sectors
   
   TH2F *m_thres_pixX_2D;
   
   TH1F *m_thres_2D_pixX;
   TH1F *m_thres_2D_pixY;

   TH1F *m_noise_2D_pixX;
   TH1F *m_noise_2D_pixY;
   TF1  *g_threshold;
   string m_filepath;
   bool m_even_pixel_only;
   ThScan(TTree *tree=0, string filepath = "", bool even_pixel_only = false);
   virtual ~ThScan();
   virtual Int_t    Cut(Long64_t entry);
   virtual Bool_t   Selected();
   virtual void     setStyleTH1(TH1F* , string , string , int);
   virtual void     setStyleTH2(TH2F* , string , string );
   virtual void     profile2Dhistogram(TH2F *twoD, TH1F *x, TH1F *y);
   virtual void     PrintHistograms(TFile *);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef ThScan_cxx
ThScan::ThScan(TTree *tree, string filepath, bool even_pixel_only ) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("test_andrea2.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("test_andrea2.root");
      }
      f->GetObject("Thres",tree);

   }
   Init(tree);
   m_even_pixel_only=even_pixel_only;
   m_filepath = filepath;
}

ThScan::~ThScan()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ThScan::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ThScan::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ThScan::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("pixX", &pixX, &b_pixX);
   fChain->SetBranchAddress("pixY", &pixY, &b_pixY);
   fChain->SetBranchAddress("DoF", &DoF, &b_DoF);
   fChain->SetBranchAddress("chi2", &chi2, &b_chi2);
   fChain->SetBranchAddress("scale", &scale, &b_Scale);
   fChain->SetBranchAddress("thres", &thres, &b_Vh_Vl);
   fChain->SetBranchAddress("sigma", &sigma, &b_sigma);
   fChain->SetBranchAddress("scantime", &scantime, &b_scantime);
   fChain->SetBranchAddress("PN1st", &PN1st, &b_PN1st);
   Notify();
}

Bool_t ThScan::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ThScan::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ThScan::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}

Bool_t ThScan::Selected()
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   bool selected = false;
   if (sigma > 3 && thres < 800 && chi2/DoF <3 ) selected= true;  //was4
   
   if (sigma < 3)    m_pix_not_sel->AddBinContent(1,1);
   if (thres > 800)  m_pix_not_sel->AddBinContent(2,1);
   if (chi2/DoF > 3) m_pix_not_sel->AddBinContent(3,1);

   return selected;
}

void ThScan::profile2Dhistogram(TH2F *twoD, TH1F *x, TH1F *y)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.

   double average =0;
   int nbins=0;
   double std=0;
   for (unsigned int i =0; i < twoD ->GetNbinsX (); ++i){
      average=0;
      nbins=0;
      std=0;
      for (unsigned int j =0; j < twoD ->GetNbinsY (); ++j){
         if (twoD->GetBinContent(i+1,j+1)==0) continue;
         average+=twoD->GetBinContent(i+1,j+1);
         nbins++;
      }
      
      for (unsigned int j =0; j < twoD ->GetNbinsY (); ++j){
         if (twoD->GetBinContent(i+1,j+1)==0) continue;
         if (nbins != 0) std+=pow((average/nbins)-twoD->GetBinContent(i+1,j+1), 2);
      }
      
      if (nbins > 1){
         x->SetBinContent(i+1, average/nbins);
         x->SetBinError(i+1, sqrt(std/(nbins-1)) );
      }
   }
   
   for (unsigned int i =0; i < twoD ->GetNbinsY (); ++i){
      average=0;
      nbins=0;
      std=0;
      for (unsigned int j =0; j < twoD ->GetNbinsX (); ++j){
         if (twoD->GetBinContent(j+1,i+1)==0) continue;
         average+=twoD->GetBinContent(j+1,i+1);
         nbins++;
      }
      for (unsigned int j =0; j < twoD ->GetNbinsX (); ++j){
         if (twoD->GetBinContent(j+1,i+1)==0) continue;
         if (nbins != 0) std+=pow((average/nbins)-twoD->GetBinContent(j+1,i+1), 2);

      }
      if (nbins > 1) {
         y->SetBinContent(i+1, average/nbins);
         y->SetBinError(i+1, sqrt(std/(nbins-1)));
      }
   }
}

void ThScan::setStyleTH1(TH1F *t, string x_axis, string y_axis = "entries", int col=1 ){
   t->SetLineColor(col);
   t->SetLineWidth(2);
   t->GetXaxis()->SetTitle(x_axis.c_str());
   t->GetYaxis()->SetTitle(y_axis.c_str());
   t->SetMinimum(0);
}


void ThScan::setStyleTH2(TH2F *t, string x_axis, string y_axis  ){
   t->GetXaxis()->SetTitle(x_axis.c_str());
   t->GetYaxis()->SetTitle(y_axis.c_str());
   t->GetZaxis()->SetNdivisions(20);
   t->SetContour(100);
}

void ThScan::PrintHistograms(TFile *f1){
   
//  TFile *f1 = TFile::Open("hsimple.root");
  
   TIter next(f1->GetListOfKeys());
   TKey *key;
   TCanvas c1("c1","c1", 700,600);
   c1.SetRightMargin(0.25);
   TLine l128(128,0,128,512); l128.SetLineWidth(2);
   TLine l192(192,0,192,512); l192.SetLineWidth(2);
   TLine l256(256,0,256,512); l256.SetLineWidth(2);

   while ((key = (TKey*)next())) {
     TClass *cl = gROOT->GetClass(key->GetClassName());
     if (cl->InheritsFrom("TH1F")){
        TH1 *h = (TH1*)key->ReadObj();
	if (h->GetEntries()==0) continue;
        h->GetXaxis()->SetRangeUser(h->GetMinimum(),h->GetMaximum());

        string title( h->GetName());
        if (title.find("profile")!=std::string::npos ) h->Draw("ep");
        else {
	  //VD: min/max protection
	  float Max=0;
	  float Min=0;
	  for ( unsigned int bin=1; bin<=h->GetNbinsX(); bin++) {
	    if (h->GetBinContent(bin)!=0) {
	      Min=h->GetXaxis()->GetBinCenter(bin)-10;
	      break;
	    }
	  }
	  for ( unsigned int bin=h->GetNbinsX(); bin>0; bin--) {
	    if (h->GetBinContent(bin)!=0) {
	      Max=h->GetXaxis()->GetBinCenter(bin)+10;
	      break;
	    }
	  }
	  h->GetXaxis()->SetRangeUser(Min,Max);
	  h->Draw();
	  h->GetXaxis()->SetRangeUser(Min,Max);
	}
        gPad->Update();
        TPaveStats *st = (TPaveStats*)h->FindObject("stats");
        st->SetX1NDC(0.85);
        c1.Print((m_filepath+"/"+title+".pdf").c_str());
        if ( title.find("profile")==std::string::npos && title.find("noise")!=std::string::npos ){
          h->SetMinimum(0.5);
  	  c1.SetLogy();
          h->Draw();
          gPad->Update();
	  c1.Print((m_filepath+"/"+title+"_log.pdf").c_str());
	}
        c1.SetLogy(0);
     }
     if (cl->InheritsFrom("TH2F")){
        TH1 *h = (TH1*)key->ReadObj();
	if (h->GetEntries()==0) continue;
        h->Draw("COLZ");
        int binmin = (int)(h->GetXaxis()->GetXmin());
        l128.Draw("same");
        l192.Draw("same");
        l256.Draw("same");
        if (h->GetNbinsX() >= 64  &&  binmin% 64 ==0 ){
           
        }

        string title( h->GetName());
        gPad->Update();
        TPaveStats *st = (TPaveStats*)h->FindObject("stats");
        st->SetX1NDC(0.85);
        c1.Print((m_filepath+"/"+title+".eps").c_str());
     }
     if (cl->InheritsFrom("TH2F")||cl->InheritsFrom("TH1F")) {

     }
  }
}


#endif // #ifdef ThScan_cxx
