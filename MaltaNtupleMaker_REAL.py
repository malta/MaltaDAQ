#!/usr/bin/env python
####################################
# Daq script for MALTA read-out
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# December 2017
# ... and then came Valerio ....
####################################

import Herakles
import datetime
import argparse
import array
import time
import sys
import ROOT
from socket import gethostname
from time import gmtime,strftime
import timeit

import MaltaSetup
import MaltaData    
import random
import os
import MaltaBase
from MaltaBase import *
import PyMaltaSlowControl

def printVal2(value):
    val=""
    val+=" "+str( format( (value) , "032b" ) )
    val+=" ::::   0x%08x"%(value)
    return val

def readInBin(word):
    tmpList=list('{0:0b}'.format(word))
    pstring=" "
    count=-1
    for obj in tmpList:
        count+=1
        if count<8:continue
        if count%8==0: pstring+="|"
        pstring+=str(obj.replace("0","_"))
    pstring+="|"
    return pstring

#################################################################################################

parser=argparse.ArgumentParser()
parser.add_argument('-c' ,'--chip'       , help='Which chip to talk to'      ,type=int,default=1)
parser.add_argument('-f' ,'--file'       , help='output file'                ,default="output.root")
parser.add_argument('-n' ,'--nevts'      , help='number of events'           ,type=int, default=-1)
parser.add_argument('-r' ,'--reps'       , help='number of repetitions'      ,type=int, default=5)
parser.add_argument('-t' ,'--time'       , help='aquisition time window (ms)',type=int,default=30)
parser.add_argument('-it','--ithr'       , help='ithreshold value'           ,type=int  , default=50)
parser.add_argument('-vl','--vlow'       , help='VLow value (from external)' ,type=int, default=600)
parser.add_argument('-co','--col'        , help='col to enable'              ,type=int, default=5)
parser.add_argument('-v' ,'--verbose'    , help='enable verbose'             ,action='store_true')
parser.add_argument('-pa','--patternAddr', help='Pattern Address'            ,type=int,default=8)
parser.add_argument('-pn','--patternNum' , help='Pattern Number'             ,type=str,default='0000')
parser.add_argument('-rf','--resetFifo'  , help='Resets the FIFO'            ,type=bool,default=False)
parser.add_argument('-p' ,'--pulse'      , help='EnablePulsing'              ,type=bool,default=False)
args=parser.parse_args()

chip=args.chip
md = MaltaData.MaltaData()
md2= MaltaData.MaltaData()

outFolder="NtupleFiles"
#outFolder="Files_plot_sigma_q"
os.system("mkdir -p "+outFolder)

chip=args.chip
print( " ")
connstr=MaltaSetup.MaltaSetup().getConnStr(chip)
connstr="udp://ep-ade-gw-02.cern.ch:50008"
print( "Trying to connect to: "+connstr)
ipName=connstr
ipb=Herakles.Uhal(ipName)

ipb.SetVerbose(args.verbose)
GetVersion(ipb,True)
writeConstDelays(ipb,4,1)


patternAddress = args.patternAddr
patternNumber  = args.patternNum

fw = ROOT.TFile(outFolder+"/"+args.file.replace(".root","_"+str(args.chip)+".root"),"RECREATE")                    

ntuple = ROOT.TTree("MALTA","DataStream")
#####bits    = array.array('d',(0,))
#####refbit  = array.array('I',(0,))
pixel   = array.array('I',(0,))
group   = array.array('I',(0,))
parity  = array.array('I',(0,))
delay   = array.array('I',(0,))
dcolumn = array.array('I',(0,))
chipbcid= array.array('I',(0,))
chipid  = array.array('I',(0,))
phase   = array.array('I',(0,))
winid   = array.array('I',(0,))
bcid    = array.array('I',(0,))
l1id    = array.array('I',(0,))
word1   = array.array('I',(0,))
word2   = array.array('I',(0,))
coordX  = array.array('I',(0,))
coordY  = array.array('I',(0,))
ithres  = array.array('I',(0,))
vlow    = array.array('I',(0,))
isDuplicate   = array.array('I',(0,))
####ntuple.Branch("bits",    bits,    "bits/L")
####ntuple.Branch("refbit",  refbit,  "refbit/i")
ntuple.Branch("pixel",   pixel,   "pixel/i")
ntuple.Branch("group",   group,   "group/i")
ntuple.Branch("parity",  parity,  "parity/i")
ntuple.Branch("delay",   delay,   "delay/i")
ntuple.Branch("dcolumn", dcolumn, "dcolumn/i")
ntuple.Branch("chipbcid",chipbcid,"chipbcid/i")
ntuple.Branch("chipid",  chipid,  "chipid/i")
ntuple.Branch("phase",   phase,   "phase/i")
ntuple.Branch("winid",   winid,   "winid/i")
ntuple.Branch("bcid",    bcid,    "bcid/i")
ntuple.Branch("l1id",    l1id,    "l1id/i")
ntuple.Branch("coordX",  coordX,  "coordX/i")
ntuple.Branch("coordY",  coordY,  "coordY/i")
ntuple.Branch("word1",   word1,   "word1/L")
ntuple.Branch("word2",   word2,   "word2/L")
ntuple.Branch("isDuplicate",   isDuplicate,   "isDuplicate/i")
ntuple.Branch("ithres",  ithres,  "ithres/i")
ntuple.Branch("vlow"  ,  vlow  ,  "vlow/i")

tmpEvent= 0.
event   = 0.
#t0 = time.clock()
#t1 = t0
fifo2Empty    = False
dupCheck      = False
markDuplicate     = False
markDuplicateNext = False
isFirst       = True

ithres[0]=args.ithr
vlow[0]=args.vlow

MaltaBase.ResetFifo(ipb)

ResetL1Counter(ipb)
##ReadoutHalfColumnsOn(ipb)
##ReadoutHalfRowsOn(ipb)
ReadoutHalfColumnsOff(ipb)
ReadoutHalfRowsOff(ipb)
ReadoutOn(ipb)
ReadoutEnableInternalL1A(ipb)
cacheWord=ipb.Read(8)
mask    = 0xFFFFFFFF ^ ( 1<<20 )
maskTRIG= 0xFFFFFFFF ^ ( 1<<19 )



commands =[]
commands1=[]
if args.pulse:
    #commands1.append( cacheWord & maskTRIG )
    #for i in range(0,250):
    commands.append( cacheWord | (1<<19) )
    #commands.append( cacheWord & maskTRIG )
    commands.append( ((cacheWord | (1<<20))) | (1<<19) )
    commands.append( ( cacheWord | (1<<20)) & maskTRIG )
    commands.append( ( cacheWord | (1<<20)) )
    commands.append( ( cacheWord | (1<<20)) )
    commands.append( ( cacheWord | (1<<20)) )
    commands.append( ( cacheWord | (1<<20)) )
    commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord | (1<<20)) )
    #commands.append( ( cacheWord & mask)    )
    #commands.append( (cacheWord & mask)    )
    #commands.append( ((cacheWord | (1<<20))) | (1<<19) )
    #commands.append( (cacheWord | (1<<20)) & maskTRIG )
    #commands.append( (cacheWord | (1<<20)) )
    #commands.append( (cacheWord | (1<<20)) )
    #commands.append( (cacheWord | (1<<20)) )
    #commands.append( (cacheWord | (1<<20)) )
    #commands.append( (cacheWord | (1<<20)) )
    #commands.append( (cacheWord | (1<<20)) )
    #commands.append( (cacheWord & mask)    )
    print( commands)
    #commands1.append(  cacheWord | (1<<19)  )
    #commands1.append(  cacheWord & maskTRIG )
    #commands1.append( ((cacheWord | (1<<20))) | (1<<19) )
    #commands1.append( (cacheWord | (1<<20)) & maskTRIG )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord & mask)    )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord | (1<<20)) )
    #commands1.append( (cacheWord & mask)    )
else:
    commands.append( cacheWord | (1<<19)  )
    for j in range(0,20): commands.append( cacheWord & maskTRIG )
    commands.append( cacheWord | (1<<19)  )
    for j in range(0,20): commands.append( cacheWord & maskTRIG )
    commands.append( cacheWord | (1<<19)  )
    for j in range(0,20): commands.append( cacheWord & maskTRIG )
    commands.append( cacheWord | (1<<19)  )
    for j in range(0,20): commands.append( cacheWord & maskTRIG )
    commands.append( cacheWord | (1<<19)  )
    for j in range(0,20): commands.append( cacheWord & maskTRIG )
    commands.append( cacheWord | (1<<19)  )
    for j in range(0,20): commands.append( cacheWord & maskTRIG )
    commands.append( cacheWord | (1<<19)  )
    for j in range(0,20): commands.append( cacheWord & maskTRIG )
    commands.append( cacheWord | (1<<19)  )
    for j in range(0,20): commands.append( cacheWord & maskTRIG )
    commands.append( cacheWord | (1<<19)  )
    for j in range(0,20): commands.append( cacheWord & maskTRIG )
    commands.append( cacheWord | (1<<19)  )
    for j in range(0,20): commands.append( cacheWord & maskTRIG )

repetition=args.reps
pulsesSeen=0
emptyWords=0

#start = time.clock()
print( "hello")

for rep in range(0,repetition):
    time.sleep(5)
    tmpEvent=event

    ReadoutOff(ipb)
    MaltaBase.ResetFifo(ipb)
    ReadoutOn(ipb)
    if args.pulse: print( "start pulsing ..."+str(args.time)+" TIMES")
    else         : print( "start listening to noise ..."+str(args.time)+" TIMES")

    for i in range(0,args.time):
	#print "PULSING "+str(i)
        #time.sleep(0.001)
        ipb.Write(8,commands,True)
        #ipb.Write(8,commands,True)
        #ipb.Write(8,commands,True)
        #ipb.Write(8,commands,True)
        #ipb.Write(8,commands1,True)
	#if i%2==0: 	        ipb.Write(8,commands,True)
	#else      : 	        ipb.Write(8,commands1,True)
        #for i in range(0,args.time):
        #    ipb.Write(8,cacheWord | (1<<20)) 
        #    ipb.Write(8,cacheWord & mask ) 
    ReadoutOff(ipb)
    

#    else:
#        ##########ReadoutEnableExtenralL1A(ipb)
#        ReadoutOff(ipb)
#        MaltaBase.ResetFifo(ipb)
#        ReadoutOn(ipb)
#        time.sleep( float(args.time)/1000) #### CHANGE ME IF YOU SATURATE!!!!
#        ReadoutOff(ipb)
    #####continue
    prevL1=-1000
    while (event<65000 or rep!=0): ##t1-t0<args.time):

        isRealDuplicate = markDuplicateNext
        #t1 = time.clock()
        fifo2Empty = MaltaBase.IsFIFOIPEmpty(ipb)
    
        if fifo2Empty:
            time.sleep(0.01)
            break

        fifo2Empty = MaltaBase.IsFIFOIPEmpty(ipb)
        word=MaltaBase.ReadMaltaWord(ipb,False)#ipb.Read(6,2,True)
        
        #if word[0]+word[1]!=0:
        #    print( str( word[0])+" "+str( word[1]))

        if word[0] == 0: continue
        if word[1] == 0: continue
        if isFirst:
            md.setWord1(word[0])
            md.setWord2(word[1])
            isFirst = False
            continue
    
        md2.setWord1(md.getWord1new())
        md2.setWord2(md.getWord2new())
        md.setWord1(word[0])
        md.setWord2(word[1])
        
        md.unPack()    
        md2.unPack()
        
        #Decode Output & Fill Ntuple
        ###refbit[0]  = md2.getRefbit()
        pixel[0]    = md2.getPixel()
        group[0]    = md2.getGroup()
        parity[0]   = md2.getParity()
        delay[0]    = md2.getDelay()
        dcolumn[0]  = md2.getDcolumn()
        chipbcid[0] = md2.getChipbcid()
        chipid[0]   = md2.getChipid()
        phase[0]    = md2.getPhase()
        winid[0]    = md2.getWinid()
        bcid[0]     = md2.getBcid()
        l1id[0]     = md2.getL1id()
        word1[0]    = md2.getWord1()
        word2[0]    = md2.getWord2()

        phase2     = md.getPhase()
        winid2     = md.getWinid()
        winid1     = winid[0]
        bcid2      = md.getBcid()
        bcid1      = bcid[0]
        l1id2      = md.getL1id()
        l1id1      = l1id[0]

        ###print( " "+str(l1id1))
        ###if l1id1<prevL1: print( "JUMMMMMMMMMMMMMMPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP "+str(l1id1)+" < "+str(prevL1))
        prevL1=l1id1


        if dcolumn[0]==2 and (group[0]==31 or group[0]==15): pulsesSeen+=1
        
        ####print( strftime("%Y-%m-%d %H:%M:%S", gmtime()))
        if pixel[0]!=0: pass #####md2.getInfo()
        else          : emptyWords+=1
        
        
        ##same or consecutive BCID:
        if (bcid2-bcid1)==0 or (bcid2-bcid1)==1 or (bcid2==0 and bcid1==63):
            if (winid2-winid1)==1 or (winid2==0 and winid1==7 ):
                if phase2==0 and (l1id1==l1id2):
                    if phase[0]>5: 
                        markDuplicate = True
                    elif phase[0]>3:
                        markDuplicateNext = True
                        markDuplicate = False
                    else:
                        markDuplicate = False
                else:
                    markDuplicate = False  
            else: 
                markDuplicate = False
        else:
            markDuplicate = False

        #if phase[0] > 5 and phase2 == 0: 
        #    if abs(winid2-winid1 == 1) or (winid2 == 0 and (winid1 == 7)) : markDuplicate = True
        #    else: markDuplicate = False
        #    pass
        #else: markDuplicate = False


        if isRealDuplicate: 
            isDuplicate[0] = 1
            markDuplicateNext=False
        else:
            if markDuplicate:
                isDuplicate[0] = 1
            else:
                isDuplicate[0] = 0
            
        ntuple.Fill()
        event+=1
    
    ## VD:printing the last word
    if md.getWord1()!=0:
        ###md.getInfo()

        ###refbit[0]  = md.getRefbit()
        pixel[0]    = md.getPixel()
        group[0]    = md.getGroup()
        parity[0]   = md.getParity()
        delay[0]    = md.getDelay()
        dcolumn[0]  = md.getDcolumn()
        chipbcid[0] = md.getChipbcid()
        chipid[0]   = md.getChipid()
        phase[0]    = md.getPhase()
        winid[0]    = md.getWinid()
        bcid[0]     = md.getBcid()
        l1id[0]     = md.getL1id()
        word1[0]    = md.getWord1()
        word2[0]    = md.getWord2()
        isDuplicate[0]=markDuplicateNext    
        ntuple.Fill()
        
        event+=1
    print( " ")
    print( " ..number of events after cycle "+str(rep)+" --> "+str(int(event))+" ... in this batch: "+str(int(event-tmpEvent)))
    pass

#f=file("DEBUGmonitorFifo_"+args.file.replace(".root","_"+str(args.ithr)+".txt"),"w")
content="Greetings from Valerio ... be prepared to see a shit load of text\n"
content+="\n"
content+="... start emptying the monitoring fifo \n"
'''
empty=IsFIFOMonEmpty(ipb)
readings=0
empty=True
while not empty:
    time.sleep(0.01)
    values=ReadMonitorWord(ipb,False)
    if values[0]==0: 
        content+="THIS SHOULD BE BASICALLY IMPOSSIBLE\n"
        break
    readings+=1
    content+="\n\n .... reading entry "+str(readings)+"\n"
    sep="-----------------------------------------------------------"
    count=-1
    for v in values:
        count+=1
        content+=readInBin(v)+"\n"
        #print( readInBin(v))
        if count==0 or count==16 or count==21 or count==22 or count==25 or count==33:
            content+=sep+"\n"
    empty=IsFIFOMonEmpty(ipb)
'''
content+="\n"
content+="\n"
#f.write(content)
#f.close()
fw.Write()
fw.Close()

print( " ")
print( "Number of collected events at thres: "+str(args.ithr)+" is   "+str( int(event) )+"   ... written on file: "+outFolder+"/"+args.file.replace(".root","_"+str(args.chip)+".root"))
if args.pulse: print( "Seen pulses: "+str(pulsesSeen))
##print( "Number of words with empty pixels: "+str(emptyWords))
ReadoutEnableInternalL1A(ipb) 
print( " ")
#end = time.clock()
#print( "ELAPSED TIME: "+str(end - start))

