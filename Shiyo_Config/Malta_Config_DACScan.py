#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time
import threading
import argparse

import MaltaSetup
import MaltaBase
import Herakles
import PyMaltaSlowControl
from MaltaBase import *

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################
parser=argparse.ArgumentParser()
parser.add_argument('-a'         ,'--address'   ,help='FPGA address'         ,type=str  ,required= True)
parser.add_argument('-DVDD'      ,'--DVDD'      ,help='config DVDD voltage'  ,type=float,default ="0.8")
parser.add_argument('-IDB'       ,'--IDB'       ,help='configure IDB DAC'    ,type=int  ,default =  127)
parser.add_argument('-ITHR'      ,'--ITHR'      ,help='configure ITHR DAC'   ,type=int  ,default =  127)
parser.add_argument('-IBIAS'     ,'--IBIAS'     ,help='configure IBIAS DAC'  ,type=int  ,default =   80)
parser.add_argument('-IRESET'    ,'--IRESET'    ,help='configure IRESET DAC' ,type=int  ,default =   30)
parser.add_argument('-ICASN'     ,'--ICASN'     ,help='configure ICASN DAC'  ,type=int  ,default =   10)
parser.add_argument('-VCASN'     ,'--VCASN'     ,help='configure VCASN DAC'  ,type=int  ,default =   64)
parser.add_argument('-VCLIP'     ,'--VCLIP'     ,help='configure VCLIP DAC'  ,type=int  ,default =   20)
parser.add_argument('-VPLSEHI'   ,'--VPLSEHI'   ,help='configure VPLSEHI DAC',type=int  ,default =   90)
parser.add_argument('-VPLSELO'   ,'--VPLSELO'   ,help='configure VPLSELO DAC',type=int  ,default =    5)
parser.add_argument('-VRSTD'     ,'--VRSTD'     ,help='configure VRSTD DAC'  ,type=int  ,default =   65)
parser.add_argument('-VRSTP'     ,'--VRSTP'     ,help='configure VRSTP DAC'  ,type=int  ,default =  127)
parser.add_argument('-monIDB'    ,'--monIDB'    ,help='monitor IDB DAC'      ,type=bool ,default =False)
parser.add_argument('-monITHR'   ,'--monITHR'   ,help='monitor ITHR DAC'     ,type=bool ,default =False)
parser.add_argument('-monIBIAS'  ,'--monIBIAS'  ,help='monitor IBIAS DAC'    ,type=bool ,default =False)
parser.add_argument('-monIRESET' ,'--monIRESET' ,help='monitor IRESET DAC'   ,type=bool ,default =False)
parser.add_argument('-monICASN'  ,'--monICASN'  ,help='monitor ICASN DAC'    ,type=bool ,default =False)
parser.add_argument('-monVCASN'  ,'--monVCASN'  ,help='monitor VCASN DAC'    ,type=bool ,default =False)
parser.add_argument('-monVCLIP'  ,'--monVCLIP'  ,help='monitor VCLIP DAC'    ,type=bool ,default =False)
parser.add_argument('-monVPLSEHI','--monVPLSEHI',help='monitor VPLSEHI DAC'  ,type=bool ,default =False)
parser.add_argument('-monVPLSELO','--monVPLSELO',help='monitor VPLSELO DAC'  ,type=bool ,default =False)
parser.add_argument('-monVRSTD'  ,'--monVRSTD'  ,help='monitor VRSTD DAC'    ,type=bool ,default =False)
parser.add_argument('-monVRSTP'  ,'--monVRSTP'  ,help='monitor VRSTP DAC'    ,type=bool ,default =False)

args=parser.parse_args()

os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")
os.system("MALTA_PSU.py -c setVoltage DVDD "+ str(args.DVDD)+ " ")


readA =4
writeA=5 

#connstr="192.168.200.1" #
connstr=args.address
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)
GetVersion(ipb,True)
getFifoState(ipb)
ResetMalta(ipb)
ResetFifo(ipb)

monitorFlags = {args.monIDB,args.monITHR,args.monIBIAS,args.monIRESET,args.monICASN,args.monVCASN,args.monVCLIP,args.monVPLSEHI,args.monVPLSELO,args.monVRSTD,args.monVRSTP,}

flagCount = 0
for flag in monitorFlags:
    if flag is True:
        flagCount = flagCount + 1
if flagCount > 1:
    print("Only one DAC can be monitored at once!")
    exit()

monitorIDAC = args.monIDB   or args.monITHR  or args.monIBIAS   or args.monIRESET  or args.monICASN
monitorVDAC = args.monVCASN or args.monVCLIP or args.monVPLSEHI or args.monVPLSELO or args.monVRSTD or args.monVRSTP

print("##########################################")
print("Monitor flags: IDAC->"+str(monitorIDAC)+" VDAC->"+str(monitorVDAC))
print("##########################################")
##################################################################################################
##################################################################################################
## END INIT ######################################################################################
##################################################################################################
##################################################################################################

switchClockOn(ipb)

sc=PyMaltaSlowControl.MaltaSlowControl()

############# disabling the merger
print "\n Disabling the merger structure"
ipb.Write(writeA, sc.enableMerger(False))
#ipb.Write(writeA, sc.enableMerger(True))
ipb.Write(writeA, sc.delay(750)) #was 750
DebugWord(ipb.Read(readA), 0)

############# DAC monitoring
print "\n Disabling DAC monitoring"
ipb.Write(writeA, sc.switchDACMONV(monitorVDAC))
ipb.Write(writeA, sc.switchDACMONI(monitorIDAC))
ipb.Write(writeA, sc.readRegister(21)    )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
############# DAC override
print "\n Switching on IDB"
ipb.Write(writeA, sc.switchIDB(args.monIDB) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on ITHR"
ipb.Write(writeA, sc.switchITHR(args.monITHR) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on IBIAS"
ipb.Write(writeA, sc.switchIBIAS(args.monIBIAS) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on IRESET"
ipb.Write(writeA, sc.switchIRESET(args.monIRESET) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on ICASN"
ipb.Write(writeA, sc.switchICASN(args.monICASN) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VCASN"
ipb.Write(writeA, sc.switchVCASN(args.monVCASN) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VCLIP"
ipb.Write(writeA, sc.switchVCLIP(args.monVCLIP) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VPLSE_HIGH"
ipb.Write(writeA, sc.switchVPLSE_HIGH(args.monVPLSEHI) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VPLSE_LOW"
ipb.Write(writeA, sc.switchVPLSE_LOW(args.monVPLSELO) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VRESET_D"
ipb.Write(writeA, sc.switchVRESET_D(args.monVRSTD) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VRESET_P"
ipb.Write(writeA, sc.switchVRESET_P(args.monVRSTP) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)


##### SETTING miniMALTA values in MALTA ##########
print ("\n Setting ICASN ")
ipb.Write(5, sc.setICASN(args.ICASN)) 
ipb.Write(5, sc.setICASN(args.ICASN)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting IBIAS ")
ipb.Write(5, sc.setIBIAS(args.IBIAS))
ipb.Write(5, sc.setIBIAS(args.IBIAS))
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting ITHR")
ipb.Write(5, sc.setITHR(args.ITHR)) #WAS 1 
ipb.Write(5, sc.setITHR(args.ITHR)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting IDB")
ipb.Write(5, sc.setIDB(args.IDB)) #WAS60
ipb.Write(5, sc.setIDB(args.IDB)) #WAS60
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting IRESET")
ipb.Write(5, sc.setIRESET(args.IRESET)) 
ipb.Write(5, sc.setIRESET(args.IRESET)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VCASN")
ipb.Write(5, sc.setVCASN(args.VCASN)) 
ipb.Write(5, sc.setVCASN(args.VCASN)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VRESET_P")
ipb.Write(5, sc.setVRESET_P(args.VRSTP)) 
ipb.Write(5, sc.setVRESET_P(args.VRSTP)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VRESET_D")
ipb.Write(5, sc.setVRESET_D(args.VRSTD)) 
ipb.Write(5, sc.setVRESET_D(args.VRSTD)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VPLSE_HIGH")
ipb.Write(5, sc.setVPLSE_HIGH(args.VPLSEHI)) 
ipb.Write(5, sc.setVPLSE_HIGH(args.VPLSEHI)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VPLSE_LOW")
ipb.Write(5, sc.setVPLSE_LOW(args.VPLSELO)) 
ipb.Write(5, sc.setVPLSE_LOW(args.VPLSELO)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VCLIP")
ipb.Write(5, sc.setVCLIP(args.VCLIP)) 
ipb.Write(5, sc.setVCLIP(args.VCLIP)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

ipb.Write(writeA, sc.readRegister(21)    )
print "This is the readout register"
DebugWord(ipb.Read(readA), 0)


for i in range(0,256):
    ipb.Write( writeA,sc.maskDoubleColumn(i) )
    ipb.Write( writeA,sc.maskDoubleColumn(i) )
    time.sleep(0.010)

DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)

switchClockOff(ipb)

print "\n DONE "


os.system("MALTA_PSU.py -c rampVoltage DVDD 1.8 0.4")
os.system("MALTA_PSU.py -t T_ANAG_ON.txt ")
