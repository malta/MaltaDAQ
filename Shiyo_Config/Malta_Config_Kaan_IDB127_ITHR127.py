#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time
import threading
import argparse

import MaltaSetup
import MaltaBase
import Herakles
import PyMaltaSlowControl
from MaltaBase import *

##################################################################################################
##################################################################################################
## START INIT ####################################################################################
##################################################################################################
##################################################################################################
parser=argparse.ArgumentParser()
parser.add_argument('-c' ,'--chip'    , help='Which chip to talk to',type=int,default=1)
parser.add_argument('-d' ,'--dualchip', help='second chip in dual'  ,action='store_true')
parser.add_argument('-DVDD' ,'--DVDD'       , help='DVDD when setting DAC, it will go to 1.9 when reading the DAC',type=float,default="0.8")
args=parser.parse_args()
chip=args.chip

os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")
os.system("MALTA_PSU.py -c setVoltage DVDD "+ str(args.DVDD)+ " ")


readA =4
writeA=5
if args.dualchip:
    readA =90
    writeA=91
 

#connstr="192.168.200.1" #
connstr=MaltaSetup.MaltaSetup().getConnStr(chip)
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)
GetVersion(ipb,True)
getFifoState(ipb)
ResetMalta(ipb)
ResetFifo(ipb)

##################################################################################################
##################################################################################################
## END INIT ########maltaba##############################################################################
##################################################################################################
##################################################################################################

switchClockOn(ipb)

sc=PyMaltaSlowControl.MaltaSlowControl()

############# disabling the merger
print "\n Disabling the merger structure"
ipb.Write(writeA, sc.enableMerger(False))
#ipb.Write(writeA, sc.enableMerger(True))
ipb.Write(writeA, sc.delay(750)) #was 750
DebugWord(ipb.Read(readA), 0)

############# DAC monitoring
print "\n Disabling DAC monitoring"
ipb.Write(writeA, sc.switchDACMONV(False))
ipb.Write(writeA, sc.switchDACMONI(False))
ipb.Write(writeA, sc.readRegister(21)    )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
############# DAC override
print "\n Switching on IDB"
ipb.Write(writeA, sc.switchIDB(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on ITHR"
ipb.Write(writeA, sc.switchITHR(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on IBIAS"
ipb.Write(writeA, sc.switchIBIAS(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on IRESET"
ipb.Write(writeA, sc.switchIRESET(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on ICASN"
ipb.Write(writeA, sc.switchICASN(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VCASN"
ipb.Write(writeA, sc.switchVCASN(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VCLIP"
ipb.Write(writeA, sc.switchVCLIP(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VPLSE_HIGH"
ipb.Write(writeA, sc.switchVPLSE_HIGH(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VPLSE_LOW"
ipb.Write(writeA, sc.switchVPLSE_LOW(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VRESET_D"
ipb.Write(writeA, sc.switchVRESET_D(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)
print "\n Switching on VRESET_P"
ipb.Write(writeA, sc.switchVRESET_P(False) )
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)


##### SETTING miniMALTA values in MALTA ##########
print ("\n Setting ICASN ")
ipb.Write(5, sc.setICASN(10)) 
ipb.Write(5, sc.setICASN(10)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting IBIAS ")
ipb.Write(5, sc.setIBIAS(80)) #50 miniMALTA3
ipb.Write(5, sc.setIBIAS(80)) #50 miniMALTA
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting ITHR")
ipb.Write(5, sc.setITHR(127)) #WAS 1 
ipb.Write(5, sc.setITHR(127)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting IDB")
ipb.Write(5, sc.setIDB(127)) #WAS60
ipb.Write(5, sc.setIDB(127)) #WAS60
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting IRESET")
ipb.Write(5, sc.setIRESET(30)) 
ipb.Write(5, sc.setIRESET(30)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VCASN")
ipb.Write(5, sc.setVCASN(64)) 
ipb.Write(5, sc.setVCASN(64)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VRESET_P")
ipb.Write(5, sc.setVRESET_P(127)) 
ipb.Write(5, sc.setVRESET_P(127)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VRESET_D")
ipb.Write(5, sc.setVRESET_D(65)) 
ipb.Write(5, sc.setVRESET_D(65)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VPLSE_HIGH")
ipb.Write(5, sc.setVPLSE_HIGH(90)) 
ipb.Write(5, sc.setVPLSE_HIGH(90)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VPLSE_LOW")
ipb.Write(5, sc.setVPLSE_LOW(5)) 
ipb.Write(5, sc.setVPLSE_LOW(5)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

print ("\n Setting VCLIP")
ipb.Write(5, sc.setVCLIP(127)) 
ipb.Write(5, sc.setVCLIP(127)) 
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

ipb.Write(writeA, sc.readRegister(21)    )
print "This is the readout register"
DebugWord(ipb.Read(readA), 0)


ipb.Write(writeA, sc.disablePulsePixelCol(5))
ipb.Write(writeA, sc.disablePulsePixelHor(0))
ipb.Write(writeA, sc.disablePulsePixelHor(251))
ipb.Write(writeA, sc.disablePulsePixelHor(511))
DebugWord(ipb.Read(readA), 0)
time.sleep(0.05)

#ipb.Write(writeA, sc.enablePulsePixelCol(0))
#ipb.Write(writeA, sc.enablePulsePixelHor(0))


for i in range(0,256):
    #continue
    #print i
    ipb.Write( writeA,sc.maskDoubleColumn(i) )
    ipb.Write( writeA,sc.maskDoubleColumn(i) )
    time.sleep(0.010)



DebugWord(ipb.Read(readA), 0)
DebugWord(ipb.Read(readA), 0)

switchClockOff(ipb)

print "\n DONE "


os.system("MALTA_PSU.py -c rampVoltage DVDD 1.8 0.4")
os.system("MALTA_PSU.py -t T_ANAG_ON.txt ")
