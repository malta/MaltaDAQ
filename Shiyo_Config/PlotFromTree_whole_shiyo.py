### shiyo modified from 07.31####
## this code is for whole region 
## whole region 2DMap and fitting result and....

import ROOT
import argparse
import os

ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetEndErrorSize(10)
ROOT.gStyle.SetOptStat(0)

parser=argparse.ArgumentParser()
parser.add_argument('-f' ,'--folder',help='output folder',required=True)
parser.add_argument('-i' ,'--input' ,help='prefix for the file',required=True)
args=parser.parse_args()

folder=args.folder+"/"
os.system("mkdir -p "+folder)

iT=ROOT.TChain("Thres")
iT.Add(args.input)

minTh=100
maxTh=550
nBins=int((maxTh-minTh)/10) ##/5
nBinsSig=50
minSig=0
maxSig=25

minPix1=-1.5
maxPix1=256.5 ##sector*64+0.5
nBinsPix=int(minPix1+maxPix1)
minPix2=254.5
maxPix2=512.5 ##sector*64+0.5

cutSigmin="3"
cutChi2max="10"
cutCdtAll="sigma>"+cutSigmin+" && chi2<"+cutChi2max
cutCdt1  ="sigma>"+cutSigmin+" && chi2<"+cutChi2max+"&& pixX<64"
cutCdt2  ="sigma>"+cutSigmin+" && chi2<"+cutChi2max+"&& pixX>=64 && pixX<128"
cutCdt3  ="sigma>"+cutSigmin+" && chi2<"+cutChi2max+"&& pixX>=128 && pixX<192"
cutCdt4  ="sigma>"+cutSigmin+" && chi2<"+cutChi2max+"&& pixX>=192 && pixX<256"

cut2Cdt1  ="sigma>"+cutSigmin+" && chi2<"+cutChi2max+"&& pixX>=256 && pixX<320"
cut2Cdt2  ="sigma>"+cutSigmin+" && chi2<"+cutChi2max+"&& pixX>=320 && pixX<384"
cut2Cdt3  ="sigma>"+cutSigmin+" && chi2<"+cutChi2max+"&& pixX>=384 && pixX<448"
cut2Cdt4  ="sigma>"+cutSigmin+" && chi2<"+cutChi2max+"&& pixX>=448 && pixX<512"

box1x1 = 0.10
box1x2 = box1x1 + 0.15
box2x1 = box1x2 + 0.01
box2x2 = box2x1 + 0.15
box3x1 = box2x2 + 0.01
box3x2 = box3x1 + 0.15
box4x1 = box3x2 + 0.01
box4x2 = box4x1 + 0.15
boxy1  = 0.835
boxy2  = 0.995

##########################################
ROOT.gStyle.SetOptStat(0)
canthre11=ROOT.TCanvas("canthre11","canthre11",800,600)
Hthres11=ROOT.TH1F("Hthres11",";threshold [el];# pixel SECTOR 1",nBins,minTh,maxTh)
Hthres12=ROOT.TH1F("Hthres12","Sec2;threshold [el];# pixel SECTOR 2",nBins,minTh,maxTh)
Hthres13=ROOT.TH1F("Hthres13","Sec3;threshold [el];# pixel SECTOR 3",nBins,minTh,maxTh)
Hthres14=ROOT.TH1F("Hthres14","Sec4;threshold [el];# pixel SECTOR 4",nBins,minTh,maxTh)
Hthres11.Sumw2()
Hthres12.Sumw2()
Hthres13.Sumw2()
Hthres14.Sumw2()

iT.Draw("thres>>Hthres11",cutCdt1,"goff")
iT.Draw("thres>>Hthres12",cutCdt2,"goff")
iT.Draw("thres>>Hthres13",cutCdt3,"goff")
iT.Draw("thres>>Hthres14",cutCdt4,"goff")

Hthres11.SetMaximum(Hthres11.GetMaximum()*1.50)
Hthres11.SetLineColor(2)
Hthres11.Draw("HIST")
Hthres12.SetLineColor(4)
Hthres12.Draw("HISTSAME")
Hthres13.SetLineColor(8)
Hthres13.Draw("HISTSAME")
Hthres14.SetLineColor(6)
Hthres14.Draw("HISTSAME")

ptstats = ROOT.TPaveStats(box1x1,boxy1,box1x2,boxy2,"brNDC");
ptstats.SetName("stats");
ptstats.SetBorderSize(2);
ptstats.SetFillColor(0);
ptstats.SetTextAlign(12);
ptstats.SetTextColor(2);
text = ptstats.AddText("Sec 1");
text.SetTextSize(0.0368);
text=ptstats.AddText("Entries = "+str(int(Hthres11.GetEntries())));
text=ptstats.AddText("Mean  =  "+str(round(Hthres11.GetMean(),1)));
text=ptstats.AddText("RMS   =  "+str(round(Hthres11.GetRMS(),2)));
ptstats.Draw("");
    
ptstats2 = ROOT.TPaveStats(box2x1,boxy1,box2x2,boxy2,"brNDC");
ptstats2.SetName("stats");
ptstats2.SetBorderSize(2);
ptstats2.SetFillColor(0);
ptstats2.SetTextAlign(12);
ptstats2.SetTextColor(4);
text2 = ptstats2.AddText("Sec 2");
text2.SetTextSize(0.0368);
text2=ptstats2.AddText("Entries = "+str(int(Hthres12.GetEntries())));
text2=ptstats2.AddText("Mean  =  "+str(round(Hthres12.GetMean(),1)));
text2=ptstats2.AddText("RMS   =  "+str(round(Hthres12.GetRMS(),1)));
ptstats2.Draw("");
    
ptstats3 = ROOT.TPaveStats(box3x1,boxy1,box3x2,boxy2,"brNDC");
ptstats3.SetName("stats");
ptstats3.SetBorderSize(2);
ptstats3.SetFillColor(0);
ptstats3.SetTextAlign(12);
ptstats3.SetTextColor(8);
text3 = ptstats3.AddText("Sec 3");
text3.SetTextSize(0.0368);
text3=ptstats3.AddText("Entries = "+str(int(Hthres13.GetEntries())));
text3=ptstats3.AddText("Mean  =  "+str(round(Hthres13.GetMean(),1)));
text3=ptstats3.AddText("RMS   =  "+str(round(Hthres13.GetRMS(),1)));
ptstats3.Draw("");
    
ptstats4 = ROOT.TPaveStats(box4x1,boxy1,box4x2,boxy2,"brNDC");
ptstats4.SetName("stats");
ptstats4.SetBorderSize(2);
ptstats4.SetFillColor(0);
ptstats4.SetTextAlign(12);
ptstats4.SetTextColor(6);
text4 = ptstats4.AddText("Sec 4");
text4.SetTextSize(0.0368);
text4=ptstats4.AddText("Entries = "+str(int(Hthres14.GetEntries())));
text4=ptstats4.AddText("Mean  =  "+str(round(Hthres14.GetMean(),1)));
text4=ptstats4.AddText("RMS   =  "+str(round(Hthres14.GetRMS(),1)));
ptstats4.Draw("");
    
canthre11.Print(folder+"Threshold1_sig"+cutSigmin+"_chi"+cutChi2max+".pdf")
canthre11.Print(folder+"Threshold1_sig"+cutSigmin+"_chi"+cutChi2max+".C")

canthre21=ROOT.TCanvas("canthre21","canthre21",800,600)
Hthres21=ROOT.TH1F("Hthres21",";threshold [el];# pixel SECTOR 5",nBins,minTh,maxTh)
Hthres22=ROOT.TH1F("Hthres22","Sec6;threshold [el];# pixel SECTOR 6",nBins,minTh,maxTh)
Hthres23=ROOT.TH1F("Hthres23","Sec7;threshold [el];# pixel SECTOR 7",nBins,minTh,maxTh)
Hthres24=ROOT.TH1F("Hthres24","Sec8;threshold [el];# pixel SECTOR 8",nBins,minTh,maxTh)
Hthres21.Sumw2()
Hthres22.Sumw2()
Hthres23.Sumw2()
Hthres24.Sumw2()

iT.Draw("thres>>Hthres21",cut2Cdt1,"goff")
iT.Draw("thres>>Hthres22",cut2Cdt2,"goff")
iT.Draw("thres>>Hthres23",cut2Cdt3,"goff")
iT.Draw("thres>>Hthres24",cut2Cdt4,"goff")

Hthres21.SetMaximum(Hthres21.GetMaximum()*1.50)
Hthres21.SetLineColor(2)
Hthres21.Draw("HIST")
Hthres22.SetLineColor(4)
Hthres22.Draw("HISTSAME")
Hthres23.SetLineColor(8)
Hthres23.Draw("HISTSAME")
Hthres24.SetLineColor(6)
Hthres24.Draw("HISTSAME")

ptstats21 = ROOT.TPaveStats(box1x1,boxy1,box1x2,boxy2,"brNDC");
ptstats21.SetName("stats");
ptstats21.SetBorderSize(2);
ptstats21.SetFillColor(0);
ptstats21.SetTextAlign(12);
ptstats21.SetTextColor(2);
text21 = ptstats21.AddText("Sec 5");
text21.SetTextSize(0.0368);
text21=ptstats21.AddText("Entries = "+str(int(Hthres21.GetEntries())));
text21=ptstats21.AddText("Mean  =  "+str(round(Hthres21.GetMean(),1)));
text21=ptstats21.AddText("RMS   =  "+str(round(Hthres21.GetRMS(),2)));
ptstats21.Draw("");
    
ptstats22 = ROOT.TPaveStats(box2x1,boxy1,box2x2,boxy2,"brNDC");
ptstats22.SetName("stats");
ptstats22.SetBorderSize(2);
ptstats22.SetFillColor(0);
ptstats22.SetTextAlign(12);
ptstats22.SetTextColor(4);
text22 = ptstats22.AddText("Sec 6");
text22.SetTextSize(0.0368);
text22=ptstats22.AddText("Entries = "+str(int(Hthres22.GetEntries())));
text22=ptstats22.AddText("Mean  =  "+str(round(Hthres22.GetMean(),1)));
text22=ptstats22.AddText("RMS   =  "+str(round(Hthres22.GetRMS(),1)));
ptstats22.Draw("");
    
ptstats23 = ROOT.TPaveStats(box3x1,boxy1,box3x2,boxy2,"brNDC");
ptstats23.SetName("stats");
ptstats23.SetBorderSize(2);
ptstats23.SetFillColor(0);
ptstats23.SetTextAlign(12);
ptstats23.SetTextColor(8);
text23 = ptstats23.AddText("Sec 7");
text3.SetTextSize(0.0368);
text23=ptstats23.AddText("Entries = "+str(int(Hthres23.GetEntries())));
text23=ptstats23.AddText("Mean  =  "+str(round(Hthres23.GetMean(),1)));
text23=ptstats23.AddText("RMS   =  "+str(round(Hthres23.GetRMS(),1)));
ptstats23.Draw("");
    
ptstats24 = ROOT.TPaveStats(box4x1,boxy1,box4x2,boxy2,"brNDC");
ptstats24.SetName("stats");
ptstats24.SetBorderSize(2);
ptstats24.SetFillColor(0);
ptstats24.SetTextAlign(12);
ptstats24.SetTextColor(6);
text24 = ptstats24.AddText("Sec 8");
text24.SetTextSize(0.0368);
text24=ptstats24.AddText("Entries = "+str(int(Hthres24.GetEntries())));
text24=ptstats24.AddText("Mean  =  "+str(round(Hthres24.GetMean(),1)));
text24=ptstats24.AddText("RMS   =  "+str(round(Hthres24.GetRMS(),1)));
ptstats24.Draw("");
    
canthre21.Print(folder+"Threshold2_sig"+cutSigmin+"_chi"+cutChi2max+".pdf")
canthre21.Print(folder+"Threshold2_sig"+cutSigmin+"_chi"+cutChi2max+".C")
    
##############################################################################################
##noise
    
ROOT.gStyle.SetOptStat(0)
cannoise11=ROOT.TCanvas("cannoise11","cannoise11",800,600)
Hnoise11=ROOT.TH1F("Hnoise11","; noise [el];# pixel SECTOR 1",nBinsSig,minSig,maxSig)
Hnoise12=ROOT.TH1F("Hnoise12","Sec2; noise [el];# pixel SECTOR 2",nBinsSig,minSig,maxSig)
Hnoise13=ROOT.TH1F("Hnoise13","Sec3; noise [el];# pixel SECTOR 3",nBinsSig,minSig,maxSig)
Hnoise14=ROOT.TH1F("Hnoise14","Sec4; noise [el];# pixel SECTOR 4",nBinsSig,minSig,maxSig)
Hnoise11.Sumw2()
Hnoise12.Sumw2()
Hnoise13.Sumw2()
Hnoise14.Sumw2()
    
iT.Draw("sigma>>Hnoise11",cutCdt1,"goff")
iT.Draw("sigma>>Hnoise12",cutCdt2,"goff")
iT.Draw("sigma>>Hnoise13",cutCdt3,"goff")
iT.Draw("sigma>>Hnoise14",cutCdt4,"goff")
   
Hnoise11.SetMaximum(Hnoise11.GetMaximum()*1.50)
Hnoise11.SetLineColor(2)
Hnoise11.Draw("HIST")
Hnoise12.SetLineColor(4)
Hnoise12.Draw("HISTSAME")
Hnoise13.SetLineColor(8)
Hnoise13.Draw("HISTSAME")
Hnoise14.SetLineColor(6)
Hnoise14.Draw("HISTSAME")
    
ptstats_sig1 = ROOT.TPaveStats(box1x1,boxy1,box1x2,boxy2,"brNDC");
ptstats_sig1.SetName("stats");
ptstats_sig1.SetBorderSize(2);
ptstats_sig1.SetFillColor(0);
ptstats_sig1.SetTextAlign(12);
ptstats_sig1.SetTextColor(2);
text_sig1 = ptstats_sig1.AddText("Sec 1");
text_sig1.SetTextSize(0.0368);
text_sig1=ptstats_sig1.AddText("Entries = "+str(int(Hnoise11.GetEntries())));
text_sig1=ptstats_sig1.AddText("Mean  =  "+str(round(Hnoise11.GetMean(),1)));
text_sig1=ptstats_sig1.AddText("RMS   =  "+str(round(Hnoise11.GetRMS(),2)));
ptstats_sig1.Draw("");

ptstats_sig2 = ROOT.TPaveStats(box2x1,boxy1,box2x2,boxy2,"brNDC");
ptstats_sig2.SetName("stats");
ptstats_sig2.SetBorderSize(2);
ptstats_sig2.SetFillColor(0);
ptstats_sig2.SetTextAlign(12);
ptstats_sig2.SetTextColor(4);
text_sig2 = ptstats_sig2.AddText("Sec 2");
text_sig2.SetTextSize(0.0368);
text_sig2=ptstats_sig2.AddText("Entries = "+str(int(Hnoise12.GetEntries())));
text_sig2=ptstats_sig2.AddText("Mean  =  "+str(round(Hnoise12.GetMean(),1)));
text_sig2=ptstats_sig2.AddText("RMS   =  "+str(round(Hnoise12.GetRMS(),1)));
ptstats_sig2.Draw("");
    
ptstats_sig3 = ROOT.TPaveStats(box3x1,boxy1,box3x2,boxy2,"brNDC");
ptstats_sig3.SetName("stats");
ptstats_sig3.SetBorderSize(2);
ptstats_sig3.SetFillColor(0);
ptstats_sig3.SetTextAlign(12);
ptstats_sig3.SetTextColor(8);
text_sig3 = ptstats_sig3.AddText("Sec 3");
text_sig3.SetTextSize(0.0368);
text_sig3=ptstats_sig3.AddText("Entries = "+str(int(Hnoise13.GetEntries())));
text_sig3=ptstats_sig3.AddText("Mean  =  "+str(round(Hnoise13.GetMean(),1)));
text_sig3=ptstats_sig3.AddText("RMS   =  "+str(round(Hnoise13.GetRMS(),1)));
ptstats_sig3.Draw("");

ptstats_sig4 = ROOT.TPaveStats(box4x1,boxy1,box4x2,boxy2,"brNDC");
ptstats_sig4.SetName("stats");
ptstats_sig4.SetBorderSize(2);
ptstats_sig4.SetFillColor(0);
ptstats_sig4.SetTextAlign(12);
ptstats_sig4.SetTextColor(6);
text_sig4 = ptstats_sig4.AddText("Sec 4");
text_sig4.SetTextSize(0.0368);
text_sig4=ptstats_sig4.AddText("Entries = "+str(int(Hnoise14.GetEntries())));
text_sig4=ptstats_sig4.AddText("Mean  =  "+str(round(Hnoise14.GetMean(),1)));
text_sig4=ptstats_sig4.AddText("RMS   =  "+str(round(Hnoise14.GetRMS(),1)));
ptstats_sig4.Draw("");
    
cannoise11.Print(folder+"Sigma1_sig"+cutSigmin+"_chi"+cutChi2max+".pdf")

ROOT.gStyle.SetOptStat(0)
cannoise21=ROOT.TCanvas("cannoise21","cannoise21",800,600)
Hnoise21=ROOT.TH1F("Hnoise21","; noise [el];# pixel SECTOR 5",nBinsSig,minSig,maxSig)
Hnoise22=ROOT.TH1F("Hnoise22","Sec6; noise [el];# pixel SECTOR 6",nBinsSig,minSig,maxSig)
Hnoise23=ROOT.TH1F("Hnoise23","Sec7; noise [el];# pixel SECTOR 7",nBinsSig,minSig,maxSig)
Hnoise24=ROOT.TH1F("Hnoise24","Sec8; noise [el];# pixel SECTOR 8",nBinsSig,minSig,maxSig)
Hnoise21.Sumw2()
Hnoise22.Sumw2()
Hnoise23.Sumw2()
Hnoise24.Sumw2()
    
iT.Draw("sigma>>Hnoise21",cut2Cdt1,"goff")
iT.Draw("sigma>>Hnoise22",cut2Cdt2,"goff")
iT.Draw("sigma>>Hnoise23",cut2Cdt3,"goff")
iT.Draw("sigma>>Hnoise24",cut2Cdt4,"goff")
   
Hnoise21.SetMaximum(Hnoise21.GetMaximum()*1.50)
Hnoise21.SetLineColor(2)
Hnoise21.Draw("HIST")
Hnoise22.SetLineColor(4)
Hnoise22.Draw("HISTSAME")
Hnoise23.SetLineColor(8)
Hnoise23.Draw("HISTSAME")
Hnoise24.SetLineColor(6)
Hnoise24.Draw("HISTSAME")
    
ptstats_sig21 = ROOT.TPaveStats(box1x1,boxy1,box1x2,boxy2,"brNDC");
ptstats_sig21.SetName("stats");
ptstats_sig21.SetBorderSize(2);
ptstats_sig21.SetFillColor(0);
ptstats_sig21.SetTextAlign(12);
ptstats_sig21.SetTextColor(2);
text_sig21 = ptstats_sig21.AddText("Sec 5");
text_sig21.SetTextSize(0.0368);
text_sig21=ptstats_sig21.AddText("Entries = "+str(int(Hnoise21.GetEntries())));
text_sig21=ptstats_sig21.AddText("Mean  =  "+str(round(Hnoise21.GetMean(),1)));
text_sig21=ptstats_sig21.AddText("RMS   =  "+str(round(Hnoise21.GetRMS(),2)));
ptstats_sig21.Draw("");

ptstats_sig22 = ROOT.TPaveStats(box2x1,boxy1,box2x2,boxy2,"brNDC");
ptstats_sig22.SetName("stats");
ptstats_sig22.SetBorderSize(2);
ptstats_sig22.SetFillColor(0);
ptstats_sig22.SetTextAlign(12);
ptstats_sig22.SetTextColor(4);
text_sig22 = ptstats_sig22.AddText("Sec 6");
text_sig22.SetTextSize(0.0368);
text_sig22=ptstats_sig22.AddText("Entries = "+str(int(Hnoise22.GetEntries())));
text_sig22=ptstats_sig22.AddText("Mean  =  "+str(round(Hnoise22.GetMean(),1)));
text_sig22=ptstats_sig22.AddText("RMS   =  "+str(round(Hnoise22.GetRMS(),1)));
ptstats_sig22.Draw("");
    
ptstats_sig23 = ROOT.TPaveStats(box3x1,boxy1,box3x2,boxy2,"brNDC");
ptstats_sig23.SetName("stats");
ptstats_sig23.SetBorderSize(2);
ptstats_sig23.SetFillColor(0);
ptstats_sig23.SetTextAlign(12);
ptstats_sig23.SetTextColor(8);
text_sig23 = ptstats_sig23.AddText("Sec 7");
text_sig23.SetTextSize(0.0368);
text_sig23=ptstats_sig23.AddText("Entries = "+str(int(Hnoise23.GetEntries())));
text_sig23=ptstats_sig23.AddText("Mean  =  "+str(round(Hnoise23.GetMean(),1)));
text_sig23=ptstats_sig23.AddText("RMS   =  "+str(round(Hnoise23.GetRMS(),1)));
ptstats_sig23.Draw("");

ptstats_sig24 = ROOT.TPaveStats(box4x1,boxy1,box4x2,boxy2,"brNDC");
ptstats_sig24.SetName("stats");
ptstats_sig24.SetBorderSize(2);
ptstats_sig24.SetFillColor(0);
ptstats_sig24.SetTextAlign(12);
ptstats_sig24.SetTextColor(6);
text_sig24 = ptstats_sig24.AddText("Sec 8");
text_sig24.SetTextSize(0.0368);
text_sig24=ptstats_sig24.AddText("Entries = "+str(int(Hnoise24.GetEntries())));
text_sig24=ptstats_sig24.AddText("Mean  =  "+str(round(Hnoise24.GetMean(),1)));
text_sig24=ptstats_sig24.AddText("RMS   =  "+str(round(Hnoise24.GetRMS(),1)));
ptstats_sig24.Draw("");
        
cannoise21.Print(folder+"Sigma2_sig"+cutSigmin+"_chi"+cutChi2max+".pdf")

    
###########################################################################################################
###########  2D  HIST  ###################################################
###########################################################################################################
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPadRightMargin(0.15);
    
lines=[]
Lin1B=ROOT.TLine( 64, Hthres11.GetXaxis().GetXmin(), 64, Hthres11.GetXaxis().GetXmax());
lines.append(Lin1B)
Lin2B=ROOT.TLine(128, Hthres11.GetXaxis().GetXmin(),128, Hthres11.GetXaxis().GetXmax());
lines.append(Lin2B)
Lin3B=ROOT.TLine(192, Hthres11.GetXaxis().GetXmin(),192, Hthres11.GetXaxis().GetXmax());
lines.append(Lin3B)
Lin4B=ROOT.TLine(256, Hthres11.GetXaxis().GetXmin(),256, Hthres11.GetXaxis().GetXmax());
lines.append(Lin4B)
Lin5B=ROOT.TLine(320, Hthres11.GetXaxis().GetXmin(),320, Hthres11.GetXaxis().GetXmax());
lines.append(Lin5B)
Lin6B=ROOT.TLine(384, Hthres11.GetXaxis().GetXmin(),384, Hthres11.GetXaxis().GetXmax());
lines.append(Lin6B)
Lin7B=ROOT.TLine(448, Hthres11.GetXaxis().GetXmin(),448, Hthres11.GetXaxis().GetXmax());
lines.append(Lin7B)
for l in lines: l.SetLineWidth(2)

can3=ROOT.TCanvas("can3","can3",800,600)
H2thres=ROOT.TH2F("H2thres","H2thres;pix Col; threshold [el];# pixel",nBinsPix*2,minPix1,maxPix2,50,minTh,maxTh)
H2thres.Sumw2()
iT.Draw("thres:pixX>>H2thres",cutCdtAll,"goff")
H2thres.Draw("COLZ")
for l in lines: l.Draw("SAMEL")
can3.Print(folder+"Threshold2D_sig"+cutSigmin+"_chi"+cutChi2max+".pdf")

###########################################################################################################

lines2=[]
Lin1C=ROOT.TLine( 64, Hnoise11.GetXaxis().GetXmin(), 64, Hnoise11.GetXaxis().GetXmax());
lines2.append(Lin1C)
Lin2C=ROOT.TLine(128, Hnoise11.GetXaxis().GetXmin(),128, Hnoise11.GetXaxis().GetXmax());
lines2.append(Lin2C)
Lin3C=ROOT.TLine(192, Hnoise11.GetXaxis().GetXmin(),192, Hnoise11.GetXaxis().GetXmax());
lines2.append(Lin3C)
Lin4C=ROOT.TLine(256, Hnoise11.GetXaxis().GetXmin(),256, Hnoise11.GetXaxis().GetXmax());
lines2.append(Lin4C)
Lin5C=ROOT.TLine(320, Hnoise11.GetXaxis().GetXmin(),320, Hnoise11.GetXaxis().GetXmax());
lines2.append(Lin5C)
Lin6C=ROOT.TLine(384, Hnoise11.GetXaxis().GetXmin(),384, Hnoise11.GetXaxis().GetXmax());
lines2.append(Lin6C)
Lin7C=ROOT.TLine(448, Hnoise11.GetXaxis().GetXmin(),448, Hnoise11.GetXaxis().GetXmax());
lines2.append(Lin7C)
for l in lines2: l.SetLineWidth(2)
    
can4=ROOT.TCanvas("can4","can4",800,600)
H2noise=ROOT.TH2F("H2noise","H2noise;pix Col; noise [el];# pixel",nBinsPix*2,minPix1,maxPix2,nBinsSig,minSig,maxSig)
H2noise.Sumw2()
iT.Draw("sigma:pixX>>H2noise",cutCdtAll,"goff")
H2noise.Draw("COLZ")
for l in lines2: l.Draw("SAMEL")
can4.Print(folder+"Sigma2D_sig"+cutSigmin+"_chi"+cutChi2max+".pdf")
    
###########################################################################################################
    
lines3=[]
Lin1A=ROOT.TLine( 64, 0, 64, 512);
lines3.append(Lin1A)
Lin2A=ROOT.TLine(128, 0,128, 512);
lines3.append(Lin2A)
Lin3A=ROOT.TLine(192, 0,192, 512);
lines3.append(Lin3A)
Lin4A=ROOT.TLine(256, 0,256, 512);
lines3.append(Lin4A)
Lin5A=ROOT.TLine(320, 0,320, 512);
lines3.append(Lin5A)
Lin6A=ROOT.TLine(384, 0,384, 512);
lines3.append(Lin6A)
Lin7A=ROOT.TLine(448, 0,448, 512);
lines3.append(Lin7A)
for l in lines3: l.SetLineWidth(1)
    
can5=ROOT.TCanvas("can5","can5",800,600)
H2fancy=ROOT.TH2F("H2map","H2map;pixX; pixY; threshold [el]",nBinsPix*2,minPix1,maxPix2,514,-1.5,512.5)
H2fancy.Sumw2()
iT.Draw("pixY:pixX>>H2map","thres*("+cutCdtAll+")","goff")
H2fancy.SetMaximum(maxTh)
H2fancy.SetMinimum(minTh)
H2fancy.Draw("COLZ")
for l in lines3: l.Draw("SAMEL")
can5.Print(folder+"Map2D_sig"+cutSigmin+"_chi"+cutChi2max+".pdf")


########## RMS #######################################
result01 = "{:d} {:s}\n".format(1,str(round(Hthres11.GetRMS(),1)))
result02 = "{:d} {:s}\n".format(2,str(round(Hthres12.GetRMS(),1)))
result03 = "{:d} {:s}\n".format(3,str(round(Hthres13.GetRMS(),1)))
result04 = "{:d} {:s}\n".format(4,str(round(Hthres14.GetRMS(),1)))
result05 = "{:d} {:s}\n".format(5,str(round(Hthres21.GetRMS(),1)))
result06 = "{:d} {:s}\n".format(6,str(round(Hthres22.GetRMS(),1)))
result07 = "{:d} {:s}\n".format(7,str(round(Hthres23.GetRMS(),1)))
result08 = "{:d} {:s}\n".format(8,str(round(Hthres24.GetRMS(),1)))
fileRes = open(folder+"ResultRMSthres_sig"+cutSigmin+"_chi"+cutChi2max+".txt","w")
fileRes.write("#sector ThresRMS\n")
fileRes.close()
fileRes2 = open(folder+"ResultRMSthres_sig"+cutSigmin+"_chi"+cutChi2max+".txt","a")
fileRes2.write(result01)
fileRes2.write(result02)
fileRes2.write(result03)
fileRes2.write(result04)
fileRes2.write(result05)
fileRes2.write(result06)
fileRes2.write(result07)
fileRes2.write(result08)
fileRes2.close()


#########  fitting  ##########################################
Fit11 = Hthres11.Fit("gaus","S")
Fit12 = Hthres12.Fit("gaus","S")
Fit13 = Hthres13.Fit("gaus","S")
Fit14 = Hthres14.Fit("gaus","S")
raw_input("press the enter to continue")
result11 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(1,Fit11.Get().Parameter(1),0.0,Fit11.Get().ParError(1))
result12 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(2,Fit12.Get().Parameter(1),0.0,Fit12.Get().ParError(1))
result13 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(3,Fit13.Get().Parameter(1),0.0,Fit13.Get().ParError(1))
result14 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(4,Fit14.Get().Parameter(1),0.0,Fit14.Get().ParError(1))

Fit21 = Hthres21.Fit("gaus","S")
Fit22 = Hthres22.Fit("gaus","S")
Fit23 = Hthres23.Fit("gaus","S")
Fit24 = Hthres24.Fit("gaus","S")
raw_input("press the enter to continue")
result21 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(5,Fit21.Get().Parameter(1),0.0,Fit21.Get().ParError(1))
result22 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(6,Fit22.Get().Parameter(1),0.0,Fit22.Get().ParError(1))
result23 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(7,Fit23.Get().Parameter(1),0.0,Fit23.Get().ParError(1))
result24 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(8,Fit24.Get().Parameter(1),0.0,Fit24.Get().ParError(1))

fileRes = open(folder+"ResultThres_sig"+cutSigmin+"_chi"+cutChi2max+".txt","w")
fileRes.write("#sector ThresMean secError meanError\n")
fileRes.close()
fileRes2 = open(folder+"ResultThres_sig"+cutSigmin+"_chi"+cutChi2max+".txt","a")
fileRes2.write(result11)
fileRes2.write(result12)
fileRes2.write(result13)
fileRes2.write(result14)
fileRes2.write(result21)
fileRes2.write(result22)
fileRes2.write(result23)
fileRes2.write(result24)
fileRes2.close()

fitmin=4.0
fitmax=25.0
FitSig11 = Hnoise11.Fit("gaus","S","",fitmin,fitmax)
FitSig12 = Hnoise12.Fit("gaus","S","",fitmin,fitmax)
FitSig13 = Hnoise13.Fit("gaus","S","",fitmin,fitmax)
FitSig14 = Hnoise14.Fit("gaus","S","",fitmin,fitmax)
raw_input("press the enter to continue")
resultSig11 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(1,FitSig11.Get().Parameter(1),0.0,FitSig11.Get().ParError(1))
resultSig12 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(2,FitSig12.Get().Parameter(1),0.0,FitSig12.Get().ParError(1))
resultSig13 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(3,FitSig13.Get().Parameter(1),0.0,FitSig13.Get().ParError(1))
resultSig14 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(4,FitSig14.Get().Parameter(1),0.0,FitSig14.Get().ParError(1))

FitSig21 = Hnoise21.Fit("gaus","S","",fitmin,fitmax)
FitSig22 = Hnoise22.Fit("gaus","S","",fitmin,fitmax)
FitSig23 = Hnoise23.Fit("gaus","S","",fitmin,fitmax)
FitSig24 = Hnoise24.Fit("gaus","S","",fitmin,fitmax)
raw_input("press the enter to continue")
resultSig21 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(5,FitSig21.Get().Parameter(1),0.0,FitSig21.Get().ParError(1))
resultSig22 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(6,FitSig22.Get().Parameter(1),0.0,FitSig22.Get().ParError(1))
resultSig23 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(7,FitSig23.Get().Parameter(1),0.0,FitSig23.Get().ParError(1))
resultSig24 = "{:d} {:.7f} {:.7f} {:.7f}\n".format(8,FitSig24.Get().Parameter(1),0.0,FitSig24.Get().ParError(1))
fileRes = open(folder+"ResultNoise_sig"+cutSigmin+"_chi"+cutChi2max+".txt","w")
fileRes.write("#sector ThresMean secError meanError\n")
fileRes.close()
fileRes2 = open(folder+"ResultNoise_sig"+cutSigmin+"_chi"+cutChi2max+".txt","a")
fileRes2.write(resultSig11)
fileRes2.write(resultSig12)
fileRes2.write(resultSig13)
fileRes2.write(resultSig14)
fileRes2.write(resultSig21)
fileRes2.write(resultSig22)
fileRes2.write(resultSig23)
fileRes2.write(resultSig24)
fileRes2.close()

# c1=ROOT.TCanvas("c1","c1")
# pad1=ROOT.TPad("pad1","pad1",0,0.51,0.49,1)
# pad1.SetTopMargin(0)
# pad1.SetBottomMargin(0)
# pad1.SetGridx()
# pad1.Draw()
# c1.cd()
# pad2=ROOT.TPad("pad2","pad2",0.51,0.51,1,1)
# pad2.SetTopMargin(0)
# pad2.SetBottomMargin(0)
# pad2.SetGridx()
# pad2.Draw()
# c1.cd()
# pad3=ROOT.TPad("pad3","pad3",0,0,0.49,0.49)
# pad3.SetTopMargin(0)
# pad3.SetBottomMargin(0)
# pad3.SetGridx()
# pad3.Draw()
# c1.cd()
# pad4=ROOT.TPad("pad4","pad4",0.51,0,1,0.49)
# pad4.SetTopMargin(0)
# pad4.SetBottomMargin(0)
# pad4.SetGridx()
# pad4.Draw()

# pad1.cd()
# # Hthres11.Draw("")
# # Fit11.Draw("SAME")
# # Hthres12.Draw("SAME")
# # Fit12.Draw("SAME")
# # Hthres13.Draw("SAME")
# # Fit13.Draw("SAME")
# Hthres14.Draw("SAME")
# Fit14.Draw("SAME")
# pad2.cd()
# Hthres21.Draw("")
# Fit21.Draw("SAME")
# Hthres22.Draw("SAME")
# Fit22.Draw("SAME")
# # Hthres23.Draw("SAME")
# # Fit23.Draw("SAME")
# # Hthres24.Draw("SAME")
# # Fit24.Draw("SAME")
# pad3.cd()
# # Hnoise11.Draw("")
# # Fit11.Draw("SAME")
# # Hnoise12.Draw("SAME")
# # Fit12.Draw("SAME")
# # Hnoise13.Draw("SAME")
# # Fit13.Draw("SAME")
# Hnoise14.Draw("SAME")
# Fit14.Draw("SAME")
# pad4.cd()
# Hnoise21.Draw("")
# Fit21.Draw("SAME")
# Hnoise22.Draw("SAME")
# Fit22.Draw("SAME")
# # Hnoise23.Draw("SAME")
# # Fit23.Draw("SAME")
# # Hnoise24.Draw("SAME")
# # Fit24.Draw("SAME")
# c1.Print(folder+"FitThres"+cutSigmin+"_chi"+cutChi2max+".ps")
# c1.Close()

