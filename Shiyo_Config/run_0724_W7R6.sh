#!/bin/sh

#### 24.07.2019 SHIYO for W7R6

#1st Sec
python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0724_1stSec1_10 -r 0 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0724_1stSec2_10 -r 16 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0724_1stSec3_10 -r 32 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0724_1stSec4_10 -r 48 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

