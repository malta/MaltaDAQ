#!/bin/sh

#### 26.07.2019 SHIYO for W7R6

#5th Sec				
python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_5thSec1_10 -r 256 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_5thSec2_10 -r 272 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_5thSec3_10 -r 288 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_5thSec4_10 -r 304 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

#6th Sec				
python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_6thSec1_10 -r 320 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_6thSec2_10 -r 336 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_6thSec3_10 -r 352 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_6thSec4_10 -r 368 16 0 10 -l 62 -h 85 -s 1 -t 500 -q\


### Reserver
#3rd Sec				
python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_3rdSec1_10_r -r 128 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_3rdSec2_10_r -r 144 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_3rdSec3_10_r -r 160 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_3rdSec4_10_r -r 176 16 0 10 -l 62 -h 85 -s 1 -t 500 -q
