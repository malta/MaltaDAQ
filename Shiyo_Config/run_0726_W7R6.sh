#!/bin/sh

#### 26.07.2019 SHIYO for W7R6

#3rd Sec				
python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_3rdSec1_10 -r 128 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_3rdSec2_10 -r 144 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_3rdSec3_10 -r 160 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

python Shiyo_Config/Malta_Config_W7R6.py -c 3
MaltaThresholdScan_STREAM_C -f W7R6 -o test0726_3rdSec4_10 -r 176 16 0 10 -l 62 -h 85 -s 1 -t 500 -q

