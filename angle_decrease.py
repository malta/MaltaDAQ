import os
import argparse
import time
import numpy as np

parser=argparse.ArgumentParser()
parser.add_argument('-a' ,'--angle',help='angle value',default="0")
parser.add_argument('-s' ,'--step',help='step value',default="0.05")
args=parser.parse_args()




angle = os.popen("KDC101AxisCtrl.py -i").read()
angle_val = float(angle.split("position: ")[1].split(" deg")[0])
print(angle)
while angle_val < float(args.angle):
    new_command = "KDC101AxisCtrl.py -m rel -d %f"%(float(args.step))
    angle_val +=float(args.step)	
    time.sleep(1)
    #print(new_command)
    #print(angle_val)
    angle = os.system(new_command)
