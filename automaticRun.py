import os

import time


runNumber = 1754
minutes = 30
SUB_values = [6]
config_files = ["configs/config_telescope_2020_cosmics_DUTW1R1Epi_WTN_7030Kintex.txt"]
 


def check_if_string_in_file():
    with open("/tmp/analysis.log", 'r') as read_obj:
        for line in read_obj:
            if "first check: BAD" in line:
                return -1
            if "first check: WARNING" in line:
                return 0
            if "first check: GOOD" in line:
       	        return 1
    return 1

def check_if_run_exists(run):
   if os.path.exists("TelescopeData/run_%06i_0.root.root" % (run)): 
     for i in range(0,10):
       print("\033[1;31mroot file already exists press Ctrl+C to stop the data taking! the run will start in "+str(10-i)+"s\033[0m")
       time.sleep(1)

def run_point(sub, config, runNumber):
    check_if_run_exists(runNumber)
    
    os.system("MALTA_PSU.py -c rampVoltage DUT_SUB "+str(sub)+" 10")

    os.system("python /home/sbmuser/MaltaSW/MaltaTLU/share/TLU_cmd.py -c 161.py --stop")

    os.system("MaltaMultiDAQ -c "+config+" -r "+str(runNumber)+" -p &")

    time.sleep(100)

    os.system("python /home/sbmuser/MaltaSW/MaltaTLU/share/TLU_cmd.py -c 161.py --start --counter -d "+ str(minutes))

    os.system("python /home/sbmuser/MaltaSW/MaltaTLU/share/TLU_cmd.py -c 161.py --stop")

    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")

    time.sleep(20) 

    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    time.sleep(20) 
    
    os.system('rm  /tmp/analysis.log')
    os.system('ssh sbmuser@pcatlidps08 ". ~/MaltaSW/setup.sh; cd /home/sbmuser/MaltaSW/MaltaTbAnalysis/Sr90_tests/; python MaltaDQ_Sr90_test.py -r "'+str(runNumber)+"> /tmp/analysis.log")

def check_run(config):
    file_object = open('logRuns.txt', 'a')
    isBad = check_if_string_in_file()
    if isBad == -1:
      file_object.write(str(runNumber)+", "+ str(sub) + "V, "+config+", BAD\n") 
    elif isBad == 0:
      file_object.write(str(runNumber)+", "+ str(sub) + "V, "+config+", WARNING\n") 
    else:
      file_object.write(str(runNumber)+", "+ str(sub) + "V, "+config+", GOOD\n") 
    file_object.close()
    return isBad

for config in config_files:
  for sub in SUB_values:
    print(runNumber)

    run_point(sub,config,runNumber)
    check_run(config)
    if check_if_string_in_file() ==-1:
       run_point(sub,config,runNumber)
       check_run(config)
    if check_if_string_in_file() ==-1:
       run_point(sub,config,runNumber)
       check_run(config)

    runNumber=runNumber+1


  os.system("MALTA_PSU.py -c rampVoltage DUT_SUB 6 20")
os.system("MALTA_PSU.py -c rampVoltage DUT_SUB 6 20")

#ssh sbmuser@pcatlidps08 ". ~/MaltaSW/setup.sh; cd /home/sbmuser/MaltaSW/MaltaTbAnalysis/Sr90_tests/; python MaltaDQ_Sr90_test.py -r 1577"

