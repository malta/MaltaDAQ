import os
import mysql.connector
import socket
import time
import loop_automaticRun as loop
from collections import namedtuple
from re import match
import argparse


parser = argparse.ArgumentParser()
#parser.add_argument("-y", "--ypos"     , help="ypos small stage"  , required=True)
args=parser.parse_args()


""" This file automatically runs the execution and analysis for a Telescope run. The SQL database is automatically filled.  """ 

#time.sleep(10800)     #for launching the screen after X seconds

################# Number of events ############

# triggers is number of triggers in real life #

triggers = 1200000

################# SUB Voltage #################


#SUB_values  = [[int(args.sub),int(args.sub)]]

#SUB_values = [[6,6],[9,9],[15,15],[20,20],[25,25],[30,30]]

#SUB_values = [[6,6],[9,6],[20,6],[30,6],[40,6],[50,6],[55,6]]

SUB_values = [[6,55],[6,50],[10,45],[15,40],[20,35],[25,30],[30,25],[35,20],[40,15],[45,10],[50,6],[55,6]]


################ Y-position of DUT ###########
#########New format Y_positions = [[z_pos,x_pos,angle]]

#FOR ANGLE POSITION ONLY MINUS VALUES!!!!!!!!!!!!!!!!!!!!!!!! or we break the chip. No pressure


#Y_positions = [[2.3,-27,0],[1.3,-27,0]]
#Y_positions = [[2.45,-24.5,0],[1.35,-24.5,0]]
#Y_positions = [[1.7,-54,0],[0.7,-54,0]]
Y_positions = [[1.75,-49,0]]

###New Cold Box Rotation Allignmment###
#Y_positions = [[2.0,-31,0],[,,10],[,,20],[,,30],[,,35],[,,40],[,,45],[,,50],[,,55],[,,60]]

#Y_positions = [[2.0,-64.0,-40]]
#Y_positions = [[0.925,-62.0,-35]]
#Y_positions = [[0.925,-59.0,-30]]
#Y_positions = [[0.925,-70,-50]]
#Y_positions = [[0.925,-73,-55]]
#Y_positions = [[0.925,-77,-60]]
#Y_positions = [[1.05,-59.0,-30]]
#Y_positions = [[1.05,-62.0,-35]]
#Y_positions = [[1.05,-64.0,-40]]
#Y_positions = [[1.05,-70.0,-45]]
#Y_positions = [[1.05,-72.0,-50]]
#Y_positions = [[1.05,-75.0,-55]]

#Y_positions = [[0.65,-60.0,-30]]
#Y_positions = [[0.65,-63.0,-35]]

############### Configuration Files #########
# Do not forget to add a comma after X.txt" #

#############################################################################################
#VLAD & ANUSREE: DUMB IMPLEMENATION OF ANGLE STORAGE IN loop_automaticRun.py. Before running
#Angle measurements check its disabled!!!!! 
#############################################################################################


config_files = [
"configs/DUT/config_telescope_SPS_DUT_W18R12_12030_W18R14_12030.txt",
"configs/DUT/config_telescope_SPS_DUT_W18R12_12040_W18R14_12040.txt",
"configs/DUT/config_telescope_SPS_DUT_W18R12_12060_W18R14_12060.txt",
"configs/DUT/config_telescope_SPS_DUT_W18R12_12080_W18R14_12080.txt",
"configs/DUT/config_telescope_SPS_DUT_W18R12_120100_W18R14_120100.txt",
"configs/DUT/config_telescope_SPS_DUT_W18R12_120120_W18R14_120120.txt",
#"configs/DUT/config_telescope_SPS_DUT_W15R19_120120_W18R5_120120.txt",
#"configs/DUT/config_telescope_SPS_DUT_W15R19_120100_W18R5_120100.txt",
#"configs/DUT/config_telescope_SPS_DUT_W15R19_12080_W18R5_12080.txt",
#"configs/DUT/config_telescope_SPS_DUT_W15R19_12060_W18R5_12060.txt",
#"configs/DUT/config_telescope_SPS_DUT_W15R19_12040_W18R5_12040.txt",
#"configs/DUT/config_telescope_SPS_DUT_W15R19_12020_W18R5_12020.txt",
#"configs/DUT/config_telescope_SPS_DUT_W12R7_120120_Rotational.txt",
#"configs/DUT/config_telescope_SPS_DUT_W12R7_120100_Rotational.txt",
#"configs/DUT/config_telescope_SPS_DUT_W12R7_12080_Rotational.txt",
#"configs/DUT/config_telescope_SPS_DUT_W12R7_12060_Rotational.txt",
#"configs/DUT/config_telescope_SPS_DUT_W12R7_12040_Rotational.txt",
#"configs/DUT/config_telescope_SPS_DUT_W12R7_12020_Rotational.txt",
#"configs/DUT/config_telescope_SPS_DUT_W12R7_12015_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12020_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12040_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12080_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_120120_Rotational.txt",

#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12020_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12040_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12080_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_120120_Rotational.txt",

#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12020_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12040_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12080_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_120120_Rotational.txt",

#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12020_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12040_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12080_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_120120_Rotational.txt",

#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12020_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12040_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_12080_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R10_120120_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R19_12020_Rotational.txt",
#"configs/DUT/SingleDUT/config_telescope_SPS_DUT_W12R19_12040_Rotational.txt",



]

############# Number of DUTs ################

enablePico=False
 
#example for multiple DUT where the DUTs are plane #2 and #3 dut = dict({'DUT1':2, 'DUT2':3})
#duts = dict({'DUT1':7})      # note we are now only using one DUT !
duts = dict({'DUT1':7, 'DUT2':8})
############ Applied Cuts and Type Run ######


XYcuts=dict()

#MALTA2  {'Xmin':0, 'Xmax':511,'Ymin':288,'Ymax':511}
#MALTA    {'Xmin':0, 'Xmax':511,'Ymin':0,'Ymax':511}

XYcuts['DUT1']=dict( {'Xmin':0, 'Xmax':511,'Ymin':0,'Ymax':511})
XYcuts['DUT2']=dict( {'Xmin':0, 'Xmax':511,'Ymin':0,'Ymax':511})

# Sr90 or muons or check DB
runType='SPS'


############## Starting the loop ############

loop.main_loop(runType, Y_positions, SUB_values, config_files, triggers, duts, XYcuts)
#loop.main_loop(runNumber, runType, SUB_values, config_files, triggers, duts, XYcuts)

os.system("MALTA_PSU.py -c rampVoltage DUT1_SUB 6 20")
os.system("MALTA_PSU.py -c rampVoltage DUT2_SUB 6 20")

