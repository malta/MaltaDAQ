import os
import mysql.connector
import socket
import time
import loop_automaticRun as loop
from collections import namedtuple
from re import match
import argparse


#parser = argparse.ArgumentParser()
#parser.add_argument("-s", "--sub"     , help="sub"  , required=True)
#args=parser.parse_args()


""" This file automatically runs the execution and analysis for a Telescope run. The SQL database is automatically filled.  """ 

minutes = 2000000  #triggers in real life !!!
#SUB_values  = [[int(args.sub),int(args.sub)]]

#SUB_values  = [[DUT_SUB, DUT2_SUB]]

#SUB_values  = [[6,6]]
SUB_values  = [[6,15],[10,14],[15,13],[20,12],[25,11],[30,10],[35,9],[40,8],[45,7],[50,6]]

config_files = [
    "configs/config_telescope_SPS_DUT_W7R13_IDB70_ITHR30_W14R10_IDB50_ITHR50.txt",
]


enablePico=False
 
#example for multiple DUT where the DUTs are plane #2 and #3 dut = dict({'DUT1':2, 'DUT2':3})
duts = dict()
duts = dict({'DUT1':8,'DUT2':7})

XYcuts=dict()

#MALTA2  {'Xmin':0, 'Xmax':511,'Ymin':288,'Ymax':511}
#MALTA    {'Xmin':0, 'Xmax':511,'Ymin':0,'Ymax':511}

XYcuts['DUT1']=dict( {'Xmin':0, 'Xmax':511,'Ymin':0,'Ymax':511})
XYcuts['DUT2']=dict( {'Xmin':0, 'Xmax':511,'Ymin':0,'Ymax':511})

# Sr90 or muons or check DB
runType='SPS'

loop.main_loop(runType, SUB_values, config_files, minutes, duts, XYcuts)
#loop.main_loop(runNumber, runType, SUB_values, config_files, minutes, duts, XYcuts)

os.system("MALTA_PSU.py -c rampVoltage DUT_SUB 6 20")
os.system("MALTA_PSU.py -c rampVoltage DUT2_SUB 6 20")

#ssh sbmuser@pcatlidps08 ". ~/MaltaSW/setup.sh; cd /home/sbmuser/MaltaSW/MaltaTbAnalysis/Sr90_tests/; python MaltaDQ_Sr90_test.py -r 1577"

