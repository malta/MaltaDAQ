type: MALTA2 
sample: W12R6 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 80 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R6/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 109, 334 # entries 1150 rate 11500 kHz word 	 
MASK_PIXEL: 417, 381 # entries 1192 rate 11920 kHz word 	 
MASK_PIXEL: 135, 440 # entries 1160 rate 11600 kHz word 	 
MASK_PIXEL: 436, 402 # entries 1130 rate 11300 kHz word 	 
MASK_PIXEL: 138, 330 # entries 1183 rate 11830 kHz word 	 
MASK_PIXEL: 430, 291 # entries 720 rate 7200 kHz word 	 
MASK_PIXEL: 430, 314 # entries 829 rate 8290 kHz word 	 
MASK_PIXEL: 44, 361 # entries 487 rate 4870 kHz word 	 
MASK_PIXEL: 239, 355 # entries 306 rate 3060 kHz word 	 
MASK_PIXEL: 436, 364 # entries 266 rate 2660 kHz word 	 
MASK_PIXEL: 473, 368 # entries 188 rate 1880 kHz word 	 
MASK_PIXEL: 451, 414 # entries 317 rate 3170 kHz word 	 
MASK_PIXEL: 492, 296 # entries 225 rate 2250 kHz word 	 
MASK_PIXEL: 273, 317 # entries 161 rate 1610 kHz word 	 
MASK_PIXEL: 448, 306 # entries 42 rate 420 kHz word 	 
MASK_PIXEL: 425, 341 # entries 31 rate 310 kHz word 	 
MASK_PIXEL: 466, 307 # entries 313 rate 313 kHz word 	 
MASK_PIXEL: 430, 339 # entries 194 rate 194 kHz word 	 
MASK_PIXEL: 366, 359 # entries 61 rate 61 kHz word 	 
MASK_PIXEL: 43, 299 # entries 40 rate 40 kHz word 	 
MASK_PIXEL: 57, 339 # entries 30 rate 30 kHz word 	 
MASK_PIXEL: 63, 288 # entries 17 rate 17 kHz word 	 
MASK_PIXEL: 425, 363 # entries 12 rate 12 kHz word 	 
MASK_PIXEL: 148, 327 # entries 195 rate 19.5 kHz word 	 
MASK_PIXEL: 402, 318 # entries 117 rate 11.7 kHz word 	 
MASK_PIXEL: 276, 331 # entries 103 rate 10.3 kHz word 	 
MASK_PIXEL: 446, 351 # entries 69 rate 6.9 kHz word 	 
MASK_PIXEL: 387, 364 # entries 115 rate 11.5 kHz word 	 
MASK_PIXEL: 226, 320 # entries 70 rate 7 kHz word 	 
MASK_PIXEL: 43, 294 # entries 30 rate 3 kHz word 	 
MASK_PIXEL: 418, 381 # entries 36 rate 3.6 kHz word 	 
MASK_PIXEL: 1, 323 # entries 33 rate 3.3 kHz word 	 
MASK_PIXEL: 70, 295 # entries 28 rate 2.8 kHz word 	 
MASK_PIXEL: 43, 345 # entries 32 rate 3.2 kHz word 	 
MASK_PIXEL: 442, 378 # entries 18 rate 1.8 kHz word 	 
MASK_PIXEL: 20, 329 # entries 10 rate 1 kHz word 	 
MASK_PIXEL: 213, 288 # entries 10 rate 1 kHz word 	 
MASK_PIXEL: 473, 289 # entries 141 rate 1.41 kHz word 	 
MASK_PIXEL: 375, 379 # entries 85 rate 0.85 kHz word 	 
MASK_PIXEL: 365, 398 # entries 38 rate 0.38 kHz word 	 
MASK_PIXEL: 473, 349 # entries 42 rate 0.42 kHz word 	 
MASK_PIXEL: 491, 366 # entries 16 rate 0.16 kHz word 	 
MASK_PIXEL: 420, 342 # entries 19 rate 0.19 kHz word 	 
MASK_PIXEL: 333, 291 # entries 15 rate 0.15 kHz word 	 
MASK_PIXEL: 482, 329 # entries 27 rate 0.27 kHz word 	 
MASK_PIXEL: 260, 345 # entries 72 rate 0.144 kHz word 	 
MASK_PIXEL: 503, 289 # entries 56 rate 0.112 kHz word 	 
MASK_PIXEL: 134, 294 # entries 59 rate 0.118 kHz word 	 
MASK_PIXEL: 501, 292 # entries 47 rate 0.094 kHz word 	 
MASK_PIXEL: 133, 318 # entries 30 rate 0.06 kHz word 	 
MASK_PIXEL: 97, 319 # entries 29 rate 0.058 kHz word 	 
MASK_PIXEL: 435, 328 # entries 16 rate 0.032 kHz word 	 
