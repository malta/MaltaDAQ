type: MALTA2 
sample: W12R18 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 20 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R18/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 319, 289 # entries 751 rate 7510 kHz word 
MASK_PIXEL: 488, 506 # entries 856 rate 8560 kHz word 
MASK_PIXEL: 225, 320 # entries 813 rate 8130 kHz word 
MASK_PIXEL: 93, 474 # entries 822 rate 8220 kHz word 
MASK_PIXEL: 378, 415 # entries 813 rate 8130 kHz word 
MASK_PIXEL: 382, 375 # entries 910 rate 9100 kHz word 
MASK_PIXEL: 3, 452 # entries 1029 rate 10290 kHz word 
MASK_PIXEL: 9, 325 # entries 958 rate 9580 kHz word 
MASK_PIXEL: 377, 351 # entries 957 rate 9570 kHz word 
MASK_PIXEL: 5, 488 # entries 931 rate 9310 kHz word 
MASK_PIXEL: 430, 396 # entries 1012 rate 10120 kHz word 
MASK_PIXEL: 10, 388 # entries 941 rate 9410 kHz word 
MASK_PIXEL: 265, 320 # entries 878 rate 8780 kHz word 
MASK_PIXEL: 386, 442 # entries 455 rate 4550 kHz word 
MASK_PIXEL: 27, 446 # entries 333 rate 3330 kHz word 
MASK_PIXEL: 69, 369 # entries 470 rate 4700 kHz word 
MASK_PIXEL: 5, 496 # entries 448 rate 4480 kHz word 
MASK_PIXEL: 506, 409 # entries 318 rate 3180 kHz word 
MASK_PIXEL: 249, 435 # entries 325 rate 3250 kHz word 
MASK_PIXEL: 118, 485 # entries 264 rate 2640 kHz word 
MASK_PIXEL: 493, 438 # entries 200 rate 2000 kHz word 
MASK_PIXEL: 33, 436 # entries 227 rate 2270 kHz word 
MASK_PIXEL: 20, 501 # entries 284 rate 2840 kHz word 
MASK_PIXEL: 349, 434 # entries 105 rate 1050 kHz word 
MASK_PIXEL: 24, 453 # entries 153 rate 1530 kHz word 
MASK_PIXEL: 75, 436 # entries 101 rate 1010 kHz word 
MASK_PIXEL: 337, 291 # entries 162 rate 1620 kHz word 
MASK_PIXEL: 18, 389 # entries 106 rate 1060 kHz word 
MASK_PIXEL: 236, 491 # entries 181 rate 1810 kHz word 
MASK_PIXEL: 355, 355 # entries 130 rate 1300 kHz word 
MASK_PIXEL: 91, 412 # entries 24 rate 240 kHz word 
MASK_PIXEL: 405, 359 # entries 306 rate 306 kHz word 
MASK_PIXEL: 431, 441 # entries 244 rate 244 kHz word 
MASK_PIXEL: 56, 507 # entries 106 rate 106 kHz word 
MASK_PIXEL: 438, 432 # entries 68 rate 68 kHz word 
MASK_PIXEL: 64, 467 # entries 65 rate 65 kHz word 
MASK_PIXEL: 468, 439 # entries 73 rate 73 kHz word 
MASK_PIXEL: 25, 427 # entries 58 rate 58 kHz word 
MASK_PIXEL: 0, 461 # entries 111 rate 111 kHz word 
MASK_PIXEL: 1, 444 # entries 55 rate 55 kHz word 
MASK_PIXEL: 390, 356 # entries 347 rate 34.7 kHz word 
MASK_PIXEL: 4, 493 # entries 222 rate 22.2 kHz word 
MASK_PIXEL: 352, 324 # entries 238 rate 23.8 kHz word 
MASK_PIXEL: 5, 334 # entries 171 rate 17.1 kHz word 
MASK_PIXEL: 21, 475 # entries 151 rate 15.1 kHz word 
MASK_PIXEL: 328, 507 # entries 77 rate 7.7 kHz word 
MASK_PIXEL: 471, 461 # entries 30 rate 3 kHz word 
MASK_PIXEL: 411, 505 # entries 23 rate 2.3 kHz word 
MASK_PIXEL: 375, 492 # entries 22 rate 2.2 kHz word 
MASK_PIXEL: 3, 387 # entries 16 rate 1.6 kHz word 
MASK_PIXEL: 429, 448 # entries 94 rate 0.94 kHz word 
MASK_PIXEL: 444, 433 # entries 82 rate 0.82 kHz word 
MASK_PIXEL: 254, 476 # entries 25 rate 0.25 kHz word 
MASK_PIXEL: 433, 471 # entries 44 rate 0.44 kHz word 
MASK_PIXEL: 14, 405 # entries 36 rate 0.36 kHz word 
MASK_PIXEL: 2, 422 # entries 57 rate 0.57 kHz word 
MASK_PIXEL: 344, 291 # entries 28 rate 0.28 kHz word 
MASK_PIXEL: 13, 467 # entries 95 rate 0.19 kHz word 
MASK_PIXEL: 323, 461 # entries 51 rate 0.102 kHz word 
MASK_PIXEL: 74, 367 # entries 22 rate 0.044 kHz word 
MASK_PIXEL: 504, 481 # entries 22 rate 0.044 kHz word 
MASK_PIXEL: 486, 296 # entries 22 rate 0.044 kHz word 
MASK_PIXEL: 401, 481 # entries 93 rate 35.4962 kHz word 
MASK_PIXEL: 27, 491 # entries 89 rate 3.39695 kHz word 
MASK_PIXEL: 44, 475 # entries 21 rate 0.042 kHz word 
MASK_PIXEL: 78, 330 # entries 99 rate 235.714 kHz word 
MASK_PIXEL: 22, 393 # entries 913 rate 9130 kHz word 
MASK_PIXEL: 170, 425 # entries 70 rate 4.7619 kHz word 
MASK_PIXEL: 93, 426 # entries 78 rate 0.530612 kHz word 
MASK_PIXEL: 30, 333 # entries 86 rate 0.593431 kHz word 
MASK_PIXEL: 497, 352 # entries 76 rate 0.524427 kHz word 
MASK_PIXEL: 29, 494 # entries 34 rate 0.068 kHz word 
MASK_PIXEL: 413, 403 # entries 100 rate 3.38983 kHz word 	 
MASK_PIXEL: 411, 288 # entries 47 rate 1.59322 kHz word 	 
MASK_PIXEL: 380, 288 # entries 36 rate 1.22034 kHz word 	 
MASK_PIXEL: 269, 288 # entries 35 rate 1.18644 kHz word 	 
MASK_PIXEL: 325, 288 # entries 41 rate 1.38983 kHz word 	 
MASK_PIXEL: 71, 449 # entries 30 rate 1.01695 kHz word 	 
MASK_PIXEL: 360, 288 # entries 25 rate 0.847458 kHz word 	 
MASK_PIXEL: 455, 288 # entries 24 rate 0.813559 kHz word 	 
MASK_PIXEL: 425, 288 # entries 20 rate 0.677966 kHz word 	 
MASK_PIXEL: 498, 288 # entries 24 rate 0.813559 kHz word 	 
MASK_PIXEL: 389, 511 # entries 16 rate 0.542373 kHz word 	 
MASK_PIXEL: 14, 306 # entries 17 rate 0.576271 kHz word 	 
MASK_PIXEL: 392, 464 # entries 14 rate 0.474576 kHz word 	 
MASK_PIXEL: 396, 505 # entries 13 rate 0.440678 kHz word 	 
MASK_PIXEL: 247, 468 # entries 16 rate 0.542373 kHz word 	 
MASK_PIXEL: 496, 288 # entries 9 rate 0.305085 kHz word 	 
MASK_PIXEL: 454, 288 # entries 110 rate 0.372881 kHz word 	 
MASK_PIXEL: 345, 288 # entries 79 rate 0.267797 kHz word 	 
MASK_PIXEL: 401, 385 # entries 84 rate 0.284746 kHz word 	 
MASK_PIXEL: 168, 470 # entries 71 rate 0.240678 kHz word 	 
MASK_PIXEL: 494, 288 # entries 82 rate 0.277966 kHz word 	 
MASK_PIXEL: 481, 511 # entries 71 rate 0.240678 kHz word 	 
MASK_PIXEL: 214, 345 # entries 63 rate 0.213559 kHz word 	 
MASK_PIXEL: 24, 288 # entries 66 rate 0.223729 kHz word 	 
MASK_PIXEL: 10, 465 # entries 68 rate 0.230508 kHz word 	 
MASK_PIXEL: 8, 483 # entries 61 rate 0.20678 kHz word 	 
MASK_PIXEL: 330, 339 # entries 65 rate 0.220339 kHz word 	 
MASK_PIXEL: 410, 291 # entries 46 rate 0.155932 kHz word 	 
MASK_PIXEL: 465, 319 # entries 49 rate 0.166102 kHz word 	 
