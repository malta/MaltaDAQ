type: MALTA2 
sample: W12R6 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 120 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R6/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 77, 450 # entries 355 rate 3550 kHz word 	 
MASK_PIXEL: 382, 420 # entries 399 rate 3990 kHz word 	 
MASK_PIXEL: 266, 380 # entries 406 rate 4060 kHz word 	 
MASK_PIXEL: 97, 319 # entries 377 rate 3770 kHz word 	 
MASK_PIXEL: 422, 293 # entries 457 rate 4570 kHz word 	 
MASK_PIXEL: 499, 301 # entries 455 rate 4550 kHz word 	 
MASK_PIXEL: 440, 296 # entries 507 rate 5070 kHz word 	 
MASK_PIXEL: 473, 289 # entries 469 rate 4690 kHz word 	 
MASK_PIXEL: 259, 300 # entries 521 rate 5210 kHz word 	 
MASK_PIXEL: 213, 288 # entries 542 rate 5420 kHz word 	 
MASK_PIXEL: 427, 340 # entries 524 rate 5240 kHz word 	 
MASK_PIXEL: 436, 297 # entries 603 rate 6030 kHz word 	 
MASK_PIXEL: 298, 349 # entries 673 rate 6730 kHz word 	 
MASK_PIXEL: 272, 353 # entries 538 rate 5380 kHz word 	 
MASK_PIXEL: 398, 295 # entries 579 rate 5790 kHz word 	 
MASK_PIXEL: 352, 321 # entries 565 rate 5650 kHz word 	 
MASK_PIXEL: 354, 319 # entries 740 rate 7400 kHz word 	 
MASK_PIXEL: 59, 417 # entries 603 rate 6030 kHz word 	 
MASK_PIXEL: 422, 386 # entries 588 rate 5880 kHz word 	 
MASK_PIXEL: 133, 318 # entries 626 rate 6260 kHz word 	 
MASK_PIXEL: 147, 298 # entries 579 rate 5790 kHz word 	 
MASK_PIXEL: 127, 381 # entries 784 rate 7840 kHz word 	 
MASK_PIXEL: 440, 360 # entries 554 rate 5540 kHz word 	 
MASK_PIXEL: 146, 291 # entries 516 rate 5160 kHz word 	 
MASK_PIXEL: 415, 460 # entries 529 rate 5290 kHz word 	 
MASK_PIXEL: 452, 355 # entries 542 rate 5420 kHz word 	 
MASK_PIXEL: 423, 352 # entries 454 rate 4540 kHz word 	 
MASK_PIXEL: 263, 290 # entries 532 rate 5320 kHz word 	 
MASK_PIXEL: 226, 320 # entries 732 rate 7320 kHz word 	 
MASK_PIXEL: 503, 289 # entries 508 rate 5080 kHz word 	 
MASK_PIXEL: 152, 301 # entries 558 rate 5580 kHz word 	 
MASK_PIXEL: 432, 447 # entries 488 rate 4880 kHz word 	 
MASK_PIXEL: 451, 335 # entries 407 rate 4070 kHz word 	 
MASK_PIXEL: 360, 474 # entries 460 rate 4600 kHz word 	 
MASK_PIXEL: 131, 346 # entries 887 rate 8870 kHz word 	 
MASK_PIXEL: 506, 303 # entries 358 rate 3580 kHz word 	 
MASK_PIXEL: 305, 297 # entries 514 rate 5140 kHz word 	 
MASK_PIXEL: 49, 324 # entries 431 rate 4310 kHz word 	 
MASK_PIXEL: 43, 345 # entries 393 rate 3930 kHz word 	 
MASK_PIXEL: 420, 342 # entries 408 rate 4080 kHz word 	 
MASK_PIXEL: 156, 381 # entries 311 rate 3110 kHz word 	 
MASK_PIXEL: 63, 288 # entries 616 rate 6160 kHz word 	 
MASK_PIXEL: 70, 295 # entries 512 rate 5120 kHz word 	 
MASK_PIXEL: 20, 329 # entries 319 rate 3190 kHz word 	 
MASK_PIXEL: 333, 291 # entries 300 rate 3000 kHz word 	 
MASK_PIXEL: 375, 379 # entries 315 rate 3150 kHz word 	 
MASK_PIXEL: 444, 344 # entries 372 rate 3720 kHz word 	 
MASK_PIXEL: 134, 294 # entries 246 rate 2460 kHz word 	 
MASK_PIXEL: 395, 368 # entries 232 rate 2320 kHz word 	 
MASK_PIXEL: 429, 333 # entries 239 rate 2390 kHz word 	 
MASK_PIXEL: 417, 385 # entries 350 rate 3500 kHz word 	 
MASK_PIXEL: 148, 356 # entries 247 rate 2470 kHz word 	 
MASK_PIXEL: 400, 297 # entries 345 rate 3450 kHz word 	 
MASK_PIXEL: 148, 327 # entries 292 rate 2920 kHz word 	 
MASK_PIXEL: 418, 297 # entries 999 rate 9990 kHz word 	 
MASK_PIXEL: 422, 424 # entries 145 rate 1450 kHz word 	 
MASK_PIXEL: 499, 389 # entries 221 rate 2210 kHz word 	 
MASK_PIXEL: 43, 294 # entries 87 rate 870 kHz word 	 
MASK_PIXEL: 430, 419 # entries 61 rate 610 kHz word 	 
MASK_PIXEL: 336, 355 # entries 55 rate 550 kHz word 	 
MASK_PIXEL: 276, 331 # entries 102 rate 1020 kHz word 	 
MASK_PIXEL: 155, 439 # entries 37 rate 370 kHz word 	 
MASK_PIXEL: 143, 307 # entries 35 rate 350 kHz word 	 
MASK_PIXEL: 473, 349 # entries 79 rate 790 kHz word 	 
MASK_PIXEL: 142, 322 # entries 85 rate 850 kHz word 	 
MASK_PIXEL: 156, 309 # entries 31 rate 310 kHz word 	 
MASK_PIXEL: 357, 351 # entries 14 rate 140 kHz word 	 
MASK_PIXEL: 403, 298 # entries 18 rate 180 kHz word 	 
MASK_PIXEL: 482, 329 # entries 46 rate 460 kHz word 	 
MASK_PIXEL: 442, 410 # entries 146 rate 146 kHz word 	 
MASK_PIXEL: 442, 378 # entries 134 rate 134 kHz word 	 
MASK_PIXEL: 499, 308 # entries 67 rate 67 kHz word 	 
MASK_PIXEL: 446, 351 # entries 38 rate 38 kHz word 	 
MASK_PIXEL: 249, 339 # entries 37 rate 37 kHz word 	 
MASK_PIXEL: 57, 339 # entries 123 rate 123 kHz word 	 
MASK_PIXEL: 448, 306 # entries 33 rate 33 kHz word 	 
MASK_PIXEL: 418, 383 # entries 16 rate 16 kHz word 	 
MASK_PIXEL: 260, 345 # entries 33 rate 33 kHz word 	 
MASK_PIXEL: 402, 318 # entries 15 rate 15 kHz word 	 
MASK_PIXEL: 435, 399 # entries 92 rate 9.2 kHz word 	 
MASK_PIXEL: 435, 328 # entries 289 rate 28.9 kHz word 	 
MASK_PIXEL: 501, 292 # entries 159 rate 15.9 kHz word 	 
MASK_PIXEL: 457, 303 # entries 192 rate 19.2 kHz word 	 
MASK_PIXEL: 472, 461 # entries 65 rate 6.5 kHz word 	 
MASK_PIXEL: 419, 321 # entries 65 rate 6.5 kHz word 	 
MASK_PIXEL: 282, 305 # entries 108 rate 10.8 kHz word 	 
MASK_PIXEL: 507, 301 # entries 50 rate 5 kHz word 	 
MASK_PIXEL: 85, 463 # entries 71 rate 7.1 kHz word 	 
MASK_PIXEL: 245, 309 # entries 45 rate 4.5 kHz word 	 
MASK_PIXEL: 284, 289 # entries 38 rate 3.8 kHz word 	 
MASK_PIXEL: 339, 300 # entries 52 rate 5.2 kHz word 	 
MASK_PIXEL: 116, 502 # entries 66 rate 6.6 kHz word 	 
MASK_PIXEL: 151, 407 # entries 58 rate 5.8 kHz word 	 
MASK_PIXEL: 255, 372 # entries 37 rate 3.7 kHz word 	 
MASK_PIXEL: 465, 417 # entries 8 rate 0.8 kHz word 	 
MASK_PIXEL: 441, 470 # entries 170 rate 1.7 kHz word 	 
MASK_PIXEL: 364, 444 # entries 107 rate 1.07 kHz word 	 
MASK_PIXEL: 43, 299 # entries 76 rate 0.76 kHz word 	 
MASK_PIXEL: 458, 345 # entries 60 rate 0.6 kHz word 	 
MASK_PIXEL: 83, 397 # entries 78 rate 0.78 kHz word 	 
MASK_PIXEL: 239, 355 # entries 68 rate 0.68 kHz word 	 
