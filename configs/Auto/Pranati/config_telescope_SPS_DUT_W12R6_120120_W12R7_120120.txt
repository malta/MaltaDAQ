include: configs/DUT/TelescopePlanes.txt
[PLANE7]
address: udp://ep-ade-gw-02:50007 
type: MALTA2 
sample: W12R6 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 120 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R6/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 77, 450 # entries 355 rate 3550 kHz word 	 
MASK_PIXEL: 382, 420 # entries 399 rate 3990 kHz word 	 
MASK_PIXEL: 266, 380 # entries 406 rate 4060 kHz word 	 
MASK_PIXEL: 97, 319 # entries 377 rate 3770 kHz word 	 
MASK_PIXEL: 422, 293 # entries 457 rate 4570 kHz word 	 
MASK_PIXEL: 499, 301 # entries 455 rate 4550 kHz word 	 
MASK_PIXEL: 440, 296 # entries 507 rate 5070 kHz word 	 
MASK_PIXEL: 473, 289 # entries 469 rate 4690 kHz word 	 
MASK_PIXEL: 259, 300 # entries 521 rate 5210 kHz word 	 
MASK_PIXEL: 213, 288 # entries 542 rate 5420 kHz word 	 
MASK_PIXEL: 427, 340 # entries 524 rate 5240 kHz word 	 
MASK_PIXEL: 436, 297 # entries 603 rate 6030 kHz word 	 
MASK_PIXEL: 298, 349 # entries 673 rate 6730 kHz word 	 
MASK_PIXEL: 272, 353 # entries 538 rate 5380 kHz word 	 
MASK_PIXEL: 398, 295 # entries 579 rate 5790 kHz word 	 
MASK_PIXEL: 352, 321 # entries 565 rate 5650 kHz word 	 
MASK_PIXEL: 354, 319 # entries 740 rate 7400 kHz word 	 
MASK_PIXEL: 59, 417 # entries 603 rate 6030 kHz word 	 
MASK_PIXEL: 422, 386 # entries 588 rate 5880 kHz word 	 
MASK_PIXEL: 133, 318 # entries 626 rate 6260 kHz word 	 
MASK_PIXEL: 147, 298 # entries 579 rate 5790 kHz word 	 
MASK_PIXEL: 127, 381 # entries 784 rate 7840 kHz word 	 
MASK_PIXEL: 440, 360 # entries 554 rate 5540 kHz word 	 
MASK_PIXEL: 146, 291 # entries 516 rate 5160 kHz word 	 
MASK_PIXEL: 415, 460 # entries 529 rate 5290 kHz word 	 
MASK_PIXEL: 452, 355 # entries 542 rate 5420 kHz word 	 
MASK_PIXEL: 423, 352 # entries 454 rate 4540 kHz word 	 
MASK_PIXEL: 263, 290 # entries 532 rate 5320 kHz word 	 
MASK_PIXEL: 226, 320 # entries 732 rate 7320 kHz word 	 
MASK_PIXEL: 503, 289 # entries 508 rate 5080 kHz word 	 
MASK_PIXEL: 152, 301 # entries 558 rate 5580 kHz word 	 
MASK_PIXEL: 432, 447 # entries 488 rate 4880 kHz word 	 
MASK_PIXEL: 451, 335 # entries 407 rate 4070 kHz word 	 
MASK_PIXEL: 360, 474 # entries 460 rate 4600 kHz word 	 
MASK_PIXEL: 131, 346 # entries 887 rate 8870 kHz word 	 
MASK_PIXEL: 506, 303 # entries 358 rate 3580 kHz word 	 
MASK_PIXEL: 305, 297 # entries 514 rate 5140 kHz word 	 
MASK_PIXEL: 49, 324 # entries 431 rate 4310 kHz word 	 
MASK_PIXEL: 43, 345 # entries 393 rate 3930 kHz word 	 
MASK_PIXEL: 420, 342 # entries 408 rate 4080 kHz word 	 
MASK_PIXEL: 156, 381 # entries 311 rate 3110 kHz word 	 
MASK_PIXEL: 63, 288 # entries 616 rate 6160 kHz word 	 
MASK_PIXEL: 70, 295 # entries 512 rate 5120 kHz word 	 
MASK_PIXEL: 20, 329 # entries 319 rate 3190 kHz word 	 
MASK_PIXEL: 333, 291 # entries 300 rate 3000 kHz word 	 
MASK_PIXEL: 375, 379 # entries 315 rate 3150 kHz word 	 
MASK_PIXEL: 444, 344 # entries 372 rate 3720 kHz word 	 
MASK_PIXEL: 134, 294 # entries 246 rate 2460 kHz word 	 
MASK_PIXEL: 395, 368 # entries 232 rate 2320 kHz word 	 
MASK_PIXEL: 429, 333 # entries 239 rate 2390 kHz word 	 
MASK_PIXEL: 417, 385 # entries 350 rate 3500 kHz word 	 
MASK_PIXEL: 148, 356 # entries 247 rate 2470 kHz word 	 
MASK_PIXEL: 400, 297 # entries 345 rate 3450 kHz word 	 
MASK_PIXEL: 148, 327 # entries 292 rate 2920 kHz word 	 
MASK_PIXEL: 418, 297 # entries 999 rate 9990 kHz word 	 
MASK_PIXEL: 422, 424 # entries 145 rate 1450 kHz word 	 
MASK_PIXEL: 499, 389 # entries 221 rate 2210 kHz word 	 
MASK_PIXEL: 43, 294 # entries 87 rate 870 kHz word 	 
MASK_PIXEL: 430, 419 # entries 61 rate 610 kHz word 	 
MASK_PIXEL: 336, 355 # entries 55 rate 550 kHz word 	 
MASK_PIXEL: 276, 331 # entries 102 rate 1020 kHz word 	 
MASK_PIXEL: 155, 439 # entries 37 rate 370 kHz word 	 
MASK_PIXEL: 143, 307 # entries 35 rate 350 kHz word 	 
MASK_PIXEL: 473, 349 # entries 79 rate 790 kHz word 	 
MASK_PIXEL: 142, 322 # entries 85 rate 850 kHz word 	 
MASK_PIXEL: 156, 309 # entries 31 rate 310 kHz word 	 
MASK_PIXEL: 357, 351 # entries 14 rate 140 kHz word 	 
MASK_PIXEL: 403, 298 # entries 18 rate 180 kHz word 	 
MASK_PIXEL: 482, 329 # entries 46 rate 460 kHz word 	 
MASK_PIXEL: 442, 410 # entries 146 rate 146 kHz word 	 
MASK_PIXEL: 442, 378 # entries 134 rate 134 kHz word 	 
MASK_PIXEL: 499, 308 # entries 67 rate 67 kHz word 	 
MASK_PIXEL: 446, 351 # entries 38 rate 38 kHz word 	 
MASK_PIXEL: 249, 339 # entries 37 rate 37 kHz word 	 
MASK_PIXEL: 57, 339 # entries 123 rate 123 kHz word 	 
MASK_PIXEL: 448, 306 # entries 33 rate 33 kHz word 	 
MASK_PIXEL: 418, 383 # entries 16 rate 16 kHz word 	 
MASK_PIXEL: 260, 345 # entries 33 rate 33 kHz word 	 
MASK_PIXEL: 402, 318 # entries 15 rate 15 kHz word 	 
MASK_PIXEL: 435, 399 # entries 92 rate 9.2 kHz word 	 
MASK_PIXEL: 435, 328 # entries 289 rate 28.9 kHz word 	 
MASK_PIXEL: 501, 292 # entries 159 rate 15.9 kHz word 	 
MASK_PIXEL: 457, 303 # entries 192 rate 19.2 kHz word 	 
MASK_PIXEL: 472, 461 # entries 65 rate 6.5 kHz word 	 
MASK_PIXEL: 419, 321 # entries 65 rate 6.5 kHz word 	 
MASK_PIXEL: 282, 305 # entries 108 rate 10.8 kHz word 	 
MASK_PIXEL: 507, 301 # entries 50 rate 5 kHz word 	 
MASK_PIXEL: 85, 463 # entries 71 rate 7.1 kHz word 	 
MASK_PIXEL: 245, 309 # entries 45 rate 4.5 kHz word 	 
MASK_PIXEL: 284, 289 # entries 38 rate 3.8 kHz word 	 
MASK_PIXEL: 339, 300 # entries 52 rate 5.2 kHz word 	 
MASK_PIXEL: 116, 502 # entries 66 rate 6.6 kHz word 	 
MASK_PIXEL: 151, 407 # entries 58 rate 5.8 kHz word 	 
MASK_PIXEL: 255, 372 # entries 37 rate 3.7 kHz word 	 
MASK_PIXEL: 465, 417 # entries 8 rate 0.8 kHz word 	 
MASK_PIXEL: 441, 470 # entries 170 rate 1.7 kHz word 	 
MASK_PIXEL: 364, 444 # entries 107 rate 1.07 kHz word 	 
MASK_PIXEL: 43, 299 # entries 76 rate 0.76 kHz word 	 
MASK_PIXEL: 458, 345 # entries 60 rate 0.6 kHz word 	 
MASK_PIXEL: 83, 397 # entries 78 rate 0.78 kHz word 	 
MASK_PIXEL: 239, 355 # entries 68 rate 0.68 kHz word 	 
################################################################################################################################################
[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2 
sample: W12R7 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 120 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R7/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 100, 374 # entries 287 rate 2870 kHz word 	 
MASK_PIXEL: 94, 377 # entries 330 rate 3300 kHz word 	 
MASK_PIXEL: 118, 365 # entries 350 rate 3500 kHz word 	 
MASK_PIXEL: 26, 289 # entries 417 rate 4170 kHz word 	 
MASK_PIXEL: 437, 296 # entries 348 rate 3480 kHz word 	 
MASK_PIXEL: 340, 503 # entries 388 rate 3880 kHz word 	 
MASK_PIXEL: 13, 315 # entries 353 rate 3530 kHz word 	 
MASK_PIXEL: 39, 440 # entries 446 rate 4460 kHz word 	 
MASK_PIXEL: 479, 323 # entries 380 rate 3800 kHz word 	 
MASK_PIXEL: 458, 366 # entries 437 rate 4370 kHz word 	 
MASK_PIXEL: 4, 293 # entries 459 rate 4590 kHz word 	 
MASK_PIXEL: 46, 357 # entries 506 rate 5060 kHz word 	 
MASK_PIXEL: 110, 325 # entries 414 rate 4140 kHz word 	 
MASK_PIXEL: 438, 345 # entries 471 rate 4710 kHz word 	 
MASK_PIXEL: 111, 294 # entries 484 rate 4840 kHz word 	 
MASK_PIXEL: 39, 395 # entries 549 rate 5490 kHz word 	 
MASK_PIXEL: 429, 349 # entries 428 rate 4280 kHz word 	 
MASK_PIXEL: 90, 365 # entries 565 rate 5650 kHz word 	 
MASK_PIXEL: 97, 308 # entries 490 rate 4900 kHz word 	 
MASK_PIXEL: 29, 289 # entries 522 rate 5220 kHz word 	 
MASK_PIXEL: 47, 307 # entries 696 rate 6960 kHz word 	 
MASK_PIXEL: 493, 346 # entries 579 rate 5790 kHz word 	 
MASK_PIXEL: 67, 320 # entries 562 rate 5620 kHz word 	 
MASK_PIXEL: 92, 349 # entries 674 rate 6740 kHz word 	 
MASK_PIXEL: 5, 317 # entries 785 rate 7850 kHz word 	 
MASK_PIXEL: 54, 339 # entries 654 rate 6540 kHz word 	 
MASK_PIXEL: 483, 365 # entries 644 rate 6440 kHz word 	 
MASK_PIXEL: 10, 354 # entries 509 rate 5090 kHz word 	 
MASK_PIXEL: 8, 338 # entries 604 rate 6040 kHz word 	 
MASK_PIXEL: 506, 435 # entries 602 rate 6020 kHz word 	 
MASK_PIXEL: 72, 385 # entries 456 rate 4560 kHz word 	 
MASK_PIXEL: 121, 318 # entries 561 rate 5610 kHz word 	 
MASK_PIXEL: 3, 314 # entries 590 rate 5900 kHz word 	 
MASK_PIXEL: 214, 361 # entries 517 rate 5170 kHz word 	 
MASK_PIXEL: 5, 340 # entries 629 rate 6290 kHz word 	 
MASK_PIXEL: 482, 379 # entries 605 rate 6050 kHz word 	 
MASK_PIXEL: 110, 433 # entries 600 rate 6000 kHz word 	 
MASK_PIXEL: 489, 302 # entries 826 rate 8260 kHz word 	 
MASK_PIXEL: 95, 308 # entries 476 rate 4760 kHz word 	 
MASK_PIXEL: 5, 321 # entries 445 rate 4450 kHz word 	 
MASK_PIXEL: 106, 294 # entries 494 rate 4940 kHz word 	 
MASK_PIXEL: 110, 402 # entries 431 rate 4310 kHz word 	 
MASK_PIXEL: 26, 303 # entries 301 rate 3010 kHz word 	 
MASK_PIXEL: 5, 308 # entries 612 rate 6120 kHz word 	 
MASK_PIXEL: 437, 298 # entries 430 rate 4300 kHz word 	 
MASK_PIXEL: 91, 354 # entries 400 rate 4000 kHz word 	 
MASK_PIXEL: 88, 315 # entries 324 rate 3240 kHz word 	 
MASK_PIXEL: 102, 391 # entries 586 rate 5860 kHz word 	 
MASK_PIXEL: 98, 289 # entries 410 rate 4100 kHz word 	 
MASK_PIXEL: 20, 304 # entries 296 rate 2960 kHz word 	 
MASK_PIXEL: 70, 314 # entries 286 rate 2860 kHz word 	 
MASK_PIXEL: 111, 445 # entries 256 rate 2560 kHz word 	 
MASK_PIXEL: 85, 296 # entries 182 rate 1820 kHz word 	 
MASK_PIXEL: 99, 408 # entries 336 rate 3360 kHz word 	 
MASK_PIXEL: 85, 312 # entries 487 rate 4870 kHz word 	 
MASK_PIXEL: 499, 361 # entries 405 rate 4050 kHz word 	 
MASK_PIXEL: 491, 296 # entries 259 rate 2590 kHz word 	 
MASK_PIXEL: 16, 360 # entries 252 rate 2520 kHz word 	 
MASK_PIXEL: 31, 313 # entries 210 rate 2100 kHz word 	 
MASK_PIXEL: 21, 303 # entries 413 rate 4130 kHz word 	 
MASK_PIXEL: 486, 342 # entries 381 rate 3810 kHz word 	 
MASK_PIXEL: 498, 354 # entries 397 rate 3970 kHz word 	 
MASK_PIXEL: 94, 452 # entries 216 rate 2160 kHz word 	 
MASK_PIXEL: 173, 311 # entries 236 rate 2360 kHz word 	 
MASK_PIXEL: 94, 389 # entries 275 rate 2750 kHz word 	 
MASK_PIXEL: 107, 304 # entries 281 rate 2810 kHz word 	 
MASK_PIXEL: 41, 388 # entries 251 rate 2510 kHz word 	 
MASK_PIXEL: 97, 375 # entries 181 rate 1810 kHz word 	 
MASK_PIXEL: 10, 384 # entries 99 rate 990 kHz word 	 
MASK_PIXEL: 116, 343 # entries 138 rate 1380 kHz word 	 
MASK_PIXEL: 504, 305 # entries 228 rate 2280 kHz word 	 
MASK_PIXEL: 459, 371 # entries 158 rate 1580 kHz word 	 
MASK_PIXEL: 327, 504 # entries 186 rate 1860 kHz word 	 
MASK_PIXEL: 37, 326 # entries 146 rate 1460 kHz word 	 
MASK_PIXEL: 61, 333 # entries 107 rate 1070 kHz word 	 
MASK_PIXEL: 413, 307 # entries 126 rate 1260 kHz word 	 
MASK_PIXEL: 10, 310 # entries 985 rate 9850 kHz word 	 
MASK_PIXEL: 76, 292 # entries 290 rate 2900 kHz word 	 
MASK_PIXEL: 509, 362 # entries 104 rate 1040 kHz word 	 
MASK_PIXEL: 123, 393 # entries 105 rate 1050 kHz word 	 
MASK_PIXEL: 396, 292 # entries 148 rate 1480 kHz word 	 
MASK_PIXEL: 6, 421 # entries 84 rate 840 kHz word 	 
MASK_PIXEL: 57, 415 # entries 167 rate 1670 kHz word 	 
MASK_PIXEL: 216, 325 # entries 44 rate 440 kHz word 	 
MASK_PIXEL: 105, 397 # entries 39 rate 390 kHz word 	 
MASK_PIXEL: 381, 319 # entries 82 rate 820 kHz word 	 
MASK_PIXEL: 507, 332 # entries 33 rate 330 kHz word 	 
MASK_PIXEL: 322, 476 # entries 36 rate 360 kHz word 	 
MASK_PIXEL: 2, 338 # entries 73 rate 730 kHz word 	 
MASK_PIXEL: 387, 314 # entries 44 rate 440 kHz word 	 
MASK_PIXEL: 29, 359 # entries 46 rate 460 kHz word 	 
MASK_PIXEL: 500, 432 # entries 31 rate 310 kHz word 	 
MASK_PIXEL: 505, 355 # entries 58 rate 580 kHz word 	 
MASK_PIXEL: 78, 412 # entries 73 rate 730 kHz word 	 
MASK_PIXEL: 0, 390 # entries 118 rate 1180 kHz word 	 
MASK_PIXEL: 55, 299 # entries 106 rate 1060 kHz word 	 
MASK_PIXEL: 71, 291 # entries 27 rate 270 kHz word 	 
MASK_PIXEL: 71, 303 # entries 65 rate 650 kHz word 	 
MASK_PIXEL: 85, 381 # entries 20 rate 200 kHz word 	 
MASK_PIXEL: 0, 299 # entries 63 rate 630 kHz word 	 
MASK_PIXEL: 8, 336 # entries 22 rate 220 kHz word 	 
