include: configs/DUT/TelescopePlanes.txt
[PLANE7]
address: udp://ep-ade-gw-02:50007 
type: MALTA2 
sample: W12R6 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 30 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R6/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 416, 345 # entries 1228 rate 12280 kHz word 	 
MASK_PIXEL: 10, 346 # entries 38 rate 380 kHz word 	 
MASK_PIXEL: 417, 290 # entries 100 rate 100 kHz word 	 
MASK_PIXEL: 19, 420 # entries 83 rate 83 kHz word 	 
MASK_PIXEL: 1, 323 # entries 53 rate 53 kHz word 	 
MASK_PIXEL: 436, 402 # entries 129 rate 12.9 kHz word 	 
MASK_PIXEL: 437, 360 # entries 64 rate 6.4 kHz word 	 
MASK_PIXEL: 365, 398 # entries 38 rate 3.8 kHz word 	 
MASK_PIXEL: 430, 314 # entries 24 rate 2.4 kHz word 	 
MASK_PIXEL: 453, 359 # entries 22 rate 2.2 kHz word 	 
MASK_PIXEL: 273, 317 # entries 16 rate 1.6 kHz word 	 
MASK_PIXEL: 44, 361 # entries 165 rate 1.65 kHz word 	 
MASK_PIXEL: 430, 291 # entries 94 rate 0.94 kHz word 	 
MASK_PIXEL: 213, 288 # entries 81 rate 0.81 kHz word 	 
MASK_PIXEL: 490, 482 # entries 90 rate 0.9 kHz word 	 
MASK_PIXEL: 449, 459 # entries 73 rate 0.73 kHz word 	 
MASK_PIXEL: 109, 334 # entries 50 rate 0.5 kHz word 	 
MASK_PIXEL: 473, 289 # entries 61 rate 0.61 kHz word 	 
MASK_PIXEL: 430, 397 # entries 49 rate 0.49 kHz word 	 
MASK_PIXEL: 153, 293 # entries 32 rate 0.32 kHz word 	 
MASK_PIXEL: 284, 289 # entries 25 rate 0.25 kHz word 	 
MASK_PIXEL: 469, 289 # entries 11081 rate 110.81 kHz word 	 
MASK_PIXEL: 473, 368 # entries 100 rate 0.2 kHz word 	 
MASK_PIXEL: 453, 289 # entries 617 rate 1.234 kHz word 	 
MASK_PIXEL: 453, 288 # entries 117 rate 0.234 kHz word 	 
MASK_PIXEL: 444, 329 # entries 102 rate 0.204 kHz word 	 
MASK_PIXEL: 226, 320 # entries 77 rate 0.154 kHz word 	 
MASK_PIXEL: 79, 411 # entries 115 rate 0.23 kHz word 	 
MASK_PIXEL: 239, 355 # entries 67 rate 0.134 kHz word 	 
MASK_PIXEL: 135, 440 # entries 50 rate 0.1 kHz word 	 
MASK_PIXEL: 63, 288 # entries 48 rate 0.096 kHz word 	 
MASK_PIXEL: 417, 381 # entries 40 rate 0.08 kHz word 	 
MASK_PIXEL: 436, 364 # entries 36 rate 0.072 kHz word 	 
MASK_PIXEL: 57, 339 # entries 27 rate 0.054 kHz word 	 
################################################################################################################################################
[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2 
sample: W12R7 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 30 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R7/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 319, 293 # entries 83 rate 377.273 kHz word 	 
MASK_PIXEL: 60, 362 # entries 54 rate 245.455 kHz word 	 
MASK_PIXEL: 503, 289 # entries 18 rate 81.8182 kHz word 	 
MASK_PIXEL: 490, 288 # entries 296 rate 134.545 kHz word 	 
MASK_PIXEL: 82, 346 # entries 102 rate 46.3636 kHz word 	 
MASK_PIXEL: 485, 435 # entries 70 rate 31.8182 kHz word 	 
MASK_PIXEL: 506, 288 # entries 33 rate 15 kHz word 	 
MASK_PIXEL: 489, 330 # entries 36 rate 16.3636 kHz word 	 
MASK_PIXEL: 45, 297 # entries 15 rate 6.81818 kHz word 	 
MASK_PIXEL: 377, 327 # entries 12 rate 5.45455 kHz word 	 
MASK_PIXEL: 107, 369 # entries 155 rate 7.04545 kHz word 	 
MASK_PIXEL: 84, 294 # entries 171 rate 7.77273 kHz word 	 
MASK_PIXEL: 72, 288 # entries 134 rate 6.09091 kHz word 	 
MASK_PIXEL: 29, 289 # entries 76 rate 3.45455 kHz word 	 
MASK_PIXEL: 125, 434 # entries 50 rate 2.27273 kHz word 	 
MASK_PIXEL: 198, 360 # entries 52 rate 2.36364 kHz word 	 
MASK_PIXEL: 94, 459 # entries 41 rate 1.86364 kHz word 	 
MASK_PIXEL: 17, 303 # entries 17 rate 0.772727 kHz word 	 
MASK_PIXEL: 106, 456 # entries 12 rate 0.545455 kHz word 	 
MASK_PIXEL: 116, 391 # entries 11 rate 0.5 kHz word 	 
MASK_PIXEL: 115, 288 # entries 27 rate 1.22727 kHz word 	 
MASK_PIXEL: 9, 345 # entries 42 rate 0.190909 kHz word 	 
MASK_PIXEL: 63, 305 # entries 34 rate 0.154545 kHz word 	 
MASK_PIXEL: 115, 289 # entries 41 rate 0.186364 kHz word 	 
MASK_PIXEL: 470, 317 # entries 33 rate 0.15 kHz word 	 
MASK_PIXEL: 500, 434 # entries 39 rate 0.177273 kHz word 	 
MASK_PIXEL: 30, 295 # entries 29 rate 0.131818 kHz word 	 
MASK_PIXEL: 77, 374 # entries 27 rate 0.122727 kHz word 	 
MASK_PIXEL: 332, 293 # entries 22 rate 0.1 kHz word 	 
MASK_PIXEL: 79, 397 # entries 21 rate 0.0954545 kHz word 	 
MASK_PIXEL: 463, 384 # entries 19 rate 0.0863636 kHz word 	 
MASK_PIXEL: 20, 307 # entries 12 rate 0.0545455 kHz word 	 
MASK_PIXEL: 509, 289 # entries 36 rate 0.072 kHz word 	 
MASK_PIXEL: 501, 481 # entries 26 rate 0.052 kHz word 	 
