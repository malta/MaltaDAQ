type: MALTA2 
sample: W12R6 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 60 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R6/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 1, 323 # entries 1385 rate 13850 kHz word 	 
MASK_PIXEL: 365, 398 # entries 1433 rate 14330 kHz word 	 
MASK_PIXEL: 19, 420 # entries 1458 rate 14580 kHz word 	 
MASK_PIXEL: 437, 360 # entries 1517 rate 15170 kHz word 	 
MASK_PIXEL: 273, 317 # entries 269 rate 2690 kHz word 	 
MASK_PIXEL: 10, 346 # entries 85 rate 850 kHz word 	 
MASK_PIXEL: 430, 314 # entries 67 rate 670 kHz word 	 
MASK_PIXEL: 436, 402 # entries 25 rate 250 kHz word 	 
MASK_PIXEL: 44, 361 # entries 12 rate 120 kHz word 	 
MASK_PIXEL: 430, 291 # entries 13 rate 130 kHz word 	 
MASK_PIXEL: 109, 334 # entries 97 rate 97 kHz word 	 
MASK_PIXEL: 417, 381 # entries 117 rate 117 kHz word 	 
MASK_PIXEL: 135, 440 # entries 27 rate 27 kHz word 	 
MASK_PIXEL: 239, 355 # entries 199 rate 19.9 kHz word 	 
MASK_PIXEL: 473, 368 # entries 58 rate 5.8 kHz word 	 
MASK_PIXEL: 436, 364 # entries 20 rate 2 kHz word 	 
MASK_PIXEL: 138, 330 # entries 115 rate 1.15 kHz word 	 
MASK_PIXEL: 451, 414 # entries 106 rate 1.06 kHz word 	 
MASK_PIXEL: 57, 339 # entries 64 rate 0.64 kHz word 	 
MASK_PIXEL: 63, 288 # entries 46 rate 0.46 kHz word 	 
MASK_PIXEL: 492, 296 # entries 49 rate 0.49 kHz word 	 
MASK_PIXEL: 425, 341 # entries 15 rate 0.15 kHz word 	 
MASK_PIXEL: 366, 359 # entries 10 rate 0.1 kHz word 	 
MASK_PIXEL: 213, 288 # entries 62 rate 0.124 kHz word 	 
MASK_PIXEL: 466, 307 # entries 55 rate 0.11 kHz word 	 
MASK_PIXEL: 448, 306 # entries 134 rate 0.268 kHz word 	 
MASK_PIXEL: 148, 327 # entries 46 rate 0.092 kHz word 	 
MASK_PIXEL: 473, 289 # entries 41 rate 0.082 kHz word 	 
MASK_PIXEL: 226, 320 # entries 31 rate 0.062 kHz word 	 
