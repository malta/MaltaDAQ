include: configs/DUT/TelescopePlanes.txt
[PLANE7]
address: udp://ep-ade-gw-02:50007 
type: MALTA2 
sample: W12R6 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 80 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R6/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 109, 334 # entries 1150 rate 11500 kHz word 	 
MASK_PIXEL: 417, 381 # entries 1192 rate 11920 kHz word 	 
MASK_PIXEL: 135, 440 # entries 1160 rate 11600 kHz word 	 
MASK_PIXEL: 436, 402 # entries 1130 rate 11300 kHz word 	 
MASK_PIXEL: 138, 330 # entries 1183 rate 11830 kHz word 	 
MASK_PIXEL: 430, 291 # entries 720 rate 7200 kHz word 	 
MASK_PIXEL: 430, 314 # entries 829 rate 8290 kHz word 	 
MASK_PIXEL: 44, 361 # entries 487 rate 4870 kHz word 	 
MASK_PIXEL: 239, 355 # entries 306 rate 3060 kHz word 	 
MASK_PIXEL: 436, 364 # entries 266 rate 2660 kHz word 	 
MASK_PIXEL: 473, 368 # entries 188 rate 1880 kHz word 	 
MASK_PIXEL: 451, 414 # entries 317 rate 3170 kHz word 	 
MASK_PIXEL: 492, 296 # entries 225 rate 2250 kHz word 	 
MASK_PIXEL: 273, 317 # entries 161 rate 1610 kHz word 	 
MASK_PIXEL: 448, 306 # entries 42 rate 420 kHz word 	 
MASK_PIXEL: 425, 341 # entries 31 rate 310 kHz word 	 
MASK_PIXEL: 466, 307 # entries 313 rate 313 kHz word 	 
MASK_PIXEL: 430, 339 # entries 194 rate 194 kHz word 	 
MASK_PIXEL: 366, 359 # entries 61 rate 61 kHz word 	 
MASK_PIXEL: 43, 299 # entries 40 rate 40 kHz word 	 
MASK_PIXEL: 57, 339 # entries 30 rate 30 kHz word 	 
MASK_PIXEL: 63, 288 # entries 17 rate 17 kHz word 	 
MASK_PIXEL: 425, 363 # entries 12 rate 12 kHz word 	 
MASK_PIXEL: 148, 327 # entries 195 rate 19.5 kHz word 	 
MASK_PIXEL: 402, 318 # entries 117 rate 11.7 kHz word 	 
MASK_PIXEL: 276, 331 # entries 103 rate 10.3 kHz word 	 
MASK_PIXEL: 446, 351 # entries 69 rate 6.9 kHz word 	 
MASK_PIXEL: 387, 364 # entries 115 rate 11.5 kHz word 	 
MASK_PIXEL: 226, 320 # entries 70 rate 7 kHz word 	 
MASK_PIXEL: 43, 294 # entries 30 rate 3 kHz word 	 
MASK_PIXEL: 418, 381 # entries 36 rate 3.6 kHz word 	 
MASK_PIXEL: 1, 323 # entries 33 rate 3.3 kHz word 	 
MASK_PIXEL: 70, 295 # entries 28 rate 2.8 kHz word 	 
MASK_PIXEL: 43, 345 # entries 32 rate 3.2 kHz word 	 
MASK_PIXEL: 442, 378 # entries 18 rate 1.8 kHz word 	 
MASK_PIXEL: 20, 329 # entries 10 rate 1 kHz word 	 
MASK_PIXEL: 213, 288 # entries 10 rate 1 kHz word 	 
MASK_PIXEL: 473, 289 # entries 141 rate 1.41 kHz word 	 
MASK_PIXEL: 375, 379 # entries 85 rate 0.85 kHz word 	 
MASK_PIXEL: 365, 398 # entries 38 rate 0.38 kHz word 	 
MASK_PIXEL: 473, 349 # entries 42 rate 0.42 kHz word 	 
MASK_PIXEL: 491, 366 # entries 16 rate 0.16 kHz word 	 
MASK_PIXEL: 420, 342 # entries 19 rate 0.19 kHz word 	 
MASK_PIXEL: 333, 291 # entries 15 rate 0.15 kHz word 	 
MASK_PIXEL: 482, 329 # entries 27 rate 0.27 kHz word 	 
MASK_PIXEL: 260, 345 # entries 72 rate 0.144 kHz word 	 
MASK_PIXEL: 503, 289 # entries 56 rate 0.112 kHz word 	 
MASK_PIXEL: 134, 294 # entries 59 rate 0.118 kHz word 	 
MASK_PIXEL: 501, 292 # entries 47 rate 0.094 kHz word 	 
MASK_PIXEL: 133, 318 # entries 30 rate 0.06 kHz word 	 
MASK_PIXEL: 97, 319 # entries 29 rate 0.058 kHz word 	 
MASK_PIXEL: 435, 328 # entries 16 rate 0.032 kHz word 	 
################################################################################################################################################
[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2 
sample: W12R7 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 80 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R7/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 17, 303 # entries 1029 rate 10290 kHz word 	 
MASK_PIXEL: 490, 288 # entries 1107 rate 11070 kHz word 	 
MASK_PIXEL: 79, 397 # entries 934 rate 9340 kHz word 	 
MASK_PIXEL: 482, 412 # entries 874 rate 8740 kHz word 	 
MASK_PIXEL: 86, 357 # entries 847 rate 8470 kHz word 	 
MASK_PIXEL: 125, 434 # entries 858 rate 8580 kHz word 	 
MASK_PIXEL: 463, 384 # entries 684 rate 6840 kHz word 	 
MASK_PIXEL: 102, 381 # entries 401 rate 4010 kHz word 	 
MASK_PIXEL: 13, 357 # entries 380 rate 3800 kHz word 	 
MASK_PIXEL: 77, 374 # entries 390 rate 3900 kHz word 	 
MASK_PIXEL: 63, 305 # entries 412 rate 4120 kHz word 	 
MASK_PIXEL: 9, 345 # entries 329 rate 3290 kHz word 	 
MASK_PIXEL: 72, 288 # entries 389 rate 3890 kHz word 	 
MASK_PIXEL: 332, 293 # entries 768 rate 7680 kHz word 	 
MASK_PIXEL: 70, 307 # entries 303 rate 3030 kHz word 	 
MASK_PIXEL: 198, 360 # entries 221 rate 2210 kHz word 	 
MASK_PIXEL: 509, 289 # entries 310 rate 3100 kHz word 	 
MASK_PIXEL: 84, 294 # entries 279 rate 2790 kHz word 	 
MASK_PIXEL: 69, 296 # entries 183 rate 1830 kHz word 	 
MASK_PIXEL: 485, 435 # entries 218 rate 2180 kHz word 	 
MASK_PIXEL: 22, 324 # entries 143 rate 1430 kHz word 	 
MASK_PIXEL: 116, 391 # entries 55 rate 550 kHz word 	 
MASK_PIXEL: 506, 288 # entries 71 rate 710 kHz word 	 
MASK_PIXEL: 100, 360 # entries 68 rate 680 kHz word 	 
MASK_PIXEL: 106, 456 # entries 54 rate 540 kHz word 	 
MASK_PIXEL: 109, 301 # entries 66 rate 660 kHz word 	 
MASK_PIXEL: 107, 369 # entries 166 rate 1660 kHz word 	 
MASK_PIXEL: 66, 382 # entries 51 rate 510 kHz word 	 
MASK_PIXEL: 110, 446 # entries 35 rate 350 kHz word 	 
MASK_PIXEL: 98, 330 # entries 43 rate 430 kHz word 	 
MASK_PIXEL: 20, 307 # entries 53 rate 530 kHz word 	 
MASK_PIXEL: 489, 330 # entries 27 rate 270 kHz word 	 
MASK_PIXEL: 443, 359 # entries 31 rate 310 kHz word 	 
MASK_PIXEL: 485, 344 # entries 31 rate 310 kHz word 	 
MASK_PIXEL: 461, 329 # entries 13 rate 130 kHz word 	 
MASK_PIXEL: 217, 445 # entries 37 rate 370 kHz word 	 
MASK_PIXEL: 501, 481 # entries 12 rate 120 kHz word 	 
MASK_PIXEL: 10, 384 # entries 148 rate 148 kHz word 	 
MASK_PIXEL: 85, 309 # entries 80 rate 80 kHz word 	 
MASK_PIXEL: 503, 289 # entries 38 rate 38 kHz word 	 
MASK_PIXEL: 496, 440 # entries 40 rate 40 kHz word 	 
MASK_PIXEL: 12, 355 # entries 43 rate 43 kHz word 	 
MASK_PIXEL: 106, 377 # entries 25 rate 25 kHz word 	 
MASK_PIXEL: 4, 393 # entries 18 rate 18 kHz word 	 
MASK_PIXEL: 89, 330 # entries 15 rate 15 kHz word 	 
MASK_PIXEL: 347, 309 # entries 133 rate 13.3 kHz word 	 
MASK_PIXEL: 45, 297 # entries 89 rate 8.9 kHz word 	 
MASK_PIXEL: 5, 308 # entries 86 rate 8.6 kHz word 	 
MASK_PIXEL: 387, 314 # entries 57 rate 5.7 kHz word 	 
MASK_PIXEL: 107, 304 # entries 45 rate 4.5 kHz word 	 
MASK_PIXEL: 85, 296 # entries 62 rate 6.2 kHz word 	 
MASK_PIXEL: 507, 290 # entries 40 rate 4 kHz word 	 
MASK_PIXEL: 121, 409 # entries 11 rate 1.1 kHz word 	 
MASK_PIXEL: 484, 305 # entries 25 rate 2.5 kHz word 	 
MASK_PIXEL: 61, 301 # entries 110 rate 1.1 kHz word 	 
MASK_PIXEL: 0, 299 # entries 1706 rate 17.06 kHz word 	 
MASK_PIXEL: 503, 345 # entries 61 rate 0.61 kHz word 	 
MASK_PIXEL: 82, 346 # entries 48 rate 0.48 kHz word 	 
MASK_PIXEL: 325, 296 # entries 74 rate 0.74 kHz word 	 
MASK_PIXEL: 79, 371 # entries 61 rate 0.61 kHz word 	 
MASK_PIXEL: 29, 289 # entries 54 rate 0.54 kHz word 	 
MASK_PIXEL: 188, 300 # entries 48 rate 0.48 kHz word 	 
MASK_PIXEL: 78, 386 # entries 37 rate 0.37 kHz word 	 
MASK_PIXEL: 94, 389 # entries 24 rate 0.24 kHz word 	 
MASK_PIXEL: 90, 303 # entries 14 rate 0.14 kHz word 	 
MASK_PIXEL: 509, 362 # entries 97 rate 0.194 kHz word 	 
MASK_PIXEL: 6, 421 # entries 61 rate 0.122 kHz word 	 
MASK_PIXEL: 3, 314 # entries 34 rate 0.068 kHz word 	 
MASK_PIXEL: 91, 354 # entries 24 rate 0.048 kHz word 	 
