type: MALTA2 
sample: W12R7 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 40 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R7/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 60, 362 # entries 338 rate 3380 kHz word 	 
MASK_PIXEL: 319, 293 # entries 226 rate 2260 kHz word 	 
MASK_PIXEL: 503, 289 # entries 60 rate 600 kHz word 	 
MASK_PIXEL: 490, 288 # entries 18 rate 180 kHz word 	 
MASK_PIXEL: 489, 330 # entries 130 rate 130 kHz word 	 
MASK_PIXEL: 45, 297 # entries 62 rate 62 kHz word 	 
MASK_PIXEL: 485, 435 # entries 54 rate 54 kHz word 	 
MASK_PIXEL: 84, 294 # entries 37 rate 37 kHz word 	 
MASK_PIXEL: 107, 369 # entries 28 rate 28 kHz word 	 
MASK_PIXEL: 82, 346 # entries 17 rate 17 kHz word 	 
MASK_PIXEL: 125, 434 # entries 18 rate 18 kHz word 	 
MASK_PIXEL: 94, 459 # entries 1 rate 1 kHz word 	 
MASK_PIXEL: 198, 360 # entries 81 rate 8.1 kHz word 	 
MASK_PIXEL: 72, 288 # entries 50 rate 5 kHz word 	 
MASK_PIXEL: 506, 288 # entries 40 rate 4 kHz word 	 
MASK_PIXEL: 17, 303 # entries 17 rate 1.7 kHz word 	 
MASK_PIXEL: 106, 456 # entries 166 rate 1.66 kHz word 	 
MASK_PIXEL: 116, 391 # entries 85 rate 0.85 kHz word 	 
MASK_PIXEL: 29, 289 # entries 72 rate 0.72 kHz word 	 
MASK_PIXEL: 20, 307 # entries 42 rate 0.42 kHz word 	 
MASK_PIXEL: 77, 374 # entries 39 rate 0.39 kHz word 	 
MASK_PIXEL: 63, 305 # entries 24 rate 0.24 kHz word 	 
MASK_PIXEL: 332, 293 # entries 10 rate 0.1 kHz word 	 
MASK_PIXEL: 9, 345 # entries 53 rate 0.106 kHz word 	 
MASK_PIXEL: 463, 384 # entries 41 rate 0.082 kHz word 	 
MASK_PIXEL: 470, 317 # entries 31 rate 0.062 kHz word 	 
MASK_PIXEL: 377, 327 # entries 30 rate 0.06 kHz word 	 
MASK_PIXEL: 109, 301 # entries 25 rate 0.05 kHz word 	 
