include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W5R23
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 60
SC_IDB: 20
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W5R23/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 130, 348 #entries   12187
MASK_PIXEL:  86, 344 #entries   28678
MASK_PIXEL: 397, 332 #entries   38456
MASK_PIXEL: 376, 361 #entries   63925
MASK_PIXEL: 458, 324 #entries  196336
MASK_PIXEL: 295, 503 #entries  259349
MASK_PIXEL: 483, 326 #entries  299691
MASK_PIXEL: 309, 299 #entries   10229
MASK_PIXEL: 176, 442 #entries   10363
MASK_PIXEL: 305, 355 #entries   10850
MASK_PIXEL: 146, 328 #entries   11199
MASK_PIXEL:  90, 377 #entries   11488
MASK_PIXEL:  53, 292 #entries   12234
MASK_PIXEL: 277, 455 #entries   12523
MASK_PIXEL: 150, 498 #entries   12701
MASK_PIXEL: 153, 355 #entries   13189
MASK_PIXEL: 401, 443 #entries   13627
MASK_PIXEL: 449, 310 #entries   14077
MASK_PIXEL: 310, 336 #entries   14365
MASK_PIXEL: 493, 450 #entries   14477
MASK_PIXEL: 413, 301 #entries   14551
MASK_PIXEL: 384, 401 #entries   14629
MASK_PIXEL:   2, 334 #entries   15263
MASK_PIXEL: 343, 378 #entries   15321
MASK_PIXEL: 306, 468 #entries   15861
MASK_PIXEL: 508, 452 #entries   15969
MASK_PIXEL: 170, 369 #entries   17083
MASK_PIXEL: 435, 382 #entries   17309
MASK_PIXEL: 421, 299 #entries   17373
MASK_PIXEL: 491, 448 #entries   17404
MASK_PIXEL: 463, 305 #entries   19885
MASK_PIXEL: 440, 414 #entries   20113



###########################################################################################################################

[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W11R0
SC_ICASN: 2
SC_IBIAS: 43
SC_ITHR: 40
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W11R0/longCable_doubleChip_calib_run//calib_15.txt
MASK_PIXEL: 496, 366 #entries  153025
MASK_DOUBLE_COLUMNS: 50,256
