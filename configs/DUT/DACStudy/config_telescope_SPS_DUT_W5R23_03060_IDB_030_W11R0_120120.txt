include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W5R23
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 60
SC_IDB: 30
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W5R23/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 130, 348 #entries   12187
MASK_PIXEL:  86, 344 #entries   28678
MASK_PIXEL: 397, 332 #entries   38456
MASK_PIXEL: 376, 361 #entries   63925
MASK_PIXEL: 458, 324 #entries  196336
MASK_PIXEL: 295, 503 #entries  259349
MASK_PIXEL: 483, 326 #entries  299691


###########################################################################################################################

[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W11R0
SC_ICASN: 2
SC_IBIAS: 43
SC_ITHR: 120
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W11R0/longCable_doubleChip_calib_run//calib_15.txt
MASK_PIXEL: 496, 366 #entries  153025
MASK_DOUBLE_COLUMNS: 50,256
