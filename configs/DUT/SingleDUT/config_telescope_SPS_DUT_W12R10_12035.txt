include: configs/DUT/TelescopePlanes.txt
[PLANE7]
address: udp://ep-ade-gw-02:50007 
type: MALTA2 
sample: W12R10 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 35 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R10_1/longCable_doubleChip_calib_run//calib_15.txt

MASK_DOUBLE_COLUMNS:0,190 
MASK_PIXEL: 502, 289 # entries 102 rate 1020 kHz word 	 
MASK_PIXEL: 500, 347 # entries 111 rate 1110 kHz word 	 
MASK_PIXEL: 509, 489 # entries 191 rate 191 kHz word 	 
MASK_PIXEL: 466, 292 # entries 103 rate 103 kHz word 	 
MASK_PIXEL: 484, 309 # entries 66 rate 66 kHz word 	 
MASK_PIXEL: 390, 377 # entries 41 rate 41 kHz word 	 
MASK_PIXEL: 501, 289 # entries 45 rate 45 kHz word 	 
MASK_PIXEL: 498, 441 # entries 172 rate 17.2 kHz word 	 
MASK_PIXEL: 511, 448 # entries 165 rate 16.5 kHz word 	 
MASK_PIXEL: 459, 353 # entries 48 rate 4.8 kHz word 	 
MASK_PIXEL: 495, 404 # entries 53 rate 5.3 kHz word 	 
MASK_PIXEL: 392, 345 # entries 43 rate 4.3 kHz word 	 
MASK_PIXEL: 483, 426 # entries 21 rate 2.1 kHz word 	 
MASK_PIXEL: 489, 298 # entries 17 rate 1.7 kHz word 	 
MASK_PIXEL: 459, 376 # entries 12 rate 1.2 kHz word 	 
MASK_PIXEL: 510, 428 # entries 9 rate 0.9 kHz word 	 
MASK_PIXEL: 484, 450 # entries 115 rate 1.15 kHz word 	 
MASK_PIXEL: 499, 323 # entries 107 rate 1.07 kHz word 	 
MASK_PIXEL: 430, 413 # entries 86 rate 0.86 kHz word 	 
MASK_PIXEL: 398, 441 # entries 57 rate 0.57 kHz word 	 
MASK_PIXEL: 432, 301 # entries 61 rate 0.61 kHz word 	 
MASK_PIXEL: 479, 302 # entries 45 rate 0.45 kHz word 	 
MASK_PIXEL: 420, 451 # entries 54 rate 0.54 kHz word 	 
MASK_PIXEL: 481, 388 # entries 54 rate 0.54 kHz word 	 
MASK_PIXEL: 485, 301 # entries 43 rate 0.43 kHz word 	 
MASK_PIXEL: 458, 350 # entries 49 rate 0.49 kHz word 	 
MASK_PIXEL: 500, 360 # entries 32 rate 0.32 kHz word 	 
MASK_PIXEL: 504, 387 # entries 17 rate 0.17 kHz word 	 
MASK_PIXEL: 476, 288 # entries 24 rate 0.24 kHz word 	 
MASK_PIXEL: 486, 305 # entries 16 rate 0.16 kHz word 	 
MASK_PIXEL: 499, 501 # entries 15 rate 0.15 kHz word 	 
MASK_PIXEL: 474, 307 # entries 10 rate 0.1 kHz word 	 
MASK_PIXEL: 511, 289 # entries 55 rate 0.11 kHz word 	 
MASK_PIXEL: 447, 325 # entries 48 rate 0.096 kHz word 	 
MASK_PIXEL: 452, 316 # entries 71 rate 0.142 kHz word 	 
MASK_PIXEL: 435, 322 # entries 42 rate 0.084 kHz word 	 
MASK_PIXEL: 506, 297 # entries 38 rate 0.076 kHz word 	 
MASK_PIXEL: 405, 369 # entries 35 rate 0.07 kHz word 	 
MASK_PIXEL: 487, 462 # entries 36 rate 0.072 kHz word 	 
MASK_PIXEL: 493, 390 # entries 30 rate 0.06 kHz word 	 
MASK_PIXEL: 487, 292 # entries 27 rate 0.054 kHz word 	 
MASK_PIXEL: 497, 305 # entries 22 rate 0.044 kHz word 	 
MASK_PIXEL: 489, 288 # entries 21 rate 0.042 kHz word 	 
MASK_PIXEL: 491, 323 # entries 21 rate 0.042 kHz word 	 
MASK_PIXEL: 508, 422 # entries 20 rate 0.04 kHz word 	 
