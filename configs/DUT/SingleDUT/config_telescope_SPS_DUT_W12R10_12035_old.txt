include: configs/DUT/TelescopePlanes.txt
[PLANE7]
address: udp://ep-ade-gw-02:50007 
type: MALTA2 
sample: W12R10 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 35 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R10_1/longCable_doubleChip_calib_run//calib_15.txt



MASK_DOUBLE_COLUMNS: 0, 150
MASK_DOUBLE_COLUMNS: 200, 255
