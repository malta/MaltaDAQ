include: configs/DUT/TelescopePlanes.txt
[PLANE7]
address: udp://ep-ade-gw-02:50007 
type: MALTA2 
sample: W12R10 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 40 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R10_1/longCable_doubleChip_calib_run//calib_15.txt

MASK_DOUBLE_COLUMNS:0,190 
MASK_PIXEL: 500, 347 # entries 165 rate 1650 kHz word 	 
MASK_PIXEL: 502, 289 # entries 108 rate 1080 kHz word 	 
MASK_PIXEL: 509, 489 # entries 11 rate 110 kHz word 	 
MASK_PIXEL: 466, 292 # entries 186 rate 186 kHz word 	 
MASK_PIXEL: 390, 377 # entries 31 rate 31 kHz word 	 
MASK_PIXEL: 498, 441 # entries 13 rate 13 kHz word 	 
MASK_PIXEL: 501, 289 # entries 139 rate 13.9 kHz word 	 
MASK_PIXEL: 511, 448 # entries 112 rate 11.2 kHz word 	 
MASK_PIXEL: 484, 309 # entries 169 rate 16.9 kHz word 	 
MASK_PIXEL: 459, 353 # entries 22 rate 2.2 kHz word 	 
MASK_PIXEL: 495, 404 # entries 34 rate 3.4 kHz word 	 
MASK_PIXEL: 392, 345 # entries 20 rate 2 kHz word 	 
MASK_PIXEL: 0, 0 # entries 30 rate 3 kHz word 	 
MASK_PIXEL: 0, 0 # entries 30 rate 3 kHz word 	 
MASK_PIXEL: 483, 426 # entries 166 rate 1.66 kHz word 	 
MASK_PIXEL: 510, 428 # entries 84 rate 0.84 kHz word 	 
MASK_PIXEL: 459, 376 # entries 69 rate 0.69 kHz word 	 
MASK_PIXEL: 489, 298 # entries 77 rate 0.77 kHz word 	 
MASK_PIXEL: 499, 323 # entries 55 rate 0.55 kHz word 	 
MASK_PIXEL: 484, 450 # entries 60 rate 0.6 kHz word 	 
MASK_PIXEL: 430, 413 # entries 40 rate 0.4 kHz word 	 
MASK_PIXEL: 398, 441 # entries 48 rate 0.48 kHz word 	 
MASK_PIXEL: 481, 388 # entries 36 rate 0.36 kHz word 	 
MASK_PIXEL: 432, 301 # entries 30 rate 0.3 kHz word 	 
MASK_PIXEL: 0, 0 # entries 30 rate 0.3 kHz word 	 
MASK_PIXEL: 0, 0 # entries 31 rate 0.31 kHz word 	 
MASK_PIXEL: 0, 0 # entries 20 rate 0.2 kHz word 	 
MASK_PIXEL: 0, 0 # entries 22 rate 0.22 kHz word 	 
MASK_PIXEL: 479, 302 # entries 33 rate 0.33 kHz word 	 
MASK_PIXEL: 0, 0 # entries 24 rate 0.24 kHz word 	 
MASK_PIXEL: 0, 0 # entries 32 rate 0.32 kHz word 	 
MASK_PIXEL: 458, 350 # entries 43 rate 0.43 kHz word 	 
MASK_PIXEL: 0, 0 # entries 28 rate 0.28 kHz word 	 
MASK_PIXEL: 0, 0 # entries 19 rate 0.19 kHz word 	 
MASK_PIXEL: 0, 0 # entries 25 rate 0.25 kHz word 	 
MASK_PIXEL: 0, 0 # entries 21 rate 0.21 kHz word 	 
MASK_PIXEL: 0, 0 # entries 20 rate 0.2 kHz word 	 
MASK_PIXEL: 0, 0 # entries 31 rate 0.31 kHz word 	 
MASK_PIXEL: 0, 0 # entries 23 rate 0.23 kHz word 	 
MASK_PIXEL: 0, 0 # entries 30 rate 0.3 kHz word 	 
MASK_PIXEL: 0, 0 # entries 19 rate 0.19 kHz word 	 
MASK_PIXEL: 0, 0 # entries 27 rate 0.27 kHz word 	 
MASK_PIXEL: 0, 0 # entries 24 rate 0.24 kHz word 	 
MASK_PIXEL: 0, 0 # entries 22 rate 0.22 kHz word 	 
MASK_PIXEL: 0, 0 # entries 25 rate 0.25 kHz word 	 
MASK_PIXEL: 0, 0 # entries 25 rate 0.25 kHz word 	 
MASK_PIXEL: 0, 0 # entries 21 rate 0.21 kHz word 	 
MASK_PIXEL: 0, 0 # entries 25 rate 0.25 kHz word 	 
MASK_PIXEL: 0, 0 # entries 24 rate 0.24 kHz word 	 
MASK_PIXEL: 0, 0 # entries 22 rate 0.22 kHz word 	 
MASK_PIXEL: 420, 451 # entries 80 rate 0.16 kHz word 	 
MASK_PIXEL: 500, 360 # entries 72 rate 0.144 kHz word 	 
MASK_PIXEL: 0, 0 # entries 58 rate 0.116 kHz word 	 
MASK_PIXEL: 485, 301 # entries 58 rate 0.116 kHz word 	 
MASK_PIXEL: 0, 0 # entries 48 rate 0.096 kHz word 	 
MASK_PIXEL: 0, 0 # entries 49 rate 0.098 kHz word 	 
MASK_PIXEL: 474, 307 # entries 33 rate 0.066 kHz word 	 
MASK_PIXEL: 504, 387 # entries 32 rate 0.064 kHz word 	 
MASK_PIXEL: 499, 501 # entries 21 rate 0.042 kHz word 	 
MASK_PIXEL: 0, 0 # entries 34 rate 0.068 kHz word 	 
MASK_PIXEL: 0, 0 # entries 59 rate 0.118 kHz word 	 
MASK_PIXEL: 0, 0 # entries 57 rate 0.114 kHz word 	 
MASK_PIXEL: 0, 0 # entries 50 rate 0.1 kHz word 	 
MASK_PIXEL: 0, 0 # entries 61 rate 0.122 kHz word 	 
MASK_PIXEL: 0, 0 # entries 65 rate 0.13 kHz word 	 
MASK_PIXEL: 0, 0 # entries 53 rate 0.106 kHz word 	 
MASK_PIXEL: 0, 0 # entries 59 rate 0.118 kHz word 	 
MASK_PIXEL: 0, 0 # entries 48 rate 0.096 kHz word 	 
MASK_PIXEL: 0, 0 # entries 60 rate 0.12 kHz word 	 
MASK_PIXEL: 0, 0 # entries 62 rate 0.124 kHz word 	 
MASK_PIXEL: 0, 0 # entries 56 rate 0.112 kHz word 	 
MASK_PIXEL: 486, 305 # entries 22 rate 0.044 kHz word 	 
MASK_PIXEL: 0, 0 # entries 43 rate 0.086 kHz word 	 
MASK_PIXEL: 0, 0 # entries 59 rate 0.118 kHz word 	 
MASK_PIXEL: 0, 0 # entries 44 rate 0.088 kHz word 	 
MASK_PIXEL: 0, 0 # entries 56 rate 0.112 kHz word 	 
MASK_PIXEL: 0, 0 # entries 58 rate 0.116 kHz word 	 
MASK_PIXEL: 0, 0 # entries 45 rate 0.09 kHz word 	 
MASK_PIXEL: 0, 0 # entries 52 rate 0.104 kHz word 	 
MASK_PIXEL: 0, 0 # entries 33 rate 0.066 kHz word 	 
MASK_PIXEL: 501, 288 # entries 20 rate 0.04 kHz word 	 
