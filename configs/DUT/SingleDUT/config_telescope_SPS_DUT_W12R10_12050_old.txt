include: configs/DUT/TelescopePlanes.txt
[PLANE7]
address: udp://ep-ade-gw-02:50007 
type: MALTA2 
sample: W12R10 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 50 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R10/longCable_doubleChip_calib_run//calib_15.txt

MASK_DOUBLE_COLUMNS:128,256 
MASK_PIXEL: 193, 293 # entries 1169 rate 11690 kHz word 	 
MASK_PIXEL: 199, 301 # entries 870 rate 8700 kHz word 	 
MASK_PIXEL: 173, 296 # entries 135 rate 1350 kHz word 	 
MASK_PIXEL: 24, 481 # entries 64 rate 640 kHz word 	 
MASK_PIXEL: 81, 378 # entries 33 rate 33 kHz word 	 
MASK_PIXEL: 195, 288 # entries 16 rate 16 kHz word 	 
MASK_PIXEL: 116, 412 # entries 155 rate 15.5 kHz word 	 
MASK_PIXEL: 69, 389 # entries 102 rate 10.2 kHz word 	 
MASK_PIXEL: 200, 288 # entries 94 rate 9.4 kHz word 	 
MASK_PIXEL: 221, 334 # entries 34 rate 3.4 kHz word 	 
MASK_PIXEL: 164, 465 # entries 30 rate 3 kHz word 	 
MASK_PIXEL: 206, 312 # entries 117 rate 1.17 kHz word 	 
MASK_PIXEL: 196, 375 # entries 90 rate 0.9 kHz word 	 
MASK_PIXEL: 123, 384 # entries 70 rate 0.7 kHz word 	 
MASK_PIXEL: 75, 292 # entries 56 rate 0.56 kHz word 	 
MASK_PIXEL: 176, 334 # entries 45 rate 0.45 kHz word 	 
MASK_PIXEL: 138, 386 # entries 34 rate 0.34 kHz word 	 
MASK_PIXEL: 15, 439 # entries 38 rate 0.38 kHz word 	 
MASK_PIXEL: 131, 498 # entries 18 rate 0.18 kHz word 	 
MASK_PIXEL: 170, 288 # entries 22 rate 0.22 kHz word 	 
MASK_PIXEL: 95, 316 # entries 10 rate 0.1 kHz word 	 
MASK_PIXEL: 199, 346 # entries 10 rate 0.1 kHz word 	 
MASK_PIXEL: 159, 328 # entries 61 rate 0.122 kHz word 	 
MASK_PIXEL: 79, 359 # entries 49 rate 0.098 kHz word 	 
MASK_PIXEL: 161, 417 # entries 31 rate 0.062 kHz word 	 
MASK_PIXEL: 165, 355 # entries 29 rate 0.058 kHz word 	 
MASK_PIXEL: 134, 327 # entries 29 rate 0.058 kHz word 	 
