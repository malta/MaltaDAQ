include: configs/DUT/TelescopePlanes.txt
[PLANE7]
address: udp://ep-ade-gw-02:50007 
type: MALTA2 
sample: W12R10 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 80 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R10/longCable_doubleChip_calib_run//calib_15.txt

MASK_DOUBLE_COLUMNS: 0, 150
MASK_DOUBLE_COLUMNS: 200, 255
#MASK_DOUBLE_COLUMNS:0,128 
#MASK_PIXEL: 0, 0 # entries 6 rate 0.012 kHz word 	 

MASK_PIXEL: 331, 289 #entries  104467
MASK_PIXEL: 326, 328 #entries    1027
MASK_PIXEL: 361, 300 #entries    1430
MASK_PIXEL: 390, 377 #entries    3217
