type: MALTA2 
sample: W12R6 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 40 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R6/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 10, 346 # entries 488 rate 4880 kHz word 	 
MASK_PIXEL: 19, 420 # entries 67 rate 670 kHz word 	 
MASK_PIXEL: 416, 345 # entries 66 rate 660 kHz word 	 
MASK_PIXEL: 1, 323 # entries 16 rate 160 kHz word 	 
MASK_PIXEL: 437, 360 # entries 61 rate 61 kHz word 	 
MASK_PIXEL: 436, 402 # entries 213 rate 21.3 kHz word 	 
MASK_PIXEL: 365, 398 # entries 166 rate 16.6 kHz word 	 
MASK_PIXEL: 273, 317 # entries 84 rate 8.4 kHz word 	 
MASK_PIXEL: 430, 314 # entries 58 rate 5.8 kHz word 	 
MASK_PIXEL: 417, 290 # entries 47 rate 4.7 kHz word 	 
MASK_PIXEL: 44, 361 # entries 8 rate 0.8 kHz word 	 
MASK_PIXEL: 430, 291 # entries 201 rate 2.01 kHz word 	 
MASK_PIXEL: 109, 334 # entries 98 rate 0.98 kHz word 	 
MASK_PIXEL: 473, 368 # entries 28 rate 0.28 kHz word 	 
MASK_PIXEL: 213, 288 # entries 28 rate 0.28 kHz word 	 
MASK_PIXEL: 430, 397 # entries 40 rate 0.4 kHz word 	 
MASK_PIXEL: 239, 355 # entries 105 rate 0.21 kHz word 	 
MASK_PIXEL: 417, 381 # entries 120 rate 0.24 kHz word 	 
MASK_PIXEL: 135, 440 # entries 112 rate 0.224 kHz word 	 
MASK_PIXEL: 473, 289 # entries 37 rate 0.074 kHz word 	 
MASK_PIXEL: 453, 288 # entries 56 rate 0.112 kHz word 	 
MASK_PIXEL: 63, 288 # entries 33 rate 0.066 kHz word 	 
