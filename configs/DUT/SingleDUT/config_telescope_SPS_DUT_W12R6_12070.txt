type: MALTA2 
sample: W12R6 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 70 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R6/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 10, 346 # entries 911 rate 9110 kHz word 	 
MASK_PIXEL: 19, 420 # entries 521 rate 5210 kHz word 	 
MASK_PIXEL: 437, 360 # entries 365 rate 3650 kHz word 	 
MASK_PIXEL: 365, 398 # entries 258 rate 2580 kHz word 	 
MASK_PIXEL: 1, 323 # entries 263 rate 2630 kHz word 	 
MASK_PIXEL: 436, 402 # entries 31 rate 31 kHz word 	 
MASK_PIXEL: 430, 291 # entries 225 rate 22.5 kHz word 	 
MASK_PIXEL: 273, 317 # entries 67 rate 6.7 kHz word 	 
MASK_PIXEL: 473, 368 # entries 19 rate 1.9 kHz word 	 
MASK_PIXEL: 239, 355 # entries 196 rate 1.96 kHz word 	 
MASK_PIXEL: 109, 334 # entries 19 rate 0.19 kHz word 	 
MASK_PIXEL: 417, 381 # entries 136 rate 0.272 kHz word 	 
MASK_PIXEL: 430, 314 # entries 47 rate 0.094 kHz word 	 
