include: configs/DUT/TelescopePlanes.txt
[PLANE7]
address: udp://ep-ade-gw-02:50007 
type: MALTA2 
sample: W15R4 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 100 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W15R4/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 298, 398 # entries 388 rate 3880 kHz word 	 
MASK_PIXEL: 201, 356 # entries 13 rate 1.3 kHz word 	 
