type: MALTA2 
sample: W18R12 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 120 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W18R12/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 140, 367 # entries 1085 rate 10850 kHz word 	 
MASK_PIXEL: 447, 288 # entries 1043 rate 10430 kHz word 	 
MASK_PIXEL: 320, 317 # entries 592 rate 5920 kHz word 	 
MASK_PIXEL: 235, 339 # entries 596 rate 5960 kHz word 	 
MASK_PIXEL: 473, 293 # entries 113 rate 1130 kHz word 	 
MASK_PIXEL: 496, 297 # entries 54 rate 540 kHz word 	 
MASK_PIXEL: 192, 337 # entries 13 rate 130 kHz word 	 
MASK_PIXEL: 450, 454 # entries 147 rate 147 kHz word 	 
MASK_PIXEL: 387, 489 # entries 27 rate 27 kHz word 	 
MASK_PIXEL: 50, 332 # entries 91 rate 9.1 kHz word 	 
MASK_PIXEL: 465, 393 # entries 39 rate 3.9 kHz word 	 
MASK_PIXEL: 450, 297 # entries 776 rate 7.76 kHz word 	 
MASK_PIXEL: 462, 349 # entries 178 rate 1.78 kHz word 	 
MASK_PIXEL: 139, 291 # entries 17 rate 0.17 kHz word 	 
MASK_PIXEL: 226, 304 # entries 13 rate 0.13 kHz word 	 
MASK_PIXEL: 449, 349 # entries 75 rate 0.15 kHz word 	 
MASK_PIXEL: 463, 311 # entries 26 rate 0.052 kHz word 	 
MASK_PIXEL: 176, 288 # entries 33 rate 0.066 kHz word 	 
