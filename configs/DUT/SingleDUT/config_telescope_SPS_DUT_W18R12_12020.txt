type: MALTA2 
sample: W18R12 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 20 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W18R12/longCable_doubleChip_calib_run//calib_15.txt

MASK_DOUBLE_COLUMNS:0,128 
MASK_PIXEL: 478, 410 # entries 54 rate 0.993743 kHz word 	 
MASK_PIXEL: 496, 297 # entries 37 rate 0.680898 kHz word 	 
MASK_PIXEL: 320, 317 # entries 86 rate 0.172 kHz word 	 
MASK_PIXEL: 473, 293 # entries 53 rate 0.106 kHz word 	 
