include: configs/DUT/TelescopePlanes.txt
[PLANE7]
address: udp://ep-ade-gw-02:50007 
type: MALTA2 
sample: W12R6 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 50 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R6/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 416, 345 # entries 346 rate 3460 kHz word 	 
MASK_PIXEL: 19, 420 # entries 138 rate 1380 kHz word 	 
MASK_PIXEL: 10, 346 # entries 47 rate 47 kHz word 	 
MASK_PIXEL: 1, 323 # entries 149 rate 14.9 kHz word 	 
MASK_PIXEL: 437, 360 # entries 11 rate 1.1 kHz word 	 
MASK_PIXEL: 365, 398 # entries 141 rate 1.41 kHz word 	 
MASK_PIXEL: 436, 402 # entries 79 rate 0.79 kHz word 	 
MASK_PIXEL: 430, 291 # entries 56 rate 0.112 kHz word 	 
MASK_PIXEL: 181, 289 # entries 51 rate 0.102 kHz word 	 
################################################################################################################################################
[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2 
sample: W12R7 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 50 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R7/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 319, 293 # entries 111 rate 1110 kHz word 	 
MASK_PIXEL: 503, 289 # entries 175 rate 17.5 kHz word 	 
MASK_PIXEL: 60, 362 # entries 128 rate 12.8 kHz word 	 
MASK_PIXEL: 490, 288 # entries 137 rate 1.37 kHz word 	 
MASK_PIXEL: 45, 297 # entries 39 rate 0.39 kHz word 	 
MASK_PIXEL: 506, 288 # entries 25 rate 0.25 kHz word 	 
MASK_PIXEL: 485, 435 # entries 17 rate 0.17 kHz word 	 
MASK_PIXEL: 0, 0 # entries 7 rate 0.07 kHz word 	 
MASK_PIXEL: 84, 294 # entries 87 rate 0.174 kHz word 	 
