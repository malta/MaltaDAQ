include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2 
sample: W12R7 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 60 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W12R7/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 100, 374 # entries 287 rate 2870 kHz word 	 
MASK_PIXEL: 94, 377 # entries 330 rate 3300 kHz word 	 
MASK_PIXEL: 118, 365 # entries 350 rate 3500 kHz word 	 
MASK_PIXEL: 26, 289 # entries 417 rate 4170 kHz word 	 
MASK_PIXEL: 437, 296 # entries 348 rate 3480 kHz word 	 
MASK_PIXEL: 340, 503 # entries 388 rate 3880 kHz word 	 
MASK_PIXEL: 13, 315 # entries 353 rate 3530 kHz word 	 
MASK_PIXEL: 39, 440 # entries 446 rate 4460 kHz word 	 
MASK_PIXEL: 479, 323 # entries 380 rate 3800 kHz word 	 
MASK_PIXEL: 458, 366 # entries 437 rate 4370 kHz word 	 
MASK_PIXEL: 4, 293 # entries 459 rate 4590 kHz word 	 
MASK_PIXEL: 46, 357 # entries 506 rate 5060 kHz word 	 
MASK_PIXEL: 110, 325 # entries 414 rate 4140 kHz word 	 
MASK_PIXEL: 438, 345 # entries 471 rate 4710 kHz word 	 
MASK_PIXEL: 111, 294 # entries 484 rate 4840 kHz word 	 
MASK_PIXEL: 39, 395 # entries 549 rate 5490 kHz word 	 
MASK_PIXEL: 429, 349 # entries 428 rate 4280 kHz word 	 
MASK_PIXEL: 90, 365 # entries 565 rate 5650 kHz word 	 
MASK_PIXEL: 97, 308 # entries 490 rate 4900 kHz word 	 
MASK_PIXEL: 29, 289 # entries 522 rate 5220 kHz word 	 
MASK_PIXEL: 47, 307 # entries 696 rate 6960 kHz word 	 
MASK_PIXEL: 493, 346 # entries 579 rate 5790 kHz word 	 
MASK_PIXEL: 67, 320 # entries 562 rate 5620 kHz word 	 
MASK_PIXEL: 92, 349 # entries 674 rate 6740 kHz word 	 
MASK_PIXEL: 5, 317 # entries 785 rate 7850 kHz word 	 
MASK_PIXEL: 54, 339 # entries 654 rate 6540 kHz word 	 
MASK_PIXEL: 483, 365 # entries 644 rate 6440 kHz word 	 
MASK_PIXEL: 10, 354 # entries 509 rate 5090 kHz word 	 
MASK_PIXEL: 8, 338 # entries 604 rate 6040 kHz word 	 
MASK_PIXEL: 506, 435 # entries 602 rate 6020 kHz word 	 
MASK_PIXEL: 72, 385 # entries 456 rate 4560 kHz word 	 
MASK_PIXEL: 121, 318 # entries 561 rate 5610 kHz word 	 
MASK_PIXEL: 3, 314 # entries 590 rate 5900 kHz word 	 
MASK_PIXEL: 214, 361 # entries 517 rate 5170 kHz word 	 
MASK_PIXEL: 5, 340 # entries 629 rate 6290 kHz word 	 
MASK_PIXEL: 482, 379 # entries 605 rate 6050 kHz word 	 
MASK_PIXEL: 110, 433 # entries 600 rate 6000 kHz word 	 
MASK_PIXEL: 489, 302 # entries 826 rate 8260 kHz word 	 
MASK_PIXEL: 95, 308 # entries 476 rate 4760 kHz word 	 
MASK_PIXEL: 5, 321 # entries 445 rate 4450 kHz word 	 
MASK_PIXEL: 106, 294 # entries 494 rate 4940 kHz word 	 
MASK_PIXEL: 110, 402 # entries 431 rate 4310 kHz word 	 
MASK_PIXEL: 26, 303 # entries 301 rate 3010 kHz word 	 
MASK_PIXEL: 5, 308 # entries 612 rate 6120 kHz word 	 
MASK_PIXEL: 437, 298 # entries 430 rate 4300 kHz word 	 
MASK_PIXEL: 91, 354 # entries 400 rate 4000 kHz word 	 
MASK_PIXEL: 88, 315 # entries 324 rate 3240 kHz word 	 
MASK_PIXEL: 102, 391 # entries 586 rate 5860 kHz word 	 
MASK_PIXEL: 98, 289 # entries 410 rate 4100 kHz word 	 
MASK_PIXEL: 20, 304 # entries 296 rate 2960 kHz word 	 
MASK_PIXEL: 70, 314 # entries 286 rate 2860 kHz word 	 
MASK_PIXEL: 111, 445 # entries 256 rate 2560 kHz word 	 
MASK_PIXEL: 85, 296 # entries 182 rate 1820 kHz word 	 
MASK_PIXEL: 99, 408 # entries 336 rate 3360 kHz word 	 
MASK_PIXEL: 85, 312 # entries 487 rate 4870 kHz word 	 
MASK_PIXEL: 499, 361 # entries 405 rate 4050 kHz word 	 
MASK_PIXEL: 491, 296 # entries 259 rate 2590 kHz word 	 
MASK_PIXEL: 16, 360 # entries 252 rate 2520 kHz word 	 
MASK_PIXEL: 31, 313 # entries 210 rate 2100 kHz word 	 
MASK_PIXEL: 21, 303 # entries 413 rate 4130 kHz word 	 
MASK_PIXEL: 486, 342 # entries 381 rate 3810 kHz word 	 
MASK_PIXEL: 498, 354 # entries 397 rate 3970 kHz word 	 
MASK_PIXEL: 94, 452 # entries 216 rate 2160 kHz word 	 
MASK_PIXEL: 173, 311 # entries 236 rate 2360 kHz word 	 
MASK_PIXEL: 94, 389 # entries 275 rate 2750 kHz word 	 
MASK_PIXEL: 107, 304 # entries 281 rate 2810 kHz word 	 
MASK_PIXEL: 41, 388 # entries 251 rate 2510 kHz word 	 
MASK_PIXEL: 97, 375 # entries 181 rate 1810 kHz word 	 
MASK_PIXEL: 10, 384 # entries 99 rate 990 kHz word 	 
MASK_PIXEL: 116, 343 # entries 138 rate 1380 kHz word 	 
MASK_PIXEL: 504, 305 # entries 228 rate 2280 kHz word 	 
MASK_PIXEL: 459, 371 # entries 158 rate 1580 kHz word 	 
MASK_PIXEL: 327, 504 # entries 186 rate 1860 kHz word 	 
MASK_PIXEL: 37, 326 # entries 146 rate 1460 kHz word 	 
MASK_PIXEL: 61, 333 # entries 107 rate 1070 kHz word 	 
MASK_PIXEL: 413, 307 # entries 126 rate 1260 kHz word 	 
MASK_PIXEL: 10, 310 # entries 985 rate 9850 kHz word 	 
MASK_PIXEL: 76, 292 # entries 290 rate 2900 kHz word 	 
MASK_PIXEL: 509, 362 # entries 104 rate 1040 kHz word 	 
MASK_PIXEL: 123, 393 # entries 105 rate 1050 kHz word 	 
MASK_PIXEL: 396, 292 # entries 148 rate 1480 kHz word 	 
MASK_PIXEL: 6, 421 # entries 84 rate 840 kHz word 	 
MASK_PIXEL: 57, 415 # entries 167 rate 1670 kHz word 	 
MASK_PIXEL: 216, 325 # entries 44 rate 440 kHz word 	 
MASK_PIXEL: 105, 397 # entries 39 rate 390 kHz word 	 
MASK_PIXEL: 381, 319 # entries 82 rate 820 kHz word 	 
MASK_PIXEL: 507, 332 # entries 33 rate 330 kHz word 	 
MASK_PIXEL: 322, 476 # entries 36 rate 360 kHz word 	 
MASK_PIXEL: 2, 338 # entries 73 rate 730 kHz word 	 
MASK_PIXEL: 387, 314 # entries 44 rate 440 kHz word 	 
MASK_PIXEL: 29, 359 # entries 46 rate 460 kHz word 	 
MASK_PIXEL: 500, 432 # entries 31 rate 310 kHz word 	 
MASK_PIXEL: 505, 355 # entries 58 rate 580 kHz word 	 
MASK_PIXEL: 78, 412 # entries 73 rate 730 kHz word 	 
MASK_PIXEL: 0, 390 # entries 118 rate 1180 kHz word 	 
MASK_PIXEL: 55, 299 # entries 106 rate 1060 kHz word 	 
MASK_PIXEL: 71, 291 # entries 27 rate 270 kHz word 	 
MASK_PIXEL: 71, 303 # entries 65 rate 650 kHz word 	 
MASK_PIXEL: 85, 381 # entries 20 rate 200 kHz word 	 
MASK_PIXEL: 0, 299 # entries 63 rate 630 kHz word 	 
MASK_PIXEL: 8, 336 # entries 22 rate 220 kHz word 	
MASK_PIXEL: 106, 378 #entries   10657
MASK_PIXEL: 106, 382 #entries   11187
MASK_PIXEL: 107, 377 #entries   16304
MASK_PIXEL: 127, 506 #entries   20987
MASK_PIXEL:  84, 294 #entries   26702
MASK_PIXEL: 484, 305 #entries   34545
MASK_PIXEL:  17, 303 #entries   78286
MASK_PIXEL: 107, 369 #entries   82773
MASK_PIXEL:  98, 330 #entries   91275
MASK_PIXEL:  66, 382 #entries   91293
MASK_PIXEL:  85, 309 #entries   91409
MASK_PIXEL: 106, 377 #entries   98396
MASK_PIXEL: 125, 434 #entries  104318 
MASK_PIXEL: 121, 409 #entries   10441
MASK_PIXEL:  12, 355 #entries   14351
MASK_PIXEL:  20, 307 #entries   26156
MASK_PIXEL: 198, 360 #entries   34340
MASK_PIXEL: 110, 446 #entries   39007
MASK_PIXEL: 503, 289 #entries   52883
MASK_PIXEL:  79, 397 #entries   70701
MASK_PIXEL: 485, 435 #entries   92550
MASK_PIXEL:   4, 393 #entries  115357
MASK_PIXEL:  70, 307 #entries  180693
MASK_PIXEL: 482, 412 #entries  183850
MASK_PIXEL: 319, 293 #entries  843531
MASK_PIXEL:  60, 362 #entries  286206
MASK_PIXEL:  45, 297 #entries  472616
MASK_PIXEL:   9, 345 #entries  190954
