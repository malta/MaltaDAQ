include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W14R3
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 15
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W14R3/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 132, 457 #entries    1299
MASK_PIXEL: 366, 330 #entries    1906
MASK_PIXEL: 161, 288 #entries    3696
MASK_PIXEL: 304, 440 #entries    4749
MASK_PIXEL: 466, 431 #entries     100
MASK_PIXEL: 282, 339 #entries     112
MASK_PIXEL: 377, 503 #entries     146
MASK_PIXEL:  45, 364 #entries     169
MASK_PIXEL:   4, 345 #entries     187
MASK_PIXEL:   1, 372 #entries     193
MASK_PIXEL: 284, 297 #entries     256
MASK_PIXEL: 480, 330 #entries     303
MASK_PIXEL: 453, 320 #entries     368
MASK_PIXEL: 448, 293 #entries     376
MASK_PIXEL: 440, 295 #entries     414
MASK_PIXEL: 485, 312 #entries     459
MASK_PIXEL: 482, 301 #entries     746
MASK_PIXEL: 321, 295 #entries     895
MASK_PIXEL: 460, 378 #entries     121
MASK_PIXEL: 166, 447 #entries     124
MASK_PIXEL: 501, 365 #entries     129
MASK_PIXEL: 326, 300 #entries     215
###########################################################################################################################


[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W15R11
SC_ICASN: 2
SC_IBIAS: 43
SC_ITHR: 15
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W15R11/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 109, 369 #entries     243
MASK_PIXEL: 108, 370 #entries    1547
MASK_PIXEL: 108, 374 #entries    1824
MASK_PIXEL:  97, 289 #entries    9580
MASK_PIXEL:  12, 290 #entries   62622
MASK_PIXEL: 108, 342 #entries  900776
MASK_PIXEL: 102, 302 #entries     314
MASK_PIXEL: 348, 366 #entries     113
MASK_PIXEL: 219, 340 #entries     134
MASK_PIXEL:  96, 318 #entries     548
MASK_PIXEL: 310, 430 #entries     678
MASK_PIXEL: 341, 372 #entries     853
MASK_PIXEL:  12, 326 #entries    2138
MASK_PIXEL: 319, 431 #entries   11688
MASK_PIXEL:   0, 310 #entries   13578
MASK_PIXEL: 444, 288 #entries     114
MASK_PIXEL: 469, 288 #entries     336
MASK_PIXEL: 149, 288 #entries     226
MASK_PIXEL: 464, 288 #entries     233
MASK_PIXEL: 380, 288 #entries     307
MASK_PIXEL: 101, 288 #entries     315
MASK_PIXEL: 470, 288 #entries     402
MASK_PIXEL: 379, 288 #entries     558
MASK_PIXEL: 481, 288 #entries     598
MASK_PIXEL: 440, 288 #entries     707
MASK_PIXEL: 304, 288 #entries    1035
MASK_PIXEL: 462, 288 #entries     810
MASK_PIXEL: 107, 288 #entries     842
MASK_PIXEL: 473, 288 #entries     846
MASK_PIXEL: 417, 288 #entries     894
MASK_PIXEL: 451, 288 #entries    1230
MASK_PIXEL:  56, 288 #entries    3254
