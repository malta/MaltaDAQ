include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W14R3
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 80
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W14R3/longCable_doubleChip_calib_run//calib_15.txt

###########################################################################################################################

[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W15R11
SC_ICASN: 2
SC_IBIAS: 43
SC_ITHR: 80
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W15R11/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 109, 369 #entries     243
MASK_PIXEL: 108, 370 #entries    1547
MASK_PIXEL: 108, 374 #entries    1824
MASK_PIXEL:  97, 289 #entries    9580
MASK_PIXEL:  12, 290 #entries   62622
MASK_PIXEL: 108, 342 #entries  900776
MASK_PIXEL: 102, 302 #entries     314

