include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W14R3
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 100
SC_IDB: 80
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W14R3/longCable_doubleChip_calib_run//calib_15.txt


MASK_PIXEL: 132, 457 #entries    1299
MASK_PIXEL: 366, 330 #entries    1906
MASK_PIXEL: 161, 288 #entries    3696
MASK_PIXEL: 304, 440 #entries    4749
MASK_PIXEL: 466, 431 #entries     100
MASK_PIXEL: 282, 339 #entries     112
MASK_PIXEL: 377, 503 #entries     146
MASK_PIXEL:  45, 364 #entries     169
MASK_PIXEL:   4, 345 #entries     187
MASK_PIXEL:   1, 372 #entries     193
MASK_PIXEL: 284, 297 #entries     256
MASK_PIXEL: 480, 330 #entries     303
MASK_PIXEL: 453, 320 #entries     368
MASK_PIXEL: 448, 293 #entries     376
MASK_PIXEL: 440, 295 #entries     414
MASK_PIXEL: 485, 312 #entries     459
MASK_PIXEL: 482, 301 #entries     746
MASK_PIXEL: 321, 295 #entries     895
MASK_PIXEL: 460, 378 #entries     121
MASK_PIXEL: 166, 447 #entries     124
MASK_PIXEL: 501, 365 #entries     129
MASK_PIXEL: 326, 300 #entries     215

MASK_PIXEL: 130, 470 #entries    2957
MASK_PIXEL: 510, 300 #entries    2982
MASK_PIXEL: 129, 427 #entries    3027
MASK_PIXEL: 213, 363 #entries    3047
MASK_PIXEL: 506, 371 #entries    3130
MASK_PIXEL: 292, 438 #entries    3219
MASK_PIXEL: 347, 401 #entries    3223
MASK_PIXEL:   5, 356 #entries    3271
MASK_PIXEL: 332, 374 #entries    3274
MASK_PIXEL: 409, 386 #entries    3504
MASK_PIXEL: 452, 295 #entries    3511
MASK_PIXEL: 484, 467 #entries    3601
MASK_PIXEL:  51, 365 #entries    3698
MASK_PIXEL: 421, 360 #entries    3792
MASK_PIXEL: 507, 339 #entries    3890
MASK_PIXEL: 453, 302 #entries    4063
MASK_PIXEL: 510, 374 #entries    4091
MASK_PIXEL: 472, 302 #entries    4143
MASK_PIXEL:  63, 307 #entries    4328
MASK_PIXEL: 500, 316 #entries    4330
MASK_PIXEL: 494, 364 #entries    4363
MASK_PIXEL: 328, 369 #entries    4453
MASK_PIXEL:  46, 420 #entries    4492
MASK_PIXEL:   4, 303 #entries    4517
MASK_PIXEL: 263, 342 #entries    4793
MASK_PIXEL: 306, 367 #entries    4880
MASK_PIXEL: 482, 313 #entries    5195
MASK_PIXEL: 279, 315 #entries    5586
MASK_PIXEL: 444, 374 #entries    5808
MASK_PIXEL:  19, 314 #entries    6609
MASK_PIXEL: 462, 303 #entries    6672
MASK_PIXEL:  17, 294 #entries    6966
MASK_PIXEL:  10, 411 #entries    6978
MASK_PIXEL: 510, 307 #entries    6991
MASK_PIXEL: 332, 355 #entries    7078
MASK_PIXEL: 468, 293 #entries    7512
MASK_PIXEL: 415, 294 #entries    9356
MASK_PIXEL: 473, 343 #entries   11185
MASK_PIXEL:  10, 418 #entries   12365
MASK_PIXEL:  94, 303 #entries   14877
MASK_PIXEL: 491, 369 #entries   16871
MASK_PIXEL: 289, 331 #entries   27864

MASK_PIXEL: 496, 378 #entries     939
MASK_PIXEL: 454, 490 #entries     944
MASK_PIXEL: 497, 435 #entries     985
MASK_PIXEL: 291, 372 #entries    1028
MASK_PIXEL: 479, 361 #entries    1083
MASK_PIXEL: 301, 305 #entries    1134


###########################################################################################################################

[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W5R6
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 100
SC_IDB: 80
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W5R6/longCable_doubleChip_calib_run//calib_15.txt


MASK_PIXEL: 476, 474 #entries    1010
MASK_PIXEL: 476, 478 #entries    1014
MASK_PIXEL: 436, 329 #entries    1136
MASK_PIXEL: 451, 288 #entries    1137
MASK_PIXEL: 463, 494 #entries    1158
MASK_PIXEL: 462, 490 #entries    1159
MASK_PIXEL: 458, 378 #entries    1204
MASK_PIXEL: 459, 376 #entries    1235
MASK_PIXEL:   0, 274 #entries    1340
MASK_PIXEL: 462, 402 #entries    1411
MASK_PIXEL: 462, 442 #entries    1414
MASK_PIXEL: 463, 406 #entries    1419
MASK_PIXEL:  72, 351 #entries    1490
MASK_PIXEL: 269, 402 #entries    1509
MASK_PIXEL:   0,  34 #entries    1634
MASK_PIXEL: 463, 440 #entries    1697
MASK_PIXEL: 499, 378 #entries    1828
MASK_PIXEL: 498, 378 #entries    1833
MASK_PIXEL: 503, 466 #entries    2118
MASK_PIXEL: 502, 466 #entries    2157
MASK_PIXEL: 144, 490 #entries    2441
MASK_PIXEL: 163, 503 #entries    2502
MASK_PIXEL:   0, 106 #entries    2535
MASK_PIXEL: 148, 434 #entries    2759
MASK_PIXEL:   0, 402 #entries    3065
MASK_PIXEL: 100, 371 #entries    3357
MASK_PIXEL:   0,  18 #entries    3919
MASK_PIXEL: 216, 311 #entries    5252
MASK_PIXEL:   4, 506 #entries    8248
MASK_PIXEL: 135, 312 #entries    9256
MASK_PIXEL: 476, 321 #entries   10178
MASK_PIXEL: 105, 289 #entries   18573
MASK_PIXEL: 472, 350 #entries   21549
MASK_PIXEL: 463, 390 #entries   28923
MASK_PIXEL: 459, 312 #entries   30508
MASK_PIXEL: 499, 338 #entries   44795
MASK_PIXEL: 144, 418 #entries   56869
MASK_PIXEL:   0, 362 #entries  176984
MASK_PIXEL:   4, 402 #entries  202949
MASK_PIXEL:  17, 296 #entries    5054
MASK_PIXEL: 428, 406 #entries    7313
MASK_PIXEL: 465, 392 #entries    7428
MASK_PIXEL:  29, 345 #entries    9172
MASK_PIXEL: 108, 359 #entries   11413

MASK_PIXEL:  59, 297 #entries    1012
MASK_PIXEL: 494, 366 #entries    1207
MASK_PIXEL: 183, 303 #entries    1406
MASK_PIXEL: 468, 296 #entries    1562
MASK_PIXEL: 202, 330 #entries    1576
MASK_PIXEL: 100, 345 #entries    1605
MASK_PIXEL:  28, 460 #entries    1808
MASK_PIXEL:  67, 295 #entries    2204
MASK_PIXEL: 367, 378 #entries    2518
MASK_PIXEL: 418, 304 #entries    2854
MASK_PIXEL: 257, 510 #entries    3954
MASK_PIXEL: 460, 309 #entries    4154
MASK_PIXEL: 458, 411 #entries    4172
MASK_PIXEL: 474, 321 #entries    4766
MASK_PIXEL: 203, 356 #entries    4899
MASK_PIXEL:  72, 456 #entries    5185
MASK_PIXEL: 302, 417 #entries    6854


MASK_PIXEL: 473, 304 #entries    2915
MASK_PIXEL: 416, 296 #entries    2968
MASK_PIXEL: 458, 386 #entries    2988
MASK_PIXEL: 506, 334 #entries    3022
MASK_PIXEL: 478, 343 #entries    3121
MASK_PIXEL: 445, 332 #entries    3334
MASK_PIXEL: 478, 296 #entries    3340
MASK_PIXEL: 504, 361 #entries    3351
MASK_PIXEL: 110, 445 #entries    3552
MASK_PIXEL:  37, 297 #entries    3776
MASK_PIXEL: 453, 322 #entries    3821
MASK_PIXEL:  98, 294 #entries    3845
MASK_PIXEL: 476, 325 #entries    4163
MASK_PIXEL: 219, 398 #entries    4314
MASK_PIXEL: 453, 319 #entries    4459
MASK_PIXEL: 470, 330 #entries    4488
MASK_PIXEL: 124, 355 #entries    4909
MASK_PIXEL:  66, 463 #entries    5096
MASK_PIXEL: 483, 401 #entries    5408
MASK_PIXEL:  19, 488 #entries    5720
MASK_PIXEL:  67, 330 #entries    6243
MASK_PIXEL:  41, 436 #entries    6595
MASK_PIXEL: 491, 352 #entries    7052
MASK_PIXEL: 468, 292 #entries    7113
MASK_PIXEL: 491, 438 #entries    7653
MASK_PIXEL: 455, 402 #entries    7875
MASK_PIXEL: 434, 345 #entries    9854
MASK_PIXEL:   3, 375 #entries    9981
MASK_PIXEL: 356, 292 #entries   10653
MASK_PIXEL: 192, 394 #entries   12667
MASK_PIXEL: 458, 306 #entries   14162
MASK_PIXEL: 435, 290 #entries   14800
MASK_PIXEL: 106, 310 #entries   15082
MASK_PIXEL: 442, 323 #entries   15486
MASK_PIXEL: 478, 345 #entries   17019
MASK_PIXEL: 452, 311 #entries   18529
MASK_PIXEL: 437, 312 #entries   20790
MASK_PIXEL: 115, 319 #entries   23453
MASK_PIXEL: 110, 406 #entries   23625
MASK_PIXEL: 491, 369 #entries   30034
MASK_PIXEL: 453, 349 #entries   30359
MASK_PIXEL: 243, 440 #entries   34333
MASK_PIXEL: 102, 401 #entries   35813
MASK_PIXEL: 264, 334 #entries   39271
MASK_PIXEL: 448, 288 #entries   62421
MASK_PIXEL: 107, 331 #entries   74604

MASK_PIXEL: 220, 363 #entries     932
MASK_PIXEL:  92, 479 #entries    1204
MASK_PIXEL: 133, 499 #entries    5844


MASK_PIXEL: 494, 472 #entries    1001
MASK_PIXEL: 240, 357 #entries    1009
MASK_PIXEL: 510, 489 #entries    1011
MASK_PIXEL: 454, 303 #entries    1030
MASK_PIXEL:  39, 476 #entries    1032
MASK_PIXEL: 511, 428 #entries    1040
MASK_PIXEL: 503, 364 #entries    1041
MASK_PIXEL:  38, 477 #entries    1044
MASK_PIXEL: 183, 332 #entries    1044
MASK_PIXEL: 511, 490 #entries    1047
MASK_PIXEL: 507, 297 #entries    1048
MASK_PIXEL: 471, 331 #entries    1055
MASK_PIXEL: 183, 330 #entries    1057
MASK_PIXEL: 502, 362 #entries    1063
MASK_PIXEL: 438, 361 #entries    1070
MASK_PIXEL: 411, 300 #entries    1071
MASK_PIXEL: 499, 292 #entries    1072
MASK_PIXEL:  60, 477 #entries    1082
MASK_PIXEL: 510, 362 #entries    1092
MASK_PIXEL: 503, 489 #entries    1107
MASK_PIXEL: 167, 477 #entries    1109
MASK_PIXEL: 503, 495 #entries    1110
MASK_PIXEL:  61, 476 #entries    1112
MASK_PIXEL: 511, 297 #entries    1119
MASK_PIXEL: 511, 476 #entries    1120
MASK_PIXEL: 511, 425 #entries    1122
MASK_PIXEL: 443, 490 #entries    1125
MASK_PIXEL: 479, 469 #entries    1126
MASK_PIXEL: 479, 468 #entries    1133
MASK_PIXEL: 502, 289 #entries    1133
MASK_PIXEL: 505, 353 #entries    1146
MASK_PIXEL: 151, 364 #entries    1161
MASK_PIXEL: 503, 302 #entries    1167
MASK_PIXEL: 507, 356 #entries    1190
MASK_PIXEL: 498, 369 #entries    1210
MASK_PIXEL: 191, 330 #entries    1214
MASK_PIXEL: 191, 332 #entries    1218
MASK_PIXEL: 195, 436 #entries    1223
MASK_PIXEL: 479, 393 #entries    1226
MASK_PIXEL: 511, 360 #entries    1228
MASK_PIXEL:  52, 414 #entries    1233
MASK_PIXEL: 419, 332 #entries    1233
MASK_PIXEL: 511, 432 #entries    1259
MASK_PIXEL: 511, 364 #entries    1260
MASK_PIXEL: 446, 413 #entries    1262
MASK_PIXEL: 511, 505 #entries    1266
MASK_PIXEL: 151, 349 #entries    1268
MASK_PIXEL: 151, 362 #entries    1270
MASK_PIXEL: 443, 489 #entries    1277
MASK_PIXEL: 419, 349 #entries    1280
MASK_PIXEL: 447, 409 #entries    1284
MASK_PIXEL: 499, 289 #entries    1285
MASK_PIXEL: 415, 458 #entries    1287
MASK_PIXEL: 510, 328 #entries    1328
MASK_PIXEL: 151, 346 #entries    1330
MASK_PIXEL: 510, 361 #entries    1336
MASK_PIXEL: 411, 378 #entries    1337
MASK_PIXEL: 418, 346 #entries    1356
MASK_PIXEL: 418, 361 #entries    1375
MASK_PIXEL: 510, 456 #entries    1378
MASK_PIXEL: 511, 489 #entries    1378
MASK_PIXEL: 411, 381 #entries    1399
MASK_PIXEL: 411, 426 #entries    1404
MASK_PIXEL: 510, 413 #entries    1412
MASK_PIXEL: 479, 396 #entries    1414
MASK_PIXEL: 318, 445 #entries    1420
MASK_PIXEL: 431, 504 #entries    1425
MASK_PIXEL: 182, 477 #entries    1429
MASK_PIXEL: 511, 361 #entries    1433
MASK_PIXEL: 503, 441 #entries    1444
MASK_PIXEL: 494, 360 #entries    1447
MASK_PIXEL: 503, 444 #entries    1447
MASK_PIXEL: 506, 425 #entries    1456
MASK_PIXEL: 439, 362 #entries    1483
MASK_PIXEL: 319, 442 #entries    1485
MASK_PIXEL: 183, 474 #entries    1487
MASK_PIXEL: 166, 477 #entries    1522
MASK_PIXEL: 511, 452 #entries    1540
MASK_PIXEL: 411, 298 #entries    1549
MASK_PIXEL: 479, 426 #entries    1587
MASK_PIXEL: 447, 362 #entries    1599
MASK_PIXEL: 479, 420 #entries    1605
MASK_PIXEL: 443, 332 #entries    1614
MASK_PIXEL: 422, 474 #entries    1630
MASK_PIXEL: 415, 457 #entries    1651
MASK_PIXEL: 422, 477 #entries    1657
MASK_PIXEL: 511, 494 #entries    1662
MASK_PIXEL: 438, 330 #entries    1693
MASK_PIXEL: 499, 361 #entries    1701
MASK_PIXEL: 511, 444 #entries    1702
MASK_PIXEL: 479, 458 #entries    1710
MASK_PIXEL: 419, 364 #entries    1713
MASK_PIXEL: 511, 417 #entries    1740
MASK_PIXEL: 511, 362 #entries    1783
MASK_PIXEL: 438, 367 #entries    1807
MASK_PIXEL: 443, 494 #entries    1821
MASK_PIXEL: 510, 458 #entries    1846
MASK_PIXEL: 419, 494 #entries    1883
MASK_PIXEL: 442, 458 #entries    1905
MASK_PIXEL: 506, 353 #entries    1912
MASK_PIXEL: 415, 362 #entries    1949
MASK_PIXEL: 499, 373 #entries    1965
MASK_PIXEL: 506, 297 #entries    1980
MASK_PIXEL: 502, 441 #entries    2035
MASK_PIXEL: 479, 460 #entries    2043
MASK_PIXEL: 499, 364 #entries    2045
MASK_PIXEL: 418, 362 #entries    2054
MASK_PIXEL: 443, 460 #entries    2055
MASK_PIXEL: 442, 330 #entries    2082
MASK_PIXEL: 511, 508 #entries    2085
MASK_PIXEL: 502, 445 #entries    2118
MASK_PIXEL: 439, 330 #entries    2141
MASK_PIXEL: 464, 318 #entries    2258
MASK_PIXEL: 507, 425 #entries    2288
MASK_PIXEL: 510, 417 #entries    2341
MASK_PIXEL:  11, 425 #entries    2354
MASK_PIXEL: 479, 428 #entries    2359
MASK_PIXEL: 315, 364 #entries    2420
MASK_PIXEL: 443, 362 #entries    2428
MASK_PIXEL: 315, 362 #entries    2432
MASK_PIXEL: 417, 391 #entries    2442
MASK_PIXEL: 497, 356 #entries    2443
MASK_PIXEL: 507, 298 #entries    2499
MASK_PIXEL: 442, 362 #entries    2505
MASK_PIXEL: 511, 412 #entries    2527
MASK_PIXEL: 408, 301 #entries    2561
MASK_PIXEL: 443, 313 #entries    2698
MASK_PIXEL: 484, 289 #entries    2711
MASK_PIXEL: 498, 362 #entries    2774
MASK_PIXEL: 411, 425 #entries    2795
MASK_PIXEL: 498, 361 #entries    2857
MASK_PIXEL: 510, 360 #entries    2976
MASK_PIXEL: 502, 361 #entries    3044
MASK_PIXEL: 511, 492 #entries    3534
MASK_PIXEL: 503, 362 #entries    3681
MASK_PIXEL: 503, 361 #entries    3763
MASK_PIXEL: 511, 420 #entries    3816
MASK_PIXEL: 443, 457 #entries    3825
MASK_PIXEL: 142, 378 #entries    3937
MASK_PIXEL: 511, 460 #entries    4297
MASK_PIXEL: 491, 465 #entries    5338
MASK_PIXEL: 224, 483 #entries    5577
MASK_PIXEL: 458, 385 #entries    5614
MASK_PIXEL: 460, 302 #entries    6496
MASK_PIXEL: 420, 337 #entries    6889
MASK_PIXEL: 264, 354 #entries    7561
MASK_PIXEL: 264, 379 #entries    7941
MASK_PIXEL: 136, 300 #entries    8386
MASK_PIXEL: 432, 306 #entries    9916
MASK_PIXEL: 477, 288 #entries   11021
MASK_PIXEL: 421, 331 #entries   11741
MASK_PIXEL:  44, 318 #entries   11788
MASK_PIXEL: 440, 322 #entries   12158
MASK_PIXEL: 135, 463 #entries   12554
MASK_PIXEL: 436, 367 #entries   12841
MASK_PIXEL: 103, 412 #entries   16002
MASK_PIXEL:  39, 302 #entries   16071
MASK_PIXEL: 271, 432 #entries   16196
MASK_PIXEL: 494, 328 #entries   17757
MASK_PIXEL: 503, 289 #entries   18955
MASK_PIXEL: 418, 289 #entries   20321
MASK_PIXEL: 417, 494 #entries   24773
MASK_PIXEL: 497, 289 #entries   24955
MASK_PIXEL:  35, 332 #entries   29487
MASK_PIXEL: 129, 292 #entries   29654
MASK_PIXEL:  57, 324 #entries   32230
MASK_PIXEL: 131, 341 #entries   33393
MASK_PIXEL: 411, 393 #entries   35554
MASK_PIXEL:  36, 413 #entries   39287
MASK_PIXEL: 479, 388 #entries   39650
MASK_PIXEL: 498, 289 #entries   40520
MASK_PIXEL: 151, 330 #entries   40613
MASK_PIXEL: 283, 298 #entries   42070
MASK_PIXEL: 418, 330 #entries   43666

MASK_PIXEL: 212, 364 #entries    1146
MASK_PIXEL: 242, 357 #entries    1319
MASK_PIXEL:  10, 405 #entries    1468
MASK_PIXEL: 125, 457 #entries    1790
MASK_PIXEL:   7, 296 #entries    2256
MASK_PIXEL: 469, 356 #entries    2478
MASK_PIXEL: 452, 378 #entries    2481
MASK_PIXEL: 302, 315 #entries    2772
MASK_PIXEL: 499, 295 #entries    3188
MASK_PIXEL:   5, 352 #entries    5115
MASK_PIXEL: 208, 304 #entries    5652
MASK_PIXEL:  43, 411 #entries   10811
MASK_PIXEL: 264, 402 #entries   13772
MASK_PIXEL: 223, 295 #entries   15020
MASK_PIXEL: 400, 307 #entries   22722
MASK_PIXEL: 253, 333 #entries   42443
MASK_PIXEL: 219, 297 #entries   44225


