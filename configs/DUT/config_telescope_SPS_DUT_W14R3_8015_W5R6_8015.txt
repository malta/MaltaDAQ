include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W14R3
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 15
SC_IDB: 80
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W14R3/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 132, 457 #entries    1299
MASK_PIXEL: 366, 330 #entries    1906
MASK_PIXEL: 161, 288 #entries    3696
MASK_PIXEL: 304, 440 #entries    4749
MASK_PIXEL: 466, 431 #entries     100
MASK_PIXEL: 282, 339 #entries     112
MASK_PIXEL: 377, 503 #entries     146
MASK_PIXEL:  45, 364 #entries     169
MASK_PIXEL:   4, 345 #entries     187
MASK_PIXEL:   1, 372 #entries     193
MASK_PIXEL: 284, 297 #entries     256
MASK_PIXEL: 480, 330 #entries     303
MASK_PIXEL: 453, 320 #entries     368
MASK_PIXEL: 448, 293 #entries     376
MASK_PIXEL: 440, 295 #entries     414
MASK_PIXEL: 485, 312 #entries     459
MASK_PIXEL: 482, 301 #entries     746
MASK_PIXEL: 321, 295 #entries     895
MASK_PIXEL: 460, 378 #entries     121
MASK_PIXEL: 166, 447 #entries     124
MASK_PIXEL: 501, 365 #entries     129
MASK_PIXEL: 326, 300 #entries     215

MASK_PIXEL: 130, 470 #entries    2957
MASK_PIXEL: 510, 300 #entries    2982
MASK_PIXEL: 129, 427 #entries    3027
MASK_PIXEL: 213, 363 #entries    3047
MASK_PIXEL: 506, 371 #entries    3130
MASK_PIXEL: 292, 438 #entries    3219
MASK_PIXEL: 347, 401 #entries    3223
MASK_PIXEL:   5, 356 #entries    3271
MASK_PIXEL: 332, 374 #entries    3274
MASK_PIXEL: 409, 386 #entries    3504
MASK_PIXEL: 452, 295 #entries    3511
MASK_PIXEL: 484, 467 #entries    3601
MASK_PIXEL:  51, 365 #entries    3698
MASK_PIXEL: 421, 360 #entries    3792
MASK_PIXEL: 507, 339 #entries    3890
MASK_PIXEL: 453, 302 #entries    4063
MASK_PIXEL: 510, 374 #entries    4091
MASK_PIXEL: 472, 302 #entries    4143
MASK_PIXEL:  63, 307 #entries    4328
MASK_PIXEL: 500, 316 #entries    4330
MASK_PIXEL: 494, 364 #entries    4363
MASK_PIXEL: 328, 369 #entries    4453
MASK_PIXEL:  46, 420 #entries    4492
MASK_PIXEL:   4, 303 #entries    4517
MASK_PIXEL: 263, 342 #entries    4793
MASK_PIXEL: 306, 367 #entries    4880
MASK_PIXEL: 482, 313 #entries    5195
MASK_PIXEL: 279, 315 #entries    5586
MASK_PIXEL: 444, 374 #entries    5808
MASK_PIXEL:  19, 314 #entries    6609
MASK_PIXEL: 462, 303 #entries    6672
MASK_PIXEL:  17, 294 #entries    6966
MASK_PIXEL:  10, 411 #entries    6978
MASK_PIXEL: 510, 307 #entries    6991
MASK_PIXEL: 332, 355 #entries    7078
MASK_PIXEL: 468, 293 #entries    7512
MASK_PIXEL: 415, 294 #entries    9356
MASK_PIXEL: 473, 343 #entries   11185
MASK_PIXEL:  10, 418 #entries   12365
MASK_PIXEL:  94, 303 #entries   14877
MASK_PIXEL: 491, 369 #entries   16871
MASK_PIXEL: 289, 331 #entries   27864

MASK_PIXEL: 496, 378 #entries     939
MASK_PIXEL: 454, 490 #entries     944
MASK_PIXEL: 497, 435 #entries     985
MASK_PIXEL: 291, 372 #entries    1028
MASK_PIXEL: 479, 361 #entries    1083
MASK_PIXEL: 301, 305 #entries    1134


###########################################################################################################################


[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W5R6
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 15
SC_IDB: 80
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W5R6/longCable_doubleChip_calib_run//calib_15.txt

MASK_PIXEL: 476, 474 #entries    1010
MASK_PIXEL: 476, 478 #entries    1014
MASK_PIXEL: 436, 329 #entries    1136
MASK_PIXEL: 451, 288 #entries    1137
MASK_PIXEL: 463, 494 #entries    1158
MASK_PIXEL: 462, 490 #entries    1159
MASK_PIXEL: 458, 378 #entries    1204
MASK_PIXEL: 459, 376 #entries    1235
MASK_PIXEL:   0, 274 #entries    1340
MASK_PIXEL: 462, 402 #entries    1411
MASK_PIXEL: 462, 442 #entries    1414
MASK_PIXEL: 463, 406 #entries    1419
MASK_PIXEL:  72, 351 #entries    1490
MASK_PIXEL: 269, 402 #entries    1509
MASK_PIXEL:   0,  34 #entries    1634
MASK_PIXEL: 463, 440 #entries    1697
MASK_PIXEL: 499, 378 #entries    1828
MASK_PIXEL: 498, 378 #entries    1833
MASK_PIXEL: 503, 466 #entries    2118
MASK_PIXEL: 502, 466 #entries    2157
MASK_PIXEL: 144, 490 #entries    2441
MASK_PIXEL: 163, 503 #entries    2502
MASK_PIXEL:   0, 106 #entries    2535
MASK_PIXEL: 148, 434 #entries    2759
MASK_PIXEL:   0, 402 #entries    3065
MASK_PIXEL: 100, 371 #entries    3357
MASK_PIXEL:   0,  18 #entries    3919
MASK_PIXEL: 216, 311 #entries    5252
MASK_PIXEL:   4, 506 #entries    8248
MASK_PIXEL: 135, 312 #entries    9256
MASK_PIXEL: 476, 321 #entries   10178
MASK_PIXEL: 105, 289 #entries   18573
MASK_PIXEL: 472, 350 #entries   21549
MASK_PIXEL: 463, 390 #entries   28923
MASK_PIXEL: 459, 312 #entries   30508
MASK_PIXEL: 499, 338 #entries   44795
MASK_PIXEL: 144, 418 #entries   56869
MASK_PIXEL:   0, 362 #entries  176984
MASK_PIXEL:   4, 402 #entries  202949
MASK_PIXEL:  17, 296 #entries    5054
MASK_PIXEL: 428, 406 #entries    7313
MASK_PIXEL: 465, 392 #entries    7428
MASK_PIXEL:  29, 345 #entries    9172
MASK_PIXEL: 108, 359 #entries   11413

MASK_PIXEL:  59, 297 #entries    1012
MASK_PIXEL: 494, 366 #entries    1207
MASK_PIXEL: 183, 303 #entries    1406
MASK_PIXEL: 468, 296 #entries    1562
MASK_PIXEL: 202, 330 #entries    1576
MASK_PIXEL: 100, 345 #entries    1605
MASK_PIXEL:  28, 460 #entries    1808
MASK_PIXEL:  67, 295 #entries    2204
MASK_PIXEL: 367, 378 #entries    2518
MASK_PIXEL: 418, 304 #entries    2854
MASK_PIXEL: 257, 510 #entries    3954
MASK_PIXEL: 460, 309 #entries    4154
MASK_PIXEL: 458, 411 #entries    4172
MASK_PIXEL: 474, 321 #entries    4766
MASK_PIXEL: 203, 356 #entries    4899
MASK_PIXEL:  72, 456 #entries    5185
MASK_PIXEL: 302, 417 #entries    6854

MASK_PIXEL: 473, 304 #entries    2915
MASK_PIXEL: 416, 296 #entries    2968
MASK_PIXEL: 458, 386 #entries    2988
MASK_PIXEL: 506, 334 #entries    3022
MASK_PIXEL: 478, 343 #entries    3121
MASK_PIXEL: 445, 332 #entries    3334
MASK_PIXEL: 478, 296 #entries    3340
MASK_PIXEL: 504, 361 #entries    3351
MASK_PIXEL: 110, 445 #entries    3552
MASK_PIXEL:  37, 297 #entries    3776
MASK_PIXEL: 453, 322 #entries    3821
MASK_PIXEL:  98, 294 #entries    3845
MASK_PIXEL: 476, 325 #entries    4163
MASK_PIXEL: 219, 398 #entries    4314
MASK_PIXEL: 453, 319 #entries    4459
MASK_PIXEL: 470, 330 #entries    4488
MASK_PIXEL: 124, 355 #entries    4909
MASK_PIXEL:  66, 463 #entries    5096
MASK_PIXEL: 483, 401 #entries    5408
MASK_PIXEL:  19, 488 #entries    5720
MASK_PIXEL:  67, 330 #entries    6243
MASK_PIXEL:  41, 436 #entries    6595
MASK_PIXEL: 491, 352 #entries    7052
MASK_PIXEL: 468, 292 #entries    7113
MASK_PIXEL: 491, 438 #entries    7653
MASK_PIXEL: 455, 402 #entries    7875
MASK_PIXEL: 434, 345 #entries    9854
MASK_PIXEL:   3, 375 #entries    9981
MASK_PIXEL: 356, 292 #entries   10653
MASK_PIXEL: 192, 394 #entries   12667
MASK_PIXEL: 458, 306 #entries   14162
MASK_PIXEL: 435, 290 #entries   14800
MASK_PIXEL: 106, 310 #entries   15082
MASK_PIXEL: 442, 323 #entries   15486
MASK_PIXEL: 478, 345 #entries   17019
MASK_PIXEL: 452, 311 #entries   18529
MASK_PIXEL: 437, 312 #entries   20790
MASK_PIXEL: 115, 319 #entries   23453
MASK_PIXEL: 110, 406 #entries   23625
MASK_PIXEL: 491, 369 #entries   30034
MASK_PIXEL: 453, 349 #entries   30359
MASK_PIXEL: 243, 440 #entries   34333
MASK_PIXEL: 102, 401 #entries   35813
MASK_PIXEL: 264, 334 #entries   39271
MASK_PIXEL: 448, 288 #entries   62421
MASK_PIXEL: 107, 331 #entries   74604

MASK_PIXEL: 220, 363 #entries     932
MASK_PIXEL:  92, 479 #entries    1204
MASK_PIXEL: 133, 499 #entries    5844
