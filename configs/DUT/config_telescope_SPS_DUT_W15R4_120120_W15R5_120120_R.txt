include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W15R4
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 120
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W15R4/longCable_doubleChip_calib_run//calib_15.txt

MASK_DOUBLE_COLUMNS: 0,156
MASK_PIXEL: 456, 299 #entries    4399
MASK_PIXEL: 492, 503 #entries    5263
MASK_PIXEL: 508, 415 #entries    5867
MASK_PIXEL: 509, 411 #entries    5892
MASK_PIXEL: 511, 414 #entries    6613
MASK_PIXEL: 508, 511 #entries   11094
MASK_PIXEL: 509, 507 #entries   11106
MASK_PIXEL: 484, 407 #entries  165456
MASK_PIXEL: 460, 503 #entries  308674
MASK_PIXEL: 317, 395 #entries  361420
###########################################################################################################################
[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W15R5
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 120
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W15R5/longCable_doubleChip_calib_run//calib_15.txt
MASK_DOUBLE_COLUMNS: 0,156
MASK_PIXEL: 319, 394 #entries  134216
MASK_PIXEL: 503, 436 #entries   11369
MASK_PIXEL: 500, 436 #entries   13664
MASK_PIXEL: 336, 296 #entries   17895
MASK_PIXEL: 500, 437 #entries   18995
MASK_PIXEL: 497, 410 #entries   26463
MASK_PIXEL: 499, 409 #entries   27877
MASK_PIXEL: 498, 409 #entries   74632
MASK_PIXEL: 496, 410 #entries   81980
MASK_PIXEL: 499, 410 #entries  126242
MASK_PIXEL: 498, 410 #entries  127979
MASK_PIXEL: 503, 428 #entries   10796
MASK_PIXEL: 502, 435 #entries   11964
MASK_PIXEL: 499, 426 #entries   12068
MASK_PIXEL: 503, 436 #entries   12157
MASK_PIXEL: 503, 426 #entries   12443
MASK_PIXEL: 498, 428 #entries   14307
MASK_PIXEL: 500, 428 #entries   14741
MASK_PIXEL: 498, 435 #entries   15958
MASK_PIXEL: 502, 427 #entries   16341
MASK_PIXEL: 499, 435 #entries   19395
MASK_PIXEL: 499, 428 #entries   29738
MASK_PIXEL: 500, 435 #entries   29829
MASK_PIXEL: 501, 436 #entries   31141
MASK_PIXEL: 498, 427 #entries   33254
MASK_PIXEL: 499, 437 #entries   45777
MASK_PIXEL: 501, 414 #entries   24360
MASK_PIXEL: 497, 408 #entries   78539
MASK_PIXEL: 491, 479 #entries   10777
MASK_PIXEL: 498, 431 #entries   11104
MASK_PIXEL: 499, 431 #entries   11601
MASK_PIXEL: 502, 412 #entries   11909
MASK_PIXEL: 494, 408 #entries   12895
MASK_PIXEL: 492, 408 #entries   13316
MASK_PIXEL: 494, 409 #entries   13357
MASK_PIXEL: 502, 413 #entries   14268
MASK_PIXEL: 503, 413 #entries   15388
MASK_PIXEL: 495, 408 #entries   16467
MASK_PIXEL: 495, 409 #entries   17854
MASK_PIXEL: 500, 412 #entries   18852
MASK_PIXEL: 499, 413 #entries   24907
MASK_PIXEL: 501, 413 #entries   43701
MASK_PIXEL: 493, 480 #entries   12989
MASK_PIXEL: 492, 479 #entries   13559
MASK_PIXEL: 506, 452 #entries   13743
MASK_PIXEL: 493, 479 #entries   28188
MASK_PIXEL: 495, 437 #entries   11062
MASK_PIXEL: 495, 438 #entries   12037
MASK_PIXEL: 495, 436 #entries   14020
MASK_PIXEL: 494, 437 #entries   16426
MASK_PIXEL: 494, 411 #entries    2120
MASK_PIXEL: 490, 468 #entries    2168
MASK_PIXEL: 506, 383 #entries    2434
MASK_PIXEL: 493, 468 #entries    2499
MASK_PIXEL: 502, 444 #entries    2588
MASK_PIXEL: 502, 443 #entries    2878
MASK_PIXEL: 493, 410 #entries    3295
MASK_PIXEL: 495, 477 #entries    3462
MASK_PIXEL: 501, 443 #entries    3682
MASK_PIXEL: 495, 478 #entries    4206
MASK_PIXEL: 492, 409 #entries    4830
MASK_PIXEL: 496, 436 #entries    5119
MASK_PIXEL: 503, 443 #entries    6833
MASK_PIXEL: 502, 442 #entries    7880
MASK_PIXEL: 496, 477 #entries    8167
MASK_PIXEL: 497, 477 #entries    8537
MASK_PIXEL: 494, 438 #entries    9730
MASK_PIXEL: 511, 388 #entries   10842
MASK_PIXEL: 511, 389 #entries   11876
MASK_PIXEL: 506, 411 #entries   11936
