include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W15R4
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 15
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W15R4/longCable_doubleChip_calib_run//calib_15.txt

MASK_DOUBLE_COLUMNS: 0,156
MASK_PIXEL: 456, 299 #entries    4399
MASK_PIXEL: 492, 503 #entries    5263
MASK_PIXEL: 508, 415 #entries    5867
MASK_PIXEL: 509, 411 #entries    5892
MASK_PIXEL: 511, 414 #entries    6613
MASK_PIXEL: 508, 511 #entries   11094
MASK_PIXEL: 509, 507 #entries   11106
MASK_PIXEL: 484, 407 #entries  165456
MASK_PIXEL: 460, 503 #entries  308674
MASK_PIXEL: 317, 395 #entries  361420
MASK_PIXEL: 477, 492 #entries    1026
MASK_PIXEL: 332, 288 #entries    1060
MASK_PIXEL: 325, 325 #entries    1075
MASK_PIXEL: 486, 483 #entries    1409
MASK_PIXEL: 446, 295 #entries    1853
MASK_PIXEL: 497, 350 #entries    1889
MASK_PIXEL: 331, 358 #entries    2033
MASK_PIXEL: 474, 344 #entries    2076
MASK_PIXEL: 473, 384 #entries    2450
MASK_PIXEL: 318, 366 #entries    2632
MASK_PIXEL: 497, 306 #entries    2975
MASK_PIXEL: 388, 290 #entries    4063
MASK_PIXEL: 370, 436 #entries    5993
MASK_PIXEL: 504, 504 #entries    6396
MASK_PIXEL: 365, 389 #entries   11979
MASK_PIXEL: 439, 357 #entries   28309
MASK_PIXEL: 341, 332 #entries   51465
MASK_PIXEL: 464, 353 #entries  248992
###########################################################################################################################
[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W15R5
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 15
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W15R5/longCable_doubleChip_calib_run//calib_15.txt
MASK_DOUBLE_COLUMNS: 0,156
MASK_PIXEL: 319, 394 #entries  134216
MASK_PIXEL: 399, 466 #entries    1470
MASK_PIXEL: 337, 475 #entries    1871
MASK_PIXEL: 346, 363 #entries    2302
MASK_PIXEL: 336, 296 #entries    5350
MASK_PIXEL: 390, 440 #entries    8562
