include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W15R9
SC_ICASN: 2
SC_IBIAS: 20
SC_ITHR: 20
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W15R9/longCable_doubleChip_calib_run//calib_15.txt

MASK_DOUBLE_COLUMNS: 0,206
MASK_PIXEL: 441, 348 #entries  992441
MASK_PIXEL: 392, 476 #entries   65880
MASK_PIXEL: 486, 387 #entries   11733
MASK_PIXEL: 459, 379 #entries  221714
MASK_PIXEL: 485, 451 #entries   10733
MASK_PIXEL: 476, 427 #entries   11246
MASK_PIXEL: 510, 450 #entries   11315
MASK_PIXEL: 510, 449 #entries   11541
MASK_PIXEL: 501, 419 #entries   12209
MASK_PIXEL: 511, 450 #entries   12431
MASK_PIXEL: 508, 475 #entries   14088
MASK_PIXEL: 511, 449 #entries   14550
MASK_PIXEL: 476, 438 #entries   16023
MASK_PIXEL: 508, 385 #entries   16438
MASK_PIXEL: 423, 511 #entries   22495
MASK_PIXEL: 511, 487 #entries   25654
MASK_PIXEL: 504, 382 #entries    1020
MASK_PIXEL: 498, 408 #entries    1041
MASK_PIXEL: 497, 386 #entries    1051
MASK_PIXEL: 511, 415 #entries    1062
MASK_PIXEL: 487, 387 #entries    1085
MASK_PIXEL: 494, 391 #entries    1163
MASK_PIXEL: 505, 382 #entries    1214
MASK_PIXEL: 493, 376 #entries    1327
MASK_PIXEL: 411, 300 #entries    1336
MASK_PIXEL: 484, 387 #entries    1369
MASK_PIXEL: 486, 388 #entries    1376
MASK_PIXEL: 475, 426 #entries    1411
MASK_PIXEL: 477, 424 #entries    1546
MASK_PIXEL: 507, 460 #entries    1572
MASK_PIXEL: 485, 388 #entries    1608
MASK_PIXEL: 485, 386 #entries    1643
MASK_PIXEL: 486, 386 #entries    1764
MASK_PIXEL: 485, 430 #entries    1824
MASK_PIXEL: 491, 485 #entries    1907
MASK_PIXEL: 485, 387 #entries    2021
MASK_PIXEL: 482, 457 #entries    2428
MASK_PIXEL: 481, 458 #entries    2456
MASK_PIXEL: 481, 456 #entries    2900
MASK_PIXEL: 503, 381 #entries    2917
MASK_PIXEL: 482, 458 #entries    3025
MASK_PIXEL: 482, 456 #entries    3143
MASK_PIXEL: 483, 456 #entries    3712
MASK_PIXEL: 472, 469 #entries    3864
MASK_PIXEL: 483, 457 #entries    4491
MASK_PIXEL: 481, 457 #entries    4926
MASK_PIXEL: 484, 464 #entries    3337
MASK_PIXEL: 484, 463 #entries    3365
MASK_PIXEL: 483, 463 #entries    3463
MASK_PIXEL: 491, 389 #entries    3966
MASK_PIXEL: 483, 464 #entries    4569
MASK_PIXEL: 506, 455 #entries    6613
MASK_PIXEL: 508, 455 #entries    7372
MASK_PIXEL: 507, 455 #entries    7856
MASK_PIXEL: 441, 342 #entries    2185
MASK_PIXEL: 496, 389 #entries    1067
MASK_PIXEL: 495, 390 #entries    1212
MASK_PIXEL: 431, 474 #entries    1435
MASK_PIXEL: 418, 365 #entries    1485
MASK_PIXEL: 437, 368 #entries    1524
MASK_PIXEL: 468, 435 #entries    1535
MASK_PIXEL: 428, 386 #entries    1807
MASK_PIXEL: 492, 397 #entries    1920
MASK_PIXEL: 453, 493 #entries    1998
MASK_PIXEL: 482, 429 #entries    1998
MASK_PIXEL: 492, 398 #entries    2409
MASK_PIXEL: 481, 472 #entries    5312
MASK_PIXEL: 480, 472 #entries    6287
MASK_PIXEL: 479, 419 #entries    6569
MASK_PIXEL: 478, 419 #entries    8695
MASK_PIXEL: 477, 427 #entries    8727
MASK_PIXEL: 478, 418 #entries    8763
MASK_PIXEL: 499, 419 #entries    8830
MASK_PIXEL: 481, 471 #entries    9451
MASK_PIXEL: 480, 471 #entries    9663
MASK_PIXEL: 481, 470 #entries    9864
MASK_PIXEL: 497, 451 #entries   12031
MASK_PIXEL: 496, 480 #entries    1008
MASK_PIXEL: 489, 386 #entries    1015
MASK_PIXEL: 507, 406 #entries    1179
MASK_PIXEL: 489, 387 #entries    1224
MASK_PIXEL: 492, 386 #entries    1258
MASK_PIXEL: 494, 480 #entries    1396
MASK_PIXEL: 476, 459 #entries    1420
MASK_PIXEL: 493, 484 #entries    1694
MASK_PIXEL: 506, 411 #entries    1700
MASK_PIXEL: 510, 409 #entries    1911
MASK_PIXEL: 506, 406 #entries    2036
MASK_PIXEL: 503, 382 #entries   13441
MASK_PIXEL: 504, 383 #entries   28717
MASK_PIXEL: 490, 386 #entries    1020
MASK_PIXEL: 498, 390 #entries    1417
MASK_PIXEL: 505, 387 #entries    1724
MASK_PIXEL: 510, 448 #entries    2188
MASK_PIXEL: 495, 483 #entries    2339
MASK_PIXEL: 509, 449 #entries    6710
MASK_PIXEL: 509, 450 #entries    6757
MASK_PIXEL: 496, 484 #entries    6856
MASK_PIXEL: 478, 460 #entries    1281
MASK_PIXEL: 509, 486 #entries   10843
MASK_PIXEL: 477, 453 #entries    1025
MASK_PIXEL: 473, 430 #entries    1105
MASK_PIXEL: 477, 429 #entries    1179
MASK_PIXEL: 476, 454 #entries    1234
MASK_PIXEL: 478, 420 #entries    1286
MASK_PIXEL: 496, 416 #entries    1383
MASK_PIXEL: 488, 378 #entries    1483
MASK_PIXEL: 507, 431 #entries    1524
MASK_PIXEL: 507, 430 #entries    1613
MASK_PIXEL: 476, 428 #entries    1644
MASK_PIXEL: 506, 430 #entries    1955
MASK_PIXEL: 506, 431 #entries    2011
MASK_PIXEL: 485, 419 #entries    1276
MASK_PIXEL: 484, 419 #entries    1429
MASK_PIXEL: 503, 389 #entries    1701
MASK_PIXEL: 505, 390 #entries    3197
MASK_PIXEL: 504, 390 #entries    5961
MASK_PIXEL: 504, 389 #entries    8324
MASK_PIXEL: 503, 390 #entries   16540
MASK_PIXEL: 503, 413 #entries     520
MASK_PIXEL: 501, 402 #entries     844
MASK_PIXEL: 501, 382 #entries     988
MASK_PIXEL: 502, 381 #entries    1807
MASK_PIXEL: 500, 414 #entries    2886
MASK_PIXEL: 501, 381 #entries    4032
MASK_PIXEL: 504, 447 #entries     130
MASK_PIXEL: 503, 467 #entries     150
MASK_PIXEL: 504, 448 #entries     179
MASK_PIXEL: 505, 448 #entries     283
MASK_PIXEL: 505, 449 #entries     339
MASK_PIXEL: 504, 467 #entries     434
MASK_PIXEL: 490, 479 #entries    1032
MASK_PIXEL: 490, 409 #entries    1121
MASK_PIXEL: 490, 478 #entries    1393
MASK_PIXEL: 488, 410 #entries    1857
MASK_PIXEL: 489, 410 #entries    1895
MASK_PIXEL: 499, 394 #entries    2661
###########################################################################################################################

[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W15R10
SC_ICASN: 2
SC_IBIAS: 20
SC_ITHR: 20
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W15R10/longCable_doubleChip_calib_run//calib_15.txt

MASK_DOUBLE_COLUMNS: 0,206
MASK_PIXEL: 443, 345 #entries    5827
MASK_PIXEL: 442, 351 #entries    5888
MASK_PIXEL: 493, 301 #entries    8170
MASK_PIXEL: 492, 303 #entries    8291
MASK_PIXEL: 469, 294 #entries    8717
MASK_PIXEL: 468, 295 #entries    8826
MASK_PIXEL: 480, 295 #entries    2215
MASK_PIXEL: 490, 295 #entries    2699
MASK_PIXEL: 510, 307 #entries    3026
MASK_PIXEL: 421, 289 #entries    3473
MASK_PIXEL: 420, 295 #entries    3564
MASK_PIXEL: 510, 311 #entries    3934
MASK_PIXEL: 332, 510 #entries    4968
MASK_PIXEL: 332, 511 #entries    5014
MASK_PIXEL: 497, 461 #entries   78043
MASK_PIXEL: 496, 463 #entries   78313
MASK_PIXEL: 476, 295 #entries     400
MASK_PIXEL: 504, 319 #entries     408
MASK_PIXEL: 496, 311 #entries     414
MASK_PIXEL: 491, 289 #entries     438
MASK_PIXEL: 508, 327 #entries     443
MASK_PIXEL: 508, 303 #entries     503
MASK_PIXEL: 481, 294 #entries     510
MASK_PIXEL: 490, 295 #entries     624
MASK_PIXEL: 480, 295 #entries     673
MASK_PIXEL: 508, 327 #entries    1061
MASK_PIXEL: 508, 303 #entries    1122
MASK_PIXEL: 496, 311 #entries    1185
MASK_PIXEL: 498, 303 #entries   60053
MASK_PIXEL: 477, 290 #entries   61024
MASK_PIXEL: 506, 440 #entries   85338
MASK_PIXEL: 506, 447 #entries   85502
MASK_PIXEL: 477, 291 #entries  200727
MASK_PIXEL: 476, 295 #entries  261902
MASK_PIXEL: 488, 326 #entries   11252
MASK_PIXEL: 481, 332 #entries   11430
MASK_PIXEL: 483, 294 #entries   12301
MASK_PIXEL: 482, 295 #entries   12394
MASK_PIXEL: 502, 323 #entries   13229
MASK_PIXEL: 502, 327 #entries   13386
MASK_PIXEL: 489, 326 #entries   13767
MASK_PIXEL: 496, 408 #entries   14194
MASK_PIXEL: 496, 415 #entries   14319
MASK_PIXEL: 500, 302 #entries   16968
MASK_PIXEL: 500, 303 #entries   17167
MASK_PIXEL: 480, 335 #entries   18720
MASK_PIXEL: 496, 407 #entries   19479
MASK_PIXEL: 488, 327 #entries   25007
MASK_PIXEL: 488, 319 #entries   10482
MASK_PIXEL: 507, 322 #entries   11082
MASK_PIXEL: 494, 480 #entries   11889
MASK_PIXEL: 494, 487 #entries   11989
MASK_PIXEL: 503, 341 #entries   13597
MASK_PIXEL: 502, 351 #entries   14202
MASK_PIXEL: 502, 343 #entries   16196
MASK_PIXEL: 506, 327 #entries   20292
MASK_PIXEL: 496, 365 #entries   10466
MASK_PIXEL: 476, 299 #entries   10904
MASK_PIXEL: 477, 299 #entries   11183
MASK_PIXEL: 498, 375 #entries   11249
MASK_PIXEL: 497, 365 #entries   14394
MASK_PIXEL: 476, 303 #entries   24040
MASK_PIXEL: 490, 308 #entries   24282
MASK_PIXEL: 490, 311 #entries   24367
MASK_PIXEL: 496, 367 #entries   24791
MASK_PIXEL: 488, 319 #entries   15317
MASK_PIXEL: 511, 327 #entries   26225
MASK_PIXEL: 510, 327 #entries   28150
MASK_PIXEL: 496, 462 #entries   31573
MASK_PIXEL: 496, 461 #entries  327323
MASK_PIXEL: 496, 463 #entries  358920
MASK_PIXEL: 479, 510 #entries     975
MASK_PIXEL: 495, 422 #entries    1099
MASK_PIXEL: 478, 511 #entries    1109
MASK_PIXEL: 494, 423 #entries    1162
MASK_PIXEL: 497, 508 #entries    1327
MASK_PIXEL: 483, 483 #entries    1345
MASK_PIXEL: 496, 511 #entries    1355
MASK_PIXEL: 482, 487 #entries    1416
MASK_PIXEL: 489, 315 #entries    2969
MASK_PIXEL: 489, 314 #entries    3857
MASK_PIXEL: 496, 356 #entries    4639
MASK_PIXEL: 496, 359 #entries    4720
MASK_PIXEL: 502, 353 #entries    5951
MASK_PIXEL: 502, 359 #entries    6012
MASK_PIXEL: 488, 319 #entries    6869
MASK_PIXEL: 503, 302 #entries   14434
MASK_PIXEL: 502, 303 #entries   16543
MASK_PIXEL: 507, 406 #entries   70891
MASK_PIXEL: 506, 407 #entries   71018
