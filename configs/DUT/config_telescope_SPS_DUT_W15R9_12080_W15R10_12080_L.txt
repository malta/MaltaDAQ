include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W15R9
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 80
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W15R9/longCable_doubleChip_calib_run//calib_15.txt

MASK_DOUBLE_COLUMNS: 100,256
MASK_PIXEL: 136, 386 #entries   38525
MASK_PIXEL:  40, 308 #entries    1541
MASK_PIXEL:  42, 308 #entries    2031
MASK_PIXEL:  41, 309 #entries    3243
MASK_PIXEL:  41, 308 #entries    3698
###########################################################################################################################
[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W15R10
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 80
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W15R10/longCable_doubleChip_calib_run//calib_15.txt
MASK_DOUBLE_COLUMNS: 100,256
MASK_PIXEL:  44, 353 #entries    5592
MASK_PIXEL:  44, 359 #entries    5600
