include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W18R10
SC_ICASN: 2
SC_IBIAS: 43
SC_ITHR: 90
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W18R10/longCable_doubleChip_calib_run//calib_15.txt


MASK_DOUBLE_COLUMNS: 0,32
MASK_DOUBLE_COLUMNS: 224,256
MASK_PIXEL: 511, 508 #entries   10306
MASK_PIXEL: 511, 509 #entries   14124
MASK_PIXEL: 511, 507 #entries   14350
MASK_PIXEL: 510, 504 #entries   14495
MASK_PIXEL: 511, 510 #entries   15818
MASK_PIXEL: 510, 509 #entries   16661
MASK_PIXEL: 511, 506 #entries   17167
MASK_PIXEL: 510, 508 #entries   17515
MASK_PIXEL: 511, 511 #entries   18337
MASK_PIXEL: 510, 511 #entries   19599
MASK_PIXEL: 510, 505 #entries   19785
MASK_PIXEL: 510, 510 #entries   21172
MASK_PIXEL: 511, 505 #entries   22452
MASK_PIXEL: 307, 500 #entries     125
MASK_PIXEL: 364, 506 #entries     140
MASK_PIXEL: 387, 500 #entries     175
MASK_PIXEL: 365, 509 #entries     176
MASK_PIXEL: 382, 498 #entries     415
MASK_PIXEL: 359, 474 #entries     444
MASK_PIXEL: 383, 496 #entries     463
MASK_PIXEL: 383, 509 #entries    4462
MASK_PIXEL: 383, 504 #entries    4541
MASK_PIXEL: 352, 498 #entries   23133
MASK_PIXEL: 301, 509 #entries  231949
MASK_PIXEL: 383, 448 #entries  727646
MASK_PIXEL: 386, 383 #entries    1243
MASK_PIXEL: 244, 415 #entries    1369
MASK_PIXEL: 404, 422 #entries    8254

MASK_PIXEL: 410, 338 #entries   21896
MASK_PIXEL: 223, 440 #entries   53652


###########################################################################################################################

[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W18R9
SC_ICASN: 2
SC_IBIAS: 43
SC_ITHR: 90
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W18R9/longCable_doubleChip_calib_run//calib_15.txt

MASK_DOUBLE_COLUMNS: 0,32
MASK_DOUBLE_COLUMNS: 224,256
MASK_PIXEL: 511, 441 #entries   10485
MASK_PIXEL: 510, 445 #entries   11883
MASK_PIXEL: 510, 379 #entries   11979
MASK_PIXEL: 511, 380 #entries   12173
MASK_PIXEL: 510, 380 #entries   12263
MASK_PIXEL: 510, 378 #entries   15600
MASK_PIXEL: 510, 381 #entries   16111
MASK_PIXEL: 511, 379 #entries   16851
MASK_PIXEL: 511, 377 #entries   16855
MASK_PIXEL: 510, 377 #entries   19470
MASK_PIXEL: 511, 381 #entries   20061
MASK_PIXEL: 511, 383 #entries   20302
MASK_PIXEL: 511, 376 #entries   20342
MASK_PIXEL: 510, 376 #entries   20854
MASK_PIXEL: 511, 378 #entries   23479
MASK_PIXEL: 511, 510 #entries   43214
MASK_PIXEL: 510, 511 #entries   46539
MASK_PIXEL: 510, 507 #entries   47018
MASK_PIXEL: 510, 510 #entries   54645
MASK_PIXEL: 511, 508 #entries   56217
MASK_PIXEL: 510, 506 #entries   60679
MASK_PIXEL: 511, 507 #entries   66682
MASK_PIXEL: 510, 508 #entries   71361
MASK_PIXEL: 511, 506 #entries   85509
MASK_PIXEL: 511, 511 #entries   85988
MASK_PIXEL: 511, 505 #entries   86235
MASK_PIXEL: 511, 504 #entries   86429
MASK_PIXEL: 510, 505 #entries   90713
MASK_PIXEL: 511, 509 #entries   92534
MASK_PIXEL: 510, 509 #entries   95074
MASK_PIXEL: 510, 504 #entries  100430
MASK_PIXEL: 385, 305 #entries    1187
MASK_PIXEL: 383, 444 #entries    3188
MASK_PIXEL: 268, 333 #entries   10523
MASK_PIXEL: 385, 305 #entries    1187
MASK_PIXEL: 383, 444 #entries    3188
MASK_PIXEL: 268, 333 #entries   10523
MASK_PIXEL: 411, 309 #entries    1112
MASK_PIXEL: 256, 323 #entries    1140
MASK_PIXEL: 416, 288 #entries    1765
MASK_PIXEL: 434, 288 #entries    2050
MASK_PIXEL: 423, 311 #entries    2251
MASK_PIXEL: 428, 288 #entries    3122
MASK_PIXEL: 424, 288 #entries    3421
MASK_PIXEL: 385, 288 #entries    4306
MASK_PIXEL: 444, 288 #entries    4392
MASK_PIXEL: 399, 288 #entries    6642
MASK_PIXEL: 405, 363 #entries    6855
MASK_PIXEL: 436, 324 #entries   12195
MASK_PIXEL: 400, 289 #entries   20724
MASK_PIXEL: 421, 288 #entries  129109

MASK_PIXEL: 438, 511 #entries    1135
MASK_PIXEL: 438, 505 #entries    1187
MASK_PIXEL: 410, 314 #entries    1366
MASK_PIXEL:  95, 333 #entries    1413
MASK_PIXEL:  97, 289 #entries    1514
MASK_PIXEL: 131, 290 #entries    2132
MASK_PIXEL: 125, 291 #entries    2850
MASK_PIXEL: 418, 301 #entries   16480
MASK_PIXEL: 103, 447 #entries   17400
MASK_PIXEL: 119, 301 #entries   20589
MASK_PIXEL: 134, 357 #entries   21198
MASK_PIXEL: 440, 293 #entries   66162
MASK_PIXEL: 422, 341 #entries  100032
MASK_PIXEL: 422, 455 #entries  187749
MASK_PIXEL: 438, 377 #entries  399489

