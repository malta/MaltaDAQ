include: configs/DUT/TelescopePlanes.txt
[PLANE7]
address: udp://ep-ade-gw-02:50007 
type: MALTA2 
sample: W18R12 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 30 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W18R12/chipName_calib_run//calib_15.txt

MASK_PIXEL: 496, 297 # entries 68 rate 87.1795 kHz word 	 
MASK_PIXEL: 478, 410 # entries 2 rate 0.25641 kHz word 	 
MASK_PIXEL: 473, 293 # entries 34 rate 0.435897 kHz word 	 
MASK_PIXEL: 475, 289 # entries 29 rate 0.371795 kHz word 	 
MASK_PIXEL: 10, 324 # entries 77 rate 0.98718 kHz word 	 
MASK_PIXEL: 493, 395 # entries 22 rate 0.282051 kHz word 	 
MASK_PIXEL: 474, 511 # entries 16 rate 0.205128 kHz word 	 
MASK_PIXEL: 482, 444 # entries 11 rate 0.141026 kHz word 	 
MASK_PIXEL: 495, 398 # entries 15 rate 0.192308 kHz word 	 
MASK_PIXEL: 484, 303 # entries 10 rate 0.128205 kHz word 	 
MASK_PIXEL: 36, 373 # entries 39 rate 0.078 kHz word 	 
MASK_PIXEL: 495, 496 # entries 186 rate 0.372 kHz word 	 
MASK_PIXEL: 475, 298 # entries 27 rate 0.054 kHz word 	 
MASK_PIXEL: 498, 505 # entries 27 rate 0.054 kHz word 	 
MASK_PIXEL: 505, 406 # entries 50 rate 0.1 kHz word 	 
MASK_PIXEL: 508, 395 # entries 44 rate 0.088 kHz word 	 
MASK_PIXEL: 490, 375 # entries 24 rate 0.048 kHz word 	 
MASK_PIXEL: 10, 358 # entries 105 rate 0.21 kHz word 	 
MASK_PIXEL: 21, 436 # entries 161 rate 0.322 kHz word 	 
MASK_PIXEL: 504, 417 # entries 260 rate 0.52 kHz word 	 
MASK_PIXEL: 508, 388 # entries 260 rate 0.52 kHz word 	 
MASK_PIXEL: 503, 398 # entries 34 rate 0.068 kHz word 	 
################################################################################################################################################
[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2 
sample: W18R14 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 30 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W18R14/chipName_calib_run//calib_15.txt

MASK_PIXEL: 484, 311 # entries 144 rate 0.475185 kHz word 	 
MASK_PIXEL: 485, 320 # entries 45 rate 0.148495 kHz word 	 
