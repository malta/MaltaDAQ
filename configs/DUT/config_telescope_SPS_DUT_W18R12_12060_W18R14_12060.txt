include: configs/DUT/TelescopePlanes.txt
[PLANE7]
address: udp://ep-ade-gw-02:50007 
type: MALTA2 
sample: W18R12 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 60 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W18R12/chipName_calib_run//calib_15.txt

MASK_PIXEL: 496, 297 # entries 68 rate 109.677 kHz word 	 
MASK_PIXEL: 480, 352 # entries 30 rate 0.06 kHz word 	 
################################################################################################################################################
[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2 
sample: W18R14 
SC_ICASN: 2 
SC_IBIAS: 43 
SC_ITHR: 60 
SC_IDB: 120 
SC_VCASN: 110 
SC_VRESET_P: 29 
SC_VRESET_D: 65 
SC_VCLIP: 125 
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W18R14/chipName_calib_run//calib_15.txt

