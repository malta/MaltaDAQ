include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W18R19
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 40
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W18R19/longCable_doubleChip_calib_run//calib_15.txt

###########################################################################################################################

[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W18R21
SC_ICASN: 2
SC_IBIAS: 43
SC_ITHR: 40
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W18R21/longCable_doubleChip_calib_run//calib_15.txt
MASK_DOUBLE_COLUMNS:135,256
MASK_PIXEL: 168, 317 #entries   10867
MASK_PIXEL: 102, 404 #entries   14643
MASK_PIXEL: 127, 510 #entries   17718
MASK_PIXEL: 134, 498 #entries   53956
MASK_PIXEL: 171, 407 #entries   63976
MASK_PIXEL:  99, 476 #entries   82534
MASK_PIXEL: 178, 398 #entries   89873
MASK_PIXEL:  58, 482 #entries   94950
MASK_PIXEL: 163, 509 #entries  142998
MASK_PIXEL: 167, 423 #entries  163704
MASK_PIXEL: 132, 419 #entries  170464
MASK_PIXEL: 151, 505 #entries    1481
MASK_PIXEL: 188, 445 #entries    1920
MASK_PIXEL:  95, 371 #entries    2744
MASK_PIXEL:   3, 434 #entries    4251
MASK_PIXEL: 188, 485 #entries    4907
MASK_PIXEL: 170, 509 #entries    6560
MASK_PIXEL: 105, 422 #entries   10773
MASK_PIXEL: 154, 503 #entries   34966
MASK_PIXEL: 245, 498 #entries   55584
MASK_PIXEL: 111, 505 #entries   13282
MASK_PIXEL: 110, 511 #entries   13503
MASK_PIXEL:  37, 497 #entries  472497
MASK_PIXEL: 106, 511 #entries  491708
