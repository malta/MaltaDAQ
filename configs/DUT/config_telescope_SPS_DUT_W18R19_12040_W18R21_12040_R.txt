include: configs/DUT/TelescopePlanes.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W18R19
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 40
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W18R19/longCable_doubleChip_calib_run//calib_15.txt

###########################################################################################################################

[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W18R21
SC_ICASN: 2
SC_IBIAS: 43
SC_ITHR: 40
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W18R21/longCable_doubleChip_calib_run//calib_15.txt
MASK_DOUBLE_COLUMNS:0,128
MASK_PIXEL: 511, 507 #entries   10416
MASK_PIXEL: 408, 487 #entries   16733
MASK_PIXEL: 371, 508 #entries   18900
MASK_PIXEL: 428, 426 #entries   24329
MASK_PIXEL: 356, 347 #entries   25854
MASK_PIXEL: 383, 420 #entries   28936
MASK_PIXEL: 431, 438 #entries   33274
MASK_PIXEL: 379, 463 #entries   38903
MASK_PIXEL: 463, 403 #entries   48268
MASK_PIXEL: 370, 489 #entries   52548
MASK_PIXEL: 459, 495 #entries   64788
MASK_PIXEL: 429, 427 #entries   79342
MASK_PIXEL: 453, 379 #entries   94986
MASK_PIXEL: 451, 461 #entries   98727
MASK_PIXEL: 442, 361 #entries  108928
MASK_PIXEL: 469, 379 #entries  121174
MASK_PIXEL: 399, 450 #entries    1153
MASK_PIXEL: 373, 467 #entries    1444
MASK_PIXEL: 444, 434 #entries    1549
MASK_PIXEL: 495, 391 #entries    1580
MASK_PIXEL: 402, 501 #entries    1897
MASK_PIXEL: 448, 511 #entries    2540
MASK_PIXEL: 442, 478 #entries    4000
MASK_PIXEL: 397, 431 #entries    6992
MASK_PIXEL: 500, 500 #entries   10096
MASK_PIXEL: 400, 467 #entries   10905
MASK_PIXEL: 415, 436 #entries   13929
MASK_PIXEL: 434, 447 #entries   22895
MASK_PIXEL: 479, 490 #entries   35626
MASK_PIXEL: 358, 408 #entries   45982
MASK_PIXEL: 465, 382 #entries   46823
MASK_PIXEL: 442, 447 #entries   72277
MASK_PIXEL: 391, 451 #entries   82684
MASK_PIXEL: 384, 490 #entries  152473
MASK_PIXEL: 320, 296 #entries  183286
MASK_PIXEL: 405, 501 #entries  218169
MASK_PIXEL: 511, 507 #entries    1045
MASK_PIXEL: 510, 505 #entries    3272
MASK_PIXEL: 510, 495 #entries    3562
MASK_PIXEL: 440, 505 #entries    3855
MASK_PIXEL: 440, 511 #entries    3896
MASK_PIXEL: 511, 509 #entries    3968
MASK_PIXEL: 511, 493 #entries    4126
MASK_PIXEL: 407, 446 #entries   13030
MASK_PIXEL: 351, 475 #entries   40097
MASK_PIXEL: 378, 396 #entries   47352
MASK_PIXEL: 495, 485 #entries  258711
MASK_PIXEL: 408, 377 #entries  291040
MASK_PIXEL: 432, 495 #entries  306227
MASK_PIXEL: 510, 501 #entries    5384
MASK_PIXEL: 500, 485 #entries    6430
MASK_PIXEL: 510, 496 #entries    7845
MASK_PIXEL: 510, 503 #entries   13147
MASK_PIXEL: 420, 453 #entries  265836
MASK_PIXEL: 346, 503 #entries  310865
MASK_PIXEL: 500, 480 #entries  387985
MASK_PIXEL: 393, 482 #entries   46242
