include: configs/DUT/TelescopePlanes.txt

[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W5R23
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 40
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W5R24/longCable_doubleChip_calib_run//calib_15.txt
