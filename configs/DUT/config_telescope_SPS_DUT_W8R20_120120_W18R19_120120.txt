
[PLANE1]
address: udp://ep-ade-gw-02:50001
type: MALTAC
sample: W4R12
configvoltage: 0.8
SC_ICASN: 10
SC_IBIAS: 80
SC_ITHR: 70
SC_IDB: 100
SC_IRESET: 10
SC_VCASN: 64
SC_VRESET_P: 45
SC_VRESET_D: 65
SC_VCLIP: 127

TAP_FROM_TXT: /home/sbmuser/MaltaSW/MaltaDAQ/Results_TapCalib/W4R12/SPS_W4R12_calib_run/calib_15.txt

MASK_PIXEL: 458, 492 #entries   50002
MASK_PIXEL: 404, 407 #entries    1377
MASK_PIXEL: 444, 386 #entries    4274
MASK_PIXEL: 210, 269 #entries    1340
MASK_PIXEL: 116,  73 #entries    1835
MASK_PIXEL: 281, 274 #entries    2101

###########################################
[PLANE2]

address: udp://ep-ade-gw-02:50002
type: MALTAC
sample: W9R11
configvoltage: 0.8

SC_ICASN: 10
SC_IBIAS: 80
SC_ITHR: 70
SC_IDB: 100
SC_IRESET: 10
SC_VCASN: 64
SC_VRESET_P: 45
SC_VRESET_D: 65
SC_VCLIP: 127

TAP_FROM_TXT: /home/sbmuser/MaltaSW/MaltaDAQ/Results_TapCalib/W9R11/W9R11_calib_run/calib_15.txt
#ROI: 134,230,175,270    # still ROI from plane W7R6 
ROI: 0,150,512,450      # still ROI from plane W7R6

MASK_PIXEL:  57, 324 #entries     237
MASK_PIXEL: 407, 183 #entries     470
MASK_PIXEL: 414, 110 #entries     487
MASK_PIXEL: 243, 319 #entries     731
#MASK_PIXEL: 276, 451 #entries    1098
MASK_PIXEL: 438, 397 #entries    1165
MASK_PIXEL: 315, 141 #entries    1747
MASK_PIXEL: 277, 229 #entries    1996
MASK_PIXEL:  32, 225 #entries    3074
#MASK_PIXEL: 488,  83 #entries    4433
#MASK_PIXEL: 455,  28 #entries    5923
MASK_PIXEL: 263, 421 #entries   16986
MASK_PIXEL: 405, 229 #entries   28461

###############################################

[PLANE3]

address: udp://ep-ade-gw-02:50003
type: MALTAC
sample: W7R12Cz
configvoltage: 0.8
SC_ICASN: 10
SC_IBIAS: 80
SC_ITHR: 70
SC_IDB: 100
SC_IRESET: 10
SC_VCASN: 64
SC_VRESET_P: 45
SC_VRESET_D: 65
SC_VCLIP: 127

TAP_FROM_TXT: /home/sbmuser/MaltaSW/MaltaDAQ/Results_TapCalib/W7R12/SPS_W7R12_calib_run/calib_15.txt

MASK_PIXEL: 504, 212 #entries   64657
MASK_PIXEL: 401, 139 #entries   66712
MASK_PIXEL: 441,  80 #entries   74181
MASK_PIXEL: 477, 177 #entries   18600
MASK_PIXEL: 384,  18 #entries   12448
MASK_PIXEL: 359, 347 #entries   14411
MASK_PIXEL: 425, 368 #entries   15914

##############################################


[PLANE4]

address: udp://ep-ade-gw-02:50004
type: MALTAC
sample: W7R11
configvoltage: 0.8

SC_ICASN: 10
SC_IBIAS: 80
SC_ITHR: 70
SC_IDB: 100
SC_IRESET: 10
SC_VCASN: 64
SC_VRESET_P: 45
SC_VRESET_D: 65
SC_VCLIP: 127

TAP_FROM_TXT: /home/sbmuser/MaltaSW/MaltaDAQ/Results_TapCalib/W7R11/SPS_W7R11_calib_run/calib_15.txt

MASK_PIXEL: 341, 169 #entries    7183
MASK_PIXEL: 370, 403 #entries   11919
MASK_PIXEL: 271, 310 #entries   40142
MASK_PIXEL:  42, 306 #entries    9896
MASK_PIXEL: 447, 146 #entries   15589

###########################################

[PLANE5]
address: udp://ep-ade-gw-02:50005
type: MALTAC
sample: W10R1
configvoltage: 0.8
SC_ICASN: 10
SC_IBIAS: 80
SC_ITHR: 70
SC_IDB: 100
SC_IRESET: 10
SC_VCASN: 64
SC_VRESET_P: 45
SC_VRESET_D: 65
SC_VCLIP: 127

ROI: 0,150,512,400 
#ROI: 100,265,135,305

TAP_FROM_TXT: /home/sbmuser/MaltaSW/MaltaDAQ/Results_TapCalib/W10R1/SPS_W10R1_calib_run/calib_15.txt

MASK_PIXEL: 322, 111 #entries     207  NOT IN ROT
##MASK_PIXEL: 103, 218 #entries     243  NOT IN ROI

###############################################

[PLANE6]
address: udp://ep-ade-gw-02:50006
type: MALTAC
sample: W7R0
configvoltage: 0.8
SC_ICASN:  8
SC_IBIAS: 80
SC_ITHR: 70
SC_IDB: 100
SC_IRESET: 10
SC_VCASN: 64
SC_VRESET_P: 45
SC_VRESET_D: 65
SC_VCLIP: 127


#ROI: 100,100,400,400
TAP_FROM_TXT: /home/sbmuser/MaltaSW/MaltaDAQ/Results_TapCalib/W7R0/SPS_W7R0_calib_run/calib_15.txt

MASK_PIXEL: 335, 351 #entries   10203
MASK_PIXEL: 320, 345 #entries   13552
MASK_PIXEL: 510, 453 #entries   28791
MASK_PIXEL:  54, 147 #entries   15575
MASK_PIXEL: 256, 341 #entries   19183

MASK_PIXEL: 314, 317 #entries   14570
MASK_PIXEL: 328, 483 #entries   24703
MASK_PIXEL:  79, 419 #entries   38704
MASK_PIXEL: 469, 448 #entries   45094
MASK_PIXEL: 304, 207 #entries   13076
MASK_PIXEL: 351, 483 #entries   16719
MASK_PIXEL: 295,  55 #entries   18479

#################################################

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W8R20
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 40
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W8R20/longCable_doubleChip_calib_run//calib_15.txt

###########################################################################################################################

[PLANE8]
address: udp://ep-ade-gw-02:50008
type: MALTA2
sample: W18R19
SC_ICASN: 10
SC_IBIAS: 43
SC_ITHR: 40
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib/W18R19/longCable_doubleChip_calib_run//calib_15.txt
