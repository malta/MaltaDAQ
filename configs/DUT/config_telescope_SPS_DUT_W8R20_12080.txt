include: configs/DUT/TelescopePlanes.txt
#include: configs/TelescopePlanes/SPS_Telescope_noROI.txt

[PLANE7]
address: udp://ep-ade-gw-02:50007
type: MALTA2
sample: W8R20
SC_ICASN: 2
SC_IBIAS: 20
SC_ITHR: 80
SC_IDB: 120
SC_VCASN: 110
SC_VRESET_P: 29
SC_VRESET_D: 65
SC_VCLIP: 125
TAP_FROM_TXT: /home/sbmuser/MaltaSW/Malta2/Results_TapCalib//W8R20/W8R20_calib_run//calib_15.txt




