#!/usr/bin/env python

import os
import mysql.connector
import socket
import time
import subprocess
from collections import namedtuple
from re import match
import argparse


parser = argparse.ArgumentParser(
    description='''python -q SELECT runNumber from MALTATBSPS.configuration WHERE sample = 'W15R0'  ; ''')
parser.add_argument("-c", "--chip"     , help="chip"  , required=True)
parser.add_argument("-r", "--rangerun"     , help="range runs ; ", default='0,100000000000000'  )
args=parser.parse_args()

#SELECT DISTINCT ITHR, IDB, SUB  from MALTATBSPS.configuration WHERE sample = 'W14R10';#
#example query: "SELECT runNumber from MALTATBSPS.configuration WHERE sample = 'W15R0'  ;"

range_min, range_max= args.rangerun.split(',')

host_args = {
  "host": "dbod-adepix",
  "user": "sbmuser",
  "port": 5504,
  "database": "MALTATBSPS",
  "password": "testbeam"
}
con = mysql.connector.connect(**host_args)
cur = con.cursor(dictionary=True)
query = "SELECT DISTINCT ITHR, IDB, SUB  from MALTATBSPS.configuration WHERE sample = '%s';" % (args.chip)#, int(range_min), int(range_max)) #"SELECT runNumber from MALTATBSPS.configuration WHERE sample = 'W15R0'  ;"
#print (query)

try:
  cur.execute(query)
  result_all=cur.fetchall()
    #con.close()
    # disconnecting from server
except:
  print("Run number already exists in the database or wrong run type check both, exit(0)")
  con.close()
  exit(0)

#print(result_all) 

organise_runs= []
for result_set in result_all:
  #print(result_set)
  
  query_runs="SELECT runNumber from MALTATBSPS.configuration WHERE sample = '%s' and IDB = %i and ITHR = %i and SUB = %i  and runNumber >= %i and runNumber <= %i" % (args.chip, result_set['IDB'], result_set['ITHR'],result_set['SUB'], int(range_min), int(range_max))
  #print(query_runs)


  try:
    cur.execute(query_runs)
    result_allruns=cur.fetchall()
      #con.close()
      # disconnecting from server
  except:
    print("Run number already exists in the database or wrong run type check both, exit(0)")
    con.close()
    exit(0)

  run_list = []
  for i in result_allruns:
    run_list.append(i['runNumber'])
  #print(result_set)
  #print ("sample =  %i " %  (result_set['IDB']))#, result_set['ITHR'],result_set['SUB'] ))
  print ("sample = '%s', IDB = %i, ITHR = %i, SUB = %i, runs %s " %  (args.chip, result_set['IDB'], result_set['ITHR'],result_set['SUB'] ,str(run_list)))
  organise_runs.append('["%s__IDB%03i_ITHR%03i_SUB%02i_PWELL06", %s]' % (args.chip, result_set['IDB'], result_set['ITHR'],result_set['SUB'],str(run_list) ))


print ("for Organise runs")
for line in  organise_runs:
  print(line)


