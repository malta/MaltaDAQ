import os
import mysql.connector
import socket
import time
import subprocess
from collections import namedtuple
from re import match
_runType =""
""" This part contains the defined functions for the automatic run of the Telescope. They are defined as global variables so they can be used in multiple places.   """
#_cmdtlu = "old_versions/2022/TLU_cmdv2_andrea.py -c SPS_v2.py"
_cmdtlu = "TLU_cmd.py"

def TLUcycle():
  print("TLUcycle() has to be checked...")
  for i in range(0,minutes):
    print("\033[1;34mtriggers left: "+str(triggers-i)+"\033[0m")
    os.system("python /home/sbmuser/MaltaSW/MaltaTLU/share/%s --stop" % _cmdtlu)
    os.system("python /home/sbmuser/MaltaSW/MaltaTLU/share/%s --start -v --counter -d 1" % _cmdtlu)
    #os.system("python /home/sbmuser/MaltaSW/MaltaTLU/share/%s --start --counter -n 10000" % _cmdtlu)
    os.system("python /home/sbmuser/MaltaSW/MaltaTLU/share/%s --stop" % _cmdtlu)
    time.sleep(1)
    resetL1Triggers()
    time.sleep(1)

def resetL1Triggers():
  s = socket.socket()
  s.connect(('127.0.0.1',8080))
  #//str = raw_input("S: ")
  s.send('RESET'.encode())
  print (s.recv(1024).decode())
  s.close()

def check_if_string_in_file():

    with open("/tmp/analysis.log", 'r') as read_obj:
        for line in read_obj:
            if "first check: BAD" in line:
                return 0
            if "first check: WARNING" in line:
                return 0
            if "first check: GOOD" in line:
       	        return 1
    return 1

def check_if_run_exists(run):
   if os.path.exists("TelescopeData/run_%06i_0.root.root" % (run)): 
     for i in range(0,10):
       print("\033[1;31mroot file already exists press Ctrl+C to stop the data taking! the run will start in "+str(10-i)+"s\033[0m")
       time.sleep(1)



def run_point(sub,sub2, config, runNumber, triggers):
    
    check_if_run_exists(runNumber)
    
    os.system("MALTA_PSU.py -c rampVoltage DUT1_SUB "+str(sub)+" 10")
    os.system("MALTA_PSU.py -c rampVoltage DUT2_SUB "+str(sub2)+" 10")

    print("STOP THE RUN FIRST")
    os.system("python /home/sbmuser/MaltaSW/MaltaTLU/share/%s --stop -r %i" % (_cmdtlu,runNumber))

    print("START MaltaMultiDAQ")
    cmd="MaltaMultiDAQ -c %s -r %i > %s/MaltaMULTIDAQ%i.log &" % (config, runNumber, os.getenv("MALTA_LOGS_PATH"), runNumber)
    print(cmd)
    os.system(cmd)
    
    #time.sleep(30)
    time.sleep(60)
    #time.sleep(100)
    print("START the TLU")
    cmdtemp="python /home/sbmuser/MaltaSW/MaltaTLU/share/" + _cmdtlu + " --start --counter -t "  + str(triggers) + " -r " + str(runNumber)
    print(cmdtemp)
    os.system(cmdtemp)
    os.system("python /home/sbmuser/MaltaSW/MaltaTLU/share/%s --stop -r %i" % (_cmdtlu,runNumber))
    #TLUcycle()

    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")

    time.sleep(20) 

    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    os.system("ps -ef | grep 'MaltaMulti' | grep -v grep | awk '{print $2}' | xargs -r kill -SIGTERM")
    time.sleep(20) 
    
    #os.system('rm  /tmp/analysis.log')
    #global _runType
    #print(_runType)
    #if _runType=='Sr90':
    #   os.system('ssh sbmuser@pcatlidps08 ". ~/MaltaSW/setup.sh; cd /home/sbmuser/MaltaSW/MaltaTbAnalysis/Sr90_tests/; python MaltaDQ_Sr90_test.py -r "'+str(runNumber)+"> /tmp/analysis.log")
    #elif _runType=='muons': 
    #   os.system('ssh sbmuser@pcatlidps08 ". ~/MaltaSW/setup.sh; cd  /home/sbmuser/MaltaSW/MaltaTbAnalysis/cosmics_tests/; python MaltaDQ_cosmics_tests.py -r "'+str(runNumber)+"> /tmp/analysis.log")
    if _runType=='SPS':
       #os.system('ssh sbmuser@pcatlidps16 ". ~/MaltaSW/setup.sh; cd /home/sbmuser/MaltaSW/MaltaTbAnalysis/SPS_2023/; python MaltaDQ_SPS_PS15.py -a -t -p -c -m -d -r "'+str(runNumber)+"> log/analysis.log &")
       os.system('ssh sbmuser@pcatlidps16 -4 ". ~/MaltaSW/setup.sh; cd /home/sbmuser/MaltaSW/MaltaTbAnalysis/SPS_2023/; python MaltaDQ_SPS_PS15.py -a -t -p -c -m -d -r "'+str(runNumber)+"   > "+os.getenv("MALTA_LOGS_PATH") + "/analysis.log &")

def check_run(config):
    file_object = open('logRuns.txt', 'a')
    isBad = check_if_string_in_file()
    if isBad == -1:
      file_object.write(str(runNumber)+", "+ str(sub) + "V, "+config+", BAD\n") 
    elif isBad == 0:
      file_object.write(str(runNumber)+", "+ str(sub) + "V, "+config+", WARNING\n") 
    else:
      file_object.write(str(runNumber)+", "+ str(sub) + "V, "+config+", GOOD\n") 
    file_object.close()
    return isBad




def getVal(command):

    VALcmd =  str(command)
    VAL= str(subprocess.check_output(VALcmd, shell=True))
    VAL=str(VAL.split()[-1])
    VAL = VAL.replace("\\n'","")

    return float(VAL)


def newPlane(runNumber, runType):
    return dict({'runNumber' : runNumber, 'runType' : runType, 'planeNumber': None, 'ITHR': None, 'ICASN' : None, 'IDB' : None, 'IBIAS' : None, 'IRESET' :  None, 'VCASN' : None, 'VRESET_P' : None, 'VRESET_D' : None, 'VCLIP' : None, 'sample' : None, 'planetype' : 'trigger', 'SUB' : round(getVal("MALTA_PSU.py -c getVoltage SUB | grep SUB"),1), 'PWELL' : round(getVal("MALTA_PSU.py -c getVoltage PWELL | grep PWELL"),1)})
    """ 

    if planeNumber == 1 or  planeNumber ==  2 or planeNumber ==  5 or planeNumber ==  6:  
      return dict({'runNumber' : runNumber, 'runType' : runType, 'planeNumber': None, 'ITHR': None, 'ICASN' : None, 'IDB' : None, 'IBIAS' : None, 'IRESET' :  None, 'VCASN' : None, 'VRESET_P' : None, 'VRESET_D' : None, 'VCLIP' : None, 'sample' : None, 'planetype' : 'trigger', 'SUB' : round(getVal("MALTA_PSU.py -c getVoltage SUB | grep SUB"),1), 'PWELL' : round(getVal("MALTA_PSU.py -c getVoltage PWELL | grep PWELL"),1)})
    if planeNumber ==  3 or planeNumber ==  4:
      print (dict({'runNumber' : runNumber, 'runType' : runType, 'planeNumber': None, 'ITHR': None, 'ICASN' : None, 'IDB' : None, 'IBIAS' : None, 'IRESET' :  None, 'VCASN' : None, 'VRESET_P' : None, 'VRESET_D' : None, 'VCLIP' : None, 'sample' : None, 'planetype' : 'trigger', 'SUB' : round(getVal("MALTA_PSU.py -c getVoltage SUBH | grep SUBH"),1), 'PWELL' : round(getVal("MALTA_PSU.py -c getVoltage PWELL | grep PWELL"),1)}))
      return dict({'runNumber' : runNumber, 'runType' : runType, 'planeNumber': None, 'ITHR': None, 'ICASN' : None, 'IDB' : None, 'IBIAS' : None, 'IRESET' :  None, 'VCASN' : None, 'VRESET_P' : None, 'VRESET_D' : None, 'VCLIP' : None, 'sample' : None, 'planetype' : 'trigger', 'SUB' : round(getVal("MALTA_PSU.py -c getVoltage SUBH | grep SUBH"),1), 'PWELL' : round(getVal("MALTA_PSU.py -c getVoltage PWELL | grep PWELL"),1)})
    """

def definecuts(runNumber, runType):
  if runType=="Sr90":
    return (dict({'runNumber':runNumber, 'runType':runType, 'planetype':None, 'sample':None, 'Xmin':None, 'Xmax':None,'Ymin':None,'Ymax':None,'chi2Cut':50, 'maxDist':6000, 'distanceCut':4500, 'divider':4096}))
  elif runType=="muons":
    return (dict({'runNumber':runNumber, 'runType':runType, 'planetype':None, 'sample':None, 'Xmin':None, 'Xmax':None,'Ymin':None,'Ymax':None,'chi2Cut':50, 'maxDist':300, 'distanceCut':100, 'divider':20}))
  elif runType=="SPS":
    return (dict({'runNumber':runNumber, 'runType':runType, 'planetype':None, 'sample':None, 'Xmin':None, 'Xmax':None,'Ymin':None,'Ymax':None,'chi2Cut':10, 'maxDist':150, 'distanceCut':80, 'divider':20}))
  else:
    exit(0)




""" This part  writes the parameters into the SQL database.  """ 

def database(runNumber, runType, planes, cuts):
    host_args = {
    "host": "dbod-adepix",
    "user": "sbmuser",
    "port": 5504,
    "database": "MALTATBSPS",
    "password": "testbeam"
    }  
    con = mysql.connector.connect(**host_args)
    cur = con.cursor(dictionary=True)
    query = "INSERT INTO MALTATBSPS.runs (runNumber, runType, timeRun, pcRun) VALUES (%s, %s, CURRENT_TIMESTAMP(), %s)"  
    val = (runNumber, runType, socket.gethostname())
    print (query % val)
    try: 
      cur.execute(query, val)
      # disconnecting from server
      con.commit()
    except:
      print("Run number already exists in the database or wrong run type check both, exit(0)") 
      con.close()
      exit(0)

    for p in planes:
      placeholders = ', '.join(['%s'] * (len(planes[p])))
      columns = ', '.join(planes[p].keys())
      query = "INSERT INTO MALTATBSPS.configuration (  %s ) VALUES ( %s)" % (columns, placeholders)
      print(query,list(planes[p].values()))
      try:
        cur.execute(query, list(planes[p].values()))
        con.commit()
      except:
        print("Run number does not exist, exit(0)") 
        con.close()
        exit(0)

    for c in cuts:
      placeholders = ', '.join(['%s'] * (len(cuts[c])))
      columns = ', '.join(cuts[c].keys())
      query = "INSERT INTO MALTATBSPS.analysis (%s) VALUES ( %s)" % (columns, placeholders)
      print (query, list(cuts[c].values()))
      try:
        cur.execute(query, list(cuts[c].values()))
        con.commit()
      except:
        print("Run number does not exist, exit(0)") 
        con.close()
        exit(0)

    con.close()

def GetRunNumberFromDB():
    host_args = {
    "host": "dbod-adepix",
    "user": "sbmuser",
    "port": 5504,
    "database": "MALTATBSPS",
    "password": "testbeam"
    }
    con = mysql.connector.connect(**host_args)
    cur = con.cursor(dictionary=True)
    query = "SELECT MAX(`runNumber`) FROM MALTATBSPS.runs   ;"
    try:
      cur.execute(query)
      result_set=cur.fetchall()
      #con.close()
      # disconnecting from server
    except:
      print("Run number already exists in the database or wrong run type check both, exit(0)")
      con.close()
      exit(0)
 
    result_set= result_set[0]
    print(int(result_set['MAX(`runNumber`)'])+1)
    return (int(result_set['MAX(`runNumber`)'])+1) 






"""  This part does the main loop """



def main_loop(runType, Y_positions, SUB_values, config_files, triggers, duts, XYcuts):
    global _runType
    global _cmdtlu
    _runType=runType
    for Y in Y_positions:
      for sub, sub2 in SUB_values:
        for config in config_files:
         # os.system("/home/sbmuser/StageControlCpp/standa2/x86_64-centos7-gcc8-opt/SPS_Malta_ZAxisCtrl -m abs -u cm -d "+str(Y))
          #os.system("/home/sbmuser/StageControlCpp/standa2/x86_64-centos7-gcc8-opt/StandaZAxisCtrl.py -m abs -u cm -d "+str(Y)) 
          os.system("StandaZAxisCtrl.py -m abs -u mm -d "+str(Y[0]))
          os.system("StandaXAxisCtrl.py -m abs -u mm -d "+str(Y[1]))

          #########################UNCOMMENT HERE ONCE YOU CAN AUOMATICALY MOVE THE ANGLE STAGE
          #if (Y[2] <= 0):
          #  os.system("KDC101AxisCtrl.py -d "+str(Y[2]))

 
          runNumber=GetRunNumberFromDB()
          print(runNumber)
   
          os.system("MALTA_PSU.py -c rampVoltage DUT1_SUB "+str(sub)+" 10")
          os.system("MALTA_PSU.py -c rampVoltage DUT2_SUB "+str(sub2)+" 10")

  
          lines = []
          lines_include = []
          with open(config, 'r') as f:
            lines = f.readlines()
            for line in lines: 
              if "include" in line:
                include_file = line.split(' ')[1].strip("\n").strip()
                print (include_file)
                with open(include_file, 'r') as f2:
                  lines_include = f2.readlines()
                lines=lines_include + lines       
          #exit(0)
          cuts = dict()

          for dut in duts:
            cuts[dut]=definecuts(runNumber,runType)

          planes = dict()
          plane = newPlane(runNumber,runType) #,None)
          for line in lines:
              if 'PLANE' in line:
                  if plane['planeNumber'] != None:
                      planes[plane['planeNumber']] = plane
                      plane = newPlane(runNumber, runType) #,int(match(pattern, line)[1]))
                  pattern = '.*PLANE(\d+).*'
                  plane['planeNumber'] = int(match(pattern, line)[1])
                  if plane['planeNumber'] ==3 or plane['planeNumber'] ==4:
                     plane['SUB'] = round(getVal("MALTA_PSU.py -c getVoltage SUBH | grep SUBH"),1) 

              for key in plane.keys():
                  if plane[key] == None and key in line:
                      val = line.split(":")[1].strip("\n").strip()
                      plane[key] = val if (key == 'sample' or key=='type') else int(val)
                  #if plane[key] == None and key =='SUB': #    plane[key] = sub
                  if not None in plane.values():
                    break

              if line == lines[-1] and plane['planeNumber'] != None:
                  planes[plane['planeNumber']] = plane

              #planes[plane['SUB']] = sub
          for key in duts.keys():
            print(key)
            planes[duts[key]]['planetype']=key
            if key == 'DUT2': planes[duts[key]]['SUB']= round(getVal("MALTA_PSU.py -c getVoltage DUT2_SUB | grep SUB"),1)
            #if key == 'DUT2': planes[duts[key]]['SUB']= round(getVal("MALTA_PSU.py -c getVoltage DUT1_SUB | grep SUB"),1)
            if key == 'DUT1': planes[duts[key]]['SUB']= round(getVal("MALTA_PSU.py -c getVoltage DUT1_SUB | grep SUB"),1)
            planes[duts[key]]['PWELL']== round(getVal("MALTA_PSU.py -c getVoltage DUT_PWELL | grep PWELL"),1)
            #planes[duts[key]]['angle_z']=abs(Y[2])   #store angle
            #print(os.popen("KDC101AxisCtrl.py -i").read()) 
            #planes[duts[key]]['angle_z'] = int(os.popen("KDC101AxisCtrl.py -i").read())  #VLAD EDITED AND ANUSREE DIDNT COMPLAIN
            planes[duts[key]]['angle_z'] = 0
            cuts[key]['sample']=planes[duts[key]]['sample']
            cuts[key]['Xmin']=XYcuts[key]['Xmin']
            cuts[key]['Xmax']=XYcuts[key]['Xmax']
            cuts[key]['Ymin']=XYcuts[key]['Ymin']
            cuts[key]['Ymax']=XYcuts[key]['Ymax']
            cuts[key]['planetype']=key

          print(cuts) 
          print(planes) 
  
          database(runNumber, runType, planes, cuts)

          run_point(sub,sub2,config,runNumber,triggers)
          #runNumber=runNumber+1

      os.system("MALTA_PSU.py -c rampVoltage DUT1_SUB 6 20")
      os.system("MALTA_PSU.py -c rampVoltage DUT2_SUB 6 20")











