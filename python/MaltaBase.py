#!/usr/bin/env python

import time
import os

#################################################################################################
## Helper functions

def GetVersion(ipb,verbose=False):
    value=ipb.Read(0)
    if verbose: print("\nCODE VERSION: "+str(value)+" \n")
    return value

def GetStatus(ipb):
    value=ipb.Read(1)
    tmp1  =  (int(value) >> 16) & 0xFFFF
    tmp2  =  (int(value) >>  8) & 0x00FF
    tmp3  =  (int(value) ) & 0x00FF
    #text="\nCODE DATE   : "+('%04x' % tmp1 )+"-"+('%02x' % tmp2)+"-"+('%02x' % tmp3)+" \n "
    #print text
    text=('%04x' % tmp1 )+"-"+('%02x' % tmp2)+"-"+('%02x' % tmp3)
    return text #value

'''
def GetFifoState(isFull, isEmpty):
    if isFull   : return "FULL"
    elif isEmpty: return "EMPTY"
    else        : return "NOT EMPTY"
'''   
 
def GetFifoState2(isFull, isEmpty):
    if isFull   : return "FULL"
    elif isEmpty: return "EMPTY"
    else        : return "NOT EMPTY"

def GetFifoState3(isFull, isEmpty, isHalf):
    if isFull   : return "FULL"
    elif isEmpty: return "EMPTY"
    elif isHalf : return ">50% FULL"
    else        : return "<50% FULL"

def IsFIFOFastFull(ipb):
    value=ipb.Read(3)
    fifo1Full = (  value       & 0x1)!=0
    return fifo1Full

def IsFIFOIPEmpty(ipb):
    value=ipb.Read(3)
    fifo2Empty= ( (value >> 4) & 0x1)==0 
    return fifo2Empty

def IsFIFOMonEmpty(ipb):
    value=ipb.Read(3)
    fifoMEmpty= ( (value >> 5) & 0x1)==0
    return fifoMEmpty
        
def GetFifoStatus(ipb, quiet=True):
    value=ipb.Read(3)
    retStrings=[]
    ##if value==0: return retStrings
    fifo1Full = (  value       & 0x1)!=0
    fifo2Full = ( (value >> 1) & 0x1)!=0
    fifoMFull = ( (value >> 2) & 0x1)!=0 
    fifo1Empty= ( (value >> 3) & 0x1)==0 
    fifo2Empty= ( (value >> 4) & 0x1)==0 
    fifoMEmpty= ( (value >> 5) & 0x1)==0
    fifo1Half = ( (value >> 6) & 0x1)!=0
    
    fifoSCRFull = ( (value >> 8) & 0x1)!=0
    fifoSCWFull = ( (value >>10) & 0x1)!=0
    fifoSCREmpty= ( (value >> 7) & 0x1)==0
    fifoSCWEmpty= ( (value >> 9) & 0x1)==0

    retStrings.append( GetFifoState3(fifo1Full,fifo1Empty,fifo1Half) )
    retStrings.append( GetFifoState2(fifo2Full,fifo2Empty) )
    retStrings.append( GetFifoState2(fifoMFull,fifoMEmpty) )
    retStrings.append( GetFifoState2(fifoSCRFull,fifoSCREmpty) )
    retStrings.append( GetFifoState2(fifoSCWFull,fifoSCWEmpty) )
    if not quiet:
        print("\nFifoFast is: "+retStrings[0])
        print("FifoIP   is: "  +retStrings[1])
        print("FifoMon  is: "  +retStrings[2]+" \n")
    #return value
    return retStrings

def getFifoState(ipb):
    value=ipb.Read(3)
    print(" ")
    print( "FIFOread   full: "+str( ( (value >> 8) & 0x1)!=0 ))
    print( "FIFOread  empty: "+str( ( (value >> 7) & 0x1)==0 ))
    print( "FIFOwrite  full: "+str( ( (value >>10) & 0x1)!=0 ))
    print( "FIFOwrite empty: "+str( ( (value >> 9) & 0x1)==0 ))

def getAllRegisters(ipb):
    res=[]
    for i in range(0,23):
        w=int(sc.readRegister(i))
        ipb.Write(5, w)
        val1=ipb.Read(4)
        res.append(val1&0xFFFF)
    return res

def doSelfTests(ipb, NTests):
    C_all    =0.
    C_correct=0.
    C_wrong  =0.
    DebugWord(ipb.Read(4),0)
    DebugWord(ipb.Read(4),0)
    DebugWord(ipb.Read(4),0)
    print(" ")
    print(" ")
    
    for i in range(0,NTests):
        C_all+=1.
        time.sleep(0.001)
        ipb.Write(5,0xA000)
        v1=DebugWord(ipb.Read(4),0)
        v2=DebugWord(ipb.Read(4),1)
        v3=DebugWord(ipb.Read(4),2)
        if v2==-1 and v1==43690: C_correct+=1.
        else                   : C_wrong  +=1.

    print( " ") 
    print( "SUMMARY: sent tests: "+str(C_all)+"  ---> CORRECT results: "+str(C_correct)+" === "+str( C_correct/C_all*100)+" %")
    print( "#########################################################################################################")
    print( " "        )

#################################################################################################

## does not fully work as expected
def ResetIPBus(ipb):
    time.sleep(1)
    ipb.Write(2,1)
    ipb.Synch()
    ipb.Write(2,2)
    ipb.Write(2,0)
    pass

##Dummy Sender control bits:
#0: push signal
#1: reset signal (needed before sending a new push)
#2: reset all AO fifos
#3: synchronise SenderOserdes
## by default the sender produces a single pulse with all the bits at 1
#4: signal switch_1: enable double pulse for ALL the bits
#5: signal switch_2: switch off bits 20-36 (but only for leading puls
#6: signal switch_3: TBD
#7: signal switch_4: TBD

def ResetFifo(ipb, channel=8):
    ###ipb.Write(channel,4)
    ###ipb.Write(channel,0)
    ## also resetting the oserdes
    ###ipb.Write(channel,8)
    ###ipb.Write(channel,0)
    cacheWord=ipb.Read(channel)
    maskFIFO   =0xFFFFFFFF ^ (1<<2)
    maskOSERDES=0xFFFFFFFF ^ (1<<3)
    ipb.Write(channel, cacheWord | (1<<2) )
    ipb.Write(channel, cacheWord & maskFIFO )
    ipb.Write(channel, cacheWord | (1<<3) )
    ipb.Write(channel, cacheWord & maskOSERDES )
    
    ###############time.sleep(0.01)
    ################print "\nResetting FIFO and OSERDES\n"
    pass

    
def SendPulse(ipb, val, channel=8):
    #shift value by 3 bits
    '''
    tmpVal=int(val, 2)
    value=( tmpVal << 4)
    ipb.Write(channel,value+0)
    time.sleep(0.0000001)
    ipb.Write(channel,value+2)
    ipb.Write(channel,value+1)
    '''
    tmpVal=int(val, 2)
    value=( tmpVal << 4)
    values=[]
    values.append(value)
    values.append(value+2)
    values.append(value+1)
    ipb.Write(channel,values,True)
    pass

#################################################################################################
def WriteTap(ipb, channel,tap1, tap2):
    index=channel+10
    NewTap1=0
    if tap1>31:
        print(" WARNING ... I CANNOT SET A tap1>31 ... YOU CHOOSE "+str(tap1))
        pass
    else:
        NewTap1=tap1
        pass
    NewTap2=0
    if tap2>31:
        print( " WARNING ... I CANNOT SET A tap2>31 ... YOU CHOOSE "+str(tap2))
        pass
    else:
        NewTap2+=(tap2<<5)
        pass
    NewTap2+=NewTap1
    NewTap2+=pow(2,31)
    tmpList=list('{0:0b}'.format(NewTap2))
    #print "I am fucking writing: "+str(NewTap2)+" on: "+str(index)
    ipb.Write(index,NewTap2)
    ipb.Write(index,0)
    pass

def ReadTap(ipb, channel):
    index=channel+50
    value=ipb.Read(index)
    #######print value
    tmpList=list('{0:0b}'.format(value))
    tap1=(  value      & (0x1F) )
    tap2=( (value >>5) & (0x1F) )
    #######print "For channel: "+str(channel)+"  TAP0= "+str(tap1)+"  ,  TAP5= "+str(tap2)
    values=[]
    values.append(tap1)
    values.append(tap2)
    return values
    
#################################################################################################
def ReadMaltaWord(ipb, debug=True, channel=6):
    values=ipb.Read(channel,2,True)
    if debug: print(values)
    return values

def ReadMonitorWord(ipb, debug=True, channel=7):
    values=ipb.Read(channel,37,True)
    pin=-1
    if debug:       
        for val in values:
            pin+=1
            ##if pin!=0: continue
            tmpList=list('{0:0b}'.format(val))
            pstring=" "
            count=0
            for obj in tmpList:
                if count%8==0: pstring+="|"
                pstring+=str(obj)
                count+=1
                pass
            print(pstring)
    return values

#################################################################################################
def writeConstDelays(ipb,delay1=3,delay2=1):
    for ch in range(0,38):
        WriteTap(ipb, ch, delay1, delay2)
        pass
    pass

def writeDefaultDelays(ipb):
    v_tap0=[5, 3, 6, 2, 3, 7, 2, 6, 6, 3, 7, 3, 2, 7, 2, 6, 6, 2, 7, 2, 3, 7, 2, 5, 2, 7, 3, 5, 2, 7, 3, 6, 3, 5, 2, 3, 7]
    v_tap5=[3, 1, 4, 0, 1, 5, 0, 4, 4, 1, 5, 1, 0, 5, 0, 4, 4, 0, 5, 0, 1, 5, 0, 3, 0, 5, 1, 3, 0, 5, 1, 4, 1, 3, 0, 1, 5]
    ch=0
    for val1,val2 in zip(v_tap0,v_tap5):
        #print val1," - ",val2
        WriteTap(ipb, ch, val1, val2)
        ch+=1
        pass
    pass

def readAllDelays(ipb,Verbose=False):
    results=[[],[]]
    for ch in range(0,38):
        vals=ReadTap(ipb, ch)
        results[0].append(vals[0])
        results[1].append(vals[1])
        if Verbose: print(" channel: "+str(ch)+" with taps: ("+str(vals[0])+","+str(vals[1])+")")
    return results

def writeTapToFile(ipb, fileName):
    folderName="tapCalibFiles/"
    os.system("mkdir -p "+folderName)
    ofile=file(folderName+fileName,"w")
    for ch in range(0,38):
        vals=ReadTap(ipb, ch)
        line=" channel: "+str(ch)+" with taps: ("+str(vals[0])+","+str(vals[1])+") \n"
        ofile.write(line)
    ofile.close()

def loadTapFromFile(ipb, fileName):
    folderName="tapCalibFiles/"
    ofile=file(folderName+fileName,"r")
    lines=ofile.readlines()
    for l in lines:
        chName=int(l.split(":")[1].split(" w")[0])
        tap1=int(l.split(",")[0].split("(")[1])
        tap2=int(l.split(",")[1].split(")")[0])
        ##print " channel: "+str(chName)+" --> ("+str(tap1)+","+str(tap2)+")"
        WriteTap(ipb, chName, tap1, tap2)
    ofile.close()

#################################################################################################

##############################################################################
##############################################################################
## Stupid helper functions

##################################################################################################
def printVal(value):
    val=""
    val+=" "+str( format( (value&0xFFFF) , "016b" ) )
    val+=" ::::   0x%04x"%(value&0xFFFF)
    return val


##################################################################################################
def DebugWord(word, attempt,ref=0):
    val="Register: "+str( attempt)
    if word!=0: 
        val+="  NOT EMPTY "
        val+=" ::::  "+str( format( (word&0xFFFF) , "016b" ) )
        val+=" ::::   0x%04x"%(word&0xFFFF)
        val+=" ::::   %i"%(word&0xFFFF)
        if ref!=0:
            if ref!=(word&0xFFFF): val+="   <=========== "
    else      : val+="    EMPTY   "
    print(val)
    if word!=0: 
        return (word&0xFFFF)
    else:  
        return -1

##############################################################################
##############################################################################
## MaltaSlow control helper functions


##############################################################################

def switchClockOn(ipb, bit = 21):
    cacheWord=ipb.Read(8)
    ipb.Write(8, cacheWord | (1<<bit) )
    print("%x" % (cacheWord | (1<<bit) ))
    time.sleep(0.02)
    return

def switchClockOff(ipb):
    time.sleep(0.02)
    cacheWord=ipb.Read(8)
    mask=0xFFFFFFFF ^ ( 1<<21 )
    ipb.Write(8, cacheWord & mask)
    print("%x" % (cacheWord & mask))
    return

def EnableFastSignal(ipb):
    cacheWord=ipb.Read(9)
    ipb.Write(9, cacheWord | (1<<6) )
    time.sleep(0.02)
    return

def DisableFastSignal(ipb):
    time.sleep(0.02)
    cacheWord=ipb.Read(9)
    mask=0xFFFFFFFF ^ ( 1<<6 )
    ipb.Write(9, cacheWord & mask)
    return

##############################################################################
def ReadoutOff(ipb):
    cacheWord=ipb.Read(8)
    ipb.Write(8, cacheWord | (1<<29) )
    time.sleep(0.02)
    return

def ReadoutOn(ipb):
    cacheWord=ipb.Read(8)
    mask=0xFFFFFFFF ^ ( 1<<29 )
    ipb.Write(8, cacheWord & mask)
    return
##############################################################################
def SetReadoutDelay(ipb,delay):
    cacheWord = ipb.Read(9)
    mask1 = 0xFFFFFF00 | delay
    mask2 = 0x000000FF | cacheWord
    ipb.Write(9, mask1 & mask2)
    time.sleep(0.02)
    return

def GetReadoutDelay(ipb):
    cacheWord = ipb.Read(9)
    cacheWord = 0x000000FF & cacheWord
    return cacheWord

def SetReadoutWindow(ipb,size):
    cacheWord = ipb.Read(9)
    mask1 = 0xFFF803FF | size
    mask2 = 0x0007FC00 | cacheWord
    ipb.Write(9, mask1&mask2)
    time.sleep(0.02)
    return

def GetReadoutWindow(ipb):
    cacheWord = ipb.Read(9)
    cacheWord = 0x0007FC00 & cacheWord
    cacheWord >> 10
    return cacheWord

##############################################################################
def SetPreScale(ipb,preScaleFactor):
    ipb.Write(91,preScaleFactor)



##############################################################################
def ReadoutHalfColumnsOn(ipb):
    cacheWord=ipb.Read(8)
    ipb.Write(8, cacheWord | (1<<30) )
    time.sleep(0.02)
    return

def ReadoutHalfColumnsOff(ipb):
    cacheWord=ipb.Read(8)
    mask=0xFFFFFFFF ^ ( 1<<30 )
    ipb.Write(8, cacheWord & mask)
    return

##############################################################################
def ReadoutHalfRowsOn(ipb):
    cacheWord=ipb.Read(8)
    ipb.Write(8, cacheWord | (1<<31) )
    time.sleep(0.02)
    return

def ReadoutHalfRowsOff(ipb):
    cacheWord=ipb.Read(8)
    mask=0xFFFFFFFF ^ ( 1<<31 )
    ipb.Write(8, cacheWord & mask)
    return

##############################################################################
def ReadoutEnableExternalL1A(ipb):
    cacheWord=ipb.Read(8)
    ipb.Write(8, cacheWord | (1<<18) )
    time.sleep(0.02)
    return

def ReadoutEnableInternalL1A(ipb):
    cacheWord=ipb.Read(8)
    mask=0xFFFFFFFF ^ ( 1<<18 )
    ipb.Write(8, cacheWord & mask)
    time.sleep(0.02)
    return

def FastSignalEnable(ipb):
    cacheWord=ipb.Read(9)
    ipb.Write(9, cacheWord | (1<<7) )
    time.sleep(0.02)

def FastSignalDisable(ipb):
    cacheWord=ipb.Read(9)
    mask=0xFFFFFFFF ^ ( 1<<7 )
    ipb.Write(9, cacheWord & mask)
    time.sleep(0.02)

#########################################################################################
def ResetL1Counter(herIP):
    cacheWord=herIP.Read(8)
    herIP.Write(8, cacheWord | (1<<16) )
    time.sleep(0.02)
    maskRESET= 0xFFFFFFFF ^ (1<<16)
    herIP.Write(8, cacheWord & maskRESET )
    time.sleep(0.01)
    return 

#########################################################################################
def ResetMalta(herIP):
    mask= 0xFFFFFFFF ^ (1<<12)
    cacheWord= herIP.Read(8)
    herIP.Write(8, cacheWord | (1<<12) )
    time.sleep(1.0)
    herIP.Write(8, cacheWord & mask )
    return 





##############################################################################
##############################################################################
def EnableAllPixel(ipb):
    ipb.Write(5,0x1000); 


##############################################################################
##############################################################################
def DisableAllPixel(ipb):
    ipb.Write(5,0x1200); 
