#!/usr/bin/env python
####################################
# Data format for MALTA read-out
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# December 2017
####################################


class MaltaData():

    def __init__(self):
        self.refbit  =0
        self.pixel   =0
        self.group   =0
        self.parity  =0
        self.delay   =0
        self.dcolumn =0
        self.chipbcid=0 
        self.chipid  =0
        self.phase   =0
        self.winid   =0
        self.bcid    =0  
        self.l1id    =0   
        self.word1   =0  
        self.word2   =0  
        self.data    =[0,]*62
        self.nhits   =0
        self.rows    =[]
        self.columns =[]
        pass

###########################################################################
###### getters
    def getRefbit(self):   return self.refbit

    def getPixel(self):    return self.pixel

    def getGroup(self):    return self.group

    def getParity(self):   return self.parity

    def getDelay(self):    return self.delay

    def getDcolumn(self):  return self.dcolumn

    def getChipbcid(self): return self.chipbcid

    def getChipid(self):   return self.chipid

    def getPhase(self):    return self.phase

    def getWinid(self):    return self.winid

    def getL1id(self):     return self.l1id

    def getBcid(self):     return self.bcid

    def getWord1(self):    return self.word1

    def getWord2(self):    return self.word2

    def getWord1new(self): return self.word1

    def getWord2new(self): return self.word2

    def getNhits(self):    return self.nhits

    def getRow(self,hit):  return self.rows[hit]

    def getColumn(self,hit):  return self.columns[hit]

    def getInfo(self):
       #print( "Refbit: ",self.getRefbit(),"  --Pixel: ",self.getPixel(),"  --Group: ",self.getGroup(),"  --Parity: ",self.getParity(),"  --Delay: ",self.getDelay(),"  --DoubleCol: ",self.getDcolumn(),"  --CHIPBCID: ",self.getChipbcid(),"  --Phase: ",self.getPhase(),"  --Window ID: ",self.getWinid(), "  --L1ID: ",self.getL1id(), "  --BCID: ",self.getBcid())
       ##,"Chip ID: ",self.getChipid(),
       ##print( "Phase: ",self.getPhase(),"  --Event ID: ",self.getWinid()###,"Word1: ",self.getWord1(),"Word2: ",self.getWord2())
       ##print( "NUMBER OF PIXELS IS ",self.nhits)
        print( "Timing-- MaltaBCID: ",self.getChipbcid()," ,  Pahse: ", self.getPhase()," ,  Window: ",self.getWinid()," ,  BCID: ",self.getBcid()," ,  L1ID: ",self.getL1id() )
        #print( "info--  Pixel: ",self.getPixel()," ,  Group: ",self.getGroup()," ,  Parity: ",self.getParity()," ,  Delay: ",self.getDelay()," ,  DoubleCol: ",self.getDcolumn())
        print( "info--  Pixel: ",self.getPixel()," ,  Group: ",self.getGroup()," ,  Parity: ",self.getParity()," ,  DoubleCol: ",self.getDcolumn())
        count=-1
        for val in self.rows:
            count+=1
            print( "   - "+str(count)+" : ( "+str(self.columns[count])+ " , "+str( val ) +" )")
        #self.rows = []
        #self.columns = []
        pass


###########################################################################
###### setters
    def setRefbit(self,refbit):
        self.refbit = refbit
        return self

    def setPixel(self,pixel):
        self.pixel = pixel
        return self

    def setGroup(self,group):
        self.group = group
        return self

    def setParity(self,parity):
        self.parity = parity
        return self

    def setDelay(self,delay):
        self.delay = delay
        return self

    def setDcolumn(self,dcolumn):
        self.dcolumn = dcolumn
        return self
    
    def setChipbcid(self, chipbcid):
        self.chipbcid = chipbcid
        return self

    def setBcid(self,bcid):
        self.bcid = bcid
        return self

    def setChipid(self,chipid):
        self.chipid = chipid
        return self

    def setPhase(self,phase):
        self.phase = phase
        return self

    def setWinid(self,winid):
        self.winid = winid
        return self

    def setL1id(self, l1id):
        self.l1id = l1id
        return self

    def setWord1(self,word):
        self.word1=word
        for bit in range(31):
            self.data[bit] = (word >> bit) & 1
            pass
        return self

    def setWord2(self,word):
        self.word2=word
        for bit in range(31):
            self.data[bit+31] = (word >> bit) & 1
            pass
        return self


###########################################################################
###### general helpers
    def pack(self):
        self.int2lst(self.data,self.refbit  , 0,1)
        self.int2lst(self.data,self.pixel   , 1,17)
        self.int2lst(self.data,self.group   ,17,22)
        self.int2lst(self.data,self.parity  ,22,23)
        self.int2lst(self.data,self.delay   ,23,26)
        self.int2lst(self.data,self.dcolumn ,26,34)
        self.int2lst(self.data,self.chipbcid,34,36)
        self.int2lst(self.data,self.chipid  ,36,37)
        self.int2lst(self.data,self.phase   ,37,40)
        self.int2lst(self.data,self.winid   ,40,44)
        self.int2lst(self.data,self.bcid    ,44,50)
        self.int2lst(self.data,self.l1id    ,50,62)
        #self.word1=self.lst2int(self.data, 0,31)
        #self.word2=self.lst2int(self.data,31,62)
        pass
        
    def unPack(self):
        self.unpack()
        pass
    
    def unpack(self):
        self.word=0
        self.refbit   = self.lst2int(self.data,0,1)
        self.pixel    = self.lst2int(self.data,1,17)
        self.group    = self.lst2int(self.data,17,22)
        self.parity   = self.lst2int(self.data,22,23)
        self.delay    = self.lst2int(self.data,23,26)
        self.dcolumn  = self.lst2int(self.data,26,34)
        self.chipbcid = self.lst2int(self.data,34,36)
        self.chipid   = self.lst2int(self.data,36,37)
        self.phase    = self.lst2int(self.data,37,40)
        self.winid    = self.lst2int(self.data,40,44)
        self.bcid     = self.lst2int(self.data,44,50)
        self.l1id     = self.lst2int(self.data,50,62)
        
        self.nhits = 0
        self.rows = []
        self.columns = []
        for i in range(16):
            if ((self.pixel>>i)&1)==0: continue
            column = self.dcolumn*2
            if(i>7):column+=1

            row = self.group*16
            if(self.parity==1):row+=8
            if  (i==0 or i== 8): row+=0
            elif(i==1 or i== 9): row+=1
            elif(i==2 or i==10): row+=2
            elif(i==3 or i==11): row+=3
            elif(i==4 or i==12): row+=4
            elif(i==5 or i==13): row+=5
            elif(i==6 or i==14): row+=6
            elif(i==7 or i==15): row+=7
            self.rows.append(row)
            self.columns.append(column)
            self.nhits+=1
            pass
        pass

    def lst2int(self,data,pos1,pos2):
        ret = 0
        for i in range(pos2-pos1):
            ret |= data[i+pos1] << i
            pass
        return ret

    def int2lst(self,data,value,pos1,pos2):
        for i in range(pos2-pos1):
            data[i+pos1] = (int(value) >> i) & 1
            pass
        return data

    def getData(self):
        return self.data
    
    def toStr(self):
        s=''
        for bit in range(38):
            s.append("%i"%self.data[37-i])
            pass
        return s
        
    pass


