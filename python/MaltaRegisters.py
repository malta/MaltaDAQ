#!/usr/bin/env python
####################################
# MALTA registers
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# December 2017
####################################

class MaltaRegisters():

    def __init__(self):
        self.dummy      = 0x0
        self.status     = 0x1
        self.reset      = 0x2
        self.busy       = 0x3
        self.scr        = 0x4
        self.scw        = 0x5
        self.data       = 0x6
        self.monitor    = 0x7
        self.sender     = 0x8 
        self.reset_bcid = 0x9
        self.tapw_1     = 0xA
        self.tapr_1     = 0x32
        self.tapw       = []
        self.tapr       = []
        for i in xrange(37):
            self.tapw[i]=self.tapw_1+i
            self.tapr[i]=self.tapr_1+i
            pass
        pass

    pass
