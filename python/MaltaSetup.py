#!/usr/bin/env python
###########################
# MaltaSetup
# This class contains a map of setups
# Carlos.Solans@cern.ch
# Valerio.Dao@cern.ch
###########################

import os
import sys
import socket

class MaltaSetup:
    def __init__(self):

        self.setups={
            "pcatlidps10" :["udp://ep-ade-gw-04.cern.ch:50002"],
            "ps01" :["ep-ade-gw-05"],
            "ps03" :["udp://ep-ade-gw-07.cern.ch:50002",
                     "udp://ep-ade-gw-07.cern.ch:50003",
                     ],

            "pcatlidps02" :[
                "udp://ep-ade-gw-07.cern.ch:50001",
                "udp://ep-ade-gw-07.cern.ch:50002",
                "udp://ep-ade-gw-07.cern.ch:50003",
                ],
            "ps04" :["udp://ep-ade-gw-01.cern.ch:50001",
                     "udp://ep-ade-gw-02.cern.ch:50002",
                     "udp://ep-ade-gw-02.cern.ch:50003",
                     "udp://ep-ade-gw-02.cern.ch:50004",
                     "udp://ep-ade-gw-02.cern.ch:50005",
                 ],            
            "pcatlidps06":[
                "192.168.22.10",
                "192.168.22.11",
                "192.168.22.12",
                "192.168.22.13",
                "192.168.22.14",
                "192.168.22.15",
                "192.168.22.16",
                "192.168.22.17",
            ],
            "ps07" :[
                "udp://ep-ade-gw-04.cern.ch:50001",
                "udp://ep-ade-gw-04.cern.ch:50002",
                "udp://ep-ade-gw-04.cern.ch:50003",
            ],
             "ps05" :[
                "udp://ep-ade-gw-07.cern.ch:50001",
                "udp://ep-ade-gw-07.cern.ch:50002",
                "udp://ep-ade-gw-07.cern.ch:50003",
            ],

            "ps08" :["tcp://pcatlidrpi03:50001?target=192.168.0.1:50001"],
            "ros01":["192.168.0.1"],
            "ros02":["udp://ep-ade-gw-01.cern.ch:50001",
                     "udp://ep-ade-gw-01.cern.ch:50002",
                     "udp://ep-ade-gw-01.cern.ch:50003"],
            "ppepc153.physics.gla.ac.uk":["192.168.0.1"],
        }
        pass
    
    def getConnStr(self,chip=1,hostname=None):
        print("Getting connection string")
        if not hostname:
            hostname = socket.gethostname()
            pass
        connstr="192.168.0.1"
        for host in self.setups:
            #print( host)
            if host.lower() in hostname.lower(): 
                #print( self.setups[host])
                if len(self.setups[host])>=chip:
                    return self.setups[host][chip-1]
                else:
                    print( " multichip operation is not enable on this machine .... returnig first in the list")
                    return self.setups[host][0]
            pass
        return connstr
        pass
    pass

if __name__ == "__main__":
    print( MaltaSetup().getConnStr())
    
