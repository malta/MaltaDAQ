#!/usr/bin/env python
##################################################
# 
# Style
#
##################################################

import ROOT
import array

def SetStyle():
    ROOT.gStyle.SetPadLeftMargin(0.14)
    ROOT.gStyle.SetPadBottomMargin(0.15)
    ROOT.gStyle.SetPadRightMargin(0.10)
    ROOT.gStyle.SetPadTopMargin(0.10)
    ROOT.gStyle.SetLabelSize(0.05,"x")
    ROOT.gStyle.SetLabelSize(0.05,"y")
    ROOT.gStyle.SetTitleSize(0.05,"x")
    ROOT.gStyle.SetTitleSize(0.05,"y")
    ROOT.gStyle.SetTitleOffset(1.05,"y")
    ROOT.gStyle.SetOptTitle(0)
    ROOT.gStyle.SetOptStat(0)

    nrgbs = 5
    ncont = 250
    stops = array.array('d',[ 0.00, 0.34, 0.61, 0.84, 1.00 ])
    red   = array.array('d',[ 0.00, 0.00, 0.87, 1.00, 0.51 ])
    green = array.array('d',[ 0.00, 0.81, 1.00, 0.20, 0.00 ])
    blue  = array.array('d',[ 0.51, 1.00, 0.12, 0.00, 0.00 ])
    ROOT.TColor.CreateGradientColorTable(nrgbs, stops, red, green, blue, ncont)
    ROOT.gStyle.SetNumberContours(ncont)

    pass
