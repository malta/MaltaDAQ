import ROOT
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-f","--folder"     , type=str           , default=''	, help="specify folder e.g. './ThScansMalta/W7R12/blah'")                      
parser.add_argument("-p","--path"       , type=str           , default='./'	, help="specify path of the scans e.g. './ThScansMalta/W7R12'")                      
parser.add_argument("-e","--even"       , type=bool          , default=False	, help="specify if the scan was done only for even pixels")                      
args = parser.parse_args()

ROOT.gROOT.SetBatch(True)
print (args.folder)


dir_path=args.path
if args.path !='./' :
  dir_list = next(os.walk(dir_path))[1]


if args.folder != '':
  dir_list=[args.folder]


ROOT.gROOT.LoadMacro("src/ThScan.C")
for x in dir_list:
  if "analysis" in x:
    print ("skip folder "+x)
  else:  
    directory_analysis=dir_path+"/"+x+"/analysis"
    print(directory_analysis)

    if not os.path.exists(directory_analysis):
      os.makedirs(directory_analysis)
    if args.path !='./' :
      FileTree = ROOT.TFile.Open(dir_path+"/"+str(x)+"/th_sum_IDB_.root")
    if args.folder != '':
      FileTree = ROOT.TFile.Open(str(x)+"/th_sum_IDB_.root")
    Tree = FileTree.Thres
    t = ROOT.ThScan(Tree , str(directory_analysis), args.even)
    t.Loop()
    del t
    del Tree
    del FileTree

