#!/usr/bin/env python
## include the file with the basic instructions
import sys
import time
import argparse
import os
from time import sleep
import ROOT
import numpy as np
import array

import signal
import Keithley

#import MaltaSetup
#import MaltaBase
#import Herakles
#import PyMaltaSlowControl
#from MaltaBase import *

parser=argparse.ArgumentParser()
parser.add_argument('-a'         ,'--fpga'    ,help='FPGA address'          ,type=str  ,required =  True)
parser.add_argument('-c'         ,'--config'  ,help='config script'         ,type=str  ,required =  True)
parser.add_argument('-k'         ,'--keithley',help='Keithley address'      ,type=str  ,required =  True)
parser.add_argument('-f'         ,'--file'    ,help='actually the wafer'    ,type=str  ,required =  True)
parser.add_argument('-o'         ,'--output'  ,help='actually the base path',type=str  ,required =  True)
parser.add_argument('-DVDD'      ,'--DVDD'    ,help='config DVDD voltage'   ,type=float,default  = "0.8")
parser.add_argument('-IDB'       ,'--IDB'     ,help='scan IDB DAC'          ,type=bool ,default  = False)
parser.add_argument('-ITHR'      ,'--ITHR'    ,help='scan ITHR DAC'         ,type=bool ,default  = False)
parser.add_argument('-IBIAS'     ,'--IBIAS'   ,help='scan IBIAS DAC'        ,type=bool ,default  = False)
parser.add_argument('-IRESET'    ,'--IRESET'  ,help='scan IRESET DAC'       ,type=bool ,default  = False)
parser.add_argument('-ICASN'     ,'--ICASN'   ,help='scan ICASN DAC'        ,type=bool ,default  = False)
parser.add_argument('-VCASN'     ,'--VCASN'   ,help='scan VCASN DAC'        ,type=bool ,default  = False)
parser.add_argument('-VCLIP'     ,'--VCLIP'   ,help='scan VCLIP DAC'        ,type=bool ,default  = False)
parser.add_argument('-VPLSEHI'   ,'--VPLSEHI' ,help='scan VPLSEHI DAC'      ,type=bool ,default  = False)
parser.add_argument('-VPLSELO'   ,'--VPLSELO' ,help='scan VPLSELO DAC'      ,type=bool ,default  = False)
parser.add_argument('-VRSTD'     ,'--VRSTD'   ,help='scan VRSTD DAC'        ,type=bool ,default  = False)
parser.add_argument('-VRSTP'     ,'--VRSTP'   ,help='scan VRSTP DAC'        ,type=bool ,default  = False)
args=parser.parse_args()

scanFlags = [args.IDB,args.ITHR,args.IBIAS,args.IRESET,args.ICASN,args.VCASN,args.VCLIP,args.VPLSEHI,args.VPLSELO,args.VRSTD,args.VRSTP]
dacNames  = ["IDB","ITHR","IBIAS","IRESET","ICASN","VCASN","VCLIP","VPLSEHI","VPLSELO","VRSTD","VRSTP"]
dacLowerLimits = {"IDB":10,"ITHR":3,"IBIAS":10,"IRESET":10,"ICASN":1,"VCASN":10,"VCLIP":10,"VPLSEHI":10,"VPLSELO":1,"VRSTD":10,"VRSTP":10}

flagCount = 0
dac = ""
for i in range(0,11):
    if scanFlags[i] is True:
        flagCount = flagCount + 1
        dac = dacNames[i]
if flagCount > 1:
    print("Only one DAC can be scanned at once!")
    exit()
if flagCount < 1:
    print("Specify which DACn should be scanned!")
    exit()

print("Connect to Keithley...")
k=Keithley.Keithley(args.keithley)
k.setCurrentLimit(0.1e-3)
k.setVoltageLimit(3)
k.setSourceCurrent()
#k.beep()
k.enableOutput(True)

print("Scanning DAC {} now...").format(dac)

fw=ROOT.TFile.Open("%s/%s/DACScan_%s.root"%(args.output,args.file,dac),"RECREATE")
ntuple       = ROOT.TTree("Malta_DACScan_"+dac,"DAC scan of "+dac)
digital      = array.array('d',(0,))
analogMean   = array.array('d',(0,))
analogStdDev = array.array('d',(0,))
ntuple.Branch("digital"   ,digital     ,"digital/D")
ntuple.Branch("analogMean",analogMean  ,"analogMean/D")
ntuple.Branch("analogRMS" ,analogStdDev,"analogRMS/D")
c1 = ROOT.TCanvas("c1","DAC scan of "+dac, 800,600)
g1 = ROOT.TGraphErrors()
g1.SetName("DAC_Scan_"+dac)
g1.SetTitle("; DAC setting; measured Voltage [V]")
g1.Draw("AP")
#g1.SetDirectory(fw)
print "the DAC lower limit is: "+str(dacLowerLimits[dac])

for i in range(int(dacLowerLimits[dac]),128,10):
    print("######################")
    print("### Configure chip ###")
    print("######################")
    #print -> os.system()
    os.system("python {} -a {} -{} {}".format(args.config,args.fpga,dac,i))
    #either of these two
    volt = []
    for j in range(0,5):
        tempVal = float(k.getVoltage())
        volt.append(tempVal)
        if tempVal>2:
          os.system("MALTAOFF")
        pass
        sleep(0.5)
    meanVolt   = np.mean(volt)
    stddevVolt = np.std(volt)

    print("Set {} and measured {} +- {}".format(i,meanVolt,stddevVolt))

    #put this into a root file...
    digital      = i
    analogMean   = meanVolt
    analogStdDev = stddevVolt
    g1.SetPoint(i,i,meanVolt)
    g1.SetPointError(i,0,stddevVolt)
    ntuple.Fill()
    c1.Update()

c1.SaveAs("%s/%s/scan_%s.pdf"%(args.output,args.file,dac))

ntuple.Write()
g1.Write()
fw.Close()








