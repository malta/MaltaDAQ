import sys
import os
from time import sleep
import time
import MALTA_PSU
import signal
import Keithley
#import ROOT

cont = True
def signal_handler(signal, frame):
    print "You pressed ctrl+C to quit" 
    global cont
    cont = False
    return

signal.signal(signal.SIGINT, signal_handler)

k=Keithley.Keithley("/dev/ttyUSB2")
k.setCurrent(0)
k.setVoltageLimit(3.0)

PSU=MALTA_PSU.MALTA_PSU([])
print PSU

#can=ROOT.TCanvas("myC","myC",1200,800)
#can.Divide(2,1)


while True:
    if cont == False: break
    #time.sleep(1)
    ofile=file("logValues_MC40.txt","a")
    voltages=[]
    voltages.append(PSU.getCurrent("AVDD"))
    voltages.append(PSU.getCurrent("DVDD"))
    voltages.append(PSU.getCurrent("SUB"))
    voltages.append(PSU.getCurrent("PWELL"))
    voltages.append(PSU.getVoltage("AVDD"))
    voltages.append(PSU.getVoltage("DVDD"))
    voltages.append(PSU.getVoltage("SUB"))
    voltages.append(PSU.getVoltage("PWELL"))
    voltages.append(k.getVoltage()) #PSU.getVoltage("ANAG"))
    filetime = time.strftime("_%Y_%m_%d_%H_%M_%S")
    full=filetime+" ----- I_AVDD: "+str(voltages[0])
    full+=" , I_DVDD: " +str(voltages[1])
    full+=" , I_SUB: "  +str(voltages[2])
    full+=" , I_PWELL: "+str(voltages[3])
    full+=" , V_AVDD: " +str(voltages[4])
    full+=" , V_DVDD: " +str(voltages[5])
    full+=" , V_SUB: "  +str(voltages[6])
    full+=" , V_PWELL: "+str(voltages[7])
    full+=" , V_ANAG: " +str(voltages[8])
    print full
    full+="\n"
    ofile.write(full)
    ofile.close()
    
'''
try: 
    while True:
        if cont == False: break
        time.sleep(1)
        ofile=file("logValues_Mama.txt","a")
        voltages=[]
        voltages.append(PSU.getCurrent("AVDD"))
        voltages.append(PSU.getCurrent("DVDD"))
        voltages.append(PSU.getCurrent("SUB"))
        voltages.append(PSU.getCurrent("PWELL"))
        voltages.append(PSU.getVoltage("AVDD"))
        voltages.append(PSU.getVoltage("DVDD"))
        voltages.append(PSU.getVoltage("SUB"))
        voltages.append(PSU.getVoltage("PWELL"))
        #voltages.append(PSU.getVoltage("IMON2"))
        filetime = time.strftime("_%Y_%m_%d_%H_%M_%S")
        full=filetime+" ----- "+str(voltages[0])+" , "+str(voltages[1])+" , "+str(voltages[2])+" , "+str(voltages[3])
        #full+="   V: "+str(voltages[4])+" , "+str(voltages[5])+" , "+str(voltages[6])+" , "+str(voltages[7])+"\n"
        #+" , "+str(voltages[8])+"\n "
        print full
        ofile.write(full)
        ofile.close()
'''

#print("Finished data taking!")

#except KeyboardInterrupt:
#    print("Finished data taking!")
