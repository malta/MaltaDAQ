import ROOT
import argparse

parser = argparse.ArgumentParser()
# parser.add_argument("-f1","--chip1", type=str, default='W1RA', help="specify first chip e.g. 'W1RA'")
# parser.add_argument("-f2","--chip2", type=str, default='W1RC', help="specify second chip e.g. 'W1RA'")
# parser.add_argument("-o1","--config1", type=str, default='config_W1RA_Th550_x120_64_y140_64', help="specify desired config1 e.g. 'config_W1RA_Th550_x120_64_y140_64'")
# parser.add_argument("-o2","--config2", type=str, default='config_W1RC_Th550_x120_64_y140_64', help="specify desired config2 e.g. 'config_W1RC_Th550_x120_64_y140_64'")
# parser.add_argument("-v","--variable", type=str, default='thres', help="specify variable")
# parser.add_argument("-VP","--VRESET_P", type=str, default='127', help="specify value of VRESET_P")
args = parser.parse_args()

directory = "NoiseScansMalta/"
out_folder = "compPlots/"
folder = "/noise_masked_pix.root"
chip_dict = {
    "W1RA_WTN": 632, #kRed
    # # # "W1RB_WTN": 400, #kYellow
    "W1RC_WTN": 800, #kOrange
    "W2R1_WTN": 616, #kMagenta
    "W4R12_WTN": 416, #kOrange
    "W11R1_C": 600, #kBlue
    "W7R12_WTN": 432, #kCyan #Czochralski std
    "W9R11_WTN": 880, #kViolet #Czochralski n-gap
    "W9R14_WTN": 860, #kAzure #Czochralski n-gap
}

th_dict = {
    "Th350": ("ITH10", "IDB50", -9), #fairfair
    "Th550": ("ITH127", "IDB127", +2), #darkdark
    "ITH10_IDB127": ("ITH10", "IDB127", +0), #fair
    "ITH127_IDB50": ("ITH127", "IDB50", +1), #dark
}

side_dict = {
    "LEFTonly": ("VRESET_P127", 1), #kSolid
    "FULLchip": ("VRESET_P45", 4) #kDashDotted
}

def_config = "config_CHIP_THRES_ITH_IDB_SIDE_itr_MAX100pix/"
variable = "noise_vs_masked"

for th in th_dict:
    ith, idb, intensity = th_dict[th]
    for side in side_dict:
        side_config, line_style = side_dict[side]

        config = def_config
        config = config.replace("_CHIP","").replace("THRES",th).replace("ITH",ith).replace("IDB",idb).replace("SIDE",side).replace("/","")

        canv = ROOT.TCanvas("canv","canv",800,800)
        ROOT.SetOwnership(canv, False)

        # leg = ROOT.TLegend(0.60,0.55,0.88,0.88)
        leg = ROOT.TLegend(0.40,0.84,0.88,0.89)
        ROOT.SetOwnership(leg, False)
        leg.SetBorderSize(0)
        leg.SetNColumns(2)
        leg.SetFillColor(0)
        leg.SetTextFont(42)
        leg.Draw()
        canv.RedrawAxis()

        hist_dict = {}
        count = 0
        h_min = 1000000
        h_max = 0.0
        x_Min = 1000.0
        x_Max = 0.0
        for chip in chip_dict:
            shade = chip_dict[chip]
            fname = directory+chip+"/"+def_config+folder
            fname = fname.replace("CHIP", chip).replace("ITH",ith).replace("IDB",idb).replace("SIDE", side_config)
            if "Th" in th:
                fname = fname.replace("THRES", th)
            else:
                fname = fname.replace("THRES_", "")
                if chip=="W1RA_WTN" or chip=="W1RC_WTN":
                    continue
            f = ROOT.TFile( fname, "read" )
            ROOT.SetOwnership(f, False)
            h_name = variable
            h = f.Get(h_name)
            ROOT.SetOwnership(h, False)
            h.SetTitle("")
            h.SetLineStyle(line_style)
            h.SetLineColor(shade+intensity)
            h.SetMarkerColor(shade+intensity)
            leg.AddEntry( h, chip, "LP" )
            if count==0:
                h.Draw("AP")
            h.Draw("P")

            hist_dict.update({chip: h})
            if (h.GetYaxis().GetXmax()>h_max):
                h_max=h.GetYaxis().GetXmax()
            if (h.GetYaxis().GetXmin()<h_min):
                h_min=h.GetYaxis().GetXmin()
            if (h.GetXaxis().GetXmax()>x_Max):
                x_Max=h.GetXaxis().GetXmax()
            if (h.GetXaxis().GetXmin()<x_Min):
                x_Min=h.GetXaxis().GetXmin()
            count +=1
            h=0
            # f.Close()
            f=0
            # print (h_min, h_max)

        canv.SetLogy()
        h_max *= 2
        h_min /= 2
        print (x_Min, x_Max)
        cc = 0
        for hist in hist_dict:
            if cc==0:
                hist_dict[hist].GetYaxis().SetRangeUser(h_min, h_max)
                hist_dict[hist].GetXaxis().SetLimits(x_Min, x_Max)
                hist_dict[hist].Draw("APL")
            hist_dict[hist].Draw("PL")
            cc +=1

        leg.Draw()

        canvas_name = directory+out_folder+"comp_rateVSmasked_"+config
        canv.SaveAs(canvas_name+".pdf")
