import ROOT
import argparse

parser = argparse.ArgumentParser()
# parser.add_argument("-f1","--chip1", type=str, default='W1RA', help="specify first chip e.g. 'W1RA'")
# parser.add_argument("-f2","--chip2", type=str, default='W1RC', help="specify second chip e.g. 'W1RA'")
# parser.add_argument("-o1","--config1", type=str, default='config_W1RA_Th550_x120_64_y140_64', help="specify desired config1 e.g. 'config_W1RA_Th550_x120_64_y140_64'")
# parser.add_argument("-o2","--config2", type=str, default='config_W1RC_Th550_x120_64_y140_64', help="specify desired config2 e.g. 'config_W1RC_Th550_x120_64_y140_64'")
parser.add_argument("-v","--variable", type=str, default='thres', help="specify variable")
args = parser.parse_args()

directory = "ThScansMalta/"
folder = "/analysis/plots.root"
out_folder = "compPlots/"
chip_dict = {
   
     "W11R5_ITH127_IDB127": ("W11R5/W11R5_welcome", 2),
    # "W1RA_th350_1000p": ( "W1RA/config_W1RA_Th350_x120_64_y140_64/", 632-9 ), #kRed-9 fairfair
    # "W1RB_th350_1000p": ( "W1RB_WTN/config_W1RB_WTN_Th350_x128_64_y140_64/", 400-9 ), #kYellow-9 fairfair
    # "W1RC_th350_1000p": ( "W1RC/config_W1RC_Th350_x120_64_y140_64/", 800-3 ), #kOrange-3 fairfair
    # "W2R1_th350_1000p": ( "W2R1_WTN/config_W2R1_WTN_Th350_x120_64_y140_64/", 616-9 ), #kMagenta-9 fairfair
    #"W11R1_C_th350_1000p": ("W11R1_C/config_W11R1_C_Th350_x120_64_y140_64/", 600-9 ), #kBlue-9 fairfair
    # "W4R12_WTN_th350_1000p": ( "W4R12_WTN/config_W4R12_WTN_Th350_x120_64_y140_64/", 416-9), #kGreen-9 fairfair
    # #
    # "W1RA_th550_1000p": ( "W1RA/config_W1RA_Th550_x120_64_y140_64/", 632+2 ), #kRed+2 darkdark
    # "W1RB_th550_1000p": ( "W1RB_WTN/config_W1RB_WTN_Th550_x128_64_y140_64/", 400+2 ), #kYellow+2  darkdark   
    # "W1RC_th550_1000p": ( "W1RC/config_W1RC_Th550_x120_64_y140_64/", 800+8 ), #kOrange+8 darkdark
    # "W2R1_th550_1000p": ( "W2R1_WTN/config_W2R1_WTN_Th550_x120_64_y140_64/", 616+2 ), #kMagenta+2 darkdark
    #"W11R1_C_th550_1000p": ( "W11R1_C/config_W11R1_C_Th550_x120_64_y140_64/", 600+2 ), #kBlue+2 darkdark
    # "W4R12_WTN_th550_1000p": ( "W4R12_WTN/config_W4R12_WTN_Th550_x120_64_y140_64/", 416+2), #kGreen+2 darkdark
    # #
    # "W1RB_ITH10_IDB127_1000p": ( "W1RB_WTN/config_W1RB_WTN_ITH10_IDB127_x128_64_y140_64/", 400 ), #kYellow  fair   
    # "W2R1_ITH10_IDB127_1000p": ( "W2R1_WTN/config_W2R1_WTN_ITH10_IDB127_x120_64_y140_64/", 616 ), #kMagenta fair
    #"W11R1_C_ITH10_IDB127_1000p": ("W11R1_C/config_W11R1_C_ITH10_IDB127_x120_64_y140_64/", 600 ), #kBlue fair
    # "W4R12_WTN_ITH10_IDB127_1000p": ("W4R12_WTN/config_W4R12_WTN_ITH10_IDB127_x120_64_y140_64/", 416), #kGreen fair
    # #
    # "W1RB_ITH127_IDB50_1000p": ( "W1RB_WTN/config_W1RB_WTN_ITH127_IDB50_x128_64_y140_64/", 400+1 ), #kYellow  dark
    # "W2R1_ITH127_IDB50_1000p": ( "W2R1_WTN/config_W2R1_WTN_ITH127_IDB50_x120_64_y140_64/", 616+1 ), #kMagenta+1 dark
    #"W11R1_C_ITH127_IDB50_1000p": ("W11R1_C/config_W11R1_C_ITH127_IDB50_x120_64_y140_64/", 600+1 ), #kBlue+1 dark
    # "W4R12_WTN_ITH127_IDB50_1000p": ( "W4R12_WTN/config_W4R12_WTN_ITH127_IDB50_x120_64_y140_64/", 416+1), #kGreen dark
    #
    #"W11R1_C_ITH5_IDB50_1000p": ("W11R1_C/config_W11R1_C_ITH5_IDB50_x120_64_y140_64/", 432+2 ), #kCyan+2
    #"W11R1_C_ITH10_IDB20_1000p": ("W11R1_C/config_W11R1_C_ITH10_IDB20_x120_64_y140_64/", 432 ), #kCyan
    #"W11R1_C_ITH5_IDB20_1000p": ("W11R1_C/config_W11R1_C_ITH5_IDB20_x120_64_y140_64/", 432-9 ), #kCyan-9
    #"W11R1_C_ITH5_IDB127_1000p": ("W11R1_C/config_W11R1_C_ITH5_IDB127_x120_64_y140_64/", 840+1 ), #kTeal+1
    #"W11R1_C_ITH127_IDB20_1000p": ("W11R1_C/config_W11R1_C_ITH127_IDB20_x120_64_y140_64/", 840-9 ), #kTeal-9
    #
    # # # "W11R0_C_IDB20_ITHR50_1000p": "../Results_ThScan/W11R0_C/config_W11R0_C_IDB20_ITHR50/",
    # # # "W4R11_WTN_Andrea_finalsetup_3770p": "../Results_ThScan/W4R11_WTN/test_andrea_x128_256_y32_128_finalsetup_ps02/",
    # # # "W4R12_WTN_IDB50_ITHR10_1000p": "../Results_ThScan/W4R12_WTN/config_W4R12_WTN_IDB50_ITHR10_x128_256_y192_224/",
    # # # "W7R12_?_IDB50_ITHR1_1000p": "../Results_ThScan/W7R12_/config_W7R12_IDB50_ITHR10_x128_256_y192_224/",
    # # # "W7R4_C_IDB60_ITHR10_890p": "../Results_ThScan/W7R4_C/config_W7R4_C_IDB60_ITHR10_x128_256_y192_224/",
}



canv = ROOT.TCanvas("canv","canv",800,800)
ROOT.SetOwnership(canv, False)

# leg = ROOT.TLegend(0.60,0.55,0.88,0.88)
leg = ROOT.TLegend(0.40,0.84,0.88,0.89)
ROOT.SetOwnership(leg, False)
leg.SetBorderSize(0)
leg.SetNColumns(2)
leg.SetFillColor(0)
leg.SetTextFont(42)
leg.Draw()
canv.RedrawAxis()

ghost_h = ROOT.TH1F("ghost", "ghost",  100, 0, 2000)
ghost_h.SetStats(0)
ghost_h.SetTitle("")
ghost_h.Draw()

hist_dict = {}
config = ""
h_max = 0.0
x_Min = 1000.0
x_Max = 0.0
for chip in chip_dict:
    path, colour = chip_dict[chip]
    fname = directory+path+folder
    f = ROOT.TFile( fname, "read" )
    ROOT.SetOwnership(f, False)
    h_name = args.variable+"_1D"
    h = f.Get(h_name)
    # print (fname, h_name, h.GetBinWidth(2) )
    h.Scale( 1./h.Integral() )
    h.Rebin(4)
    if args.variable=="noise":
        h.Rebin(4)
    h.SetStats(0)
    h.SetTitle("")
    h.SetLineColor(colour)
    h.SetMarkerColor(colour)
    leg.AddEntry( h, chip, "LP" )
    h.Draw("same hist")
    print (fname, h.GetMean() )

    x_min = h.GetXaxis().GetXmin()
    x_max = h.GetXaxis().GetXmax()
    if x_min<x_Min:
        x_Min=x_min
    if x_max>x_Max:
        x_Max=x_max
    # print x_Max

    if args.variable=="thres":
        # print ("\n>>>>>>>>>>>>>>>>>> %s" % (chip))
        min_fit = h.GetMean()-2.5*h.GetRMS()
        max_fit = h.GetMean()+2.5*h.GetRMS()
        fit_f=ROOT.TF1("line","gaus", min_fit, max_fit)
        ROOT.SetOwnership(fit_f, False)
        fit_f.SetLineColor(colour)
        fit_f.SetLineWidth(3)
        fit_f.SetLineStyle(2)
        h.Fit(fit_f,"M")
        fit_f.Draw("same")

    hist_dict.update({chip: h})
    config += "__"+chip
    if (h.GetMaximum()>h_max):
        h_max=h.GetMaximum()
    h=0
    # f.Close()
    f=0

ghost_h.SetMaximum(1.1*h_max)
if x_Max>800:
    x_Max=800
ghost_h.GetXaxis().SetRangeUser(x_Min, x_Max)
leg.Draw()

canvas_name = directory+out_folder+"comp_"+args.variable+config
# canvas_name = directory+out_folder+"comp_"+args.variable
canv.SaveAs(canvas_name+".pdf")

# canv.SetLogy()
# canv.SaveAs(canvas_name+"_Log.pdf")
