import argparse
import glob


parser = argparse.ArgumentParser(description='''python script for making config files''')
parser.add_argument("-dt", "--DUTTRUE"           , help="DUT1"  , required=True)
parser.add_argument("-itt","--ITHRTRUE"          , help = "ITHR DUT1", required = True)
parser.add_argument("-idt","--IDBTRUE"           , help = "IDB DUT1", required = True)
parser.add_argument("-ibt","--IBIASTRUE"         , help = "IBIAS DUT1", required = True)
parser.add_argument("-df", "--DUTFALSE"          , help="DUT2"  , required=True)
parser.add_argument("-itf","--ITHRFALSE"         , help = "ITHR DUT2", required = True)
parser.add_argument("-idf","--IDBFALSE"          , help = "IDB DUT2", required = True)
parser.add_argument("-ibf","--IBIASFALSE"        , help = "IBIAS DUT2", required = True)

args = parser.parse_args()

c1 =    args.DUTTRUE
ithr1 = args.ITHRTRUE.split(",")
idb1 =  args.IDBTRUE.split(",")
ib1 =   args.IBIASTRUE.split(",")
c2 =    args.DUTFALSE
ithr2 = args.ITHRFALSE.split(",")
idb2 =  args.IDBFALSE.split(",")
ib2 =   args.IBIASFALSE.split(",")

config_path = "/home/sbmuser/MaltaSW/MaltaDAQ/configs/Auto/"

files_c1 = []
files_c2 = []

for t in ithr1:
  for d in idb1:
    for b in ib1:
      files_c1.append(glob.glob("%s*%s*%s%s%s*"%(config_path,c1,d,t,b))[0])

for t in ithr2:
  for d in idb2:
    for b in ib2:
      files_c2.append(glob.glob("%s*%s*%s%s%s*"%(config_path,c2,d,t,b))[0])

#files_c1.sort()
#files_c2 = glob.glob("%s*%s*"%(config_path,c2))
#files_c2.sort()

print(files_c1)

print(files_c2)
print("")
for i in range(len(files_c1)):
  name_c1 = files_c1[i].split("DUT_")[1].split(".txt")[0]
  #print(files_c1[i])
  name_c2 = files_c2[i].split("DUT_")[1].split(".txt")[0]
  name_p = files_c2[i].split("Auto/")[1].split("_W")[0]
  combine = "/home/sbmuser/MaltaSW/MaltaDAQ/configs/Auto/%s_%s_%s.txt"%(name_p,name_c1,name_c2)
  file_combine = open(combine,'w+')
  
  f1 = open(files_c1[i],'r')
  f2 = open(files_c2[i],'r')

  file_combine.write("include: configs/DUT/TelescopePlanes.txt\n[PLANE7]\naddress: udp://ep-ade-gw-02:50007 \n")
  file_combine.write(f1.read())
  file_combine.write("################################################################################################################################################\n[PLANE8]\naddress: udp://ep-ade-gw-02:50008\n")  
  file_combine.write(f2.read())
  
  print("Config file created successfuly: %s" %(combine))

f1.close()
f2.close()
file_combine.close()


