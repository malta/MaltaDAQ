import os
import glob
import argparse
import sys

parser = argparse.ArgumentParser(
    description='''python script for making config files''')
#parser.add_argument("-m", "--mask"          , help="mask path"  , required=True)
parser.add_argument("-c", "--chip"          , help="chip"  , required=True)
#parser.add_argument("-d", "--DUT"           , help="DUT 1/2"  , required=True)
parser.add_argument("-it", "--ITHR"         , help="ITHR tuple"  , default='120')
parser.add_argument("-id", "--IDB"          , help="IDB tuple"  , default='120')
parser.add_argument("-ic", "--ICASN"        , help="ICASN tuple"  , default='10')
parser.add_argument("-ib", "--IBIAS"        , help="IBIAS tuple"  , default='43')
parser.add_argument("-vc", "--VCASN"        , help="VCASN tuple"  , default='110')
parser.add_argument("-vp", "--VRESET_P"     , help="VRESET_P tuple"  , default='29')
parser.add_argument("-vd", "--VRESET_D"     , help="VRESET_D tuple"  , default='65')
parser.add_argument("-vcl", "--VCLIP"       , help="VCLIP tuple"  , default='125')
parser.add_argument("-t", "--type"          , help="DUT type"  , default = 'MALTA2')
parser.add_argument("-tp", "--tapath"       , help="DUT type"  , default = '')
args=parser.parse_args()

#mask = args.mask
chip = args.chip
#dut = args.DUT
ithr = args.ITHR.split(",")
idb = args.IDB.split(",")
ic = args.ICASN
ib = args.IBIAS.split(",")
vc = args.VCASN
vp = args.VRESET_P
vd = args.VRESET_D
vcl = args.VCLIP
t = args.type
tp = args.tapath

if not tp:
  tap = "/home/sbmuser/MaltaSW/Malta2/Results_TapCalib//%s/longCable_doubleChip_calib_run//calib_15.txt" %(chip)
else:
  tap = "/home/sbmuser/MaltaSW/Malta2/Results_TapCalib//%s/longCable_doubleChip_calib_run//calib_15.txt" %(tp)

mask_path = "/home/sbmuser/MaltaSW/MaltaDAQ/configs/Auto/masks/" + chip + '.txt'

try: 
  masking = open(mask_path).read().splitlines()
except:
  print("Mask not found. But here are some suggestions: ")
  mask_oldpath = "/home/sbmuser/MaltaSW/MaltaDAQ/configs/DUT/"
  

  files = glob.glob("%s*%s*" %(mask_oldpath,chip))
  #print("%s*%s"%(mask_oldpath,chip))
  files.sort()
  #print(files)
  File = open(files[0])
  #print(File)
  lines = File.readlines()
  #print (lines)
  i = 0
  for line in lines:
    if "##" in line:
      break
    i+=1
  #print(lines[15:i+1])  ##### Implement more smart
  if "DUT_"+chip in File:
    print("################This configuration was used as DUT1 in %s################"%(files[0]))
    [print(line.strip('\n')) for line in lines[15:i]]
  else:
    print("################This configuration was used as DUT2 in %s################"%(files[0]))
    [print(line.strip('\n')) for line in lines[i+15:]]    
  print("Copy mask config to: %s" %(mask_path))
  sys.exit()

configs = []
for i in idb:
  for j in ithr:
    for k in ib:

      conf_name = "config_telescope_SPS_DUT_%s_%s%s%s.txt"%(chip, i,j,k)
      configs.append(conf_name)
      config = open("/home/sbmuser/MaltaSW/MaltaDAQ/configs/Auto/%s" %(conf_name),"w+")
      '''
      if dut == "False":
        config.write("[PLANE7]\naddress: udp://ep-ade-gw-02:50007 \n")

      else:
        config.write("[PLANE8]\naddress: udp://ep-ade-gw-02:50008 \n")
      '''
      config.write("type: %s \nsample: %s \nSC_ICASN: %s \nSC_IBIAS: %s \nSC_ITHR: %s \nSC_IDB: %s \nSC_VCASN: %s \nSC_VRESET_P: %s \nSC_VRESET_D: %s \nSC_VCLIP: %s \nTAP_FROM_TXT: %s\n\n" %(t, chip, ic, k, j, i, vc, vp, vd, vcl, tap))
      for l in masking:
        config.write("%s \n" %(l))
[print("Succesfully created config: %s%s"%("/home/sbmuser/MaltaSW/MaltaDAQ/configs/Auto/",c))for c in configs]
print("")
config.close()













































