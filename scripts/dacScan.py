#!/usr/bin/env python
######################################################
# DAC scanning script
# the idea: hook up a keithley to a DAC for scanning
# run the script and get a root file with the data
######################################################

import os
import sys
import math
import time
import ROOT
import datetime
import argparse
import array
import MALTA_PSU
import numpy as np
#import AtlasStyle
import signal
import sys

import MaltaSetup
import MaltaBase
import Herakles
import PyMaltaSlowControl
from MaltaBase import *

g_cont=True
def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    global g_cont
    g_cont=False

def setDAC(sc,ipb,dac,val):
    writeA=5 

    #set config mode
    os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")
    os.system("MALTA_PSU.py -c setVoltage DVDD 0.8")

    print ("Setting {} to value {}".format(dac,val))
    readA = 0

    print "\n Enabling DAC monitoring"
    ipb.Write(writeA, sc.switchDACMONV(False))
    ipb.Write(writeA, sc.switchDACMONI(True))
    ipb.Write(writeA, sc.readRegister(21)    )
    DebugWord(ipb.Read(readA), 0)
    time.sleep(0.05)

    if dac == "ICASN":
        ipb.Write(writeA, sc.switchICASN(True) )
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
        ipb.Write(5, sc.setICASN(val)) 
        ipb.Write(5, sc.setICASN(val)) 
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
    elif dac == "IBIAS":
        ipb.Write(writeA, sc.switchIBIAS(True) )
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
        ipb.Write(5, sc.setIBIAS(val))
        ipb.Write(5, sc.setIBIAS(val))
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
    elif dac == "ITHR":
        ipb.Write(writeA, sc.switchITHR(True) )
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
        ipb.Write(5, sc.setITHR(val))
        ipb.Write(5, sc.setITHR(val)) 
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
    elif dac == "IDB":
        ipb.Write(writeA, sc.switchIDB(True) )
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
        ipb.Write(5, sc.setIDB(val))
        ipb.Write(5, sc.setIDB(val))
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
    elif dac == "IRESET":
        ipb.Write(writeA, sc.switchIRESET(True) )
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
        ipb.Write(5, sc.setIRESET(val)) 
        ipb.Write(5, sc.setIRESET(val)) 
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
    elif dac == "VCASN":
        ipb.Write(writeA, sc.switchVCASN(True) )
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
        ipb.Write(5, sc.setVCASN(val)) 
        ipb.Write(5, sc.setVCASN(val)) 
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
    elif dac == "VRESET_P":
        ipb.Write(writeA, sc.switchVRESET_P(True) )
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
        ipb.Write(5, sc.setVRESET_P(val)) 
        ipb.Write(5, sc.setVRESET_P(val)) 
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
    elif dac == "VRESET_D":
        ipb.Write(writeA, sc.switchVRESET_D(True) )
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
        ipb.Write(5, sc.setVRESET_D(val)) 
        ipb.Write(5, sc.setVRESET_D(val)) 
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
    elif dac == "VPLSE_HIGH":
        ipb.Write(writeA, sc.switchVPLSE_HIGH(True) )
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
        ipb.Write(5, sc.setVPLSE_HIGH(val)) 
        ipb.Write(5, sc.setVPLSE_HIGH(val)) 
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
    elif dac == "VPLSE_LOW":
        ipb.Write(writeA, sc.switchVPLSE_LOW(True) )
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
        ipb.Write(5, sc.setVPLSE_LOW(val)) 
        ipb.Write(5, sc.setVPLSE_LOW(val)) 
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
    elif dac == "VCLIP":
        ipb.Write(writeA, sc.switchVCLIP(True) )
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
        ipb.Write(5, sc.setVCLIP(val)) 
        ipb.Write(5, sc.setVCLIP(val)) 
        DebugWord(ipb.Read(readA), 0)
        time.sleep(0.05)
    else :
        print("Unknown DAC: {}".format(dac))
        print("Don't invent new DACs, give me a real one! Bye...")
        exit()

    #unset config mode
    os.system("MALTA_PSU.py -c rampVoltage DVDD 1.8 0.4")
    os.system("MALTA_PSU.py -t T_ANAG_ON.txt ")
    pass



parser=argparse.ArgumentParser()
parser.add_argument("-min","--minDAC",type=int,default= 0)
parser.add_argument("-max","--maxDAC",type=int,default=127)
parser.add_argument("-f","--file",type=str,default="test")
parser.add_argument("-st","--sensortype",required=True,type=str)
parser.add_argument("-o","--output",type=str,default ='/home/sbmuser/MaltaSW/MaltaDAQ/DACScansMALTA')
parser.add_argument("-pw","--pwell",required=True,type=float)
parser.add_argument("-dac","--dac",required=True,type=str)
parser.add_argument("-a","--address",required=True,type=str)
compliance = 0.01 # in amps 

args=parser.parse_args()
actualMaxDAC = args.maxDAC

if args.sensortype == "MINIMALTA" and args.maxDAC is not None :
    actualDACMax = 255
    print("max DAC not given, chip is MINIMALTA, setting maxDAC to 255")

print ("args parsed")

print ("########################################################################################################")
print ("#### This script uses 1 KEITHLEY to measure DAC voltage and 2 TTi PSUs to power the chip ##################")
print ("############################### Patrick and Flo ########################################################")
print ("Example : <<insert fancy example here>>")


print ("############################### Patrick and Flo ########################################################")

print ("############################### SCANNING DAC: {} ########################################################".format(args.dac))
print ("########################################################################################################")
psu=MALTA_PSU.MALTA_PSU()
psu.addPSU("AVDD", "/dev/ttyUSB1","m",2, 2.1,1.200)
psu.addPSU("DVDD", "/dev/ttyUSB1","m",1, 2.1,1.200)
psu.addPSU("SUB",  "/dev/ttyUSB0","m",2, args.pwell+0.1, compliance)
psu.addPSU("PWELL","/dev/ttyUSB0","m",1, args.pwell+0.1, compliance)
##### kEITHLEY FOR READ ONLY BOB
psu.addPSU("DACREAD",  "/dev/ttyUSB6","k",0,0.1, 20e-3)

print ("psus setup")
if args.pwell<0 :
    print("Please use positive values for the biases so you DO NOT FORWARD BIAS the sample")
    sys.exit() 

'''
print ("Please check the setup:")
psu.showConnectedPSUs()

ans = raw_input("Is the setup ok? (type yes to confirm): ")
if ans!="yes":
  print "In that case, have a nice day"
  sys.exit()
'''

print ("Power up the chip")


psu.setCurrentSource("DACREAD")
psu.enablePSU("DACREAD")
time.sleep(0.1)
psu.setVoltageRange("DACREAD",2.0)
time.sleep(0.1)
psu.setVoltageHighPrecision("DACREAD")


#turn of analog and digital
psu.setVoltage("DVDD",0.0)
psu.disablePSU("DVDD")
psu.setVoltage("AVDD",0.0)
psu.disablePSU("AVDD")

#ramp sub and pwell to 0.0 to get to defined state
'''
vPWELL = psu.getVoltage("PWELL")
vSUB = psu.getVoltage("SUB")

if vSUB > vPWELL:
    psu.rampVoltage("SUB",vPWELL)

while 1:
    if vSUB < 0.01 and vPWELL < 0.01:
        break
    vPWELL = max(0.0,vPWELL-1.0)
    vSUB = max(0.0,vSUB-1.0)
    
    psu.rampVoltage("PWELL",vPWELL)
    psu.rampVoltage("SUB",vSUB)
    time.sleep(0.3)
    
#ramp to operational voltages
psu.rampVoltage("SUB",1)
time.sleep(0.3)
psu.rampVoltage("PWELL",1)
time.sleep(0.3)
psu.rampVoltage("SUB",args.pwell)
time.sleep(0.3)
psu.rampVoltage("PWELL",args.pwell)
time.sleep(0.3)

psu.setVoltage("DVDD",1.8)
psu.enablePSU("DVDD")
psu.setVoltage("AVDD",1.8)
psu.enablePSU("AVDD")
time.sleep(0.3)

if args.sensortype == "MINIMALTA"       :
  os.system("MiniMaltaConfig.py") 
elif args.sensortype == "MALTA": 
  os.system("MaltaConfig -c  /home/sbmuser/MaltaSW/MaltaDAQ/configs/config_malta_dacScan.txt") 
else:
  print ("Please write sensor type: -st MINIMALTA or -st MALTA")
  sys.exit()
'''
connstr=args.address
print "Trying to connect to: "+connstr
ipb=Herakles.Uhal(connstr)
GetVersion(ipb,True)
sc=PyMaltaSlowControl.MaltaSlowControl()

print ("Create output file")

#AtlasStyle.SetAtlasStyle()

snow=datetime.datetime.now().strftime("%Y%m%d__%H_%M_%S")
outdir=args.output

os.system("mkdir -p {}".format(outdir)) 
fw=ROOT.TFile.Open("%s/dacScan_%s_%s.root"%(outdir,args.file,snow),"RECREATE")

ntuple       = ROOT.TTree("MaltaIV","MaltaIV")
dac          = array.array('i',(0,))
dacV         = array.array('f',(0,))     

ntuple.Branch("dacV", dacV, "dacV/F")

c1 = ROOT.TCanvas("c1","DAC {} Voltage measurement".format(args.dac), 800,600)
g1 = ROOT.TGraphErrors()
g1.SetName("gDACvsV")
g1.SetTitle(";DAC Setting ; DAC Voltage [V]")
g1.Draw("AP")

print("Ramping DAC setting for {} from {} to {}".format(args.dac,args.minDAC,actualMaxDAC))

signal.signal(signal.SIGINT, signal_handler)
time.sleep(3)
for daci in range(args.minDAC,actualMaxDAC+1):

    if not g_cont: break
    
    print ("Next DAC value: {}".format(daci))

    setDAC(sc,ipb,args.dac,daci)  
    time.sleep(1)

    numdaqs = 5

    meanDACV = 0.0
    meanDACVStddev = 0.0
    for daqs in xrange(numdaqs):
        if not g_cont: break
        print (".")
        if daqs == 0: time.sleep(0.3)
        dacMeas=psu.getVoltage("DACREAD")
        if dacMeas == -999:
          numdaqs -= 1 
          continue
        print("DAC {}: {}".format(args.dac,dacMeas))
        meanDACV  += dacMeas
        meanDACVStddev += (dacMeas*dacMeas)
        pass

    meanDACV /= numdaqs
    meanDACVStddev /= numdaqs
    meanDACVStddev = np.sqrt(meanDACVStddev - meanDACV*meanDACV)/np.sqrt(numdaqs-1)

    dac[0]       = int(daci)
    dacV[0]      = float(meanDACV)   
    
    print ("DAC V: {} +- {} at {}".format(meanDACV, meanDACVStddev, daci))

    p=g1.GetN()
    g1.SetPoint(p,daci,meanDACV)
    g1.SetPointError(p,0,meanDACVStddev)
    c1.Update()
    ntuple.Fill()
    pass

ntuple.Write()
c1.Update()
c1.Print("%s/%s_DACscan_%s.pdf" %(outdir, args.file, args.dac) )
g1.Write()

fw.Close()

print ("Power down the chip")

sys.exit()
while curVoltage>args.pwell:
    curVoltage-=0.1
    psu.setVoltage("SUB",curVoltage)
    time.sleep(1)
    pass

while curVoltage>2:
    curVoltage-=1
    psu.setVoltage("SUB",curVoltage)
    time.sleep(0.1)
    psu.setVoltage("PWELL",curVoltage)
    time.sleep(1)
    pass

psu.rampVoltage("PWELL",0)
psu.rampVoltage("SUB",0)
psu.rampVoltage("AVDD",0)
psu.rampVoltage("DVDD",0)


psu.disablePSU("DVDD")
psu.disablePSU("AVDD")
psu.disablePSU("PWELL")
psu.disablePSU("SUB")

print ("")

raw_input("Scan finished... press any key to continue")

print("Have a nice day")
