#!/usr/bin/env python
####################################
# Daq script for MALTA read-out
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# December 2017
####################################

import os
import argparse
import array
import sys
import ROOT
#import Style
#Style.SetStyle()
ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)

parser=argparse.ArgumentParser()
parser.add_argument('-f' ,'--file',help='output file',default="output.root")
parser.add_argument('-dr','--duplicateRemoval',help='remove duplicates',type=bool,default=True)
parser.add_argument('-t' ,'--applyTimingCut',help='apply timing cut [30,300]',type=bool,default=False)
#####parser.add_argument('-a' ,'--applyFlorescenceCut',help='apply noise cut for florescence setup',type=bool,default=False)
args=parser.parse_args()


class Tuple(dict):
    def __init__(self):
        self.treenumber=-1
    def LoadBranches(self,t):
        if self.treenumber == t.GetTreeNumber(): return
        self.treenumber = t.GetTreeNumber()
        for i in range(t.GetListOfLeaves().GetEntries()):
            self.__setattr__(t.GetListOfLeaves().At(i).GetName(),
                             t.GetListOfLeaves().At(i))
            pass
        pass

ROOT.gStyle.SetPadLeftMargin(0.134)
ROOT.gStyle.SetPadBottomMargin(0.146)
ROOT.gStyle.SetPadRightMargin(0.035)
ROOT.gStyle.SetPadTopMargin(0.078)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetLabelSize(0.05,"x")
ROOT.gStyle.SetLabelSize(0.05,"y")
ROOT.gStyle.SetTitleSize(0.05,"t")
ROOT.gStyle.SetTitleSize(0.05,"x")
ROOT.gStyle.SetTitleSize(0.05,"y")
ROOT.gStyle.SetTitleOffset(1.40,"y")
#ROOT.gStyle.SetOptTitle(0);
ROOT.gStyle.SetOptStat(0);

chain = ROOT.TChain("MALTA")
chain.AddFile(args.file)
tree = Tuple()

refbit  = []
pixel   = []
group   = []
parity  = []
delay   = []
dcolumn = []
bcid    = []
chipid  = []
phase   = []
evtid   = []
word1   = []
word2   = []
l1id    = []
timer   = []
isDuplicate   = []


entry = 0
while True:
    chain.GetEntry(entry)
    if chain.LoadTree(entry)<0:
        break
    entry+=1
    if entry%100000==0: print entry
    tree.LoadBranches(chain)
    ####refbit.append(tree.refbit.GetValue())
    pixel.append(tree.pixel.GetValue())
    group.append(tree.group.GetValue())
    parity.append(tree.parity.GetValue())
    #delay.append(tree.delay.GetValue())
    dcolumn.append(tree.dcolumn.GetValue())
    bcid.append(tree.bcid.GetValue())
    #chipid.append(tree.chipid.GetValue())
    phase.append(tree.phase.GetValue())
    evtid.append(tree.winid.GetValue())
    l1id.append(tree.l1id.GetValue())
    #print tree.word1.GetValue()
    #print tree.word2.GetValue()
    #word1.append(tree.word1.GetValue())
    #word2.append(tree.word2.GetValue())
    ###########timer.append(tree.timer.GetValue())
    isDuplicate.append(tree.isDuplicate.GetValue())
    pass


#################################################################################################################################################
xmin= -0.5
xmax=511.5
ymin= -0.5
ymax=511.5

cPixelHitMap = ROOT.TCanvas("cPixelHitMap","PixelHitMap",600,600)
cPixelHitMap.SetRightMargin(0.11)
hPixelHitMap = ROOT.TH2D("hPixelHitMap","PixelHitMap",512,xmin,xmax,512,ymin,ymax)

allEvents=0
uniqueEvents=0
a = 0
eventsPassed=0
for event in xrange(len(dcolumn)):
    if isDuplicate[event] == 1: continue #add args
    if phase[event]==7        : continue ##VD: noise protection

    fullTime=bcid[event]*25+evtid[event]*3.125+phase[event]*0.39
    ###print fullTime
    if args.applyTimingCut:
        if fullTime>370: continue
        if fullTime<270: continue

    ##if bcid[event]!=0: continue ##VALERIO!!!!!!!!!!!!!!11 AAAAAAARRRRRHHHHHHH
    FUCKpix = int(pixel[event])
    eventsPassed+=1
    #pixelStr = format(int("%.f"%(pixel[event])),'016b')
    for pix in xrange(16):
        if ((FUCKpix>>pix)&1)==0: continue
        
        xpos = dcolumn[event]*2
        if pix>7: xpos+=1
        
        ypos = group[event]*16
        if parity[event]==1: ypos += 8
        
        if (pix == 0) or (pix == 8):  ypos+=0
        if (pix == 1) or (pix == 9):  ypos+=1
        if (pix == 2) or (pix == 10): ypos+=2
        if (pix == 3) or (pix == 11): ypos+=3
        if (pix == 4) or (pix == 12): ypos+=4
        if (pix == 5) or (pix == 13): ypos+=5
        if (pix == 6) or (pix == 14): ypos+=6
        if (pix == 7) or (pix == 15): ypos+=7
        #print " XPOS: "+str(xpos)+" -- YPOS: "+str(ypos)
        
        hPixelHitMap.Fill(xpos,ypos)

        pass
    pass
oFile=ROOT.TFile("Plots/PixelHitMap_%s.root"%args.file[:-5],"RECREATE")
print "Total event number: %s"%len(dcolumn)
print "Number of events after duplication: %s"%(eventsPassed)
print "Number of pixel hits after duplication: %s"%(hPixelHitMap.Integral())
hPixelHitMap.SetTitle("Frequency;Pixel PosX;Pixel PosY")
hPixelHitMap.SetMinimum(0.)
###hPixelHitMap.SetMaximum(1.)

hPixelHitMap.Draw("COLZ") ###TEXT
AllHit=float(hPixelHitMap.Integral())
print " " 
sumV=0
seen=0
for x in xrange(0,516):
    for y in xrange(0,516):
        cont=hPixelHitMap.GetBinContent(x,y)
        if cont!=0: seen+=1
        ####if cont!=0: ###IVAN  and hPixelHitMap.GetXaxis().GetBinCenter(x)==222 and hPixelHitMap.GetXaxis().GetBinCenter(y)==267: ###IVAN and 
        if float(cont)/AllHit>0.001:
            theString=""
            theString+="(x,y)= %2i :: %2i"%(hPixelHitMap.GetXaxis().GetBinCenter(x),hPixelHitMap.GetXaxis().GetBinCenter(y))
            ###IVAN 
            print " X="+str(int(hPixelHitMap.GetXaxis().GetBinCenter(x)))+" :: "+str(int(hPixelHitMap.GetYaxis().GetBinCenter(y)))+"  entries: "+str(int(cont))
            ##print ""+str(int(hPixelHitMap.GetXaxis().GetBinCenter(x)))+","+str(int(hPixelHitMap.GetYaxis().GetBinCenter(y)))+","+str(int(cont))
            #print theString
            sumV+=cont
print " "
for x in xrange(0,516):
    totCol=0
    for y in xrange(0,516):
        cont=hPixelHitMap.GetBinContent(x,y)
        totCol+=cont
    ###if totCol!=0: print "Col: "+str(x-1)+"  saw "+str(int(totCol))+" hits!"

print " "
print " Represented fraction is: "+str( float(sumV)/AllHit*100 )+" %"
print " number of pixels with hits: "+str(seen)
print " "
#hPixelHitMap.GetZaxis().SetRangeUser(5000,1000)
hPixelHitMap.SetMaximum(hPixelHitMap.GetMaximum()*0.03)
hPixelHitMap.RebinX(2)
hPixelHitMap.RebinY(2)
cPixelHitMap.SetLogz()
cPixelHitMap.Update()
oFile.WriteObject(hPixelHitMap,"Map2D")
cPixelHitMap.Write()
oFile.Close()
oName="Plots/PixelHitMap_%s.pdf"%args.file[:-5]
cPixelHitMap.SaveAs(oName)
###IVAN 
os.system("evince "+oName+" &")
#cPixelHitMap.SaveAs("Plots/PixelHitMap_%s.root"%args.file[:-5])
#cPixelHitMap.SaveAs("Plots/PixelHitMap_%s.c"%args.file[:-5])

