import os
import argparse
import re
import numpy as np
import time
from ROOT import *

parser = argparse.ArgumentParser()
parser.add_argument("-f","--folder"     , type=str           , default='W7R12_testanalysis', help="specify folder e.g. './ThScansMalta/W7R12_testanalysis/blahblah'")
parser.add_argument("-x","--xvariable"     , type=str           , default='IDB', help="specify variable")
parser.add_argument("-y","--yvariable"     , type=str           , default='ITHR', help="specify variable")
parser.add_argument("-no","--novariable"     , type=str           , default='IRESET', help="specify variable")
args = parser.parse_args()

gROOT.SetBatch(True)

dir_scans = "ThScansMalta/"+args.folder

dir_list = next(os.walk(dir_scans))[1]
print(dir_list, dir_scans) 
np_x = np.array([])
np_y = np.array([])
np_t = np.array([])
np_n = np.array([])
np_th = np.array([])
np_nh = np.array([])
np_all = np.array([])

h_pixel=TH1F("n_pixels","n_pixels", len(dir_list),0, len(dir_list)/2)

h_bin=0
for i in dir_list:
   
   if args.yvariable in i and args.novariable  in i:
     print (i)
     s=i
     m1 = re.search(args.xvariable+'(\d+)', i, re.IGNORECASE)
     print( args.xvariable, m1.group(1))
     np_x=np.append(np_x, [float(m1.group(1))])
     m2 = re.search(args.yvariable+'(\d+)', i, re.IGNORECASE)
     print( args.yvariable, m2.group(1))
     np_y=np.append(np_y, [float(m2.group(1))])
     #     [int(s) for s in str.split() if s.isdigit()]
     f = TFile.Open(dir_scans+"/"+i+"/analysis/plots.root", "read")
     h = f.Get("thres_1D")
     print(h.GetMean())
     np_t=np.append(np_t, [h.GetMean()])
     np_th=np.append(np_th, [h.GetRMS()/pow(h.GetEntries(),0.5)])
     print(h.GetRMS())
     h= f.Get("noise_1D")
     np_n=np.append(np_n, [h.GetMean()])
     np_nh=np.append(np_nh, [h.GetRMS()/pow(h.GetEntries(),0.5)])

     h_bin=h_bin+1
     h_pixel.SetBinContent(h_bin, h.GetEntries())
     h_pixel.GetXaxis().SetBinLabel(h_bin, (args.xvariable+" "+str(m1.group(1))+" "+args.yvariable+" "+str(m2.group(1))  ))

print (np_x)
print (np.unique(np_x))

print (np_y)
print (np.unique(np_y))
print (np_t)

c = TCanvas("c","c", 800,600)
c1 = TCanvas("cnoise","cnoise", 800,600)
c2 = TCanvas("ct_over_n","ct_over_n", 800,600)
g = []
gnoise = []
gt_over_n = []

first = True
k=0
for y in np.unique(np_y):
  g.append( TGraphErrors())
  gnoise.append( TGraphErrors())
  gt_over_n.append( TGraph())
  g[k].SetNameTitle("thres"+args.yvariable+str(y),"thres"+args.yvariable+str(y))
  gnoise[k].SetNameTitle("noise"+args.yvariable+str(y),"noise"+args.yvariable+str(y))
  gt_over_n[k].SetNameTitle("t_over_n"+args.yvariable+str(y),"t_over_n"+args.yvariable+str(y))
  counter = 0
  for x in np.unique(np.sort(np_x)):
    #print ( x, y )
    for i in range (0, len(np_y)):
      #print (x, y, np_y[i], np_x[i])
      if x == np_x[i] and y == np_y[i]:
       # print (x, y, np_t[i], counter)
       # print ('IDB',  np_x[i], 'ITH', np_y[i],np_t[i], counter)
        g[k].SetPoint(counter, float(np_x[i]), float(np_t[i]))
        gnoise[k].SetPoint(counter, float(np_x[i]), float(np_n[i]))
        gt_over_n[k].SetPoint(counter, float(np_x[i]), float(np_t[i]/np_n[i]))
        g[k].SetPointError(counter, 0, float(np_th[i]))
        gnoise[k].SetPointError(counter, 0, float(np_nh[i]))
        counter = counter +1
  g[k].Print()
  if k==0:
    c.cd()
    g[0].Draw("apl")
    c1.cd()
    gnoise[0].Draw("apl")
    c2.cd()
    gt_over_n[0].Draw("apl")
  else:
    c.cd()
    g[k].Draw("pl,same")
    c1.cd()
    gnoise[k].Draw("pl,same")
    c2.cd()
    gt_over_n[k].Draw("pl,same")
  k=k+1
f = TFile("test.root","RECREATE")
f.cd()
c.Write()
c1.Write()
c2.Write()
h_pixel.LabelsDeflate("X")
h_pixel.LabelsDeflate("Y")
h_pixel.LabelsOption("v")
#h_pixel.GetXaxis().LabelsOption("")
h_pixel.Write()
for x in g:
  x.Write()
for x in gnoise:
  x.Write()
for x in gt_over_n:
  x.Write()

f.Close()
#c.SaveAs("test.pdf")
#c.SaveAs("test.root")


