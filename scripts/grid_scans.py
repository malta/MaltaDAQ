import os
import argparse
import re
import numpy as np
import time
from ROOT import *
from ROOT import TLegend

parser = argparse.ArgumentParser()
parser.add_argument("-f","--folder"     , type=str           , default='W7R12_testanalysis', help="specify folder e.g. './Results_ThScan/W7R12_testanalysis/blahblah'")
parser.add_argument("-x","--xvariable"     , type=str           , default='IDB', help="specify variable")
parser.add_argument("-y","--yvariable"     , type=str           , default='ITHR', help="specify variable")
parser.add_argument("-no","--novariable"     , type=str           , default='IRESET', help="specify variable")
parser.add_argument("-c","--chipname"     , type=str           , default='WXRX', help="chip name")

args = parser.parse_args()

gROOT.SetBatch(True)

dir_scans = "Results_ThScan/"+args.folder

dir_list = next(os.walk(dir_scans))[1]
print(dir_list, dir_scans) 
np_x = np.array([])
np_y = np.array([])
np_t = np.array([])
np_n = np.array([])
np_th = np.array([])
np_nh = np.array([])
np_all = np.array([])

h_pixel=TH1F("n_pixels","n_pixels", len(dir_list),0, len(dir_list)/2)

v=['Glue', 'CABLE', 'SUB', 'IRESET', 'ICASN']
a=[]
for vv in v:
  for i in dir_list:
    #dir_list.remove(i) 
    if vv in i:
     a.append(i)
for i in a:
  dir_list.remove(i)
print (dir_list)
h_bin=0
for i in dir_list:
   
   if args.yvariable in i and args.novariable not in i and args.xvariable in i:
     print (i)
     s=i
     m1 = re.search(args.xvariable+'(\d+)', i, re.IGNORECASE)
     print( args.xvariable, m1.group(1))
     m2 = re.search(args.yvariable+'(\d+)', i, re.IGNORECASE)
     print( args.yvariable, m2.group(1))
     #     [int(s) for s in str.split() if s.isdigit()]
     f = TFile.Open(dir_scans+"/"+i+"/analysis/plots.root", "read")
     h = f.Get("thres_1D")
     print(h.GetMean())
     if h.GetEntries()>0:
       np_x=np.append(np_x, [float(m1.group(1))])
       np_y=np.append(np_y, [float(m2.group(1))])
       np_t=np.append(np_t, [h.GetMean()])
       np_th=np.append(np_th, [h.GetRMS()/pow(h.GetEntries(),0.5)])
       print(h.GetRMS())
       h= f.Get("noise_1D")
       np_n=np.append(np_n, [h.GetMean()])
       np_nh=np.append(np_nh, [h.GetRMS()/pow(h.GetEntries(),0.5)])

       h_bin=h_bin+1
       h_pixel.SetBinContent(h_bin, h.GetEntries())
       h_pixel.GetXaxis().SetBinLabel(h_bin, (args.xvariable+" "+str(m1.group(1))+" "+args.yvariable+" "+str(m2.group(1))  ))
       # np_x.pop()
       #np_y.pop()
print (np_x)
print (np.unique(np_x))

print (np_y)
print (np.unique(np_y))
print (np_t)

c = TCanvas("c","c", 800,600)
c1 = TCanvas("cnoise","cnoise", 800,600)
c2 = TCanvas("ct_over_n","ct_over_n", 800,600)
g = []
gnoise = []
gt_over_n = []

first = True
k=0
legend = TLegend(0.1,0.7,0.48,0.9);
for y in np.unique(np_y):
  g.append( TGraphErrors())
  gnoise.append( TGraphErrors())
  gt_over_n.append( TGraph())
  g[k].SetNameTitle("thres"+args.yvariable+str(y),"thres"+args.yvariable+str(y))
  gnoise[k].SetNameTitle("noise"+args.yvariable+str(y),"noise"+args.yvariable+str(y))
  gt_over_n[k].SetNameTitle("t_over_n"+args.yvariable+str(y),"t_over_n"+args.yvariable+str(y))
  g[k].SetLineColor(k+1)
  g[k].SetMarkerColor(k+1)
  g[k].GetYaxis().SetTitle("threshold el")
  g[k].GetXaxis().SetTitle(args.xvariable)
  gnoise[k].SetLineColor(k+1)
  gnoise[k].SetMarkerColor(k+1)
  legend.SetHeader(args.chipname,"C"); #// option "C" allows to center the header
  legend.AddEntry(g[k],args.yvariable+str(y),"l");

  counter = 0
  for x in np.unique(np.sort(np_x)):
    #print ( x, y )
    for i in range (0, len(np_y)):
      #print (x, y, np_y[i], np_x[i])
      if x == np_x[i] and y == np_y[i]:
       # print (x, y, np_t[i], counter)
       # print ('IDB',  np_x[i], 'ITH', np_y[i],np_t[i], counter)
        g[k].SetPoint(counter, float(np_x[i]), float(np_t[i]))
        gnoise[k].SetPoint(counter, float(np_x[i]), float(np_n[i]))
        gt_over_n[k].SetPoint(counter, float(np_x[i]), float(np_t[i]/np_n[i]))
        g[k].SetPointError(counter, 0, float(np_th[i]))
        gnoise[k].SetPointError(counter, 0, float(np_nh[i]))
        counter = counter +1
  g[k].Print()
  if k==0:
    c.cd()
    g[0].Draw("apl")
    c1.cd()
    gnoise[0].Draw("apl")
    c2.cd()
    gt_over_n[0].Draw("apl")
  else:
    c.cd()
    g[k].Draw("pl,same")
    legend.Draw("same")
    c1.cd()
    gnoise[k].Draw("pl,same")
    legend.Draw("same")
    c2.cd()
    gt_over_n[k].Draw("pl,same")
  k=k+1
f = TFile(args.chipname+".root","RECREATE")
f.cd()
c.Write()
c1.Write()
c2.Write()
h_pixel.LabelsDeflate("X")
h_pixel.LabelsDeflate("Y")
h_pixel.LabelsOption("v")
#h_pixel.GetXaxis().LabelsOption("")
h_pixel.Write()
for x in g:
  x.Write()
for x in gnoise:
  x.Write()
for x in gt_over_n:
  x.Write()

f.Close()
#c.SaveAs("test.pdf")
#c.SaveAs("test.root")

#useful
#for i in "${IDB[@]}"; do for j in "${ICASN[@]}"; do echo $i $j; python makeConfigFile.py -c W7R12 -IDB $i -ICASN $j -ITHR 10; done ; done

#for i in "${IDB[@]}"; do for j in "${ITHR[@]}"; do echo $i $j; python makeConfigFile.py -c W7R12 -IDB $i -ITHR $j; echo "MaltaThresholdScan -e -F -q -t 500 -h 89 -l 30 -s 1 -f W7R12_WTN -o config_W7R12_WTN_IDB${i}_ITHR${j}_SUB30_x210_260_y220_250  -r 210 50 220 30 -a udp://ep-ade-gw-07.cern.ch:50001 -c configs/config_W7R12_IDB${i}_ITHR${j}.txt" >> run_scan.sh; done ; done
