import os
import argparse
import re
import numpy as np
import time

import sys

parser = argparse.ArgumentParser()
parser.add_argument("-c"		,"--chip"    	   , type=str           , default='', help="specify chip name e.g. W7R12'")
parser.add_argument("-ICASN"		,"--ICASN"         , type=int           , default=10 	 , help="specify ICASN value")
parser.add_argument("-IBIAS"		,"--IBIAS"     	   , type=int           , default=80	 , help="specify IBIAS value")
parser.add_argument("-ITHR"		,"--ITHR"    	   , type=int           , default=30	 , help="specify ITHR value")
parser.add_argument("-IDB"		,"--IDB"     	   , type=int           , default=70	 , help="specify IDB value")
parser.add_argument("-IRESET"		,"--IRESET"        , type=int           , default=30	 , help="specify IRESET value")
parser.add_argument("-VCASN"		,"--VCASN"         , type=int           , default=64	 , help="specify VCASN value")
parser.add_argument("-VRESET_P"		,"--VRESET_P"      , type=int           , default=45	 , help="specify VRESET_P value")
parser.add_argument("-VRESET_D"		,"--VRESET_D"	   , type=int           , default=65	 , help="specify VRESET_D value")
parser.add_argument("-VPULSE_HIGH"	,"--VPULSE_HIGH"   , type=int           , default=90	 , help="specify VPULSE_HIGH value")
parser.add_argument("-VPULSE_LOW"	,"--VPULSE_LOW"    , type=int           , default=5	 , help="specify VPULSE_LOW value")
parser.add_argument("-VCLIP"		,"--VCLIP" 	   , type=int           , default=127	 , help="specify VCLIP value")
args = parser.parse_args()


if args.chip =='':
  print ("provide chip name")
  exit(0)


fullCmdArguments = sys.argv
argumentList = fullCmdArguments[1:]
print(argumentList)
argumentList= np.array(argumentList).reshape(len(argumentList)/2,2 )
print(argumentList)
#print value
outputfile = "config_"+args.chip
if len (argumentList[0])>1 : 
  for x, y in argumentList:
    x=x.replace("-","")
    if x != "c":

      outputfile+="_"+x+y
      print x , y

print ("creating the following file: %s\n" % outputfile)

f= open("configs/"+outputfile+".txt","w+")
f.write("chip ID: %s\n" % args.chip)
f.write("SC_ICASN: %d\n" % args.ICASN)
f.write("SC_IBIAS: %d\n" % args.IBIAS)
f.write("SC_ITHR: %d\n" % args.ITHR)
f.write("SC_IDB: %d\n" % args.IDB)
f.write("SC_IRESET: %d\n" % args.IRESET)
f.write("SC_VCASN: %d\n" % args.VCASN)
f.write("SC_VRESET_P: %d\n" % args.VRESET_P)
f.write("SC_VRESET_D: %d\n" % args.VRESET_D)
f.write("SC_VPULSE_HIGH: %d\n" % args.VPULSE_HIGH)
f.write("SC_VPULSE_LOW: %d\n" % args.VPULSE_LOW)
f.write("SC_VCLIP: %d\n" % args.VCLIP)
f.close()


#default values used for the config
"""
SC_ICASN: 10
SC_IBIAS: 80
SC_ITHR: 30
SC_IDB: 70
SC_IRESET: 30
SC_VCASN: 64
SC_VRESET_P: 127
SC_VRESET_D: 65
SC_VPULSE_HIGH: 90
SC_VPULSE_LOW: 5
SC_VCLIP: 127
"""

