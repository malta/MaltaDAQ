import subprocess

#result = subprocess.check_output(batcmd, shell=True)
#print(result)
#import matplotlib.pyplot as plt
#import numpy as np
#plt.ion() ## Note this correction
#fig=plt.figure()
#plt.axis([0,1000,0,1])
import os
import time



def getVal(command):

    VALcmd =  str(command)
    VAL= str(subprocess.check_output(VALcmd, shell=True))
    VAL=str(VAL.split()[-1])
    VAL = VAL.replace("\\n'","")

    return VAL

i=0
x=list()
y=list()

while i <1:
    #temp_y=np.random.random();
    #x.append(1);
    #y.append(1);
    #plt.scatter(i,temp_y);
    i+=1;
    #plt.show()
    #plt.pause(10) #Note this correction
    #SUB =  str(os.system("MALTA_PSU.py -c getVoltage SUB | grep SUB"))


    SUB_V=getVal("MALTA_PSU.py -c getVoltage SUB | grep SUB")
    PWELL_V =getVal("MALTA_PSU.py -c getVoltage PWELL | grep PWELL") 
    SUB_A = getVal("MALTA_PSU.py -c getCurrent SUB | grep SUB")
    PWELL_A =getVal("MALTA_PSU.py -c getCurrent PWELL | grep PWELL") 
    DVDD_V= getVal("MALTA_PSU.py -c getVoltage DVDD | grep DVDD")
    DVDD_A=getVal("MALTA_PSU.py -c getCurrent DVDD | grep DVDD")
    AVDD_V=getVal("MALTA_PSU.py -c getVoltage AVDD | grep AVDD")
    AVDD_A=getVal("MALTA_PSU.py -c getCurrent AVDD | grep AVDD")
    #print(SUB_V, SUB_A, PWELL_V, PWELL_A, DVDD_V, DVDD_A, AVDD_V, AVDD_A)

    print(f'SUB = {SUB_V} V, SUB = {SUB_A} mA, PWELL = {PWELL_V} V, PWELL = {PWELL_A} mA, DVDD = {DVDD_V} V, DVDD = {DVDD_A} mA, AVDD = {AVDD_V} V, AVDD = {AVDD_A} mA')
  

    SUB_V=getVal("MALTA_PSU.py -c getVoltage DUT_SUB | grep SUB")
    PWELL_V =getVal("MALTA_PSU.py -c getVoltage DUT_PWELL | grep PWELL") 
    SUB_A = getVal("MALTA_PSU.py -c getCurrent DUT_SUB | grep SUB")
    PWELL_A =getVal("MALTA_PSU.py -c getCurrent DUT_PWELL | grep PWELL") 
    DVDD_V= getVal("MALTA_PSU.py -c getVoltage DUT_DVDD | grep DVDD")
    DVDD_A=getVal("MALTA_PSU.py -c getCurrent DUT_DVDD | grep DVDD")
    AVDD_V=getVal("MALTA_PSU.py -c getVoltage DUT_AVDD | grep AVDD")
    AVDD_A=getVal("MALTA_PSU.py -c getCurrent DUT_AVDD | grep AVDD")

    print(f'DUT_SUB = {SUB_V} V, DUT_SUB = {SUB_A} mA, DUT_PWELL = {PWELL_V} V, DUT_PWELL = {PWELL_A} mA, DUT_DVDD = {DVDD_V} V, DUT_DVDD = {DVDD_A} mA, DUT_AVDD = {AVDD_V} V, DUT_AVDD = {AVDD_A} mA')
  



    #time.sleep(20)

