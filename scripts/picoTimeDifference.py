import os
import math
import numpy as np
from  ROOT import TCanvas ,TFile, TH1F, TTree, TGraphErrors, gROOT, gStyle
gROOT.SetBatch(True)
gStyle.SetOptFit(True)

texfile = open("./blahblah/texfile_plots.tex", "w")

texfile.write("\documentclass[aspectratio=169]{beamer} \n")
texfile.write("\usepackage{graphicx} \n")
texfile.write("\usepackage{changepage} \n")
texfile.write("\\begin{document} \n")

files = os.listdir("./blahblah")


v_pix = []
for f in files:
  if "run_" in f:
    pix=f.replace("run_col","")
    pix=pix.replace(".root.root","")
    pix=pix.split("_")
    v_pix.append(int(pix[0]))

v_pix = np.unique(np.array(v_pix))

for pixX in v_pix:  
  npoint = 0
  g=TGraphErrors()
  g_tot=TGraphErrors()
  g_totRMS=TGraphErrors()
  gsecon_peak=TGraphErrors()
  points80 = []
  points50 = []
  
  for f in files:
     
    if "run_" in f:
      pix=f.replace("run_col","")
      pix=pix.replace(".root.root","")
      pix=pix.split("_")
      if int(pix[0]) == pixX:
        tfile = TFile("./blahblah/"+f,"read")
        if tfile.IsZombie() ==True:
          continue
        tree=tfile.Get("PICOTDC")
        diff=TH1F("diff","diff",500,0,50)
        tot=TH1F("tot","tot",500,0,500)
        ch0=TH1F("ch0","ch0",500,0,100)
        ch0_tot=TH1F("ch0_tot","ch0_tot",500,0,100)
        ch0_2nd=TH1F("ch0_2nd","ch0_2nd",500,43,100)
        nhits=TH1F("nhits","nhits", 10, 0, 10)
        c = TCanvas("c","c", 800,800)
        tree.Draw("channel0_leading-channel15_leading[0]>>ch0","")
        c.SaveAs("./blahblah/ch0_"+str(pixX)+"_"+str(pix[1])+".eps")
        tree.Draw("channel0_leading-channel15_leading[0]>>ch0_2nd","")
        tree.Draw("channel0_tot>>ch0_tot","channel0_leading-channel15_leading[0]>43")
        #if pixX == 51:
          #ch0.SaveAs("ch0_"+str(pixX)+"_"+str(pix[1])+".root")
        tree.Draw("channel0_leading-channel0_leading[0]>>diff","abs(channel0_leading-channel0_leading[0])>0.0")
        c.SaveAs("./blahblah/diff_"+str(pixX)+"_"+str(pix[1])+".eps")
        tree.Draw("Length$(channel0_leading)>>nhits")
        c.SaveAs("./blahblah/nhits_"+str(pixX)+"_"+str(pix[1])+".eps")
        texfile.write("\\begin{frame} \\begin{adjustwidth}{-4em}{-4em} \n")
        texfile.write("\\frametitle{pixX "+str(pixX)+", pixY"+str(pix[1])+"-504: channel 0 leading edge (left), difference 2 peaks (center), number of hits (right)} \n")
        texfile.write("\\begin{figure} \n")
        texfile.write("\includegraphics[scale=0.25]{ch0_"+str(pixX)+"_"+str(pix[1])+".eps} \n")
        texfile.write("\includegraphics[scale=0.25]{diff_"+str(pixX)+"_"+str(pix[1])+".eps} \n")
        texfile.write("\includegraphics[scale=0.25]{nhits_"+str(pixX)+"_"+str(pix[1])+".eps} \n")
        texfile.write("\end{figure} \n")
        texfile.write("\end{adjustwidth} \end{frame} \n")
        twohits = nhits.GetBinContent(3)/nhits.Integral()
        del c
  
        #if twohits <= 0.5:
        #  points50.append ( [int(pix[1]),twohits])
        #  continue
        if twohits <= 0.8:
          points80.append ( [504-int(pix[1]),twohits])
          if twohits < 0.1:
            continue
        val = diff.GetMean()
        g.SetPoint(npoint, 504-int(pix[1]), val) 
        g_tot.SetPoint(npoint, 504-int(pix[1]), ch0_tot.GetMean()) 
        g_totRMS.SetPoint(npoint, 504-int(pix[1]), ch0_tot.GetRMS()) 
        g.SetPointError(npoint, 0, diff.GetRMS())#/math.sqrt(diff.Integral())) 
	gsecon_peak.SetPoint(npoint, 504-int(pix[1]), ch0_2nd.GetRMS())
	#gsecon_peak.SetPointError(npoint, 0, ch0_2nd.GetRMS())
        npoint=npoint+1
        tfile.Close()
        del nhits
        del diff
        del ch0
        del tree
        #PICOTDC->Draw("channel0_leading-channel0_leading[0]","channel0_leading-channel0_leading[0]>0.01")
  
  g.Print("v")
  c = TCanvas("c1","c1", 800,800)
  g.SetMarkerStyle(9)
  g.GetXaxis().SetTitle("#Delta pixY")
  g.GetYaxis().SetTitle("#Delta t ns")
  g.GetYaxis().SetLimits(0,30)
  
  g.Draw("ap")
  g.Fit("pol1")
  g.GetXaxis().SetLimits(0, 512);
  c.Update();
  g.GetYaxis().SetRangeUser(0, 35);

  c.Update();
  c.SaveAs("./blahblah/linearfit"+str(pixX)+".eps")
  c.SaveAs("./blahblah/linearfit"+str(pixX)+".root")
  gsecon_peak.Draw("ap")
  gsecon_peak.Fit("pol1")
  gsecon_peak.GetXaxis().SetLimits(0, 512);
  c.Update();
  gsecon_peak.GetYaxis().SetRangeUser(30, 70);
  c.SaveAs("./blahblah/linearfitsecond"+str(pixX)+".eps")
  c.SaveAs("./blahblah/linearfitsecond"+str(pixX)+".root") 

  g_tot.Draw("ap")
  #g_tot.Fit("pol1")
  g_tot.GetXaxis().SetLimits(0, 512);
  c.Update();
  #g_tot.GetYaxis().SetRangeUser(30, 70);
  c.SaveAs("./blahblah/tot"+str(pixX)+".eps")
  c.SaveAs("./blahblah/tot"+str(pixX)+".root") 

  g_totRMS.Draw("ap")
  #g_tot.Fit("pol1")
  g_totRMS.GetXaxis().SetLimits(0, 512);
  c.Update();
  #g_tot.GetYaxis().SetRangeUser(30, 70);
  c.SaveAs("./blahblah/totRMS"+str(pixX)+".eps")
  c.SaveAs("./blahblah/totRMS"+str(pixX)+".root") 


 

  texfile.write("\\begin{frame}  \n")
  texfile.write("\\frametitle{linear fit col "+str(pixX)+"} \n")
  texfile.write("\\begin{tabular}{cl} \n")
  texfile.write("\\begin{tabular}{c} \n")
  texfile.write("\includegraphics[scale=0.3]{linearfit"+str(pixX)+".eps} \n")
  texfile.write("\end{tabular} \n")
  texfile.write("\\begin{tabular}{l}")
  if len(points80) > 0:
    texfile.write("\\begin{tabular}{ll} \n")
    texfile.write("\centering \n")
    texfile.write("pixY missing & events with $<$ 2 hits\% \\\\ \n")
    for pair in points80:
      texfile.write(str(int(pair[0]))+" & "+ str(int(100*pair[1])) +"\% \\\\ \n")
    texfile.write("\end{tabular} \n")
  texfile.write("\end{tabular} \n")
  texfile.write("\end{tabular} \n")
  texfile.write("\end{frame} \n") 
  
  print ("points less than 50%")
  
  print (points50)
  print ("points less than 80%")
  print (points80)
  del g
  del c

texfile.write("\end{document} \n")
texfile.close()
