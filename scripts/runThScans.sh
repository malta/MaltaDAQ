 

MALTA_PSU.py  -c setVoltage DUT_PWELL 5

MaltaThresholdScan -e -F -q -t 300 -h 89 -l 30 -s 1 -f W11R11Cz -o config_W11R11_WTN_IDB50_ITHR10_PWELL5_1  -r 160 32 224 32 -a udp://ep-ade-gw-01.cern.ch:50003 -c configs/config_plane3_for_cosmics.txt  -d
MaltaThresholdScan -e -F -q -t 300 -h 89 -l 30 -s 1 -f W11R11Cz -o config_W11R11_WTN_IDB50_ITHR10_PWELL5_2  -r 192 32 224 32 -a udp://ep-ade-gw-01.cern.ch:50003 -c configs/config_plane3_for_cosmics.txt  -d


MALTA_PSU.py  -c setVoltage DUT_PWELL 4


MaltaThresholdScan -e -F -q -t 300 -h 89 -l 30 -s 1 -f W11R11Cz -o config_W11R11_WTN_IDB50_ITHR10_PW4_1  -r 160 32 224 32 -a udp://ep-ade-gw-01.cern.ch:50003 -c configs/config_plane3_for_cosmics.txt  -d
MaltaThresholdScan -e -F -q -t 300 -h 89 -l 30 -s 1 -f W11R11Cz -o config_W11R11_WTN_IDB50_ITHR10_PW4_2  -r 192 32 224 32 -a udp://ep-ade-gw-01.cern.ch:50003 -c configs/config_plane3_for_cosmics.txt  -d



MALTA_PSU.py  -c setVoltage DUT_PWELL 3


MaltaThresholdScan -e -F -q -t 300 -h 89 -l 30 -s 1 -f W11R11Cz -o config_W11R11_WTN_IDB50_ITHR10_PW3_1  -r 160 32 224 32 -a udp://ep-ade-gw-01.cern.ch:50003 -c configs/config_plane3_for_cosmics.txt  -d
MaltaThresholdScan -e -F -q -t 300 -h 89 -l 30 -s 1 -f W11R11Cz -o config_W11R11_WTN_IDB50_ITHR10_PW3_2  -r 192 32 224 32 -a udp://ep-ade-gw-01.cern.ch:50003 -c configs/config_plane3_for_cosmics.txt  -d


MALTA_PSU.py  -c setVoltage DUT_PWELL 2


MaltaThresholdScan -e -F -q -t 300 -h 89 -l 30 -s 1 -f W11R11Cz -o config_W11R11_WTN_IDB50_ITHR10_PW2_1  -r 160 32 224 32 -a udp://ep-ade-gw-01.cern.ch:50003 -c configs/config_plane3_for_cosmics.txt  -d
MaltaThresholdScan -e -F -q -t 300 -h 89 -l 30 -s 1 -f W11R11Cz -o config_W11R11_WTN_IDB50_ITHR10_PW2_2  -r 192 32 224 32 -a udp://ep-ade-gw-01.cern.ch:50003 -c configs/config_plane3_for_cosmics.txt  -d


MALTA_PSU.py  -c setVoltage DUT_PWELL 1

MaltaThresholdScan -e -F -q -t 300 -h 89 -l 30 -s 1 -f W11R11Cz -o config_W11R11_WTN_IDB50_ITHR10_PW1_1  -r 160 32 224 32 -a udp://ep-ade-gw-01.cern.ch:50003 -c configs/config_plane3_for_cosmics.txt  -d
MaltaThresholdScan -e -F -q -t 300 -h 89 -l 30 -s 1 -f W11R11Cz -o config_W11R11_WTN_IDB50_ITHR10_PW1_2  -r 192 32 224 32 -a udp://ep-ade-gw-01.cern.ch:50003 -c configs/config_plane3_for_cosmics.txt  -d


MALTA_PSU.py  -c setVoltage DUT_PWELL 6
##MaltaThresholdScan -f W11R11Cz -o Glue_6V_R0 -r 200 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003  -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_6V_R1 -r 200 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_6V_R2 -r 200 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_6V_R3 -r 310 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_6V_R4 -r 310 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_6V_R5 -r 310 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_6V_R6 -r 245 10 45 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MALTA_PSU.py  -c setVoltage DUT_SUB 9

##MaltaThresholdScan -f W11R11Cz -o Glue_9V_R0 -r 200 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_9V_R1 -r 200 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_9V_R2 -r 200 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_9V_R3 -r 310 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_9V_R4 -r 310 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_9V_R5 -r 310 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_9V_R6 -r 245 10 45 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MALTA_PSU.py  -c setVoltage DUT_SUB 10

##MaltaThresholdScan -f W11R11Cz -o Glue_10V_R0 -r 200 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_10V_R1 -r 200 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_10V_R2 -r 200 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_10V_R3 -r 310 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_10V_R4 -r 310 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_10V_R5 -r 310 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_10V_R6 -r 245 10 45 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MALTA_PSU.py  -c setVoltage DUT_SUB 11

##MaltaThresholdScan -f W11R11Cz -o Glue_11V_R0 -r 200 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_11V_R1 -r 200 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_11V_R2 -r 200 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_11V_R3 -r 310 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_11V_R4 -r 310 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_11V_R5 -r 310 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_11V_R6 -r 245 10 45 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MALTA_PSU.py  -c setVoltage DUT_SUB 12

##MaltaThresholdScan -f W11R11Cz -o Glue_12V_R0 -r 200 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_12V_R1 -r 200 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_12V_R2 -r 200 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_12V_R3 -r 310 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_12V_R4 -r 310 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_12V_R5 -r 310 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

##MaltaThresholdScan -f W11R11Cz -o Glue_12V_R6 -r 245 10 45 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MALTA_PSU.py  -c setVoltage DUT_SUB 13

#MaltaThresholdScan -f W11R11Cz -o Glue_13V_R0 -r 200 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MaltaThresholdScan -f W11R11Cz -o Glue_13V_R1 -r 200 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MaltaThresholdScan -f W11R11Cz -o Glue_13V_R2 -r 200 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MaltaThresholdScan -f W11R11Cz -o Glue_13V_R3 -r 310 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MaltaThresholdScan -f W11R11Cz -o Glue_13V_R4 -r 310 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MaltaThresholdScan -f W11R11Cz -o Glue_13V_R5 -r 310 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MaltaThresholdScan -f W11R11Cz -o Glue_13V_R6 -r 245 10 45 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MALTA_PSU.py  -c setVoltage DUT_SUB 14

#MaltaThresholdScan -f W11R11Cz -o Glue_14V_R0 -r 200 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MaltaThresholdScan -f W11R11Cz -o Glue_14V_R1 -r 200 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MaltaThresholdScan -f W11R11Cz -o Glue_14V_R2 -r 200 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MaltaThresholdScan -f W11R11Cz -o Glue_14V_R3 -r 310 10 50 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MaltaThresholdScan -f W11R11Cz -o Glue_14V_R4 -r 310 10 250 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MaltaThresholdScan -f W11R11Cz -o Glue_14V_R5 -r 310 10 450 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d

#MaltaThresholdScan -f W11R11Cz -o Glue_14V_R6 -r 245 10 45 10 -c configs/config_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003 -t 300  -F -q -d


#MALTA_PSU.py  -c setVoltage DUT_SUB 6
