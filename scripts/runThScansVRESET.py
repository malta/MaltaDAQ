#!/usr/bin/env python
import sys,os  


commandPSU = "MALTA_PSU.py  -c setVoltage DUT_SUB 6"

os.system(commandPSU)

for v in range(121, 126, 5):
  config = open("configs/config_W11R11Cz.txt",'r')
  configOut = open("configs/config_VRESET_W11R11Cz.txt",'w') 
  for line in config:
    newLine = line
    var = line.split(":")[0]
 
    if var =="SC_VRESET_D":
     print "This line has the energy"
     newLine = var+":"+str(v)+"\n"
     print "old line "+line 
     print "new line "+newLine
   
    configOut.write(newLine)

  configOut.close()

  #edit the config file 
  
  command0 = "MaltaThresholdScan -f W11R11Cz -o Glue_6V_R0_VRED_%03i -r 200 10 50 10 -c configs/config_VRESET_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003  -t 300  -F -q -d" %v
  command1 = "MaltaThresholdScan -f W11R11Cz -o Glue_6V_R1_VRED_%03i -r 200 10 250 10 -c configs/config_VRESET_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003  -t 300  -F -q -d" %v
  command2 = "MaltaThresholdScan -f W11R11Cz -o Glue_6V_R2_VRED_%03i -r 200 10 450 10 -c configs/config_VRESET_W11R11Cz.txt -l 5 -h 89 -s 1 -a  udp://ep-ade-gw-01.cern.ch:50003  -t 300  -F -q -d" %v

  print command0
  print command1
  print command2
  print "\n"
  os.system(command0)
  os.system(command1)
  os.system(command2)


commandPSU2 = "MALTA_PSU.py  -c setVoltage SUB 6"
os.system(commandPSU2)



