import os, sys

chip_name = "W7R11_WTN"
address = "udp://ep-ade-gw-07.cern.ch:50002"

orig_stdout = sys.stdout
f = open('out_scans_QA.txt', 'w')
sys.stdout = f

print("Starting processing scans...")
sys.stdout.flush()
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

### Make desired config file
config_command_string = "python makeConfigFile.py -c chip_name -VRESET_P vreset_p"
config_command_string = config_command_string.replace("chip_name",chip_name).replace("vreset_p","127")
print(config_command_string)
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
sys.stdout.flush()
# os.system(config_command_string)

### Run Threshold scan
thscan_command_string = "MaltaThresholdScan -e -F -q -t 500 -h 89 -l 30 -s 1 -f chip_name -o config_chip_name_ITHITH_IDBIDB_x120_64_y140_64 -r 120 64 140 64 -a address -c configs/ANL_configs/config_ITHITH_IDBIDB.txt"
thscan_command_string = thscan_command_string.replace("chip_name", chip_name).replace("ITHITH","ITH10").replace("IDBIDB","IDB50").replace("address",address)
print(thscan_command_string)
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
sys.stdout.flush()
# os.system(thscan_command_string)

### Run Noise scan with iterative masking
itmask_command_string = "python scripts/run_iterative_masking.py -f chip_name -o config_chip_name_THRES_ITHITH_IDBIDB_VRESET_PVRESET_P_itr_MAX100pix -a address -c configs/ANL_configs/config_ITHITH_IDBIDB_VRESET_PVRESET_P.txt -m 100"
itmask_command_string = itmask_command_string.replace("chip_name", chip_name).replace("ITHITH","ITH10").replace("IDBIDB","IDB50").replace("VRESET_PVRESET_P","VRESET_P127").replace("address",address)
print(itmask_command_string)
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
sys.stdout.flush()
# os.system(itmask_command_string)

print("End of QA scans. Have a nice day")
sys.stdout.flush()
sys.stdout = orig_stdout
f.close()
