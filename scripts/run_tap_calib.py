import os

config  ="configsDefault/config_127127.txt"

chipName="W15R14_WTN"
address ="udp://ep-ade-gw-04.cern.ch:50002"
output  ="ValerioMaster"

##############################################################################################################

pixels=[
  [510,504], [510,505],  [510,506],  [510,507],  [510,508],  [510,509],  [510,510],  [510,511],  
  [511,504], [511,505],  [511,506],  [511,507],  [511,508],  [511,509],  [511,510],  [511,511],  
]

count=0
for p in pixels:
  if count==0:
    os.system('MaltaTapCalibration_realChip -n 150  -c '+config+' -x '+str(p[0])+' -y  '+str(p[1])+' -a '+address+' -f '+chipName+' -o '+output+'_calib_run  -w calib_'+str(count)+'.txt ') 
  else:
    os.system('MaltaTapCalibration_realChip -n 150  -c '+config+' -x '+str(p[0])+' -y  '+str(p[1])+' -a '+address+' -f '+chipName+' -o '+output+'_calib_run  -w calib_'+str(count)+'.txt  -i calib_'+str(count-1)+'.txt ') 
  count+=1
