#define tap_cxx
#include "tap.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <string>     // std::string, std::to_string

void tap::Loop(int column)
{
//   In a ROOT session, you can do:
//      root> .L tap.C
//      root> tap t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   TH1D *shift[37];
   

   for (int i =0; i < 37; ++i){
     shift[i] = new TH1D ( ("bit_"+to_string(i)).c_str(), ("bit_"+to_string(i)).c_str(), 100, -800, 800);
   }

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
      for (unsigned int i =0; i < tap_per_bit->size(); ++i){
        if (tap_per_bit->at(i)> -99 && pixX ==column)  shift[i]->Fill( 80*tap_per_bit->at(i));
	// if (tap_per_bit->at(15)>-99 && pixX==254) std::cout<<pixY<<std::endl;
      }
   }
   TGraphErrors *g = new TGraphErrors();
   g->SetNameTitle("col26", "col26");
   TCanvas *c= new TCanvas("c","c", 800,800);
   for (unsigned int i =0; i < 37; ++i)  { g->SetPoint(i, i+0.1, shift[i]->GetMean()); g->SetPointError(i, 0, shift[i]->GetRMS());     }
   for (unsigned int i =0; i < 37; ++i)  {shift[i]->Draw(); c->SaveAs(("bit_"+to_string(i)+"_col"+to_string(column)+".eps").c_str());}
   TFile *o = new TFile (("histos"+to_string(column)+".root").c_str(), "recreate");
   for (unsigned int i =0; i < 37; ++i)  shift[i]->Write();
   g->Write();
   o->Close();

}
