#tap plotting
import ROOT

ROOT.gStyle.SetOptStat(0)

chip = 'W1RA'

cols = [253, 254, 255]

l = ROOT.TLine(0.5,-800,0.5,800)
l.SetLineStyle(8)
l1 = ROOT.TLine(16.5,-800,16.5,800)
l1.SetLineStyle(8)

mg = ROOT.TMultiGraph()
mg.SetNameTitle("bit timing","Timing of bits;bit;time (ps)")
#h2 = ROOT.TH2I("timing"+str(col),"Timing of column"+str(col)+";bit;time (ps)",37, -0.5, 37.5, 100, -800, 800)
count = 0

for col in cols:
	count+=1
	f = ROOT.TFile("histos"+str(col)+".root")
	g = ROOT.TGraphErrors()
	g.SetMarkerColor(count)
	g.SetLineColor(count)
	g.SetMarkerStyle(count+20)
	g.SetNameTitle("Col"+str(col),"Col"+str(col))
	h2 = ROOT.TH2I("timing"+str(col),"Timing of column"+str(col)+";bit;time (ps)",37, -0.5, 37.5, 100, -800, 800)
	print "reading file",f
	for bit in range(0,37):
		#~ print "reading histogram for bit ",bit
		h = f.Get("bit_"+str(bit))
		mean = h.GetMean();
		rms = h.GetRMS()
		n = h.GetEntries()
		error = rms#/(pow(n,0.5)+0.000001)
		g.SetPoint(bit, bit+(count -2)*0.1,mean)
		g.SetPointError(bit, 0,error)
		bins = h.GetNbinsX() 
		#~ print "there are this many bins:",bins
		for b in range(0,bins+1):
		  #~ print h.GetBinCenter(b),h.GetBinContent(b)
		  h2.SetBinContent(int(bit), b,h.GetBinContent(b))
	can2 = ROOT.TCanvas()
	h2.Draw("colz")
	l.Draw()
	l1.Draw()
	can2.SaveAs(chip+"bitTimingCol"+str(col)+".pdf")
	mg.Add(g)
		  
		  
		  
can = ROOT.TCanvas()
mg.Draw("AP")
l = ROOT.TLine(0.5,mg.GetYaxis().GetXmin(),0.5,mg.GetYaxis().GetXmax())
#~ l = ROOT.TLine(0.5,can.GetUymin(),0.5,can.GetUymax())
l.SetLineStyle(8)
#~ l1 = ROOT.TLine(16.5,can.GetUymin(),16.5,can.GetUymax())
l1 = ROOT.TLine(16.5,mg.GetYaxis().GetXmin(),16.5,mg.GetYaxis().GetXmax())
l1.SetLineStyle(8)
l.Draw()
l1.Draw()
can.BuildLegend(0.7,0.2, 0.85,0.40)
can.SaveAs(chip+"bitTiming.pdf")



raw_input("Press any key to end script")

