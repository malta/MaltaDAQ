import os
import sys
import time
import importlib
import threading
import argparse
from socket import gethostname

import MaltaBase
from MaltaBase import *
import Herakles
import PyMaltaSlowControl
from asyncore import read
from plistlib import readPlist

hostname=gethostname()
ipName="ADE-ID-ROUTER"
if "ros01" in hostname or "ros02" in hostname:
    ipName="192.168.0.1"
ipb=Herakles.Uhal(ipName)

##############################################

cacheWord=ipb.Read(8)
mask=0xFFFFFFFF ^ ( 1<<20 )

minV  = 400
maxV  = 1400
step  = 10
number= 1
nVal= int((maxV-minV)/step)

os.system("/afs/cern.ch/atlas/project/tdaq/inst/tdaq-common/tdaq-common-02-02-00/installed/share/bin/tdaq_python ../MaltaScans/MaltaScan_setup2.py -v %.3f"%(maxV/1000.) )
time.sleep(1)

############# sweeping VLOW

for i in xrange(0,nVal):
    value=float(maxV-step*i)
    vlow="%i"%(value)
    print "Vlow value: "+str(value/1000)+" -- "+vlow
    print i
    os.system("/afs/cern.ch/atlas/project/tdaq/inst/tdaq-common/tdaq-common-02-02-00/installed/share/bin/tdaq_python ../MaltaScans/MaltaScan_setup2.py -v %.3f"%(value/1000.) )

############# pulsing

    print "Pulsing "+str(number)+" times"
    time.sleep(1)
    for i in xrange(0,number):
        print i
        ipb.Write(8,cacheWord | (1<<20)) 
        ipb.Write(8,cacheWord & mask )
        time.sleep(1)
print " "
print "End of script.\n"

os.system("/afs/cern.ch/atlas/project/tdaq/inst/tdaq-common/tdaq-common-02-02-00/installed/share/bin/tdaq_python ../MaltaScans/MaltaScan_setup2.py -v 1.200" )
