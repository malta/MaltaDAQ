import sys,os
import ROOT
from array import *
import math
import argparse

#gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#SetAtlasStyle()
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(1)
ROOT.gStyle.SetOptFit(1111)
ROOT.gROOT.SetBatch(1)

pixels=[1,251,511]
vals=[]
thresholds=[10]
parser=argparse.ArgumentParser()
parser.add_argument('-f' ,'--folder',help='output folder',required=True)
parser.add_argument('-p' ,'--prefix',help='prefix for the file',required=True)
parser.add_argument('-s' ,'--start' ,help='starting point of the scan',type=int,default=1200)
args=parser.parse_args()

theCol=-1
thePix=-1
imonS =""
##make sure that the prefix contains COL and IMON2
if "COL" not in args.prefix:
    print " "
    print " PREFIX MUST CONTAIN: 'COL_x__' block .... please fix it"
    print " " 
    sys.exit()
else:
    colS=args.prefix.split("COL_")[1].split("__")[0]
    print " pulsed column: "+colS
    theCol=int(colS)
if "PIX" not in args.prefix:
    print " "
    print " PREFIX MUST CONTAIN: '__PIX_x__' block with 'p' as a separator  .... please fix it"
    print " " 
    sys.exit()
else:
    pixS=args.prefix.split("PIX_")[1].split("__")[0]
    print " pulsed pixel: "+pixS
    thePix=int(pixS)
if "IMON2" not in args.prefix:
    print " "
    print " PREFIX MUST CONTAIN: '__IMON2_x__' block with 'p' as a separator  .... please fix it"
    print " " 
    sys.exit()
else:
    imonS=args.prefix.split("IMON2_")[1].split("__")[0]
    print " imon2 value: "+imonS
    theIMON2=float(imonS.split("p")[0])+float(imonS.split("p")[1])/100.
    print " imon2 final: "+str(theIMON2)

outFile=args.folder+"/IMON2_"+imonS+"/PIX_"+str(thePix)+"/Result_"+args.prefix+".root"

#############################################################################################

def myTextV(x, y, text, tsize):
    l=ROOT.TLatex()
    l.SetTextSize(tsize)
    l.SetNDC()
    l.SetTextColor(1)
    l.DrawLatex(x,y,text)
    pass 

#############################################################################################


trees=[]
for i in xrange(0,110):
    trees.append(ROOT.TChain("MALTA"))


start=args.start

names=[]
for i in xrange(0,75):
    names.append("VLow="+str(start+i*10)+" mV")

######print names
newCut =""
cutName=""
#############################################################################################


def getCount(tree, cuts):
    theH = ROOT.TH1D("histo_1", "1", 10, -10, 20)
    theH.Sumw2()
    #print " entries: "+str(tree.GetEntries())
    #print " cut: "+cuts
    ######thecuts=cuts+" && phase!=7 "
    thecuts=cuts+" && isDuplicate==0 "
    ###thecuts=cuts
    thecuts+=" && (bcid*25.+winid*3.125)>300 "
    thecuts+=" && (bcid*25.+winid*3.125)<450 "
    ##print thecuts
    tree.Draw( "bcid >>histo_1" , thecuts, "goff")#,50000)
    return theH.Integral(0,-1)


def GetPlot(tree, cut, color):
    plot=ROOT.TGraphErrors()
    point=-1
    print " "
    for i in xrange(0,128):
        count=getCount(tree, cut+" && ithres=="+str(i)+" " )
        if count!=0:
            #print "THRESHOLD= "+str(i)+" has count: %i "%count
            point+=1
            plot.SetPoint(point, i, count )
            plot.SetPointError(point, 0, math.sqrt(count) )
    plot.SetLineColor(color)
    plot.SetMarkerColor(color)
    plot.SetLineWidth(3)
    plot.SetMarkerStyle(20)
    plot.SetMarkerSize(0.8)
    return plot

def GetPlotVL(threshold, cut, color):
    plot=ROOT.TGraphErrors()
    point=-1
    #print " "
    for name in names:
        point+=1
        count=getCount(trees[point], cut+" && ithres=="+str(threshold)+" " )
        if count==0: count=1
        value=float(name.split("=")[1].split(" ")[0])
        #print " VLow= "+str(value)+" has count: %i "%count
        plot.SetPoint(point, value, count )
        plot.SetPointError(point, 2, math.sqrt(count) )
    plot.SetLineColor(color)
    plot.SetMarkerColor(color)
    plot.SetLineWidth(3)
    plot.SetMarkerStyle(20)
    plot.SetMarkerSize(0.8)
    return plot

def  ScanThreshold(xLabel,newCut,cutName):
    can=ROOT.TCanvas( cutName, cutName, 800, 600)
    baseH=ROOT.TH1D("base","base",130,0,128)
    baseH.SetTitle(";"+xLabel+";counts")
    
    plot1=GetPlot(trees[0], newCut,1)
    plot2=GetPlot(trees[1], newCut,2)
    plot3=GetPlot(trees[2], newCut,8)
    plot4=GetPlot(trees[3], newCut,4)
    px1=ROOT.Double_t(0)
    py1=ROOT.Double_t(0)
    plot1.GetPoint(0,px1,py1)
    baseH.SetMaximum(py1*6)##1.50)
    baseH.Draw("AXIS")
    plot1.Draw("P")
    plot2.Draw("P")
    plot3.Draw("P")
    plot4.Draw("P")

    leg = ROOT.TLegend(0.65,0.75,0.95,0.95)
    leg.SetNColumns(2)
    leg.SetBorderSize(1)
    leg.SetTextFont(62)
    leg.SetTextSize(0.04)
    leg.SetLineColor(0)
    leg.SetLineStyle(1)
    leg.SetLineWidth(1)
    leg.SetFillColor(0)
    leg.SetFillStyle(1001)
    leg.AddEntry(baseH,cutName,"")
    leg.AddEntry(plot1,names[0],"PE")
    leg.AddEntry(plot2,names[1],"PE")
    leg.AddEntry(plot3,names[2],"PE")
    leg.AddEntry(plot4,names[3],"PE")
    leg.Draw("SAME")
    can.Update()
    can.Print( baseDir+cutName+".pdf")
    #os.system( "evince "+baseDir+cutName+".pdf &")

    
###########################################################################################
###########################################################################################
def ScanVLow(xLabel,newCut,cutName,values, col, pixel):
    can=ROOT.TCanvas( cutName, cutName, 800, 800)
    baseH=ROOT.TH1D("base","base",10, start,1750)
    baseH.SetTitle(";"+xLabel+";counts")
    
    group=int((pixel-0.01)/16)

    pix=""
    if col%2==0:
        if pixel%8==0: pix="0x0001"
        if pixel%8==1: pix="0x0002"
        if pixel%8==2: pix="0x0004"
        if pixel%8==3: pix="0x0008"
        if pixel%8==4: pix="0x0010"
        if pixel%8==5: pix="0x0020"
        if pixel%8==6: pix="0x0040"
        if pixel%8==7: pix="0x0080"
    else:
        if pixel%8==0: pix="0x0100"
        if pixel%8==1: pix="0x0200"
        if pixel%8==2: pix="0x0400"
        if pixel%8==3: pix="0x0800"
        if pixel%8==4: pix="0x1000"
        if pixel%8==5: pix="0x2000"
        if pixel%8==6: pix="0x4000"
        if pixel%8==7: pix="0x8000"

    ##column part
    newCut="( ("
    newCut+=" dcolumn=="+str( int(col/2) )
    newCut+=" && group=="+str( group )
    newCut+=" && parity=="+str( int(group%2!=0) )
    newCut+=" && (pixel&"+pix+")!=0 "
    newCut+=" ) "
    ##recovery
    if pixel!=511: 
        newCut+=" || (dcolumn=="+str( int(col/2) )
        newCut+=" && (pixel&"+pix+")!=0 ) "
    newCut+=" ) "
        

    print newCut
    ###sys.exit()

    plots=[]
    count=0
    for th in values:
        count+=1
        plots.append( GetPlotVL(th, newCut,count) ) 
    px1=ROOT.Double(0)
    py1=ROOT.Double(0)
    plots[0].GetPoint(0,px1,py1)
    baseH.SetMaximum(py1*1.50)
    baseH.Draw("AXIS")
    for p in plots: p.Draw("P")
    leg = ROOT.TLegend(0.70,0.65,0.95,0.95)
    leg.SetBorderSize(1)
    leg.SetTextFont(62)
    leg.SetTextSize(0.04)
    leg.SetLineColor(0)
    leg.SetLineStyle(1)
    leg.SetLineWidth(1)
    leg.SetFillColor(0)
    leg.SetFillStyle(1001)
    leg.AddEntry(baseH,cutName,"")
    th=-1
    lines=[]
    curves=[]
    out=ROOT.TFile(outFile,"RECREATE")
    for p in plots:
        th+=1
        #fit=TF1("fit"+str(values[th]),"[0]/2*(1+TMath::Erf(TMath::Sqrt(2)*(x-[1])/[2]))+[3]",550,950)
        fit=ROOT.TF1("fit"+str(values[th]),"[0]/2*(1-TMath::Erf(1/TMath::Sqrt(2)*(x-[1])/[2]))",start+10,1750)
        curves.append(fit)
        fit.SetParName(0,"base")
        fit.SetParName(1,"#mu")
        fit.SetParName(2,"#sigma")
        fit.SetParameter(0,1000)
        fit.SetParLimits(0, 200,8000)
        fit.SetParameter(1,1500)
        fit.SetParLimits(1,1000,1750)
        fit.SetParameter(2,30)
        fit.SetParLimits(2,1.0,50)
        #fit.SetParLimits(0,0,g2.GetMaximum())
        #fit.SetParLimits(1,0,720)
        #fit.SetParameter(1,50)
        #fit.SetParLimits(2,0,30)
        #fit.SetParameter(2,1)
        #fit.SetParLimits(3,0,g2.GetMaximum())
        #fit.SetParameter(3,g2.GetMinimum())
        p.Fit(fit,"RE0")
        fit.SetLineStyle(2)
        fit.SetLineColor(p.GetLineColor())
        l=ROOT.TLine(fit.GetParameter(1),0,fit.GetParameter(1),baseH.GetMaximum()*0.7)
        l.SetLineWidth(1)
        l.Draw("SAMEC")
        fit.Draw("SAMEC")
        print " " 
        elTh=(1780-fit.GetParameter(1))*1.43
        print " THRESHOLD IN ELECTRONS is: %3.0f el "%( elTh )
        print " " 
        myTextV(0.15, 0.75, "thresh= %3.0f el "%( elTh ), 0.05)
        vals.append(elTh)
        #leg.AddEntry(p,"iTH="+str(values[th]),"P")
        out.WriteObject(p  ,"Scan_"+cutName.replace("VLOW_",""))
        out.WriteObject(fit,"Fit_"+cutName.replace("VLOW_",""))
    out.Close()
    myTextV(0.15, 0.3, cutName.replace("VLOW_","").replace("x","x "), 0.05)
    #leg.Draw("SAME")
    can.Update()
    can.Print( args.folder+"/"+args.prefix+"__"+cutName+".pdf")
    ###os.system( "evince "+args.folder+"/"+args.prefix+"__"+cutName+".pdf &")
    

#############################################################################################
count=0
for v in xrange(0,100):
    vlow=int( start+v*10)
    fileName=args.folder+"/IMON2_"+imonS+"/PIX_"+str(thePix)+"/ThScan_"+args.prefix+"__"+args.folder.split("/")[1]+"__VL"+str(vlow)+"_10.root"
    trees[count].Add(fileName)
    count+=1
count=0
#for v in xrange(0,21):
#    print trees[count].GetEntries()
#    count+=1 


##############################################################################################

doubleCol=int( (float(theCol)-0.01)/2 )


baseCut001=" parity==0 && group== 0 && dcolumn=="+str(doubleCol)
baseCut251=" parity==1 && group==15 && dcolumn=="+str(doubleCol)
baseCut511=" parity==1 && group==31 && dcolumn=="+str(doubleCol)

if theCol%2!=0:
    baseCut001+=" && (pixel& 0x100)!=0 " 
    #baseCut001+=" && (pixel& 0x200)!=0 " 
    baseCut251+=" && (pixel& 0x800)!=0 "
    baseCut511+=" && (pixel&0x8000)!=0 "
else:
    baseCut001+=" && (pixel& 0x1)!=0 "
    #baseCut001+=" && (pixel& 0x2)!=0 " 
    baseCut251+=" && (pixel& 0x8)!=0 "
    baseCut511+=" && (pixel&0x80)!=0 "


print " " 
ScanVLow("VLow (mV)",baseCut001,"VLOW_Pix"+str(theCol)+"-"+str(thePix), thresholds, float(theCol), float(thePix) )
if thePix==0:
    ScanVLow("VLow (mV)",baseCut001,"VLOW_Pix"+str(theCol)+"-251", thresholds, float(theCol), 251)
    ScanVLow("VLow (mV)",baseCut001,"VLOW_Pix"+str(theCol)+"-511", thresholds, float(theCol), 511)
    

print " " 
print " "
