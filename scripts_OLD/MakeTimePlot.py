import sys,os
from ROOT import *
from array import *
import math

#gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#SetAtlasStyle()
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gStyle.SetOptFit(1111)
gROOT.SetBatch(1)
#gROOT.ProcessLine(".L GetCat.C++g")

#############################################################################################


baseDir="ScanPlots/"
os.system("mkdir -p "+baseDir)

trees=[]
for i in xrange(0,110):
    trees.append(TChain("MALTA"))
start=1100
step =  50

names=[]
for i in xrange(0,50):
    val=start+i*step
    if val>1500: continue
    names.append("VLow="+str(val)+" mV")

print names

normalize     =False
normalizeRatio=True

newCut =""
cutName=""

##################################################################################################################################################
def getPlot(tree, cuts):
    theH = TH1D("histo_1", "1", 1000, 0, 1000)
    theH.Sumw2()
    #thecuts=cuts+" && (bcid*25.+winid*3.125)>240 && (bcid*25.+winid*3.125)<500 && phase!=7 "
    thecuts=cuts+" && isDuplicate==0 "
    tree.Draw( " (bcid*25.+winid*3.125+phase*0.39)>>histo_1" , thecuts, "goff")#,50000)
    theH.Rebin(2)
    theH.SetLineColor(1)
    theH.SetLineWidth(2)
    return theH##.Integral(0,-1)

#################################################################################################################################################
def ScanTime(xLabel,newCut,cutName):
    can=TCanvas( cutName, cutName, 800, 600)
    baseH=TH1D("base","base",10, 0,1000)
    baseH.SetTitle(";"+xLabel+";counts")
    baseH.GetXaxis().SetRangeUser(300,1000)
    baseH.Draw("AXIS")
    
    maxV=0
    plots=[]
    count=-1
    for th in names:
        count+=1
        t=trees[count]
        plots.append( getPlot(t,newCut) ) 
        plots[count].SetLineColor(count+1)
        maxL=plots[count].GetMaximum()
        if maxL>maxV: maxV=maxL
    baseH.SetMaximum(maxV*1.1)

    for p in plots:
        p.Draw("SAMEHIST")

    leg = TLegend(0.60,0.50,0.95,0.95)
    leg.SetBorderSize(1)
    leg.SetTextFont(62)
    leg.SetTextSize(0.04)
    leg.SetLineColor(0)
    leg.SetLineStyle(1)
    leg.SetLineWidth(1)
    leg.SetFillColor(0)
    leg.SetFillStyle(1001)
    leg.AddEntry(baseH,cutName,"")
    
    count=-1
    for p in plots:
        count+=1
        p.Draw("SAMEHIST")
        leg.AddEntry(p,names[count],"L")
    leg.Draw("SAME")
   
    can.Update()
    can.SetLogy()
    can.Print( baseDir+cutName+".pdf")
    os.system( "evince "+baseDir+cutName+".pdf &")
    ## zoom
    baseH.GetXaxis().SetRangeUser(340,400)
    can.SetLogy(False)
    can.Print( baseDir+cutName+"_ZOOM.pdf")
    os.system( "evince "+baseDir+cutName+"_ZOOM.pdf &")

#################################################################################################################################################
#################################################################################################################################################
#################################################################################################################################################
#################################################################################################################################################
count=0
for v in xrange(0,100):
    vlow=int( 1100+v*step)
    trees[count].Add( "/home/sbmuser/MaltaSW/MaltaDAQ/ThScan_2018-04-16_BEST_COL62_PW-6_IBIAS-OPT_ITHR-1p286_IDB-20_ICASN-1p4_VALE_VL"+str(vlow)+"_10.root") 
    count+=1 
count=0
for v in xrange(0,21):
    print trees[count].GetEntries()
    count+=1 


################################################################################################################################################
##############
newCut=" isDuplicate==0 && parity==0 && ( dcolumn==31 && group==0) && pixel==18 " ##because of duplicate hit
ScanTime("time since pulse [ns]",newCut,"TIME_Pix62-1")
newCut=" isDuplicate==0 && parity==1 && ( dcolumn==31 && group==15) && pixel==24 "
ScanTime("time since pulse [ns]",newCut,"TIME_Pix62-251")
