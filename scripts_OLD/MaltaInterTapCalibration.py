#!/usr/bin/env python
####################################
# MaltaTapMeasurement
# Script to measure the best taps
# Requires dummy emulator
# Abhishek.Sharma@cern.ch
# Carlos.Solans@cern.ch
# January 2018
####################################

import os
import sys
import time
import argparse
import Herakles
import MaltaBase
import ROOT
import Style
Style.SetStyle()

parser = argparse.ArgumentParser()
parser.add_argument("-c","--conn",help="ipbus conn str",default="192.168.0.1")
parser.add_argument("-p","--pattern",help="sender pattern",default="0000")
parser.add_argument("-n","--npulses",help="number of pulses",type=int,default=100)
parser.add_argument("-d","--defaultTaps",help="start with default taps",action='store_true',default=False)
parser.add_argument("-w","--write",help="write taps back",action='store_true',default=False)
parser.add_argument("-t","--tap",help="tap duration in picoseconds",type=int,default=80)
parser.add_argument("-to","--tapOffset",help="offset between tap1 and tap2",type=int,default=2)
parser.add_argument("-ct","--calculateTap",help="automatic calculation of tap duration",action='store_true',default=False)

args = parser.parse_args()


ipb = Herakles.Uhal(args.conn)
ipb.SetVerbose(False)
print "Version: %s" % MaltaBase.GetVersion(ipb)


#############################################################
# Writing all tap configurations incrementally ##############
#############################################################

hist = {}
count = 0
for tap1 in xrange(1,16):
    for tap2 in xrange(1,16):
        if args.defaultTaps:
            print "Writing tap1: %s and tap2: %s"%(tap1,tap2)
            val=0x80000000
            val|= tap1&0x1F
            val|=(tap2&0x1F)<<5
            ipb.Write(10,val)
            ipb.Write(10,0)
            pass
        time.sleep(1)
    MaltaBase.readAllDelays(ipb)    

    #############################################################
    # Collecting Datastream #####################################
    #############################################################

    hist[count]=ROOT.TH1D("h%"%count,";sample;bit",16,0,16)
    for i in xrange(args.npulses):
        MaltaBase.SendPulse(ipb,args.pattern)
        values = MaltaBase.ReadMonitorWord(ipb)
        if values[0]==0: break
        start=0
        for bit in xrange(16):
            if (values[0]>>(bit))&0x1==1:
                start=bit
                break
            pass
        print "0b%x" % values[j]
        for b in xrange(16):
            if (values[0]>>(b))&0x1==0:continue
            hist[count].Fill(b-start+2)
            pass
        pass
    c1=ROOT.TCanvas("c1","c1",800,600)
    hist[count].Draw("AL")
    count += 1

'''
#############################################################
# Plotting Inter-Tap Distributions for a single bit channel #
#############################################################

#c2 = ROOT.TCanvas("proj","proj",500,600)
mg1 = ROOT.TMultiGraph()
g1 = {}

lg = ROOT.TLegend(0.0989975,0.0208696,0.221805,0.96)

mean = []
ymean = {}
zSumPerYbin = {} 
for y in xrange(h1.GetNbinsY()):
    y+=1
    #print "y: ",y
    g1[y] = ROOT.TGraph()
    g1[y].SetLineColor(y)
    ymean[y]=0
    zSumPerYbin[y] = 0
    for x in xrange(h1.GetNbinsX()):
        x+=1
        #if x<7: continue
        #else:
        #print "x: ",x
        g1[y].SetPoint(x,x,h1.GetBinContent(x,y))
        #g1[y].SetAxisRange(8,16,"X")
        mg1.Add(g1[y])
        ymean[y]+=h1.GetBinContent(x,y)*x
        zSumPerYbin[y]+=h1.GetBinContent(x,y)
        #print "ymean[y]: ",ymean[y],"zSumPerYbin[y]: ",zSumPerYbin[y]
    g1[y].SetFillColor(40)
    lg.AddEntry(g1[y],"l")
#print "second part"
y = 0
for y in xrange(h1.GetNbinsY()):
    y+=1
    #print "y: ",y
    if ymean[y]==0: continue
    mean.append(ymean[y]/zSumPerYbin[y])
    print ymean[y]/zSumPerYbin[y]

#print "Max mean: ",max(mean)
#print "mean diff part:"
diff = 0
count = 0
taps1= [] 
taps2= []
for j in mean:
    count+=1
    print "channel: ",count,"\ttime difference (in ps): %7.3f"%((max(mean)-j)*250),
    if (max(mean)-j)<args.tap/250.:
        print "\t# of taps: 0"
        taps1.append(int(args.tapOffset))
        taps2.append(int(0.0))
        pass
    elif (max(mean)-j)>=args.tap/250.:
        print "\t# of taps: %.1f"%((max(mean)-j)/(args.tap/250.))
        tap = (max(mean)-j)/args.tap/250.
        taps1.append(int(round(tap)+args.tapOffset))
        taps2.append(int(round(tap)))
        #print tap
        pass
    pass


#############################################################
# Writing Taps back to FPGA #################################
#############################################################

if args.write:
    print "Writing Corrected Tap Values"
    for chn in xrange(37):
        val=ipb.Read(chn+50)
        tap1=(val>>0)&0x1F
        tap2=(val>>5)&0x1F
        ntap1=tap1+taps1[chn]
        #ntap2=tap2+taps2[chn]
        ntap2=ntap1-2
        val =0x80000000
        val|= ntap1&0x1F
        val|=(ntap2&0x1F)<<5
        print "Bit %2i: Tap1 %2i => %2i , Tap2 %2i => %2i" % (chn,tap1,ntap1,tap2,ntap2)
        ipb.Write(chn+10,val)
        ipb.Write(chn+10,0)
        MaltaBase.ReadTap(ipb,chn)
        pass
    print "Correction taps written to FPGA."
    pass


'''
raw_input("press any key...")
print "Have a nice day"
#sys.exit()


