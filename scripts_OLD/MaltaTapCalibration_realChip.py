#!/usr/bin/env python
####################################
# MaltaTapMeasurement
# Script to measure the best taps
# Abhishek.Sharma@cern.ch
# Carlos.Solans@cern.ch
# and then Valerio.Dao@cern.ch came along ......
# January 2018
####################################

import os
import sys
import time
import math
import argparse
import Herakles
import MaltaBase
import ROOT
import Style
from socket import gethostname
import PyMaltaSlowControl
import MaltaSetup

Style.SetStyle()
ROOT.gROOT.SetBatch(1)

parser = argparse.ArgumentParser()
parser.add_argument("-m" ,"--mode"        ,help="pulsing mode",type=int,default=0) ##used for automatic chip calibration
parser.add_argument("-p" ,"--pattern"     ,help="sender pattern",default="0000")
parser.add_argument("-n" ,"--npulses"     ,help="number of pulses",type=int,default=100)
parser.add_argument("-f" ,"--first"       ,help="first time we run?",action='store_true',default=False)
parser.add_argument("-d" ,"--defaultTaps" ,help="start with default taps",action='store_true',default=False)
parser.add_argument("-w" ,"--write"       ,help="write taps back",action='store_true',default=False)
parser.add_argument("-t" ,"--tap"         ,help="tap duration in picoseconds",type=float,default=80) ###WE SHOULD NOT ALLOW PEOPLE TO EDIT THIS!!! 
parser.add_argument("-to","--tapOffset"   ,help="offset between tap1 and tap2",type=int,default=3)   ###'optimised' for 


sampleLength=390. ##VD: change to 250 for 500MHz operation
lowerLimit  =10

theCol2     =  -1
theCol      = 127
theRow      =   1

args = parser.parse_args()

startP=0
mode=args.mode
if mode==0:
    ## here we are pulsing (511,127) ensuring 6 double column bits and all the group, the parity and MS Pixel
    pass
elif mode==1:
    theCol=128
    ## here we are pulsing (511,128) ensuring the 7th dcolumn bit and a different pixel bit
    pass
elif mode==2:
    ## enable left side of the matrix hoping to catch some noise (neel to pulse for longer time)
    pass
###########################################################################################################################
elif mode==3:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =126
    theRow=startP+0
    pass
elif mode==4:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =126
    theRow=startP+1
    pass
elif mode==5:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =126
    theRow=startP+2
    pass
elif mode==6:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =126
    theRow=startP+3
    pass
elif mode==7:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =126
    theRow=startP+4
    pass
elif mode==8:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =126
    theRow=startP+5
    pass
elif mode==9:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =126
    theRow=startP+6
    pass
elif mode==10:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =126
    theRow=startP+7
    pass
elif mode==11:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =127
    theRow=startP+1
    pass
elif mode==12:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =127
    theRow=startP+2
    pass
elif mode==13:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =127
    theRow=startP+3
    pass
elif mode==14:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =127
    theRow=startP+4
    pass
elif mode==15:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =127
    theRow=startP+5
    pass
elif mode==16:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =127
    theRow=startP+6
elif mode==17:
    ## disable left and 3/4 of rows to gain full control of what we are pulsing
    theCol =127
    theRow=startP+7
    pass

else:
    print "MODE: "+str(mode)+" NOT RECOGNISED .... BYE"
    sys.exit()


hostname=gethostname()
connstr=MaltaSetup.MaltaSetup().getConnStr()
print "Trying to connect to: "+connstr
ipName=connstr

ipb = Herakles.Uhal(ipName)
ipb.SetVerbose(False)
print "Version: %s" % MaltaBase.GetVersion(ipb)
print " "

if args.defaultTaps:
    print "Writing default taps"
    MaltaBase.writeConstDelays(ipb,15,15-args.tapOffset)
    ####MaltaBase.WriteTap(ipb, 0, 7, 7-args.tapOffset)
    time.sleep(1)
else:
    if args.first: MaltaBase.loadTapFromFile(ipb, "TapCalib_"+str(mode-1)+".txt")
    else         : MaltaBase.loadTapFromFile(ipb, "TapCalib_"+str(mode)+".txt") 
print " "
MaltaBase.readAllDelays(ipb,True)
print " "


#############################################################
# Collecting Datastream #####################################
#############################################################

h1=ROOT.TH2D("h2",";sample;bit",16,0,16,37,0,37)
lines=[]
tmpL=ROOT.TLine(0,1.0,16,1.0)
tmpL.SetLineWidth(1)
tmpL.SetLineStyle(2)
lines.append(tmpL)
tmpL2=ROOT.TLine(0,17,16,17)
tmpL2.SetLineWidth(1)
tmpL2.SetLineStyle(2)
lines.append(tmpL2)
tmpL3=ROOT.TLine(0,+22,16,22)
tmpL3.SetLineWidth(1)
tmpL3.SetLineStyle(2)
lines.append(tmpL3)
tmpL4=ROOT.TLine(0,+23,16,23)
tmpL4.SetLineWidth(1)
tmpL4.SetLineStyle(2)
lines.append(tmpL4)
tmpL5=ROOT.TLine(0,+26,16,26)
tmpL5.SetLineWidth(1)
tmpL5.SetLineStyle(2)
lines.append(tmpL5)
tmpL6=ROOT.TLine(0,+34,16,34)
tmpL6.SetLineWidth(1)
tmpL6.SetLineStyle(2)
lines.append(tmpL6)
tmpL7=ROOT.TLine(0,+36,16,36)
tmpL7.SetLineWidth(1)
tmpL7.SetLineStyle(2)
lines.append(tmpL7)

sc=PyMaltaSlowControl.MaltaSlowControl()

os.system("MALTA_PSU.py -t T_SC_ON.txt ")
os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")

time.sleep(0.1)
MaltaBase.switchClockOn(ipb)
if mode!=2:
    ipb.Write(5, sc.enablePulsePixelCol(theCol) )
    if theCol2!=-1: ipb.Write(5, sc.enablePulsePixelCol(theCol2) )
    ipb.Write(5, sc.enablePulsePixelHor(theRow) )
MaltaBase.switchClockOff(ipb)
time.sleep(0.1)

os.system("MALTA_PSU.py -t T_SC_OFF.txt ")
os.system("MALTA_PSU.py -t T_ANAG_ON.txt ")

time.sleep(0.1)

##first noise hits:
if mode==2:
    MaltaBase.ReadoutHalfColumnsOff(ipb)
    MaltaBase.ReadoutHalfRowsOff(ipb)
elif mode<2:
    MaltaBase.ReadoutHalfColumnsOn(ipb)
    MaltaBase.ReadoutHalfRowsOff(ipb)
else:
    MaltaBase.ReadoutHalfColumnsOn(ipb)
    MaltaBase.ReadoutHalfRowsOn(ipb)
MaltaBase.ReadoutOn(ipb)

cacheWord=ipb.Read(8)
mask    = 0xFFFFFFFF ^ ( 1<<20 )
maskTRIG= 0xFFFFFFFF ^ ( 1<<19 )
commands=[]
commands.append( ((cacheWord | (1<<20))) | (1<<19) )
commands.append( (cacheWord | (1<<20)) & maskTRIG )
commands.append( (cacheWord | (1<<20)) )
commands.append( (cacheWord | (1<<20)) )
commands.append( (cacheWord & mask)    )

MaltaBase.ReadoutOff(ipb)
MaltaBase.ResetFifo(ipb)

MaltaBase.ReadoutOn(ipb)
if mode==2:
    time.sleep( float(args.npulses)/1000) 
else:
    print "start pulsing ..."+str(args.npulses)+" TIMES"
    for i in xrange(0,args.npulses):
        ipb.Write(8,commands,True)
MaltaBase.ReadoutOff(ipb)


##read untill fifo empty:
fifoMonEmpty=MaltaBase.IsFIFOMonEmpty(ipb)
nEntry=0
while (not fifoMonEmpty) and nEntry<200:
    values = MaltaBase.ReadMonitorWord(ipb,False)
    #print values
    if values[0]==0: 
        print "Empty word ... this should never happen!!"
        break
    start=0
    for bit in xrange(16):
        if (values[0]>>(bit))&0x1==1:
            start=bit
            break
        pass
    for j in xrange(len(values)):
        for b in xrange(16):
            if (values[j]>>(b))&0x1==0:continue
            h1.Fill(b-start+2,j)
            pass
        pass
    nEntry+=1
    fifoMonEmpty=MaltaBase.IsFIFOMonEmpty(ipb)
print "Number of collected hits is: "+str(nEntry)


c1=ROOT.TCanvas("c1","c1",800,600)
h1.Draw("COLZtext")
for l in lines:
    l.Draw("SAMEL")
fDate=time.strftime("%Y%m%d_%Hh%Mm%Ss")
fileName="Output/CalibProcedure_M"+str(mode)
if args.write:
    if args.first or args.defaultTaps: fileName+="_FIRST"
    else                             : fileName+="_BEFORE"
else:
    fileName+="_AFTER"
c1.SaveAs(fileName+".pdf")
oFile=ROOT.TFile(fileName+".root","RECREATE")
#c1.SaveAs(fileName+".root")
c1.Write("canvas")
h1.Write("histo")
oFile.Close()
#if not args.write: 
os.system("evince "+fileName+".pdf &")

#if args.defaultTaps:
#    c1.SaveAs("Output/%s_UncorrectedOutput.pdf"%fDate)
#   c1.SaveAs("Output/%s_UncorrectedOutput.root"%fDate)
#    os.system("evince "+("Output/%s_UncorrectedOutput.pdf"%fDate)+" &")
#else:
#    c1.SaveAs("Output/%s_CorrectedOutput.pdf"%fDate)
#    c1.SaveAs("Output/%s_CorrectedOutput.root"%fDate)
#    os.system("evince "+("Output/%s_CorrectedOutput.pdf"%fDate)+" &")



#############################################################
# Calculating Tap Corrections ###############################
#############################################################

#######mg1 = ROOT.TMultiGraph()
###g1 = {}

mean = []
ymean = {}
zSumPerYbin = {} 
for y in xrange(h1.GetNbinsY()):
    y+=1
    #####print "y: ",y
    #g1[y] = ROOT.TGraph()
    g1 = ROOT.TGraph()
    ymean[y]=0
    zSumPerYbin[y] = 0
    for x in xrange(h1.GetNbinsX()):
        x+=1
        ##stability protection: at least 5 hits in bin:
        if h1.GetBinContent(x,y)<lowerLimit: continue
        #g1[y].SetPoint(x,x,h1.GetBinContent(x,y))
        #mg1.Add(g1[y])
        ###############g1.SetPoint(x,x,h1.GetBinContent(x,y))
        ###############mg1.Add(g1)
        ymean[y]+=h1.GetBinContent(x,y)*x
        zSumPerYbin[y]+=h1.GetBinContent(x,y)
        #print "ymean[y]: ",ymean[y],"zSumPerYbin[y]: ",zSumPerYbin[y]

############################################################################################################
#print "second part"
y = 0
for y in xrange(h1.GetNbinsY()):
    y+=1
    if ymean[y]==0: 
        mean.append(0.0)
        continue
    mean.append(ymean[y]/zSumPerYbin[y])
    ##############print "Channel: "+str(y)+" has mean: "+str(ymean[y]/zSumPerYbin[y])
print " "

############################################################################################################
diff = 0
count = 0
taps1= [] 
##taps2= []
for j in mean:
    count+=1
    if j==0: 
        taps1.append(-9999)
        continue
    ##VD: always do it w.r.t. Reference!!!
    #distanceToLatest=( (max(mean)-j)*sampleLength )    
    distanceToLatest=( (mean[0]-j)*sampleLength )    
    theString= ("channel: "+str(count)+("\ttime difference (in ps): %7.3f"%(distanceToLatest)))

    if math.fabs(distanceToLatest)< args.tap/2:
        theString+=("\t# of taps: 0")
        taps1.append(0)
        pass
    elif math.fabs(distanceToLatest)>15.*args.tap:
        print "WARNING: DISTANCE TOO LARGE ... I WOULD NOT BEABLE TO COMPENSATE!!!  "+str(distanceToLatest)
        taps1.append(-99999)
    else:
        tapShift=distanceToLatest/args.tap
        theString+=("\t# of taps: %.1f translate into %s"%(tapShift,int(round(tapShift))))
        taps1.append( int(round(tapShift)) )
        pass
    print theString
    pass
print " "
###print taps1
###print " "


#############################################################
# Writing Taps back to FPGA #################################
#############################################################

'''
##atm I am always starting from default ... need to change it!!!
if args.write:
    print "Writing Corrected Tap Values"
    for chn in xrange(37):
        previous=MaltaBase.ReadTap(ipb, chn)
        tapCorrection=taps1[chn]
        if tapCorrection==-9999: tapCorrection=taps1[chn]
        val1=previous[0]+tapCorrection ## the +1 is a protection
        val2=val1-args.tapOffset
        MaltaBase.WriteTap(ipb, chn, val1, val2)
    print "Correction taps written to FPGA."
    MaltaBase.writeTapToFile(ipb, "TapCalib_"+str(mode)+".txt")
    MaltaBase.readAllDelays(ipb,True)
    pass

os.system("MALTA_PSU.py -t T_SC_ON.txt ")
os.system("MALTA_PSU.py -t T_ANAG_OFF.txt ")

time.sleep(0.1)
MaltaBase.switchClockOn(ipb)
ipb.Write(5, sc.disablePulsePixelCol(theCol) )
if theCol2!=-1: ipb.Write(5, sc.disablePulsePixelCol(theCol2) )
ipb.Write(5, sc.disablePulsePixelHor(theRow) )
MaltaBase.switchClockOff(ipb)
time.sleep(0.1)
'''

print "#########################################################################################################"
print "#########################################################################################################"
print "#########################################################################################################"
print "#########################################################################################################"
