#!/usr/bin/env python
##################################################################################################
## include the file with the basic instructions
import sys
import time
import threading
from socket import gethostname

import MaltaSetup
import MaltaBase
from MaltaBase import *
import Herakles

import ROOT
from ROOT import TStopwatch,TH2D,TH1D,TLine,TGMainFrame,TGVerticalFrame,kFixedSize,kDoubleBorder,TGGroupFrame,kHorizontalFrame,kChildFrame,TGTextButton,TGLayoutHints,kLHintsBottom,kLHintsCenterY,kLHintsLeft,kVerticalFrame,TGLabel,kLHintsRight,kLHintsTop,kLHintsExpandX,TGHorizontalFrame,TRootEmbeddedCanvas,TCanvas,kLHintsExpandY,TGNumberEntry,TGNumberFormat,TGCheckButton,TTimer,kDeepCleanup,gApplication,kButtonDown,kButtonUp,TGClient,gClient

##################################################################################################

import Style
Style.SetStyle()
#gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#SetAtlasStyle()
#gStyle.SetPalette(1)
timer=TStopwatch()
##################################################################################################

shiftWord=16
isSpecial=True
if isSpecial:
    shiftWord=8

herIP =None
histoM=None
window=None
sent  =0
readM =0
readO =0

xlabels=[
    "2_7","2_6","2_5","2_4","2_3","2_2","2_1","2_0",
    "1_7","1_6","1_5","1_4","1_3","1_2","1_1","1_0",
    ]
xlabels2=[
    "3_7","3_6","3_5","3_4","3_3","3_2","3_1","3_0",
    "2_7","2_6","2_5","2_4","2_3","2_2","2_1","2_0",
    "1_7","1_6","1_5","1_4","1_3","1_2","1_1","1_0",
    ]

pins=[
    "PIN_0" ,"PIN_1" ,"PIN_2" ,"PIN_3" ,"PIN_4" ,"PIN_5" ,"PIN_6" ,"PIN_7" ,"PIN_8" ,"PIN_9" ,
    "PIN_10","PIN_11","PIN_12","PIN_13","PIN_14","PIN_15","PIN_16","PIN_17","PIN_18","PIN_19",
    "PIN_20","PIN_21","PIN_22","PIN_23","PIN_24","PIN_25","PIN_26","PIN_27","PIN_28","PIN_29",
    "PIN_30","PIN_31","PIN_32","PIN_33","PIN_34","PIN_35","PIN_36",
    ]
histoM=TH2D("bits2","bits2",
           len(xlabels),-0.5,len(xlabels)-0.5,
           len(pins)   ,-0.5,len(pins)   -0.5  )
histoM.SetTitle("; sampling bit; channel;")
#histoM.Draw("AXIS")
histoM.GetXaxis().SetTickLength(0.00)
histoM.GetYaxis().SetTickLength(0.00)
histoM2=TH2D("bits2ns","bits2ns",
           len(xlabels2),-0.5,len(xlabels2)-0.5,
           len(pins)   ,-0.5,len(pins)   -0.5  )
histoM2.SetTitle("; sampling bit; channel;")
#histoM2.Draw("AXIS")
histoM2.GetXaxis().SetTickLength(0.00)
histoM2.GetYaxis().SetTickLength(0.00)

if isSpecial:
    histoM=histoM2

lines=[]
for bin in xrange(0,len(xlabels)):
    tmpL=TLine(0.5+bin,-0.5,0.5+bin,len(pins)-0.5)
    tmpL.SetLineWidth(1)
    tmpL.SetLineStyle(2)
    tmpL.SetLineColor(1)
    lines.append(tmpL)
if isSpecial:
    for bin in xrange(len(xlabels),len(xlabels)+8):
        tmpL=TLine(0.5+bin,-0.5,0.5+bin,len(pins)-0.5)
        tmpL.SetLineWidth(1)
        tmpL.SetLineStyle(2)
        tmpL.SetLineColor(1)
        lines.append(tmpL)
    
L1=TLine(7.5,-0.5,7.5,len(pins)-0.5)
L1.SetLineWidth(3)
L1.SetLineStyle(1)
L1.SetLineColor(1)
lines.append(L1)
L2=TLine(15.5,-0.5,15.5,len(pins)-0.5)
L2.SetLineWidth(3)
L2.SetLineStyle(1)
L2.SetLineColor(1)
lines.append(L2)


    
##################################################################################################################################################
##################################################################################################################################################
##################################################################################################################################################
def isConnectionOK(herIP):
    if herIP==None: return False
    else          : return (herIP.Read(1)!=0)

def Sync(herIP):
    if herIP==None: return 
    herIP.Sync()
        
##################################################################################################################################################
def Connect():
    global herIP
    if herIP!=None:
        Sync(herIP)
        return
    
    connstr=MaltaSetup.MaltaSetup().getConnStr()
    connstr='udp://ep-ade-gw-05:50001'
    print "Trying to connect to: "+connstr
    ipName=connstr
    herIP=Herakles.Uhal(ipName)
    herIP.SetVerbose(False)
    herIP.SetTimeout(300)
    readAllDelays(herIP,True)
    writeConstDelays(herIP, 4, 1)   # bojan
    #writeConstDelays(herIP, 1, 1)   # bojan
    readAllDelays(herIP,True)
    #ReadoutOn(herIP)
    #EnableFastSignal(herIP)
    return

##################################################################################################################################################
def Disconnect():
    global herIP
    if herIP==None: return
    herIP=None

##################################################################################################################################################
def CMD_ReSync():
    global herIP
    if herIP==None: return
    Sync(herIP)

##################################################################################################################################################
def CMD_LoadDefaultDelays():
    global herIP
    if herIP==None: return
    writeDefaultDelays(herIP)
    readAllDelays(herIP)
    
##################################################################################################################################################
def CMD_ToggleConnect():
    global window
    global herIP
    window.B_Connect.SetState(kButtonDown)
    if not window.connected:
        Connect()
        if isConnectionOK(herIP):
            window.connected=True
            window.B_Connect.SetText("  Disconnect ")
            CMD_Update()
    else:
        Disconnect()
        if herIP==None:
            window.connected=False 
            window.B_Connect.SetText("   Connect   ")
            CMD_Update()
    window.B_Connect.SetState(kButtonUp)


##################################################################################################################################################
def UpdateHisto():
    global window
    global histoM
    global lines
    c1 = window.E_Can.GetCanvas()
    histoM.Draw("SAMECOLZTEXT")
    for l in lines: l.Draw("SAMEL")
    c1.Update();
    c1.Modified();
    return
        
##################################################################################################################################################
def CMD_ResetHisto():
    global histoM
    if histoM!=None: histoM.Reset()
    UpdateHisto()

##################################################################################################################################################
def CMD_Update():
    global window
    global herIP
    global sent
    global readM
    global readO
    if window is None: return

    #print "about to update"
    isOK=isConnectionOK(herIP)
    if not isOK:
        window.L_Date.SetText("XX-XX-XXXX")
        window.L_Version.SetText("XXXXXX")
        window.L_Connect.SetTextColor(16711680)
        window.L_Connect.SetText("NOT CONNECTED")
    else:
        window.L_Date.SetText( GetStatus(herIP) )
        window.L_Version.SetText(" "+str( GetVersion(herIP) )+" ")
        window.L_Connect.SetTextColor(65280)
        window.L_Connect.SetText("CONNECTED")

    UpdateFifoStatus()
    window.L_Sent.SetText( str(sent) )
    window.L_ReadM.SetText( str(readM) )
    window.L_ReadO.SetText( str(readO) )
    

##################################################################################################################################################
def WriteFifoStatus(obj, value):
    obj.SetText(value)
    if   value=="FULL"                           : obj.SetTextColor(16711680)
    elif value=="EMPTY"                          : obj.SetTextColor(   65280)
    elif value=="NOT EMPTY" or value=="<50% FULL": obj.SetTextColor(     255)
    elif value==">50% FULL"                      : obj.SetTextColor(16776960)
    else                                         : obj.SetTextColor(       1)
    return
        
##################################################################################################################################################
def UpdateFifoStatus():
    global herIP
    global window
    values=[]
    if herIP!=None: values=GetFifoStatus(herIP)
    else          : values=["","",""]
    WriteFifoStatus(window.L_Fast, values[0])
    WriteFifoStatus(window.L_IPB , values[1])
    WriteFifoStatus(window.L_Mon , values[2])
    return
        
##################################################################################################################################################
def CMD_ResetFifo():
    global herIP
    global sent
    global readM
    global readO
    if not isConnectionOK(herIP): return
    ResetFifo(herIP)
    UpdateFifoStatus()
    sent  =0
    readM =0
    readO =0

        
##################################################################################################################################################
def CMD_ReadBoth():
    CMD_ReadOfficial()
    CMD_ReadMonitor()
    
def SendPulseLoc(ipb, val, channel=8):
    cacheWord=ipb.Read(8)
    maskTRIG= 0xFFFFFFFF ^ (1<<19)
    maskENB = 0xFFFFFFFF ^ (1<< 0)
    maskRST = 0xFFFFFFFF ^ (1<< 1)

    ### THIS IS UGLY AS FUCK!!! NEED TO REWRITE IT ASAP!!!!
    tmpVal=int(val, 2)
    value=( tmpVal << 4)
    ########print "val: "+val+" ---> "+str(tmpVal)+" ,,, "+str(value)
    values=[]
    values.append(  cacheWord+value             )
    values.append(  (cacheWord+value) | (1<<1)  ) ##reset pulse
    values.append(  (cacheWord+value) & maskRST ) ##clear reset

##testA)
##    values.append(  (cacheWord+value) | (1<<19) | (1<<1)  )
#    values.append( ((cacheWord+value) & maskTRIG) | (1<<1)  )
##    values.append((((cacheWord+value) & maskTRIG) & maskRST) | (1<< 0)  )

##testC)
#    values.append(  (cacheWord+value) | (1<<19) | (1<<1)  )
#    values.append(  (cacheWord+value) | (1<<19) | (1<<1)  )
#    values.append((((cacheWord+value) & maskTRIG) & maskRST) | (1<< 0)  )

##testB)
    values.append( ((cacheWord+value) | (1<<19) ) | (1<< 0) )
    values.append( (cacheWord+value) & maskTRIG )

##testD)
#    values.append(  (cacheWord+value) | (1<<19)   | (1<<1) )
#    values.append( ((cacheWord+value) & maskTRIG) | (1<<1) )
#    values.append((((cacheWord+value) & maskTRIG) & maskRST) | (1<< 0)  )
    
    values.append(  (cacheWord) & maskRST )
    ######print values
    ipb.Write(channel,values,True)
    time.sleep(0.001)
    tmpW=herIP.Read(3)
    L1id=(tmpW>>20)
    print "--> L1id: "+str(L1id)
    pass

##################################################################################################################################################
def CMD_SendSignal():
    global herIP
    global sent
    global window
    if not isConnectionOK(herIP): return
    if IsFIFOFastFull(herIP): return
    nAttempt=window.N_Send.GetNumberEntry().GetIntNumber()
    if nAttempt==0: nAttempt=1
    timer.Start()
    pattern=""
    if window.C_B4.IsOn(): pattern+="1"
    else                 : pattern+="0"
    if window.C_B3.IsOn(): pattern+="1"
    else                 : pattern+="0"
    if window.C_B2.IsOn(): pattern+="1"
    else                 : pattern+="0"
    if window.C_B1.IsOn(): pattern+="1"
    else                 : pattern+="0"

    ReadoutOn(herIP)
    maskENB= 0xFFFFFFFF ^ (1<<29)    
    cacheWord=herIP.Read(8) 
    cacheWord=cacheWord&maskENB
    #####herIP.Write(8, cacheWord & maskENB )

    print "Pattern is: "+pattern
    for i in xrange(0,nAttempt):
        if (i!=0 and i%100==0 and IsFIFOFastFull(herIP) ): break
        #if i<10: ReadoutOn(herIP)
        #else   : ReadoutOff(herIP)
        SendPulseLoc(herIP,pattern) ##'00000')
        sent+=1
    timer.Stop()
    print "Timing report: "+str(nAttempt)+" words in "+str(timer.RealTime())+" seconds --> "+str(nAttempt/(timer.RealTime()))+" w/s"
    UpdateFifoStatus()
    CMD_Update()

##################################################################################################################################################
def CMD_ReadMonitor():
    global herIP
    global window
    global readM
    if not isConnectionOK(herIP): return
    if IsFIFOMonEmpty(herIP): return
    nAttempt=window.N_ReadM.GetNumberEntry().GetIntNumber()
    if nAttempt==0: nAttempt=1
    readAll=window.C_RAllM.IsOn()
    fixPin0=window.C_Align.IsOn()
    isCumul=window.C_Cumul.IsOn()
    thisRead=0
    while thisRead<nAttempt:
        if not isCumul:
            time.sleep(0.1)
            histoM.Reset()
        if thisRead!=0 and thisRead%10==0 and IsFIFOMonEmpty(herIP): break 
        values=ReadMonitorWord(herIP,False)
        if values[0]==0: break
        #print values
        newVal=[]
        for v in values:
            newVal.append( v & 0xFFFF )
        print newVal
        readM+=1
        thisRead+=1
        if readAll: nAttempt+=1
        ###### actual filling ######
        tmpList=list('{0:0b}'.format(values[0]))
        count=-1 
        start=0 #position of the first up bit (from right)
        for obj in tmpList:
            count+=1
            if count<shiftWord: continue
            if obj=='1': start=count-shiftWord
        start=shiftWord-1-start
        start+=10 ##VALERIO
        
        bin=0
        for val in values:
            bin+=1
            tmpList=list('{0:0b}'.format(val))
            count=-1 
            for test in tmpList:
                count+=1
                if count<shiftWord: continue
                if test=="1":
                    if  fixPin0: histoM.Fill(count-shiftWord+start-3,bin-1, 1)
                    else       : histoM.Fill(count-shiftWord        ,bin-1, 1)
        if not isCumul: UpdateHisto()
    UpdateHisto()
    CMD_Update()
    UpdateFifoStatus()

##################################################################################################################################################
def AO_SimpleConfig():
    print "FUCK, it's really complicated!!!"
    
##################################################################################################################################################
def CMD_ReadOfficial():
    global herIP
    global window
    global readO
    if not isConnectionOK(herIP): return
    if IsFIFOIPEmpty(herIP): return
    nAttempt=window.N_ReadO.GetNumberEntry().GetIntNumber()
    if nAttempt==0: nAttempt=1
    readAll=window.C_RAllO.IsOn()
    thisRead=0
    while thisRead<nAttempt:
        if thisRead!=0 and thisRead%100==0 and IsFIFOIPEmpty(herIP): break 
        values=ReadMaltaWord(herIP)
        tmpList1=list('{0:0b}'.format(values[0]))
        tmpList2=list('{0:0b}'.format(values[1]))
        print tmpList1
        print tmpList2
        if values[0]==0:
            if IsFIFOIPEmpty(herIP): break
        readO+=1
        thisRead+=1
        ## ask Abi on how to do it better:
        malta=tmpList2[26] +tmpList2[27] +tmpList2[28] +tmpList2[29]+tmpList2[30]+tmpList2[31]
        count=-1
        for obj in tmpList1:
            count+=1
            if count==0: continue
            malta+=str(obj)
        window.L_LW_MaltaW.SetText(malta)
        edge =tmpList2[23] +tmpList2[24] +tmpList2[25]
        timeC=tmpList2[19] +tmpList2[20] +tmpList2[21] +tmpList2[22]
        # bojan+valerio+lluis
        trigID = ''
        for counter_bit in xrange(1,13): #19
            # loop through bits 1-19 of the second word. 
            # Unknown effects if counter ID is smaller than 18 bits
            trigID += str(tmpList2[counter_bit])
        window.L_LW_Phase.SetText(edge)
        window.L_LW_Counter.SetText(timeC)
        #window.L_LW_TrigID.SetText(trigID)
        window.L_LW_L1A.SetText(trigID)

        bcid = ''
        for counter_bit in xrange(14,19): #19
            bcid += str(tmpList2[counter_bit])
        window.L_LW_InitBCID.SetText(bcid)
        
    CMD_Update()
    UpdateFifoStatus()

############################################################################

# bojan
def CMD_SendTrigIDSignal():
    global herIP
    global window
    if not isConnectionOK(herIP): return

    cacheWord=herIP.Read(8)
    maskTRIG= 0xFFFFFFFF ^ (1<<19)
    ##herIP.Write(8, cacheWord | (1<<19) )
    ####time.sleep(0.1)
    ####herIP.Write(8, cacheWord & maskTRIG )
    values=[]
    values.append(cacheWord | (1<<19))
    values.append(cacheWord & maskTRIG)
    herIP.Write(8,values,True)

    time.sleep(0.001)
    
    CMD_Update()

############################################################################

def CMD_ResetTrigID():
    global herIP
    global window
    if not isConnectionOK(herIP): return
    cacheWord=herIP.Read(8)
    
    herIP.Write(8, cacheWord | (1<<16) )
    time.sleep(1)
    maskRESET= 0xFFFFFFFF ^ (1<<16)
    herIP.Write(8, cacheWord & maskRESET )
    time.sleep(0.001)
    CMD_Update()

############################################################################

def CMD_SetTrigIDMode():
    global herIP
    global window
    if not isConnectionOK(herIP): return
    cacheWord=herIP.Read(8)
    
    setIt= window.C_B_trig_mode.IsOn()
    if setIt: herIP.Write(8, cacheWord | (1<<18) )
    else:
        maskMODE= 0xFFFFFFFF ^ (1<<18)
        herIP.Write(8, cacheWord & maskMODE )
    time.sleep(0.001)
    CMD_Update()
#end bojan

##################################################################################################################################################

#def HandleMenu():
#    val=-1
#    print " CALLED WITH val: "+str(val)
#    if val==0: AO_SimpleConfig()
#    if val==1: print "I am too tired to do anything on Slow Control"
#    return 
#P_no = TPyDispatcher( HandleMenu() )

###########################################################################
class pMainFrame( TGMainFrame ):

    def Notify():
        CMD_Update()
        return True

    def __init__( self, parent, width, height ):
        TGMainFrame.__init__( self, parent, width, height )

        ###def Handle(self): print "FUCK"
        
        self.connected=False
        
        self.mainW = TGVerticalFrame( self, 1100, 1000, kFixedSize|kDoubleBorder)

        '''
        self.fMenuDock = TGDockableFrame(self.mainW);
        self.mainW.AddFrame(self.fMenuDock, TGLayoutHints(kLHintsExpandX, 0, 0, 1, 0));
        self.M_AO = TGPopupMenu(gClient.GetRoot() );
        self.M_AO.AddEntry("S&impleConfig", 0)
        self.M_SC = TGPopupMenu(gClient.GetRoot() );
        self.M_SC.AddEntry("Si&mpleConfig", 1)
        self.M_Main = TGMenuBar(self.fMenuDock,1,1,kHorizontalFrame);
        self.M_Main.AddPopup("&AO configuration", self.M_AO, TGLayoutHints(kLHintsTop|kLHintsLeft,0,4,0,0) );
        self.M_Main.AddPopup("&Slow Control"    , self.M_SC, TGLayoutHints(kLHintsTop|kLHintsLeft,0,4,0,0) );
        #self.M_AO.Connect("Activated(Int_t)", "TPyDispatcher", P_no  , "Dispatch()" )
        #self.M_SC.Connect("Activated(Int_t)", "pMainFrame"   , self, "Handle()")
        self.fMenuDock.AddFrame(self.M_Main, TGLayoutHints(kLHintsTop | kLHintsExpandX));
        '''
        
        #################################################################################################################################
        #### COMMUNICATION
        self.F_Status = TGGroupFrame(self.mainW, "Communication status",kHorizontalFrame|kChildFrame);
        self.B_Connect= TGTextButton(self.F_Status, '   Connect   ','TPython::Eval("CMD_ToggleConnect()")')
        self.F_Status.AddFrame(self.B_Connect, TGLayoutHints(kLHintsBottom|kLHintsLeft,  5, 2, 15, 2));
        self.B_Sync = TGTextButton(self.F_Status, ' Re-Sync ','TPython::Eval("CMD_ReSync()")')
        self.F_Status.AddFrame(self.B_Sync, TGLayoutHints(kLHintsBottom|kLHintsLeft,  5, 2, 15, 2));
        self.B_Del = TGTextButton(self.F_Status, ' LoadDelays ','TPython::Eval("CMD_LoadDefaultDelays()")')
        self.F_Status.AddFrame(self.B_Del, TGLayoutHints(kLHintsBottom|kLHintsLeft,  5, 2, 15, 2));        

        self.F_Date = TGGroupFrame(self.F_Status, "date",kVerticalFrame|kChildFrame);
        self.L_Date = TGLabel(self.F_Date, "XX-XX-XXXX");
        self.F_Date.AddFrame(self.L_Date, TGLayoutHints(kLHintsBottom,   0, 0, 5, -5) );
        self.F_Status.AddFrame(self.F_Date  , TGLayoutHints(kLHintsBottom|kLHintsRight, 5, 2, 0, -8) );

        self.F_Version = TGGroupFrame(self.F_Status, "version",kVerticalFrame|kChildFrame);
        self.L_Version = TGLabel(self.F_Version, "XXXXXX");
        self.F_Version.AddFrame(self.L_Version,  TGLayoutHints(kLHintsBottom,0,0,5,-5) );
        self.F_Status.AddFrame(self.F_Version,  TGLayoutHints(kLHintsBottom|kLHintsRight, 5, 2, 0, -8));

        self.L_Connect = TGLabel(self.F_Status, "NOT CONNECTED");
        self.L_Connect.SetTextColor(16711680);
        self.F_Status.AddFrame(self.L_Connect,  TGLayoutHints(kLHintsBottom|kLHintsRight, 5, 2, 15, 2));
        
        self.mainW.AddFrame( self.F_Status, TGLayoutHints(kLHintsTop|kLHintsExpandX,20,20,20,20) )

        
        #################################################################################################################################
        #### FIFOS
        self.F_FIFO = TGGroupFrame(self.mainW, "FIFO status",kHorizontalFrame|kChildFrame);

        self.B_ResetF = TGTextButton( self.F_FIFO, ' Reset FIFOs ','TPython::Eval("CMD_ResetFifo()")')
        self.F_FIFO.AddFrame(self.B_ResetF, TGLayoutHints(kLHintsBottom,  5, 2, 15, 2));

        self.F_Fast = TGGroupFrame(self.F_FIFO, "AO-fifo",kVerticalFrame|kChildFrame);
        self.L_Fast = TGLabel(self.F_Fast, "XXXXXXXXX");
        self.F_Fast.AddFrame(self.L_Fast,  TGLayoutHints(kLHintsBottom,0,0,5,-5) );
        self.F_FIFO.AddFrame(self.F_Fast,  TGLayoutHints(kLHintsBottom, 120, 2, 0, -8));

        self.F_IPB = TGGroupFrame(self.F_FIFO, "AO-ipbus",kVerticalFrame|kChildFrame);
        self.L_IPB = TGLabel(self.F_IPB, "XXXXXXXXX");
        self.F_IPB.AddFrame(self.L_IPB,  TGLayoutHints(kLHintsBottom,0,0,5,-5) );
        self.F_FIFO.AddFrame(self.F_IPB,  TGLayoutHints(kLHintsBottom, 180, 2, 0, -8));

        self.F_Mon = TGGroupFrame(self.F_FIFO, "AO-monitor",kVerticalFrame|kChildFrame);
        self.L_Mon = TGLabel(self.F_Mon, "XXXXXXXXX");
        self.F_Mon.AddFrame(self.L_Mon,  TGLayoutHints(kLHintsBottom,0,0,5,-5) );
        self.F_FIFO.AddFrame(self.F_Mon,  TGLayoutHints(kLHintsBottom, 200, 2, 0, -8));
        
        self.mainW.AddFrame( self.F_FIFO, TGLayoutHints(kLHintsTop|kLHintsExpandX,20,20,20,20) ) 

        
        #################################################################################################################################
        #### bottom
        self.F_bottom = TGHorizontalFrame(self.mainW,400,800,kDoubleBorder);

        #### dispaly histo
        self.E_Can = TRootEmbeddedCanvas("test",self.F_bottom,550,510);
        wid   = self.E_Can.GetCanvasWindowId();
        self.Can = TCanvas("MyCanvas", 200,200,wid);
        self.E_Can.AdoptCanvas(self.Can);
        self.F_bottom.AddFrame(self.E_Can, TGLayoutHints(kLHintsTop|kLHintsLeft|kLHintsExpandY,0,0,1,1))

        self.F_right =TGVerticalFrame(self.F_bottom,800,200,kChildFrame)

        ##-------------------------------------------------------------------------------------------------------------------------------
        #bojan
        self.F_OptionTrigID = TGGroupFrame(self.F_right, "Trigger ID options",kHorizontalFrame|kChildFrame);

        self.C_B_trig_mode = TGCheckButton(self.F_OptionTrigID, "Internal ", 'TPython::Eval("CMD_SetTrigIDMode()")');
        self.F_OptionTrigID.AddFrame(self.C_B_trig_mode, TGLayoutHints(kLHintsTop|kLHintsLeft,25, 5, 15, -8));
        
        self.B_ResetTrigID = TGTextButton( self.F_OptionTrigID,  "Reset TrigID", 'TPython::Eval("CMD_ResetTrigID()")')
        self.F_OptionTrigID.AddFrame(self.B_ResetTrigID, TGLayoutHints(kLHintsBottom|kLHintsLeft,  5, 2, 15, 2));

        self.B_SendTrigID = TGTextButton( self.F_OptionTrigID, ' Random TrigID ','TPython::Eval("CMD_SendTrigIDSignal()")')
        self.F_OptionTrigID.AddFrame(self.B_SendTrigID, TGLayoutHints(kLHintsBottom|kLHintsLeft,  5, 2, 15, 2));
        
        self.F_right.AddFrame(self.F_OptionTrigID,  TGLayoutHints(kLHintsTop|kLHintsExpandX, 2, 2, 2, 2));
        
        ##-------------------------------------------------------------------------------------------------------------------------------
        self.F_OptionS = TGGroupFrame(self.F_right, "Sender options",kHorizontalFrame|kChildFrame);

        self.C_B1 = TGCheckButton(self.F_OptionS, "Bit1", 1);
        self.C_B2 = TGCheckButton(self.F_OptionS, "Bit2", 2);
        self.C_B3 = TGCheckButton(self.F_OptionS, "Bit3", 3);
        self.C_B4 = TGCheckButton(self.F_OptionS, "2ns" , 4);
        #self.C_B5 = TGCheckButton(self.F_OptionS, "Bit5", 5);
        self.F_OptionS.AddFrame(self.C_B1, TGLayoutHints(kLHintsTop|kLHintsLeft,25, 5, 15, -8));
        self.F_OptionS.AddFrame(self.C_B2, TGLayoutHints(kLHintsTop|kLHintsLeft,25, 5, 15, -8));
        self.F_OptionS.AddFrame(self.C_B3, TGLayoutHints(kLHintsTop|kLHintsLeft,25, 5, 15, -8));
        self.F_OptionS.AddFrame(self.C_B4, TGLayoutHints(kLHintsTop|kLHintsLeft,25, 5, 15, -8));
        #self.F_OptionS.AddFrame(self.C_B5, TGLayoutHints(kLHintsTop|kLHintsLeft,25, 5, 15, -8));
        self.F_right.AddFrame(self.F_OptionS,  TGLayoutHints(kLHintsTop|kLHintsExpandX, 2, 2, 2, 2));

        ##-------------------------------------------------------------------------------------------------------------------------------
        self.F_Sender = TGGroupFrame(self.F_right, "Sender",kHorizontalFrame|kChildFrame);

        self.B_Send = TGTextButton( self.F_Sender, ' SendSignal ','TPython::Eval("CMD_SendSignal()")')
        self.F_Sender.AddFrame(self.B_Send, TGLayoutHints(kLHintsTop,  5, 2, 15, 2));
    
        self.N_Send = TGNumberEntry(self.F_Sender, 0, 9, 9999999, TGNumberFormat.kNESInteger,
                                    TGNumberFormat.kNEANonNegative, TGNumberFormat.kNELLimitMinMax,0, 99999999);
        self.F_Sender.AddFrame(self.N_Send, TGLayoutHints(kLHintsTop, 80, 5, 15, 2));

        self.F_Sent = TGGroupFrame(self.F_Sender, "# sent",kVerticalFrame|kChildFrame);
        self.L_Sent = TGLabel(self.F_Sent, "            ");
        self.F_Sent.AddFrame(self.L_Sent,  TGLayoutHints(kLHintsBottom,0,0,5,-5) );
        self.F_Sender.AddFrame(self.F_Sent,  TGLayoutHints(kLHintsBottom|kLHintsRight, 200, 2, 0, -8));

        self.F_right.AddFrame(self.F_Sender, TGLayoutHints(kLHintsTop|kLHintsExpandX, 2, 2, 2, 2));
    
        ##-------------------------------------------------------------------------------------------------------------------------------
        self.F_ReaderM = TGGroupFrame(self.F_right, "Reader Monitor",kHorizontalFrame|kChildFrame);

        self.B_ReadM = TGTextButton( self.F_ReaderM, ' Read Monitor ','TPython::Eval("CMD_ReadMonitor()")')
        self.F_ReaderM.AddFrame(self.B_ReadM, TGLayoutHints(kLHintsTop,  5, 2, 15, 2));

        self.N_ReadM = TGNumberEntry(self.F_ReaderM, 0, 9, 999999, TGNumberFormat.kNESInteger,
                                    TGNumberFormat.kNEANonNegative, TGNumberFormat.kNELLimitMinMax,0, 9999999);
        self.F_ReaderM.AddFrame(self.N_ReadM, TGLayoutHints(kLHintsTop, 45, 5, 15, 2));

        self.C_RAllM = TGCheckButton(self.F_ReaderM, "&Read All", 1);
        self.F_ReaderM.AddFrame(self.C_RAllM, TGLayoutHints(kLHintsTop|kLHintsLeft,40, 5, 15, -8));
        ##fCheckMulti->Connect("Clicked()", "MyMainFrame", this, "HandleButtons()");
        
        self.F_ReadM = TGGroupFrame(self.F_ReaderM, "# read",kVerticalFrame|kChildFrame);
        self.L_ReadM = TGLabel(self.F_ReadM, "          ");
        self.F_ReadM.AddFrame(self.L_ReadM,  TGLayoutHints(kLHintsBottom,0,0,5,-5) );
        self.F_ReaderM.AddFrame(self.F_ReadM,  TGLayoutHints(kLHintsBottom|kLHintsRight, 200, 2, 0, -8));

        self.F_right.AddFrame(self.F_ReaderM,  TGLayoutHints(kLHintsTop|kLHintsExpandX, 2, 2, 30, 2));

        ##-------------------------------------------------------------------------------------------------------------------------------
        
        self.B_both = TGTextButton( self.F_right, 'Read BOTH [use with CAUTION]','TPython::Eval("CMD_ReadBoth()")')
        self.F_right.AddFrame( self.B_both, TGLayoutHints(kLHintsBottom|kLHintsExpandX,20,20,20,20) )

                
        ##-------------------------------------------------------------------------------------------------------------------------------
        self.F_ReaderLW = TGGroupFrame(self.F_right, "Last Seen Word",kVerticalFrame|kChildFrame);

        self.F_LW_top = TGHorizontalFrame(self.F_ReaderLW,400,100,kDoubleBorder);

        self.L_LW_topTitle = TGLabel(self.F_LW_top, "MaltaBit:");
        self.F_LW_top.AddFrame(self.L_LW_topTitle,  TGLayoutHints(kLHintsLeft,2,2,2,2) );
        self.L_LW_MaltaW = TGLabel(self.F_LW_top, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        self.F_LW_top.AddFrame(self.L_LW_MaltaW,  TGLayoutHints(kLHintsLeft,20,2,2,2) );
        
        #self.F_MaltaW = TGGroupFrame(self.F_LW_top, "",kVerticalFrame|kChildFrame);
        #self.L_MaltaW = TGLabel(self.F_MaltaW, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        #self.F_LW_top.AddFrame(self.L_MaltaW,  TGLayoutHints(kLHintsLeft|kLHintsBottom,0,0,5,-5) );
        #self.F_ReaderLW.AddFrame(self.F_LW_top,  TGLayoutHints(kLHintsTop|kLHintsBottom,0,0,5,-5) );

        self.F_ReaderLW.AddFrame(self.F_LW_top,  TGLayoutHints(kLHintsTop|kLHintsExpandX, 20, 2, 2, 2));


        self.F_LW_bot = TGHorizontalFrame(self.F_ReaderLW,400,100,kDoubleBorder);

        self.L_LW_botTitle = TGLabel(self.F_LW_bot, "Timer: ");
        self.F_LW_bot.AddFrame(self.L_LW_botTitle,  TGLayoutHints(kLHintsLeft,2,2,2,2) );
        self.L_LW_Counter = TGLabel(self.F_LW_bot, "XXXX");
        self.F_LW_bot.AddFrame(self.L_LW_Counter,  TGLayoutHints(kLHintsLeft,20,2,2,2) );
        
        self.L_LW_botTitle2 = TGLabel(self.F_LW_bot, "Phase: ");
        self.F_LW_bot.AddFrame(self.L_LW_botTitle2,  TGLayoutHints(kLHintsLeft,60,2,2,2) );
        self.L_LW_Phase = TGLabel(self.F_LW_bot, "XXX");
        self.F_LW_bot.AddFrame(self.L_LW_Phase,  TGLayoutHints(kLHintsLeft,70,2,2,2) );
        
        self.F_ReaderLW.AddFrame(self.F_LW_bot,  TGLayoutHints(kLHintsCenterY|kLHintsExpandX, 20, 2, 2, 2));

        # bojan
        self.F_LW_very_bot = TGHorizontalFrame(self.F_ReaderLW,400,100,kDoubleBorder);
        #self.L_LW_botTitle3 = TGLabel(self.F_LW_very_bot, "Trigger ID counter: ");
        #self.F_LW_very_bot.AddFrame(self.L_LW_botTitle3,  TGLayoutHints(kLHintsLeft,2,2,2,2) );
        #self.L_LW_TrigID = TGLabel(self.F_LW_very_bot, "XXXXXXXXXXXXXXXXXX");
        #self.F_LW_very_bot.AddFrame(self.L_LW_TrigID,  TGLayoutHints(kLHintsLeft,20,2,2,2) );
        self.L_LW_botTitle3 = TGLabel(self.F_LW_very_bot, "Int BCID: ");
        self.F_LW_very_bot.AddFrame(self.L_LW_botTitle3,  TGLayoutHints(kLHintsLeft,2,2,2,2) );
        self.L_LW_InitBCID  = TGLabel(self.F_LW_very_bot, "XXXXXX");
        self.F_LW_very_bot.AddFrame(self.L_LW_InitBCID,  TGLayoutHints(kLHintsLeft,20,2,2,2) );

        self.L_LW_botTitle4 = TGLabel(self.F_LW_very_bot, "L1A: ");
        self.F_LW_very_bot.AddFrame(self.L_LW_botTitle4,  TGLayoutHints(kLHintsLeft,60,2,2,2) );
        self.L_LW_L1A  = TGLabel(self.F_LW_very_bot, "XXXXXXXXXXXX");
        self.F_LW_very_bot.AddFrame(self.L_LW_L1A,  TGLayoutHints(kLHintsLeft,70,2,2,2) );
        

        self.F_ReaderLW.AddFrame(self.F_LW_very_bot,  TGLayoutHints(kLHintsBottom|kLHintsExpandX, 20, 2, 2, 2));
          
        #end bojan        

        self.F_right.AddFrame(self.F_ReaderLW,  TGLayoutHints(kLHintsBottom|kLHintsExpandX, 2, 2, 2, 2));

        
        ##-------------------------------------------------------------------------------------------------------------------------------
        self.F_ReaderO = TGGroupFrame(self.F_right, "Reader Official",kHorizontalFrame|kChildFrame);
        
        self.B_ReadO = TGTextButton( self.F_ReaderO, ' Read Official ','TPython::Eval("CMD_ReadOfficial()")')
        self.F_ReaderO.AddFrame(self.B_ReadO, TGLayoutHints(kLHintsTop,  5, 2, 15, 2));

        self.N_ReadO = TGNumberEntry(self.F_ReaderO, 0, 9, 999999, TGNumberFormat.kNESInteger,
                                    TGNumberFormat.kNEANonNegative, TGNumberFormat.kNELLimitMinMax,0, 9999999);
        self.F_ReaderO.AddFrame(self.N_ReadO, TGLayoutHints(kLHintsTop, 45, 5, 15, 2));

        self.C_RAllO = TGCheckButton(self.F_ReaderO, "&Read All", 2);
        self.F_ReaderO.AddFrame(self.C_RAllO, TGLayoutHints(kLHintsTop|kLHintsLeft,40, 5, 15, -8));
        
        self.F_ReadO = TGGroupFrame(self.F_ReaderO, "# read",kVerticalFrame|kChildFrame);
        self.L_ReadO = TGLabel(self.F_ReadO, "          ");
        self.F_ReadO.AddFrame(self.L_ReadO,  TGLayoutHints(kLHintsBottom,0,0,5,-5) );
        self.F_ReaderO.AddFrame(self.F_ReadO,  TGLayoutHints(kLHintsBottom|kLHintsRight, 200, 2, 0, -8));
        
        self.F_right.AddFrame(self.F_ReaderO,  TGLayoutHints(kLHintsBottom|kLHintsExpandX, 2, 2, 2, 2));

        ##-------------------------------------------------------------------------------------------------------------------------------
        self.F_OptionR = TGGroupFrame(self.F_right, "Plotting options",kHorizontalFrame|kChildFrame);

        self.C_Cumul = TGCheckButton(self.F_OptionR, "&Cumul", 3);
        self.F_OptionR.AddFrame(self.C_Cumul, TGLayoutHints(kLHintsTop|kLHintsLeft,10, 5, 15, 2));
    
        self.C_Align = TGCheckButton(self.F_OptionR, "&FixFirst", 4);
        self.F_OptionR.AddFrame(self.C_Align, TGLayoutHints(kLHintsTop|kLHintsLeft,30, 5, 15, 2));

        '''
        self.L_Histo = TGLabel(self.F_OptionR, "Histo type: ");
        self.F_OptionR.AddFrame(self.L_Histo,  TGLayoutHints(kLHintsLeft,50,5,15, 2) );

        self.CBox_Histo = TGComboBox(self.F_OptionR);
        self.CBox_Histo.AddEntry("Timing",0);
        self.CBox_Histo.AddEntry("2D XY" ,1);
        #combo->Connect("Selected(Int_t)", "TGTextEntry", entry, "SetEchoMode(Int_t)");
        self.CBox_Histo.Select(0);
        self.F_OptionR.AddFrame(self.CBox_Histo,  TGLayoutHints(kLHintsRight,5,5,11, 0) );
        self.CBox_Histo.Resize(70, 20)
        '''
        
        self.B_ResH = TGTextButton( self.F_OptionR, 'ClearHisto','TPython::Eval("CMD_ResetHisto()")')
        self.F_OptionR.AddFrame( self.B_ResH, TGLayoutHints(kLHintsRight,80, 2, 15, 2) )
        
        self.F_right.AddFrame(self.F_OptionR,  TGLayoutHints(kLHintsTop|kLHintsExpandX, 2, 2, 2, 2));


        
        self.F_bottom.AddFrame( self.F_right, TGLayoutHints(kLHintsTop|kLHintsExpandY|kLHintsExpandX,20,20,20,20) ) 
    
        self.mainW.AddFrame( self.F_bottom, TGLayoutHints(kLHintsTop|kLHintsExpandX,20,20,20,20) ) 
        #################################################################################################################################
        
        self.ExitButton = TGTextButton( self.mainW, '&Exit','TPython::Exec("raise SystemExit")')
        self.mainW.AddFrame( self.ExitButton, TGLayoutHints(kLHintsBottom|kLHintsExpandX,20,20,20,20) )

        
        self.AddFrame( self.mainW, TGLayoutHints(kLHintsBottom | kLHintsRight, 2, 2, 5, 1)) 
            
        self.SetCleanup(kDeepCleanup);
        self.SetWindowName( 'MaltaCommunication GUI' )
        self.MapSubwindows()
        self.Resize( self.GetDefaultSize() )
        self.MapWindow()
        self.timer=TTimer(3000);
        self.timer.SetCommand('TPython::Eval("CMD_Update()")');
        #self.timer.SetObject(self)
        self.timer.TurnOn();
        
        #self.CheckButton  = TGTextButton( self.fGframe, '&Check'  ,'print "idiot"')
        #self.fGframe.AddFrame(self.CheckButton  , TGLayoutHints(kLHintsBottom, 50,2,15,2));

        pass
    
   #def __del__(self):
   #    self.Cleanup()


if __name__ == '__main__':
    #window = pMainFrame( gClient.GetRoot(), 1000, 1000 )
    window = pMainFrame(TGClient.Instance().GetRoot(), 1000, 800 )
    gApplication.Run()
    
#theApp.Run()
#print gClient.GetRoot()
#
#ROOT.gApplication.Run()
