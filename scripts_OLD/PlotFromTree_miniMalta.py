import ROOT
import argparse
import os

#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#ROOT.SetAtlasStyle()

print ("HELLOOOOOOOOOOOOOOOOOOOOO")

ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetEndErrorSize(10)
ROOT.gStyle.SetOptStat(0)

parser=argparse.ArgumentParser()
parser.add_argument('-f' ,'--folder',help='output folder',required=True)
parser.add_argument('-i' ,'--input' ,help='prefix for the file',required=True)
args=parser.parse_args()

folder=args.folder+"/miniMalta_thScan/"
os.system("mkdir -p "+folder)

iT=ROOT.TChain("Thres")
iT.Add(args.input)

tmpF=ROOT.TFile(folder+"/summary.root", "RECREATE")

#########################
exclude_p = 0
minTh=   0
maxTh= 1000
maxNoise = 50
nBins=int((maxTh-minTh)/5) ##/5
######################

can=ROOT.TCanvas("can","can",800,600)
Hthres =ROOT.TH1F("All" ,"All;threshold [el];# pixels"         ,nBins,minTh,maxTh)
Hthres2=ROOT.TH1F("Hthres2","Sec0;threshold [el];# pixels SECTOR 0",nBins,minTh,maxTh)
Hthres3=ROOT.TH1F("Hthres3","Sec1;threshold [el];# pixels SECTOR 2",nBins,minTh,maxTh)
#lsHthres.Rebin(2)
#Hthres=ROOT.TH1F("Hthres","Hthres;threshold [el];# pixel",1000,0,1000)
Hthres.Sumw2()
Hthres2.Sumw2()
Hthres3.Sumw2()
#iT.Draw("thres>>Hthres","thres<800 && pixY!=1 && chi2<3","goff")
if exclude_p:
   iT.Draw("thres>>All"    ,"thres<1600 && chi2<3 && (pixY<32 || pixY>=48)","goff")
   iT.Draw("thres>>Hthres2","thres<1600 && chi2<3 && (pixY<32 || pixY>=48) && pixX<8","goff")
   iT.Draw("thres>>Hthres3","thres<1600 && chi2<3 && (pixY<32 || pixY>=48) && pixX>=8 && pixX<16","goff")
else:
   iT.Draw("thres>>All"    ,"thres<1600 && chi2<3","goff")
   iT.Draw("thres>>Hthres2","thres<1600 && chi2<3 && pixX<8","goff")
   iT.Draw("thres>>Hthres3","thres<1600 && chi2<3 && pixX>=8 && pixX<16","goff")
Hthres.SetMaximum(Hthres.GetMaximum()*1.20)
Hthres.Draw("AXIS")##HIST")
Hthres.SetLineWidth(3)
Hthres2.SetLineColor(2)
Hthres2.SetLineWidth(3)
Hthres2.Draw("HISTSAME")
Hthres3.SetLineColor(4)
Hthres3.SetLineWidth(3)
Hthres3.Draw("HISTSAME")


ptstats = ROOT.TPaveStats(0.28,0.835,0.48,0.995,"brNDC");
ptstats.SetName("stats");
ptstats.SetBorderSize(2);
ptstats.SetFillColor(0);
ptstats.SetTextAlign(12);
#ptstats.SetTextFont(42);
ptstats.SetTextColor(2);
text = ptstats.AddText("Sec 1");
text.SetTextSize(0.0368);
text=ptstats.AddText("Entries = "+str(int(Hthres2.GetEntries())));
text=ptstats.AddText("Mean  =  "+str(round(Hthres2.GetMean(),1)));
text=ptstats.AddText("RMS   =  "+str(round(Hthres2.GetRMS(),2)));
#ptstats.SetOptStat(1111);
#ptstats.SetOptFit(0);
ptstats.Draw("");


ptstats2 = ROOT.TPaveStats(0.53,0.835,0.73,0.995,"brNDC");
ptstats2.SetName("stats");
ptstats2.SetBorderSize(2);
ptstats2.SetFillColor(0);
ptstats2.SetTextAlign(12);
ptstats2.SetTextColor(4);
text2 = ptstats2.AddText("Sec 2");
text2.SetTextSize(0.0368);
text2=ptstats2.AddText("Entries = "+str(int(Hthres3.GetEntries())));
text2=ptstats2.AddText("Mean  =  "+str(round(Hthres3.GetMean(),1)));
text2=ptstats2.AddText("RMS   =  "+str(round(Hthres3.GetRMS(),1)));
ptstats2.Draw("");

'''
ptstats3 = ROOT.TPaveStats(0.78,0.835,0.98,0.995,"brNDC");
ptstats3.SetName("stats");
ptstats3.SetBorderSize(2);
ptstats3.SetFillColor(0);
ptstats3.SetTextAlign(12);
ptstats3.SetTextColor(1);
text3 = ptstats3.AddText("All");
text3.SetTextSize(0.0368);
text3=ptstats3.AddText("Entries = "+str(int(Hthres.GetEntries())));
text3=ptstats3.AddText("Mean  =  "+str(round(Hthres.GetMean(),1)));
text3=ptstats3.AddText("RMS   =  "+str(round(Hthres.GetRMS(),1)));
ptstats3.Draw("");
'''
if exclude_p:
   can.Print(folder+"Threshold_noP.pdf")
   can.Print(folder+"Threshold_noP.C")
else: 
   can.Print(folder+"Threshold.pdf")
   can.Print(folder+"Threshold.C")
##can.Print(folder+"Threshold_S1_"+args.folder+".pdf")

###############################
Hnoise =ROOT.TH1F("Allnoise" ,"All;noise [el];# pixels"        ,nBins,0,maxNoise)
Hnoise2=ROOT.TH1F("Hnoise2","Sec0;noise [el];# pixels SECTOR 0",nBins,0,maxNoise)
Hnoise3=ROOT.TH1F("Hnoise3","Sec1;noise [el];# pixels SECTOR 2",nBins,0,maxNoise)
#lsHthres.Rebin(2)
#Hthres=ROOT.TH1F("Hthres","Hthres;threshold [el];# pixel",1000,0,1000)
Hnoise.Sumw2()
Hnoise2.Sumw2()
Hnoise3.Sumw2()
#iT.Draw("thres>>Hthres","thres<800 && pixY!=1 && chi2<3","goff")
if exclude_p:
   iT.Draw("sigma>>Allnoise"    ,"thres<1600 && chi2<3 && (pixY<32 || pixY>=48)","goff")
   iT.Draw("sigma>>Hnoise2","thres<1600 && chi2<3 && (pixY<32 || pixY>=48) && pixX<8","goff")
   iT.Draw("sigma>>Hnoise3","thres<1600 && chi2<3 && (pixY<32 || pixY>=48) && pixX>=8 && pixX<16","goff")
else:
   iT.Draw("sigma>>Allnoise"    ,"thres<1600 && chi2<3","goff")
   iT.Draw("sigma>>Hnoise2","thres<1600 && chi2<3 && pixX<8","goff")
   iT.Draw("sigma>>Hnoise3","thres<1600 && chi2<3 && pixX>=8 && pixX<16","goff")
Hnoise.SetMaximum(Hthres.GetMaximum()*1.20)
Hnoise.Draw("AXIS")
Hnoise.SetLineWidth(3)
Hnoise2.SetLineColor(2)
Hnoise2.SetLineWidth(3)
Hnoise2.Draw("HISTSAME")
Hnoise3.SetLineColor(4)
Hnoise3.SetLineWidth(3)
Hnoise3.Draw("HISTSAME")

ptstats = ROOT.TPaveStats(0.28,0.835,0.48,0.995,"brNDC");
ptstats.SetName("stats");
ptstats.SetBorderSize(2);
ptstats.SetFillColor(0);
ptstats.SetTextAlign(12);
#ptstats.SetTextFont(42);
ptstats.SetTextColor(2);
text = ptstats.AddText("Sec 1");
text.SetTextSize(0.0368);
text=ptstats.AddText("Entries = "+str(int(Hnoise2.GetEntries())));
text=ptstats.AddText("Mean  =  "+str(round(Hnoise2.GetMean(),1)));
text=ptstats.AddText("RMS   =  "+str(round(Hnoise2.GetRMS(),2)));
#ptstats.SetOptStat(1111);
#ptstats.SetOptFit(0);
ptstats.Draw("");

ptstats2 = ROOT.TPaveStats(0.53,0.835,0.73,0.995,"brNDC");
ptstats2.SetName("stats");
ptstats2.SetBorderSize(2);
ptstats2.SetFillColor(0);
ptstats2.SetTextAlign(12);
ptstats2.SetTextColor(4);
text2 = ptstats2.AddText("Sec 2");
text2.SetTextSize(0.0368);
text2=ptstats2.AddText("Entries = "+str(int(Hnoise3.GetEntries())));
text2=ptstats2.AddText("Mean  =  "+str(round(Hnoise3.GetMean(),1)));
text2=ptstats2.AddText("RMS   =  "+str(round(Hnoise3.GetRMS(),1)));
ptstats2.Draw("");

'''
ptstats3 = ROOT.TPaveStats(0.78,0.835,0.98,0.995,"brNDC");
ptstats3.SetName("stats");
ptstats3.SetBorderSize(2);
ptstats3.SetFillColor(0);
ptstats3.SetTextAlign(12);
ptstats3.SetTextColor(1);
text3 = ptstats3.AddText("All");
text3.SetTextSize(0.0368);
text3=ptstats3.AddText("Entries = "+str(int(Hnoise.GetEntries())));
text3=ptstats3.AddText("Mean  =  "+str(round(Hnoise.GetMean(),1)));
text3=ptstats3.AddText("RMS   =  "+str(round(Hnoise.GetRMS(),1)));
ptstats3.Draw("");
'''
if exclude_p:
   can.Print(folder+"Noise_noP.pdf")
   can.Print(folder+"Noise_noP.C")
else: 
   can.Print(folder+"Noise.pdf")
   can.Print(folder+"Noise.C")

#################################


ROOT.gStyle.SetOptStat(1111)
can2=ROOT.TCanvas("can2","can2",800,600)
Hnoise=ROOT.TH1F("Hnoise","miniMalta Hnoise; noise [el];# pixels",60,0,80)
Hnoise.Sumw2()
##iT.Draw("sigma>>Hnoise","thres<800 && pixY!=1 && chi2<3","goff")
if exclude_p:
   iT.Draw("sigma>>Hnoise","thres<1600 && chi2<3 && (pixY<32 || pixY>=48)","goff")
else: 
   iT.Draw("sigma>>Hnoise","thres<1600 && chi2<3","goff")
Hnoise.Rebin(2)
Hnoise.Draw("HIST")
if exclude_p:
   can2.Print(folder+"Sigma_noP.pdf")
else: 
   can2.Print(folder+"Sigma.pdf")

###########################################################################################################


ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPadRightMargin(0.15);

lines=[]
Lin1B=ROOT.TLine( 64, Hthres.GetXaxis().GetXmin(), 64, Hthres.GetXaxis().GetXmax());
lines.append(Lin1B)
Lin2B=ROOT.TLine(128, Hthres.GetXaxis().GetXmin(),128, Hthres.GetXaxis().GetXmax());
lines.append(Lin2B)
Lin3B=ROOT.TLine(192, Hthres.GetXaxis().GetXmin(),192, Hthres.GetXaxis().GetXmax());
lines.append(Lin3B)
for l in lines: l.SetLineWidth(2)

can3=ROOT.TCanvas("can3","can3",800,600)
H2thres=ROOT.TH2F("H2thres","miniMalta H2thres;pixel Column; threshold [el];# pixels",16,-0.5,15.5,50,minTh,maxTh)
H2thres.Sumw2()
###iT.Draw("thres:pixX>>H2thres","thres<800 && pixY!=1 && chi2<3","goff")
if exclude_p:
   iT.Draw("thres:pixX>>H2thres","thres<1600 && chi2<3 && (pixY<32 || pixY>=48)","goff")
else: 
   iT.Draw("thres:pixX>>H2thres","thres<1600 && chi2<3","goff")
##H2thres.Rebin(2)
H2thres.Draw("COLZ")
for l in lines: l.Draw("SAMEL")
if exclude_p:
   can3.Print(folder+"Threshold2D_noP.pdf")
else: 
   can3.Print(folder+"Threshold2D.pdf")

###########################################################################################################

lines2=[]
Lin1C=ROOT.TLine( 64, Hnoise.GetXaxis().GetXmin(), 64, Hnoise.GetXaxis().GetXmax());
lines2.append(Lin1C)
Lin2C=ROOT.TLine(128, Hnoise.GetXaxis().GetXmin(),128, Hnoise.GetXaxis().GetXmax());
lines2.append(Lin2C)
Lin3C=ROOT.TLine(192, Hnoise.GetXaxis().GetXmin(),192, Hnoise.GetXaxis().GetXmax());
lines2.append(Lin3C)
for l in lines2: l.SetLineWidth(2)

can4=ROOT.TCanvas("can4","can4",800,600)
H2noise=ROOT.TH2F("H2noise","miniMalta H2noise;pixel Column; noise [el];# pixels",16,-0.5,15.5,60,0,maxNoise)
H2noise.Sumw2()
##iT.Draw("sigma:pixX>>H2noise","thres<800 && pixY!=1 && chi2<3","goff")
if exclude_p:
   iT.Draw("sigma:pixX>>H2noise","thres<1600 && chi2<3 && (pixY<32 || pixY>=48)","goff")
else: 
   iT.Draw("sigma:pixX>>H2noise","thres<1600 && chi2<3","goff")
##H2thres.Rebin(2)
H2noise.Draw("COLZ")
for l in lines2: l.Draw("SAMEL")
if exclude_p:
   can4.Print(folder+"Sigma2D_noP.pdf")
else:
   can4.Print(folder+"Sigma2D.pdf")

###########################################################################################################

lines3=[]
Lin1A=ROOT.TLine( 7.5, 0, 7.5, 64);
lines3.append(Lin1A)
Lin2A=ROOT.TLine(15.5, 0,15.5, 64);
lines3.append(Lin2A)
Lin3A=ROOT.TLine(32, 0,32, 64);
lines3.append(Lin3A)
Lin4A=ROOT.TLine( -0.5,15.5, 15.5,  15.5);
lines3.append(Lin4A);
Lin5A=ROOT.TLine( -0.5,31.5, 15.5,  31.5);
lines3.append(Lin5A);
Lin6A=ROOT.TLine( -0.5,47.5, 15.5,  47.5);
lines3.append(Lin6A);
for l in lines3: l.SetLineWidth(1)

can5=ROOT.TCanvas("can5","can5",800,600)
##H2fancy=ROOT.TH2F("H2map","H2map;pix X; pixY; threshold [el]",257,-1.5,255.5,514,-1.5,512.5)
H2fancy=ROOT.TH2F("H2map","miniMalta Threhold map;pix X; pixY; threshold [el]",16,-0.5,15.5,64,-0.5,63.5)
H2fancy.Sumw2()
if exclude_p:
   iT.Draw("pixY:pixX>>H2map","thres*(thres<1600 && chi2<3 && (pixY<32 || pixY>=48))","goff")
else: 
   iT.Draw("pixY:pixX>>H2map","thres*(thres<1600 && chi2<3 )","goff")
H2fancy.SetMaximum(maxTh)
H2fancy.SetMinimum(minTh)
H2fancy.GetZaxis().SetTitleOffset(1.5)
H2fancy.Draw("COLZ")
for l in lines3: l.Draw("SAMEL")
if exclude_p:
   can5.Print(folder+"Map2D_noP.pdf")
else:
   can5.Print(folder+"Map2D.pdf")
   

H2fancy2=ROOT.TH2F("H2map2","miniMalta Noise map;pix X; pixY; noise [el]",16,-0.5,15.5,64,-0.5,63.5)
H2fancy2.Sumw2()
if exclude_p:
   iT.Draw("pixY:pixX>>H2map2","sigma*(thres<1600 && chi2<3 && (pixY<32 || pixY>=48) )","goff")
else:
   iT.Draw("pixY:pixX>>H2map2","sigma*(thres<1600 && chi2<3)","goff")
H2fancy2.SetMaximum(maxNoise)
H2fancy2.SetMinimum(0)
H2fancy2.GetZaxis().SetTitleOffset(1.5)
H2fancy2.Draw("COLZ")
for l in lines3: l.Draw("SAMEL")
if exclude_p:
   can5.Print(folder+"Map2D_noise_noP.pdf")
else:
  can5.Print(folder+"Map2D_noise.pdf")

tmpF.Write()
tmpF.Close()

