import os
import sys
import time
import importlib
import argparse
import autoPlotting
import math

niter = 200

IDB = 120

chip_name = 'W7R1_1e15_n30C'

dir_name = 'ITHR30'


#os.system("mkdir ")
os.system("touch NoiseScansMalta/"+chip_name+"/"+dir_name+"/Mask_IDB"+str(IDB)+".txt")

for i in range(niter):
    print " "
    print "###############"
    os.system("MaltaNoiseScan -f " +chip_name +" -l" +str(IDB-1)+ " -h "+str(IDB)+ " -s 10 -a udp://ep-ade-gw-04.cern.ch:50002 -c configs/config_noiseScan_W7R1.txt  -t 1 -r 1 -n 1 -o "+dir_name+" -m NoiseScansMalta/"+chip_name+"/"+dir_name+"/Mask_IDB"+str(IDB)+".txt")
    os.system("cp NoiseScansMalta/"+chip_name+"/"+dir_name+"/Mask_IDB"+str(IDB)+".txt  NoiseScansMalta/"+chip_name+"/"+dir_name+"/Mask_IDB"+str(IDB)+"_"+str(i+1)+".txt")
    print "###############"
