import os
import sys
import time
import importlib
import argparse
import autoPlotting
import math

############################################################################

## IMON2 values:
#imon2Vs=[1.38, 1.36, 1.34, 1.32, 1.30, 1.25, 1.20, 1.15, 1.10]
#imon2Vs=[1.34, 1.32, 1.30, 1.25, 1.20, 1.15, 1.10]
#imon2Vs=[1.32,1.30,1.27,1.25,1.22,1.20,1.17,1.15,1.12,1.10,1.07,1.05,1.02,1.00]
#imon2Vs=[1.25,1.22,1.20,1.17,1.15,1.12,1.10,1.07,1.05,1.02,1.00]
imon2Vs=[1.20,1.17,1.15,1.12,1.10,1.07,1.05,1.02,1.00]
noiseInt=[]
noiseNum=[]

numberOfPixel=256*512 ##half chip

parser=argparse.ArgumentParser()
parser.add_argument('-f' ,'--folder',help='output folder',required=True)
parser.add_argument('-r' ,'--repli' ,help='number of replication',type=int,default=2)
parser.add_argument('-t' ,'--window',help='number of aquisition windows',type=int,default=1000)
parser.add_argument('-c' ,'--chip',help='chip to test',default=1)
parser.add_argument("-OA",'--onlyAnalysis',help="Only run analysis, do not take data",action="store_true")
parser.add_argument("-OP",'--onlyPlot'    ,help="Only run plotting",action="store_true")
args=parser.parse_args()

import ROOT
ROOT.gROOT.SetBatch(1)
##todo when not taking data, need to infer valueR and valurT from the fileNaming!!!!
chip= args.chip
folderName=args.folder+"/"#+str(chip)+"/"
OnlyAnalysis=(args.onlyAnalysis or args.onlyPlot)
OnlyPlot    =args.onlyPlot
valueR= args.repli

valueT= args.window
timing=valueR*valueT*0.5/1000000. ##in sec

os.system("mkdir -p "+folderName)

############################################################################

for v in imon2Vs:
    print " "
    print "###############"
    print "##  IMON2: "+str(v)
    print "###############"
    fileName="Noise__"+str(v).replace(".","p")+"__"+str(valueT)+"__"+str(chip)+".root"####+str(valueR)+".root"
    local_plots=folderName+"Plots__"+str(v).replace(".","p")+"__"+str(valueT)+"__"+str(chip)+".root"#+str(valueR)+".root"
    if not OnlyAnalysis:
        os.system( "MALTA_PSU.py -c setVoltage IMON2 "+str(v) )
        fileName="Noise__"+"C"+str(chip)+"_"+str(v).replace(".","p")+"__"+str(valueT)+".root"#+str(valueR)+".root"
        command ="python MaltaNtupleMaker_REAL.py -c %s" % str(chip) 
        command+=" -r "+str(valueR) 
        command+=" -t "+str(valueT)
        command+=" -f "+fileName
        print " "
        print command
        os.system(command)
        command2="mv  NtupleFiles/"+fileName.replace(".root","_"+str(chip)+".root")+" "+folderName
        print " " 
        print command2
        os.system(command2)
    
    if not OnlyPlot:
        print " "
        print "---Running autoPlotting"
        local_malta=folderName+fileName.replace(".root","_"+str(chip)+".root")
        autoPlotting.autoPlotting(local_malta,local_plots,False,True,True)#,True)
        print "---autoplotting done"
        
    print " "
    f=ROOT.TFile(local_plots)
    mainH=f.Get("Map2D")
    
    numberOfHits=mainH.Integral()
    totPix=0
    for x in xrange(0,516):
        for y in xrange(0,516):
            cont=mainH.GetBinContent(x,y)
            if cont!=0: totPix+=1
    noiseInt.append(numberOfHits)
    noiseNum.append(totPix)

    cPixelHitMap = ROOT.TCanvas("cPixelHitMap","PixelHitMap",900,800)
    cPixelHitMap.SetRightMargin(0.11)
    cPixelHitMap.SetLogz()
    mainH.SetMaximum(mainH.GetMaximum()*0.05)
    #mainH.GetXaxis().SetRangeUser(0,256)
    #mainH.GetYaxis().SetRangeUser(0,512)
    cPixelHitMap.SetLogz()
    mainH.Draw("COLZ") ###TEXT
    #mainH.GetXaxis().SetRangeUser(0,256)
    #mainH.GetYaxis().SetRangeUser(0,256)
    cPixelHitMap.Update()
    cPixelHitMap.Print(local_plots.replace(".root","_hitMap.pdf"))

    f.Close()
        
print " "
print "Summay"
print imon2Vs
print noiseInt
print noiseNum

baseH=ROOT.TH1D("base","base;IMON2 [V]; Chip Noise rate (KHz)",100,1.00,1.35)
baseH.SetMaximum(noiseInt[0]*2/timing/1000)
if noiseInt[-1]>0:
    baseH.SetMinimum(noiseInt[-1]/2/timing/1000)
else:
    baseH.SetMinimum(1./(2.*float(timing)*1000.))
print "Minimum is: "+str(baseH.GetMinimum())+" ... .   "+str(baseH.GetMaximum())

noiseRate=ROOT.TCanvas("NoiseRate","NoiseRate",900,800)
noiseRate.SetLogy()
baseH.Draw("AXIS")
gra=ROOT.TGraphErrors()
gra.SetLineColor(1)
gra.SetLineWidth(2)
gra.SetMarkerColor(1)
gra.SetMarkerStyle(20)
gra.SetMarkerSize(1.2)
count=0
for v in imon2Vs:
    count+=1
    gra.SetPoint(count-1,v,noiseInt[count-1]/timing/1000)
    gra.SetPointError(count-1,0.0001,math.sqrt(noiseInt[count-1])/timing/1000)
gra.Draw("PL")
noiseRate.Update()
noiseRate.Print(folderName+"C"+str(chip)+"_Summary_Rate.pdf")
os.system("evince "+folderName+"C"+str(chip)+"_Summary_Rate.pdf &")

######################################################################################
baseH2=ROOT.TH1D("base","base;IMON2 [V]; firing pixels",100,1.00,1.35)
baseH2.SetMaximum(noiseNum[0]*2)
baseH2.SetMinimum(0.5)

noisePix=ROOT.TCanvas("NoisePix","NoisePix",900,800)
noisePix.SetLogy()
baseH2.Draw("AXIS")
gra2=ROOT.TGraphErrors()
gra2.SetLineColor(1)
gra2.SetLineWidth(2)
gra2.SetMarkerColor(1)
gra2.SetMarkerStyle(20)
gra2.SetMarkerSize(1.2)
count=0
for v in imon2Vs:
    count+=1
    gra2.SetPoint(count-1,v,noiseNum[count-1])
    gra2.SetPointError(count-1,0.0001,math.sqrt(noiseNum[count-1]) )
gra2.Draw("PL")
noisePix.Update()
noisePix.Print(folderName+"C"+str(chip)+"_Summary_Pix.pdf")
os.system("evince "+folderName+"C"+str(chip)+"_Summary_Pix.pdf &")


outFile=ROOT.TFile(folderName+"C"+str(chip)+"_Summary.root","RECREATE")
outFile.WriteObject(gra ,"Noise_Rate")
outFile.WriteObject(gra2,"Noise_Pix")
outFile.Close()


print " "
