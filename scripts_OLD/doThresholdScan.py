import os
import sys
import time
import importlib
import argparse
import ROOT

thePython="/sw/atlas/LCG_87/Python/2.7.10/x86_64-slc6-gcc62-opt/bin/python "
#thePython="/afs/cern.ch/atlas/project/tdaq/inst/tdaq-common/tdaq-common-02-02-00/installed/share/bin/tdaq_python "

baseFolder=""

parser=argparse.ArgumentParser()
parser.add_argument('-f' ,'--folder',help='output folder',required=True)
parser.add_argument('-p' ,'--prefix',help='prefix for the file',required=True)
parser.add_argument("-OP",'--onlyPlot'    ,help="Only run plotting",action="store_true")
parser.add_argument('-Vm','--Vmin'  , help='firstRow',type=int,default=1300)
parser.add_argument('-Vx','--Vmax'  , help='lastRow' ,type=int,default=1700)
#arser.add_argument('-r' ,'--repli' ,help='number of replication',type=int,default=1)
#arser.add_argument('-t' ,'--window',help='number of aquisition windows',type=int,default=1000)
#arser.add_argument("-OA",'--onlyAnalysis',help="Only run analysis, do not take data",action="store_true")
#arser.add_argument("-OP",'--onlyPlot'    ,help="Only run plotting",action="store_true")
args=parser.parse_args()

if args.Vmin<900:
    print ""
    print "ERROR: Vmin cannot go lower than 900 mV .... pleare retry .... BYE"
    print ""
    sys.exit()
if args.Vmax>1750:
    print ""
    print "ERROR: Vmax cannot go higher than 1750 mV .... pleare retry .... BYE"
    print ""
    sys.exit()
if args.Vmin>args.Vmax:
    print " "
    print "You set Vmin > Vmax: "+str(args.Vmin)+">"+str(args.Vmax)+" ... please be serious ... BYE"
    sys.exit()


theCol  =-1
thePix  =-1
theIMON2=-1
imonS   =""

##make sure that the prefix contains COL and IMON2
if "COL" not in args.prefix:
    print " "
    print " PREFIX MUST CONTAIN: 'COL_x__' block .... please fix it"
    print " " 
    sys.exit()
else:
    colS=args.prefix.split("COL_")[1].split("__")[0]
    print " pulsed column: "+colS
    theCol=int(colS)

if "IMON2" not in args.prefix:
    print " "
    print " PREFIX MUST CONTAIN: '__IMON2_x__' block with 'p' as a separator  .... please fix it"
    print " " 
    sys.exit()
else:
    imonS=args.prefix.split("IMON2_")[1].split("__")[0]
    print " imon2 value: "+imonS
    theIMON2=float(imonS.split("p")[0])+float(imonS.split("p")[1])/100.
    print " imon2 final: "+str(theIMON2)

if "PIX" not in args.prefix:
    print " "
    print " PREFIX MUST CONTAIN: '__PIX_x__' block with 'p' as a separator  .... please fix it"
    print " " 
    sys.exit()
else:
    pixS=args.prefix.split("PIX_")[1].split("__")[0]
    print " pulsed pixel: "+pixS
    thePix=int(pixS)

fullOutFolder="ThScans/"+args.folder+"/IMON2_"+imonS+"/PIX_"+str(thePix)+"/"
print fullOutFolder
os.system("mkdir -p "+fullOutFolder)

if not args.onlyPlot: 
    os.system( "MALTA_PSU.py -c setVoltage IMON2  "+str(theIMON2) )

minV  = args.Vmin ##1300#1350 #1200
maxV  = args.Vmax ##1700
step  =  10
ithres=  10
nVal= int((maxV-minV)/step)
print nVal
for i in xrange(0,nVal):
    if args.onlyPlot: continue 
    value=float(minV+step*i)
    vlow="%i"%(value)
    print " "
    print "###############"
    print "## Vlow value: "+str(value)
    print "###############"
    Name="ThScan_"+args.prefix+"__"+args.folder+"__VL"+vlow+".root"
    
    ##os.system( "SetVoltage_VPulse.py -f -s -v "+str(value/1000) )
    ##os.system( "SetVoltage_VPulse.py -f -v "+str(value/1000) )
    os.system( "SetVoltage_VPulse.py -v "+str(value/1000) )
    
    ##os.system("python MaltaNtupleMaker_REAL.py --ithr "+str(ithres)+" --vlow "+vlow+" -t 2000 -r 1 -f "+Name+" --pulse True")
    os.system("python MaltaNtupleMaker_REAL.py --ithr "+str(ithres)+" --vlow "+vlow+" -t 500 -r 1 -f "+Name+" --pulse True")
    abort=False
    if i==0:
        abort=True
        ##automatic detection of failed bins
        command="python hasHitInPixel.py -c "+str(theCol)+" -r "+str(thePix)+" -f  NtupleFiles/"+Name.replace(".root","_"+str(ithres)+".root")+" | tee Debug.txt"
        os.system(command)
        probe=file("Debug.txt")
        lines=probe.readlines()
        for l in lines:
            if "target"not in l: continue 
            print l
            if "target: 0" not in l:
                abort=False
                break
        os.system("rm Debug.txt")
        
    os.system("mv NtupleFiles/"+Name.replace(".root","_"+str(ithres)+".root")+"  "+fullOutFolder)
    if abort:
        print "Could not find hits in the target pixel at the start of the scan ... ABORTING!!!"
        break

    
print " "
if not args.onlyPlot: 
    os.system("SetVoltage_VPulse.py -s -v 1.20")

#################################
##plotting
print " "
command="python MakeScanPlot.py -f ThScans/"+args.folder+" -p "+args.prefix +" -s "+str(minV)
os.system(command)
