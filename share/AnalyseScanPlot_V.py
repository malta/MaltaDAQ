import sys,os
from ROOT import *
from array import *
import math

#gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#SetAtlasStyle()
gStyle.SetOptStat(0)
gStyle.SetPalette(1)
gStyle.SetOptFit(1111)
gROOT.SetBatch(1)
#gROOT.ProcessLine(".L GetCat.C++g")

###############################################################################
###############################################################################

baseDir="ScanPlotsAnaV/"
os.system("mkdir -p "+baseDir)
curves=[]

def GetThreshold(histo, name):
    can=TCanvas( name, name, 800, 600)
    histo.SetTitle(";VPulse (V); counts;")
    histo.GetXaxis().SetRangeUser(1.35,1.6)
    histo.Draw("AXIS")
    
    plot=TGraphErrors()
    point=-1
    for bin in xrange(0,histo.GetNbinsX()+1):
        cont=histo.GetBinContent(bin)
        if cont==0: continue
        point+=1
        plot.SetPoint(point, histo.GetXaxis().GetBinCenter(bin), cont )
        plot.SetPointError(point, .001, math.sqrt(cont) )
    plot.SetMarkerSize(1.2)
    plot.SetLineWidth(3)
    plot.Draw("PE")
    
    fit=TF1("fit_"+name,
            "[0]/2*(1-TMath::Erf(1/TMath::Sqrt(2)*(x-[1])/[2]))",
            histo.GetXaxis().GetXmin(),  histo.GetXaxis().GetXmax())
    curves.append(fit)
    fit.SetParName(0,"base")
    fit.SetParName(1,"#mu")
    fit.SetParName(2,"#sigma")
    fit.SetParameter(0,1000)
    fit.SetParLimits(0,100,10000)
    fit.SetParameter(1,1.5)
    fit.SetParLimits(1,histo.GetXaxis().GetXmin(),  histo.GetXaxis().GetXmax())
    fit.SetParameter(2,10)
    fit.SetParLimits(2,2/1e3,50/1e3)
    result=plot.Fit(fit,"RE0S")
    fit.SetLineStyle(2)
    fit.SetLineColor(2)
    fit.SetLineWidth(3)
    l=TLine(fit.GetParameter(1),0,fit.GetParameter(1),histo.GetMaximum())
    l.SetLineWidth(1)
    l.Draw("SAMEC")
    fit.Draw("SAMEC")

    can.Print(baseDir+name+".pdf")
    chi2=result.Chi2()/result.Ndf()
    return [(1.780-fit.GetParameter(1))*1.43*1000,chi2]


###############################################################################
###############################################################################

#mFile=TFile("results_longNight.root")
mFile=TFile("ScanPlotsAna_64pixels/results.root")
##mFile.ls()
m_hThres =TH1F("Threshold",";Threshold (e^{-});Pixels",1000,0,1000)
m_hThres2=TH1F("Threshold",";Threshold (e^{-});Pixels",1000,0,1000) ##only good
m_hChi2  =TH1F("Chi2"     ,";Fit #chi^{2}/Ndf;Pixels",100,0,50) 

for x in xrange(0,252):
    for y in xrange(0,512):
        
        #if y>20 and y!=251 and y!=511: continue
        if x!=1: continue
        
        fullName="hits_"
        if x<10   : fullName+="0"+str(x)
        #elif x<100: fullName+="0"+str(x)
        else      : fullName+=str(x)
        fullName+="_"
        if y<10   : fullName+="0"+str(y)
        #elif y<100: fullName+="0"+str(y)
        else      : fullName+=str(y)
        
        h=mFile.Get(fullName)
        if h==None: continue
        print fullName+" has integral: "+str(h.Integral())
        if h.Integral()<100: continue
        thres=GetThreshold(h,fullName)
        print thres
        m_hThres.Fill(thres[0])
        if thres[1]<3.0:  m_hThres2.Fill(thres[0])
        m_hChi2.Fill(thres[1])

can2=TCanvas( "Summary", "Summary", 800, 600)
m_hThres.SetLineWidth(3)
m_hThres.SetLineStyle(2)
m_hThres.SetLineColor(4)
m_hThres.Draw("HIST")

m_hThres2.SetLineWidth(2)
m_hThres2.SetLineColor(2)
m_hThres2.Draw("SAMEHIST")

can2.Print(baseDir+"Summary.pdf")
    

##################################

can3=TCanvas( "Summary", "Summary", 800, 600)
m_hChi2.SetLineWidth(2)
m_hChi2.Draw("HIST")

can3.Print(baseDir+"Chi2.pdf")

sys.exit()
############################################################################
############################################################################
############################################################################
############################################################################
############################################################################
############################################################################


