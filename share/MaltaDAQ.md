# MaltaDAQ {#PackageMaltaDAQ}

MaltaDAQ is one of the core packages of MaltaSW.

## Applications

- MaltaConfig: Application to configure one or more ReadoutModule planes through a configuration file.
- MaltaMultiDAQ: Application to start the data acquisition on one or more ReadoutModule planes given a configuration file.

## Libraries

- MaltaDAQ: Library containing core classes for the DAQ

## MaltaDAQ library

- ConfigParser: Reader of configuration files into ReadoutConfig objects describing planes
- ReadoutConfig: Configuration representation of a plane
- ModuleLoader: Tool to load a ReadoutModule exdended classes at runtime
- ReadoutModule: Base class describing a plane for MaltaMultiDAQ

