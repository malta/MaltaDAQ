#!/usr/bin/env python
import ROOT
import argparse
import array
import glob
import argparse

import AtlasStyle



def plotThSub(baseName,region):
 r=  region

 h = []#ROOT.TH1F()
 inFile = []
 title= baseName.split("/")[-3].split("_")[1].split("V")[1]
 print title
 thVSub= ROOT.TGraphErrors("%s Threshold"%title,"%s Threshold"%title)
 thVSub.SetName(title) 
 nVSub= ROOT.TGraphErrors("%s Noise"%title,"%s Noise"%title)

 #outdir = args.file[0].split("/")[-4]
 files = glob.glob(baseName)
 print files
 count = -1
 for f in files:
  count+=1
  #open file
  print f 
  sub = f.split("/")[-3].split("_")[1].split("V")[0]
  print "the sub is ",sub
  sub = float(sub)
  inFile = ROOT.TFile(f)
  h = inFile.Get("thres_1D")
  mean = h.GetMean()
  hN = inFile.Get("noise_1D")
  meanN = hN.GetMean()

  print "the mean is ",mean
  if h.GetEntries()==0:
   count-=1
   continue
  std = h.GetRMS()/(pow(h.GetEntries(),0.5)+.000001)
  stdN = hN.GetRMS()/(pow(hN.GetEntries(),0.5)+.000001)
  thVSub.SetPoint(count, sub, mean)
  thVSub.SetPointError(count, 0, std)
  nVSub.SetPoint(count, sub, meanN)
  nVSub.SetPointError(count, 0, stdN)
  #thVSub.Draw("PL")
  #thVSub.Print()


 #thVSub.Draw("APL")

 return [thVSub, nVSub]
pass
###################################################


def plotThVRESETD(baseName,region):
 r=  region

 h = []#ROOT.TH1F()
 inFile = []
 title= baseName.split("/")[-3].split("_")[1].split("V")[1]
 print title
 thVVRESETD= ROOT.TGraphErrors("%s"%title,"%s"%title)
 thVVRESETD.SetName(title) 
 nVVRESETD= ROOT.TGraphErrors()

 #outdir = args.file[0].split("/")[-4]
 files = glob.glob(baseName)
 files.sort()
 print files
 count = -1
 for f in files:
  count+=1
  #open file
  print f 
  VRESETD = f.split("/")[-3].split("_")[-1]
  print "the VRESETD is ",VRESETD
  VRESETD = float(VRESETD)
  inFile = ROOT.TFile(f)
  h = inFile.Get("thres_1D")
  mean = h.GetMean()
  
  hN = inFile.Get("noise_1D")
  meanN = hN.GetMean()
  print "the mean is ",mean
  if h.GetEntries()==0:
   count-=1
   continue
  std = h.GetRMS()/(pow(h.GetEntries(),0.5)+.000001)  
  stdN = hN.GetRMS()/(pow(hN.GetEntries(),0.5)+.000001)
  thVVRESETD.SetPoint(count, VRESETD, mean)
  thVVRESETD.SetPointError(count, 0, std)
  nVVRESETD.SetPoint(count, VRESETD, meanN)
  nVVRESETD.SetPointError(count, 0, stdN)
  #thVSub.Draw("PL")
  #thVSub.Print()


 #thVSub.Draw("APL")

 return [thVVRESETD, nVVRESETD]
pass

################################################


parser=argparse.ArgumentParser()
parser.add_argument('-c','--chip',help="name of the chip",type=str,required=True)
parser.add_argument('-b','--bias',help="bias voltage for VRESET scans",type=int,default=6)
parser.add_argument('-r','--VRESET',help="use for VRESET scan",action='store_true')
parser.add_argument('-o','--output',help="name for output",type=str,default ="")
args=parser.parse_args()

chip =args.chip
bias = args.bias
output = args.output
##############################################

colors = [ROOT.kOrange,ROOT.kRed+2, ROOT.kRed, ROOT.kBlue +2, ROOT.kAzure, ROOT.kCyan+2, ROOT.kGreen+2 ]

can  = ROOT.TCanvas("can", "Threshold for Regions of %s" %chip, 800, 600)

base = ROOT.TH1F("base","Threshold for Regions of %s, SUB = %i V;Threshold [e];Entries" %(chip,bias), 100, 5, 20)
base.SetMaximum(1000)
base.Draw("AXES")

if args.VRESET:
	mTh = ROOT.TMultiGraph("th","Threshold for %s, SUB = %i V;VRESET_D;Threshold [e]" %(chip,bias))
	mN = ROOT.TMultiGraph("th","Noise for %s, SUB = %i V;VRESET_D;Noise [e]" %(chip,bias))	
	nRegions = 3
else:
	mTh = ROOT.TMultiGraph("th","Threshold for %s;SUB [V];Threshold [e]" %chip)
	mN = ROOT.TMultiGraph("th","Noise for %s;SUB [V];Noise [e]" %chip)	
	nRegions = 7




for p in range (0,nRegions):
  print p
  if args.VRESET:
    g = plotThVRESETD("../ThScansMalta/%s/Glue_%iV_R%i_VRED_*/analysis/plots.root"%(chip,bias,p),p)
  else:
    g = plotThSub("../ThScansMalta/%s/Glue_*V_R%i/analysis/plots.root"%(chip,p),p)

  g[0].SetTitle("Region %i" %p)
  g[0].SetLineColor(colors[p])
  mTh.Add(g[0])
  
  g[1].SetTitle("Region %i Noise" %p)
  g[1].SetLineColor(colors[p])
  mN.Add(g[1])


mTh.Draw("APL")
can.BuildLegend(0.3, 0.52, 0.5, 0.87)
#baseName = "../ThScansMalta/W11R11Cz/Glue_*V_R6/analysis/plots.root"
outDir = "/home/sbmuser/MaltaSW/MaltaDAQ/ThScansMalta/"+chip
can.Update()
can.Print("%s/%sThSub%s.pdf"  %(outDir, chip, output))
can.Print("%s/%sThSub%s.png"  %(outDir, chip, output))
can.Print("%s/%sThVSub%s.C"  %(outDir, chip, output))

canN  = ROOT.TCanvas("canN", "Noise for Regions of %s" %chip, 800, 600)
mN.Draw("APL")
canN.BuildLegend(0.2, 0.5, 0.4, 0.75)
outDir = "/home/sbmuser/MaltaSW/MaltaDAQ/ThScansMalta/"+chip
canN.Update()
canN.Print("%s/%sNoiseSub%s.pdf" %(outDir, chip, output))
canN.Print("%s/%sNoiseSub%s.png" %(outDir, chip, output))
canN.Print("%s/%sNoiseVSub%s.C" %(outDir, chip, output))

g[1].Delete()
g[0].Delete()
mTh.Delete()
mN.Delete()

raw_input("Press any key to continue")
