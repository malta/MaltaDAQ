import sys,os
import math
import argparse
import ROOT

import AtlasStyle
from prettytable import PrettyTable

parser=argparse.ArgumentParser()
parser.add_argument('-i','--inputFile',help="input file (results.root)",type=str,required=True)
parser.add_argument('-o','--outDir',help="output directory",type=str,default="")
parser.add_argument('-x','--x',help="from to [] in x",nargs='+',type=int,required=True)
parser.add_argument('-y','--y',help="from to [] in y",nargs='+',type=int,required=True)
parser.add_argument('--batch',help="run in batch mode",action='store_true')
parser.add_argument('-c','--chi2',help="upper value for chi2",type=float,default=3.0)
parser.add_argument('-n','--noise',help="upper value for noise",type=float,default=3.0)
parser.add_argument('-cap','--hitCap',help="upper value for hits at a given VPulse",type=int,default=10**9)
args=parser.parse_args()

#AtlasStyle.SetAtlasStyle()

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(1)
ROOT.gStyle.SetPadRightMargin(0.15)

outDir=args.outDir
if outDir[-1] != "/": outDir = outDir+"/"
outDir=outDir+"ThresholdScanPlots/"
if not os.path.exists(outDir): os.makedirs(outDir)
if not os.path.exists(outDir+"Scurves/"): os.makedirs(outDir+"Scurves/")

curves=[]

def getall(d, basepath="/"):
    for key in d.GetListOfKeys():
        kname = key.GetName()
        if key.IsFolder():
            for i in getall(d.Get(kname), basepath+kname+"/"):
                yield i
                pass
            pass
        else: yield basepath+kname, d.Get(kname)
        pass
    pass

f = ROOT.TFile(args.inputFile)

hChi2={}
hThresV={}
hThres={}
hNoiseV={}
hNoise={}

hEntriesMap={}
hThresMap={}
hNoiseMap={}

hCat1Map={}
hCat2Map={}

cScurves={}
hScurves={}
gScurves={}
hScurvesMap={}

mgScurves={}

counter={}

cScurves={}
cChi2={}
cThresV={}
cThres={}
cNoiseV={}
cNoise={}
cScurvesMap={}
cThresMap={}
cNoiseMap={}
cEntriesMap={}
hYmax={}

cuts = ["none","cap","chi2","noise"]
for cut in cuts:
    counter[cut]   = 0
    hYmax[cut]=0

    cScurves[cut]  = {}
    hScurves[cut]  = {}
    gScurves[cut]  = {}
    mgScurves[cut] = ROOT.TMultiGraph()

    #1D Histograms
    hChi2[cut]     = ROOT.TH1F("Chi2%s"        %cut,";Fit #chi^{2}/Ndf;Pixels",100,0,50)
    hThresV[cut]   = ROOT.TH1F("hThresV%s"     %cut,";Threshold [e^{-}];Pixels",100,1.35,1.60)
    hThres[cut]    = ROOT.TH1F("hThres%s"      %cut,";Threshold [V];Pixels",1000,0,1000)
    hNoiseV[cut]   = ROOT.TH1F("hNoiseV%s"     %cut,";Noise [V];Pixels",100,0,10)
    hNoise[cut]    = ROOT.TH1F("hNoise%s"      %cut,";Noise [V];Pixels",100,0,10)
    
    #2D Histograms
    hScurvesMap[cut] = ROOT.TH2F("hScurveMap%s"     %cut,"hScurveMap",25,1.35,1.60,1000,0,10000)
    hEntriesMap[cut] = ROOT.TH2I("hEntriesMap%s"%cut,";Columns;Rows;",512,0,512,512,0,512)
    hThresMap[cut]   = ROOT.TH2I("hThresMap%s"      %cut,";Columns;Rows;",512,0,512,512,0,512)
    hNoiseMap[cut]   = ROOT.TH2I("hNoiseMap%s"      %cut,";Columns;Rows;",512,0,512,512,0,512)
    hCat1Map[cut]    = ROOT.TH2I("hCat1Map%s"       %cut,";Columns;Rows;",512,0,512,512,0,512)
    hCat2Map[cut]    = ROOT.TH2I("hCat2Map%s"       %cut,";Columns;Rows;",512,0,512,512,0,512)
    pass

hDataPoints = ROOT.TH1I("hDataPoints",";Number of Data Points;Frequency",40,0,40)
hCatDist    = ROOT.TH2I("hCatDist"   ,";Undershoots;Overshoots;",10,0,10,10,0,10)

xmin=args.x[0]
xmax=args.x[1]
ymin=args.y[0]
ymax=args.y[1]

countAll=0
countGood=0
countColor=0
cat1counter=0
cat2counter=0

curves=[]
fit={}
test=[]
noiselst = []

for histName, hist in getall(f):
    if hist.ClassName()=="TH1F" and "hits" in histName:
        countColor+=1
        if countColor>40:countColor=0
        x = int(histName.split("_")[1])
        y = int(histName.split("_")[2])
        if y>args.y[1] and y!=251 and y!=511: continue
        if histName==None: continue
        if hist.Integral()<100: continue

        entryPoints = {}
        for i in xrange(hist.GetNbinsX()):
            if hist.GetBinContent(i) != 0:
                entryPoints[i] = []
                entryPoints[i].append(hist.GetBinCenter(i))
                entryPoints[i].append(hist.GetBinContent(i))
                pass
            pass

        #Get Histograms from ROOT file.
        cScurves["none"][hist]=ROOT.TCanvas( histName, histName, 800, 600)
        hScurves["none"][hist]=hist
        hScurves["none"][hist].SetTitle("Pixel %s %s;VPulse [V]; counts"%(x,y))
        hScurves["none"][hist].GetXaxis().SetRangeUser(1.30,1.6)
        hScurves["none"][hist].Draw("AXIS")

        #Build TGraph using Histogram Data Points
        gScurves["none"][hist]=ROOT.TGraphErrors()
        point=-1
        for bin in xrange(0,hScurves["none"][hist].GetNbinsX()+1):
            cont=hScurves["none"][hist].GetBinContent(bin)
            if cont==0: continue
            point+=1
            gScurves["none"][hist].SetPoint(point,hScurves["none"][hist].GetXaxis().GetBinCenter(bin),cont)
            gScurves["none"][hist].SetPointError(point, .001, math.sqrt(cont) )
        gScurves["none"][hist].SetMarkerSize(1.2)
        gScurves["none"][hist].SetLineWidth(1)
        gScurves["none"][hist].SetLineStyle(1)
        gScurves["none"][hist].Draw("PE")
        ttPixel=ROOT.TLatex()
        ttPixel.DrawLatexNDC(0.448622,0.95993,"Pixel %s,%s"%(x,y))

        #Apply Sigmoid Fit on TGraph
        fit[hist]=ROOT.TF1("fit_"+histName,"[0]/2*(1-TMath::Erf(1/TMath::Sqrt(2)*(x-[1])/[2]))",hScurves["none"][hist].GetXaxis().GetXmin(), hScurves["none"][hist].GetXaxis().GetXmax())
        curves.append(fit)
        fit[hist].SetParName(0,"base")
        fit[hist].SetParName(1,"#mu")
        fit[hist].SetParName(2,"#sigma")
        fit[hist].SetParameter(0,1000)
        fit[hist].SetParLimits(0,100,10000)
        fit[hist].SetParameter(1,1.5)
        fit[hist].SetParLimits(1,hScurves["none"][hist].GetXaxis().GetXmin(), hScurves["none"][hist].GetXaxis().GetXmax())
        fit[hist].SetParameter(2,10)
        fit[hist].SetParLimits(2,2/1e3,50/1e3)
        result=gScurves["none"][hist].Fit(fit[hist],"QRE0S")
        fit[hist].SetLineStyle(2)
        fit[hist].SetLineColor(2)
        fit[hist].SetLineWidth(2)
        l=ROOT.TLine(fit[hist].GetParameter(1),0,fit[hist].GetParameter(1),hScurves["none"][hist].GetMaximum())
        l.SetLineWidth(1)
        l.Draw("SAMEC")
        fit[hist].Draw("SAMEC")
        chi2=100
        if result.Ndf()!=0: chi2=result.Chi2()/result.Ndf()
        if chi2<args.chi2: cScurves["none"][hist].SaveAs(outDir+"Scurves/Selected_%s.pdf"%histName[1:])
        if chi2>=args.chi2: cScurves["none"][hist].SaveAs(outDir+"Scurves/%s.pdf"%histName[1:])
        gScurves["none"][hist].SetLineColor(countColor)
        if countColor==0 or countColor == 10 or countColor == 19: gScurves["none"][hist].SetLineColor(9)
        gScurves["none"][hist].Draw("hist")

        #Get Fit Parameters
        thres   = fit[hist].GetParameter(1)
        eThres  = (1.780-fit[hist].GetParameter(1))*1.43*1000
        noise   = fit[hist].GetParameter(2)*1000 #Adding a factor of 1000 due to very small noise values causing display problems.
        eNoise   = (noise)*1.43
        noiselst.append(noise)
        entries = int(hist.GetEntries())

        #Populate Histograms
        hDataPoints.Fill(len(entryPoints))
        for cut in cuts:
            if "none" in cut:
                mgScurves[cut].Add(gScurves["none"][hist])
                hChi2[cut].Fill(chi2)
                hThresV[cut].Fill(thres)
                hThres[cut].Fill(eThres)
                hNoiseV[cut].Fill(noise)
                hNoise[cut].Fill(eNoise)
                hThresMap[cut].Fill(x,y,eThres)
                hNoiseMap[cut].Fill(x,y,eNoise)
                hEntriesMap[cut].Fill(x,y,entries)
                if max(gScurves["none"][hist].GetY())>hYmax[cut]:hYmax[cut]=max(gScurves["none"][hist].GetY())
                for point in entryPoints: hScurvesMap[cut].Fill(entryPoints[point][0],entryPoints[point][1])
                counter[cut]+=1
                pass

            if "cap" in cut:
                if (max(gScurves["none"][hist].GetY())<args.hitCap):
                    mgScurves[cut].Add(gScurves["none"][hist])
                    hChi2[cut].Fill(chi2)
                    hThresV[cut].Fill(thres)
                    hThres[cut].Fill(eThres)
                    hNoiseV[cut].Fill(noise)
                    hNoise[cut].Fill(eNoise)
                    hThresMap[cut].Fill(x,y,eThres)
                    hNoiseMap[cut].Fill(x,y,eNoise)
                    hEntriesMap[cut].Fill(x,y,entries)
                    if max(gScurves["none"][hist].GetY())>hYmax[cut]:hYmax[cut]=max(gScurves["none"][hist].GetY())
                    for point in entryPoints: hScurvesMap[cut].Fill(entryPoints[point][0],entryPoints[point][1])
                    counter[cut]+=1
                    pass
                pass

            if "chi2" in cut:
                if (max(gScurves["none"][hist].GetY())<args.hitCap) and (chi2<args.chi2):
                    mgScurves[cut].Add(gScurves["none"][hist])
                    hChi2[cut].Fill(chi2)
                    hThresV[cut].Fill(thres)
                    hThres[cut].Fill(eThres)
                    hNoiseV[cut].Fill(noise)
                    hNoise[cut].Fill(eNoise)
                    hThresMap[cut].Fill(x,y,eThres)
                    hNoiseMap[cut].Fill(x,y,eNoise)
                    hEntriesMap[cut].Fill(x,y,entries)
                    if max(gScurves["none"][hist].GetY())>hYmax[cut]:hYmax[cut]=max(gScurves["none"][hist].GetY())
                    for point in entryPoints: hScurvesMap[cut].Fill(entryPoints[point][0],entryPoints[point][1])
                    counter[cut]+=1
                    pass
                pass

            if "noise" in cut:
                if (max(gScurves["none"][hist].GetY())<args.hitCap) and (chi2<args.chi2) and (eNoise<args.noise):
                    mgScurves[cut].Add(gScurves["none"][hist])
                    hChi2[cut].Fill(chi2)
                    hThresV[cut].Fill(thres)
                    hThres[cut].Fill(eThres)
                    hNoiseV[cut].Fill(noise)
                    hNoise[cut].Fill(eNoise)
                    hThresMap[cut].Fill(x,y,eThres)
                    hNoiseMap[cut].Fill(x,y,eNoise)
                    hEntriesMap[cut].Fill(x,y,entries)
                    if max(gScurves["none"][hist].GetY())>hYmax[cut]:hYmax[cut]=max(gScurves["none"][hist].GetY())
                    for point in entryPoints: hScurvesMap[cut].Fill(entryPoints[point][0],entryPoints[point][1])
                    counter[cut]+=1
                    
                    #Category Identification
                    first6bins=[]
                    first6binsCounter=0
                    preThreslst=[]
                    preThresCounter=0
                    preThresDataPoint=0
                    cat2Check=False
                    cat1Check=True
                    cat1Checklst=[]
                    postThreslst=[]
                    for point in sorted(entryPoints):
                        if point<hist.FindBin(thres):
                            if preThresDataPoint<point:
                                preThresDataPoint=point
                                preThresCounter+=1
                                pass
                            preThreslst.append(entryPoints[point][1])
                            if first6binsCounter<6: first6bins.append(entryPoints[point][1])
                            pass
                        first6binsCounter+=1
                        if point>hist.FindBin(thres): postThreslst.append(entryPoints[point][1])
                        pass
                    for m in xrange(len(postThreslst)):
                        if m<(len(postThreslst)-1):
                            if postThreslst[m]<postThreslst[m+1]:cat1Checklst.append(True)
                            pass
                        pass
                    for n in cat1Checklst:
                        if n == False: cat1Check=False
                    if len(preThreslst)!=0:preThreslst.pop(preThresCounter-1)

                    mean6=0
                    rms6=0
                    sigma6=0
                    for k in first6bins:
                        rms6+=k*k/len(first6bins)
                        mean6+=k/(len(first6bins)*1.)
                    for l in first6bins:
                        sigma6+=(l-mean6)**2/len(first6bins)
                    sigma6=math.sqrt(sigma6)

                    overshoots=0
                    overshootslst=[]
                    undershoots=0
                    undershootslst=[]
                    sigmaRange=10
                    if sigma6<5:
                        for point in sorted(entryPoints):
                            if point<preThresDataPoint:
                                if (entryPoints[point][1]-mean6)>sigmaRange:
                                    overshootslst.append(point)
                                    if point-2 not in overshootslst:
                                        print "overshoot: ",point,entryPoints[point]
                                        overshoots+=1
                                if (entryPoints[point][1]-mean6)<-sigmaRange:
                                    undershootslst.append(point)
                                    if point-2 not in undershootslst:
                                        print "undershoot: ",point,entryPoints[point]
                                        undershoots+=1
                    if overshoots !=0 or undershoots !=0: print overshoots,undershoots
                    hCatDist.Fill(undershoots,overshoots)
                    pass
                pass
            pass
        pass
    pass

if not args.batch:ROOT.gROOT.SetBatch(0)

#Print Histograms
cDataPoints=ROOT.TCanvas("Data Points","Data Points",800,600)
hDataPoints.Draw("SAME")
hDataPoints.GetXaxis().SetRangeUser(0,40)
cDataPoints.Update()
cDataPoints.SaveAs(outDir+"cDataPoints.pdf")

cCatDist=ROOT.TCanvas("Distribution of Over/Under-shoots","Distribution of Over/Under-shoots",800,600)
hCatDist
hCatDist.SetTitle(";Undershoots;Overshoots;Entries")
hCatDist.Draw("COLZtext")
hCatDist.SetName("hCatDist%s"%cut)
cCatDist.Update()
cCatDist.SaveAs(outDir+"cCatDist%s.pdf"%cut)

for cut in cuts:
    cScurves[cut]=ROOT.TCanvas("Scurves %s"%cut,"Scurves %s"%cut,800,600)
    mgScurves[cut].SetTitle(";VPulse [V];Hit Count")
    mgScurves[cut].Draw("AL")
    mgScurves[cut].SetName("mgScurves%s"%cut)
    cScurves[cut].Update()
    cScurves[cut].SaveAs(outDir+"cScurves%s.pdf"%cut)
    
    cChi2[cut]=ROOT.TCanvas("Chi2 %s"%cut,"Chi2 %s"%cut,800,600)
    hChi2[cut].SetLineWidth(2)
    hChi2[cut].Draw("HIST")
    cChi2[cut].Update()
    cChi2[cut].SaveAs(outDir+"cChi2%s.pdf"%cut)
    
    cThresV[cut]=ROOT.TCanvas("ThresV %s"%cut,"ThresV %s"%cut,800,600)
    hThresV[cut].SetTitle(";VPulse [V];Entries")
    hThresV[cut].Draw("HIST")
    hThresV[cut].SetName("hThresV%s"%cut)
    cThresV[cut].Update()
    cThresV[cut].SaveAs(outDir+"cThresV%s.pdf"%cut)

    cThres[cut]=ROOT.TCanvas("Thres %s"%cut,"Thres %s"%cut,800,600)
    hThres[cut].SetTitle(";Electrons;Entries")
    hThres[cut].Draw("HIST")
    hThres[cut].SetName("hThres%s"%cut)
    cThres[cut].Update()
    cThres[cut].SaveAs(outDir+"cThres%s.pdf"%cut)
    
    cNoiseV[cut]=ROOT.TCanvas("NoiseV %s"%cut,"NoiseV %s"%cut,800,600)
    hNoiseV[cut].SetTitle(";VPulse [V];Entries")
    hNoiseV[cut].Draw("HIST")
    hNoiseV[cut].SetName("hNoiseV%s"%cut)
    cNoiseV[cut].Update()
    cNoiseV[cut].SaveAs(outDir+"cNoiseV%s.pdf"%cut)
   
    cNoise[cut]=ROOT.TCanvas("Noise %s"%cut,"Noise %s"%cut,800,600)
    hNoise[cut].SetTitle(";ENC;Entries")
    hNoise[cut].Draw("HIST")
    hNoise[cut].SetName("hNoise%s"%cut)
    cNoise[cut].Update()
    cNoise[cut].SaveAs(outDir+"cNoise%s.pdf"%cut)
   
    cScurvesMap[cut]=ROOT.TCanvas("ScurvesMap %s"%cut,"ScurvesMap %s"%cut,800,600)
    hScurvesMap[cut].SetTitle(";VPulse [V];Hit Count;Entries")
    hScurvesMap[cut].Draw("COLZ")
    hScurvesMap[cut].GetYaxis().SetRangeUser(0,hYmax[cut])
    hScurvesMap[cut].SetName("hScurvesMap%s"%cut)
    cScurvesMap[cut].Update()
    cScurvesMap[cut].SaveAs(outDir+"cScurvesMap%s.pdf"%cut)

    cThresMap[cut]=ROOT.TCanvas("ThresMap %s"%cut,"ThresMap %s"%cut,800,600)
    hThresMap[cut].SetTitle(";Columns;Rows;Threshold (e)")
    hThresMap[cut].Draw("COLZ")
    hThresMap[cut].SetName("hThresMap%s"%cut)
    cThresMap[cut].Update()
    cThresMap[cut].SaveAs(outDir+"cThresMap%s.pdf"%cut)
    
    cNoiseMap[cut]=ROOT.TCanvas("NoiseMap %s"%cut,"NoiseMap %s"%cut,800,600)
    hNoiseMap[cut].SetTitle(";Columns;Rows;Noise (e)")
    hNoiseMap[cut].Draw("COLZ")
    hNoiseMap[cut].SetName("hNoiseMap%s"%cut)
    cNoiseMap[cut].Update()
    cNoiseMap[cut].SaveAs(outDir+"cNoiseMap%s.pdf"%cut)
    
    cEntriesMap[cut]=ROOT.TCanvas("EntriesMap %s"%cut,"EntriesMap %s"%cut,800,600)
    hEntriesMap[cut].SetTitle(";Columns;Rows;Entries")
    hEntriesMap[cut].Draw("COLZ")
    hEntriesMap[cut].SetName("hEntriesMap%s"%cut)
    cEntriesMap[cut].Update()
    cEntriesMap[cut].SaveAs(outDir+"cEntriesMap%s.pdf"%cut)
    pass

#Pretty Plots
cChi2=ROOT.TCanvas("Chi2 Comparison","Chi2 Comparison",800,600)
lgChi2=ROOT.TLegend(0.723058,0.780105,0.85589,0.900524)
lgChi2.SetFillStyle(0)
ttChi2 = ROOT.TLatex()

cThresV=ROOT.TCanvas("ThresV Comparison","ThresV Comparison",800,600)
lgThresV=ROOT.TLegend(0.20802,0.780105,0.756892-0.414787,0.900524)
lgThresV.SetFillStyle(0)
ttThresV = ROOT.TLatex()

cThres=ROOT.TCanvas("Thres Comparison","Thres Comparison",800,600)
lgThres=ROOT.TLegend(0.622807,0.780105,0.756892,0.900524)
lgThres.SetFillStyle(0)
ttThres = ROOT.TLatex()

cNoiseV=ROOT.TCanvas("NoiseV Comparison","NoiseV Comparison",800,600)
lgNoiseV=ROOT.TLegend(0.622807,0.780105,0.756892,0.900524)
lgNoiseV.SetFillStyle(0)
ttNoiseV = ROOT.TLatex()

cNoise=ROOT.TCanvas("Noise Comparison","Noise Comparison",800,600)
lgNoise=ROOT.TLegend(0.622807,0.780105,0.756892,0.900524)
lgNoise.SetFillStyle(0)
ttNoise = ROOT.TLatex()

color=1
for cut in cuts:
    cChi2.cd()
    hChi2[cut].SetLineColor(color)
    hChi2[cut].Draw("SAME")
    lgChi2.AddEntry(hChi2[cut],"%s"%cut,"l")
    lgChi2.Draw()

    cThresV.cd()
    hThresV[cut].SetLineColor(color)
    hThresV[cut].Draw("SAME")
    lgThresV.AddEntry(hThresV[cut],"%s"%cut,"l")
    lgThresV.Draw()
    ttThresV.SetTextColor(color)
    if color==1:ttThresV.DrawLatexNDC(0.713033-0.414787,0.905759,"#scale[0.5]{Mean  RMS}")
    ttThresV.DrawLatexNDC(0.704261-0.414787,0.877792-color/33.+1/33.,"#scale[0.5]{%06.3f  %06.3f}"%(hThresV[cut].GetMean(),hThresV[cut].GetRMS()))

    cThres.cd()
    hThres[cut].SetLineColor(color)
    hThres[cut].Draw("SAME")
    lgThres.AddEntry(hThres[cut],"%s"%cut,"l")
    lgThres.Draw()
    ttThres.SetTextColor(color)
    if color==1:ttThres.DrawLatexNDC(0.721805,0.905759,"#scale[0.5]{Mean   RMS}")
    ttThres.DrawLatexNDC(0.704261,0.877792-color/33.+1/33.,"#scale[0.5]{%06.3f  %06.3f}"%(hThres[cut].GetMean(),hThres[cut].GetRMS()))

    cNoiseV.cd()
    hNoiseV[cut].SetLineColor(color)
    hNoiseV[cut].Draw("SAME")
    lgNoiseV.AddEntry(hNoiseV[cut],"%s"%cut,"l")
    lgNoiseV.Draw()
    ttNoiseV.SetTextColor(color)
    if color==1:ttNoiseV.DrawLatexNDC(0.713033,0.905759,"#scale[0.5]{Mean  RMS}")
    ttNoiseV.DrawLatexNDC(0.704261,0.877792-color/33.+1/33.,"#scale[0.5]{%06.3f  %06.3f}"%(hNoiseV[cut].GetMean(),hNoiseV[cut].GetRMS()))

    cNoise.cd()
    hNoise[cut].SetLineColor(color)
    hNoise[cut].Draw("SAME")
    lgNoise.AddEntry(hNoise[cut],"%s"%cut,"l")
    lgNoise.Draw()
    ttNoise.SetTextColor(color)
    if color==1:ttNoise.DrawLatexNDC(0.713033,0.905759,"#scale[0.5]{Mean  RMS}")
    ttNoise.DrawLatexNDC(0.704261,0.877792-color/33.+1/33.,"#scale[0.5]{%06.3f  %06.3f}"%(hNoise[cut].GetMean(),hNoise[cut].GetRMS()))

    color+=1
    pass

cChi2.Update()
cChi2.SaveAs(outDir+"cChi2.pdf")
cThresV.Update()
cThresV.SaveAs(outDir+"cThresV.pdf")
cThres.Update()
cThres.SaveAs(outDir+"cThres.pdf")
cNoiseV.Update()
cNoiseV.SaveAs(outDir+"cNoiseV.pdf")
cNoise.Update()
cNoise.SaveAs(outDir+"cNoise.pdf")

#Generate Cut-Flow Stats
print "\n\n##################################################################"
print "######################## Cut-Flow Summary ########################"
print "##################################################################"
cutflow = PrettyTable(['Cuts', 'Total Pixels','% of Pixels','Threshold (e)','Noise (e)'])
for cut in cuts: cutflow.add_row(['%s'%cut, counter[cut], "%.3f"%((counter[cut]*100.)/counter["none"]), "%.3f"%hThres[cut].GetMean(), "%.3f"%hThres[cut].GetRMS()])
print cutflow

#Save Plots in ROOT file
fw=ROOT.TFile.Open(outDir+"plots.root","RECREATE")
hDataPoints.Write()
cChi2.Write()
cThresV.Write()
cThres.Write()
cNoiseV.Write()
cNoise.Write()
hCatDist.Write()
for cut in cuts:
    mgScurves[cut].Write()
    hChi2[cut].Write()    
    hThresV[cut].Write()
    hThres[cut].Write()   
    hNoiseV[cut].Write()  
    hNoise[cut].Write()   
    hScurvesMap[cut].Write()
    hEntriesMap[cut].Write()
    hThresMap[cut].Write() 
    hNoiseMap[cut].Write()
    pass
fw.Close()

if not args.batch:raw_input("")

print "\nHave a nice day"






'''
#if y>=args.y[0] and y<args.y[1] and x>=args.x[0] and x<args.x[1]:
hHitMapGoodZoom.Fill(x,y,entries)
hThresMapGoodZoom.Fill(x,y,eThres)
hNoiseMapGoodZoom.Fill(x,y,noise)

cHitMapGoodZoom = ROOT.TCanvas("Hit Map GoodZoom","Hit Map GoodZoom",800,600)
hHitMapGoodZoom.SetTitle(";Columns;Rows;Hit Count")
hHitMapGoodZoom.Draw("COLZ")
hHitMapGoodZoom.GetXaxis().SetRange(xmin+1,xmax)
hHitMapGoodZoom.GetYaxis().SetRange(ymin+1,ymax)
hHitMapGoodZoom.Rebin2D(1,1)
if xmax<=10: hHitMapGoodZoom.GetXaxis().SetNdivisions(xmax)
if ymax<=10: hHitMapGoodZoom.GetYaxis().SetNdivisions(ymax)
if xmax>10: hHitMapGoodZoom.GetXaxis().SetNdivisions(int(xmax*1./4))
if ymax>10: hHitMapGoodZoom.GetYaxis().SetNdivisions(int(ymax*1./2))
hHitMapGoodZoom.GetYaxis().CenterLabels()
hHitMapGoodZoom.GetXaxis().CenterLabels()
cHitMapGoodZoom.Update()
cHitMapGoodZoom.SaveAs(outDir+"HitMapGoodZoom.pdf")

cThresMapAll = ROOT.TCanvas("Threshold Map All","Threshold Map All",800,600)
hThresMapAll.SetTitle(";Columns;Rows;Threshold (e)")
hThresMapAll.Draw("COLZ")
hThresMapAll.Rebin2D(1,1)
hThresMapAll.GetXaxis().SetNdivisions(512)
hThresMapAll.GetYaxis().SetNdivisions(512)
hThresMapAll.GetYaxis().CenterLabels()
hThresMapAll.GetXaxis().CenterLabels()
cThresMapAll.Update()
cThresMapAll.SaveAs(outDir+"ThresMapAll.pdf")

        ###########Category 1 Check

        #if chi2<args.chi2 and (overshoots != 0 or undershoots != 0):raw_input("")
        if (sigma > 10):
            cat1Check = False
            cat2Check = False
        if (chi2<args.chi2) and (sigma <= 10) and cat1Check == True and ((hist.GetBinContent(preThresDataPoint)-mean)>50): cat2Check = True
        if (chi2<args.chi2) and cat2Check == False and sigma < 10 and cat1Check == True: cat1Check = True
        else: cat1Check=False
        #print "cat1Check:",cat1Check,"cat2Check: ",cat2Check,"sigma: ", sigma,"preThresDataPoint-mean: ",(hist.GetBinContent(preThresDataPoint)-mean)
        #if cat2Check == True:raw_input("")
        if cat1Check == True:
            hCat1Map.Fill(x,y,eThres)
            cat1counter+=1
            pass
        if cat2Check == True:
            hCat2Map.Fill(x,y,eThres)
            cat2counter+=1
            pass
        ##############################
'''


