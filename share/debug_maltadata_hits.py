#!/usr/bin/env python

import os
import sys
import argparse
import PyMaltaData

parser=argparse.ArgumentParser()
parser.add_argument('--hits', help='Hit pairs',type=int,nargs='*',required=True)
parser.add_argument('--icps', help='Intercept pairs',type=int,nargs='*',required=True)
parser.add_argument('-v','--verbose',help='Enable verbose mode',action='store_true')
args=parser.parse_args()

hits=[]
icps=[]

for i in range(0,int(len(args.hits)),2):
    hit=PyMaltaData.MaltaData()
    hit.setHit(args.hits[i],args.hits[i+1])
    if args.verbose: print(hit.getInfo())
    hit.pack()
    hit.unpack()
    if args.verbose: print(hit.getInfo())
    hits.append(hit)
    pass

for i in range(0,len(args.icps),2):
    icp=PyMaltaData.MaltaData()
    icp.setHit(args.icps[i],args.icps[i+1])
    if args.verbose: print(hit.getInfo())
    icp.pack()
    icp.unpack()
    if args.verbose: print(hit.getInfo())
    icps.append(icp)
    pass

for hit in hits:
    print ("hit %3i %3i: %s"%(hit.getHitColumn(0),hit.getHitRow(0),hit.toString()))
    for icp in icps:
        print ("icp %3i %3i: %s"%(icp.getHitColumn(0),icp.getHitRow(0),icp.toString()))
        pass
    pass

print ("Have a nice day")
