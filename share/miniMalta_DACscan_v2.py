#!/usr/bin/env python

import Herakles
import MaltaBase
import MaltaSetup
import time
import datetime
import sys
from array import array
import Keithley
import argparse
from MiniMaltaMap import *
#import MiniMaltaMap
import PyMiniMalta

parser=argparse.ArgumentParser()
#parser.add_argument("reg",help="reg name")
parser.add_argument('-c' ,'--chip'    , help='Which chip to talk to',type=int,default=1)
parser.add_argument('-r' ,'--reg'    , help='Which register to change',type=str,default='SC_VCASN')

args=parser.parse_args()
chip=args.chip
variable_st=args.reg
variable= globals()[variable_st]

path="minimalta_dac_scans/"


from ROOT import TCanvas, TFile, TH1F, TH2F, gStyle, TGraph, TTree

connstr=MaltaSetup.MaltaSetup().getConnStr(chip)



print(connstr)
mm=PyMiniMalta.MiniMalta()
dir(mm)
mm.Connect(connstr)
mm.Reset()
time.sleep(1)

#
k=Keithley.Keithley("/dev/ttyUSB2")
#k.setVerbose(True)
#k.enableOutput(True)

#######################################################################################################################
#for Voltage

if variable_st[3]=="I":
    type1="current"
    k.setCurrent(0.0)
    k.setVoltageLimit(3.0)
    pass
elif variable_st[3]=="V":
    type1="voltage"
    k.setVoltage(0.0)
    k.setCurrentLimit(0.0001)
    pass
else:
    print("Reg not valid")
    exit()
#######################################################################################################################
mm.SetSCClock(True)
mm.SetDefaults()


mm.Write(SC_ICASN, 0x3ff)
mm.Write(SC_VCLIP, 0x3ff)
mm.Write(SC_VCASN, 0x3ff)
mm.Write(SC_VCASP, 0x0000)
mm.Write(SC_VRESET_D, 0x0000)
mm.Send()


now=datetime.datetime.now()
now_st=now.strftime("%Y%m%d__%H_%M_%S")
hfile = TFile( path+'scan_'+variable_st+"_"+now_st+'.root', 'RECREATE', '' )
#h_i_Volt = TH2F( 'hiVolt', 'i vs Voltage', 256, 0, 255, 40, 0, 0.2 )
#h_r_Volt = TH2F( 'hrVolt', 'readout vs Voltage', 256, 0, 255, 40, 0, 0.2 )
#h_i_r = TH2F( 'hir', 'i vs readout', 256, 0, 255, 256, 0, 255 )

#start = 227
#n = 256

start = 2
n = 256

prefix = 0x100
#Values_vclip=[]
x_i, r_i, v_i = array( 'f' ), array( 'f' ), array( 'f' )
ntuple       = TTree("DAC","")
n_x_i     = array('f',(0,))     
n_r_i     = array('f',(0,))  
n_v_i     = array('f',(0,))  
ntuple.Branch("current",   n_x_i,   "current/F")
ntuple.Branch("readout",   n_r_i,   "readout/F")
ntuple.Branch("voltage",   n_v_i,   "voltage/F")
#variable=SC_ITHR

print(variable)

attempt=0
def doAction(target):
    global attempt
    attempt+=1
    print ("Attempt: "+str(attempt))
    mm.SetDefaults()
    mm.Write(variable, target)
    mm.Send()
    time.sleep(0.001)
    mm.Recv()
    #readOut = mm.Read(variable)
    #if attempt==100: return True
    #if readOut!=target: 
    #    print "readOut: %s [%s]. Target %s [%s]" % (str(readOut), str(bin(readOut)), str(target), str(bin(target)))
    #    return False
    #else : 
    return True

for i in range(2,n):
    attempt=0
    #time.sleep(0.1)
    mm.Write(variable, prefix+i)
    #mm.Write(SC_VPULSE_H, prefix+i)
    mm.Send()
    time.sleep(0.1)
    #mm.Recv()
    readOut = mm.Read(variable)
    #readOut = mm.Read(SC_VPULSE_H)
    
    #res=doAction(prefix+i)
    #while res==False:
    #    res=doAction(prefix+i)
    #    pass
    #readOut = mm.Read(variable)
    #time.sleep(0.01)

    #debug = mm.Read(SC_VCLIP)
    #time.sleep(1.)
    #mean = 0.0
    #for j in range(0,5):
    #    time.sleep(0.3)
    #    mean = mean + k.getVoltage()
    if type1=="current":
        val = -k.getCurrent() 
        pass
    else:
        #print "we are in else"
        #val=0
        val=k.getVoltage()
        pass
    print ("input value  : " + "dec: " + str(i) + "  bin: " + bin(prefix+i) + "; readOut: " + str(readOut) + " = " + bin(readOut) + "; "+type1+" = " + str(val) )
    #print bin(debug)
#    h_i_Volt.Fill(i, voltage)
#    h_r_Volt.Fill(readOut-256, voltage)
#    h_i_r.Fill(i, readOut-256)
    #print i, type(i)
    x_i.append(i)
    r_i.append(readOut-256)
    v_i.append(val)
    n_x_i[0]=i
    n_r_i[0]=readOut-256
    n_v_i[0]=val
    pass


gr_i_v = TGraph( n-start, x_i, v_i )
gr_i_v.SetMarkerStyle( 22 )
gr_i_v.GetXaxis().SetTitle( 'inputDAC' )
gr_i_v.GetYaxis().SetTitle( type1 +" "+variable_st)
gr_i_v.Write( "graph_input_vs_measure" )

gr_r_v = TGraph( n-start, r_i, v_i )
gr_r_v.SetMarkerStyle( 23 )
gr_r_v.GetXaxis().SetTitle( 'outputDAC' )
gr_r_v.GetYaxis().SetTitle( type1 +" "+variable_st)
gr_r_v.Write( "graph_output_vs_measure")

gr_i_r = TGraph( n-start, x_i, r_i )
gr_i_r.SetMarkerStyle( 21 )
gr_i_r.GetXaxis().SetTitle( 'inputDAC' )
gr_i_r.GetYaxis().SetTitle( 'outputDAC' )
gr_i_r.Draw( 'AP' )
gr_i_r.Write( "graph_input_vs_output" )

ntuple.Fill()
hfile.Write()

c1 = TCanvas( 'c1', 'E', 200, 10, 700, 500 )
gr_i_v.Draw("APL")
c1.Print(path+'scan_'+variable_st+"_"+now_st+'.pdf')

# To hold canvas window open 
text = raw_input()

print ("Done")
sys.exit()
