#!/usr/bin/env python
####################################
# Script for analyzing source scans
# patrick.moriishi.freeman@cern.ch
####################################

import os
import argparse
import array
import sys
import ROOT


ROOT.gStyle.SetPalette(56)
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

parser=argparse.ArgumentParser()
parser.add_argument('-f' ,'--file',help='input file',type=str,default="input.root")
parser.add_argument('-o' ,'--outdir',help='output directory',type=str,default="output")
parser.add_argument('-i' ,'--IDB',help='IDB value',type=str,default="62")
parser.add_argument('-m' ,'--maximum',help='max',type=int,default="100")

args=parser.parse_args()

inputFile =args.file
outDir = args.outdir
idb = args.IDB

sub = outDir.split("/")[-2].split("_")[-1].split("V")[0] 
print "the sub is:  ",sub," V"

########################################
# Get histograms from .root file, make histogram of zombies pixels
#######################################


inFile = ROOT.TFile(inputFile )

vacancy = inFile.Get("Vacancy_IDB_%s" %idb)
occupancy = inFile.Get("Occupancy_IDB_%s" %idb)
masked = inFile.Get("MaskedPix")
ghostsNmasked = inFile.Get("MaskedPixGhosts")


nMask = masked.Integral()
nMaskG = ghostsNmasked.Integral()
nVacant = vacancy.Integral()
nOcc = occupancy.Integral()
 
masked.SetTitle("Mask pixels, n = %i;x;y" %nMask)
ghostsNmasked.SetTitle("Mask pixels and ghosts, n = %i;x;y" %nMaskG)
occupancy.SetMaximum(args.maximum)
ghostsNmasked.SetMaximum(1)
masked.SetMaximum(1)

vacancy.SetTitle("Vacancy, N = %i;x;y" %nVacant)
dZ= ROOT.TH2I("DeadZombies","Dead and Zombies;x;y;counts",512,-0.5,511.5,512,-0.5,511.5)

dZ.SetTitle("Dead Pixels and Zombies")
dZ.Add(vacancy)
dZ.Add(ghostsNmasked, -1)

dZ.SetMinimum(-1)
dZ.SetMaximum(1)




######################################33##
# Get remaining stats
###################################
nZombies = 0 
nDead = 0
intZombies =0 


h_hits  = ROOT.TH1I("test","Number of hits;N hits",args.maximum*6,0,args.maximum*6) 
h_zombies = ROOT.TH1I("test","Hits in Zombies",args.maximum*6,0,args.maximum*6) 

for i in range (1,512):
  for j in range(1,512):
    if dZ.GetBinContent(i,j) == -1: 
      nZombies+=1
      intZombies+=occupancy.GetBinContent(i,j)
      h_zombies.Fill(int(occupancy.GetBinContent(i,j)))
    if dZ.GetBinContent(i,j) == 0:  h_hits.Fill(int(occupancy.GetBinContent(i,j)))
    if dZ.GetBinContent(i,j) == 1:
      nDead += 1

intZombies = float(intZombies)/(float(nZombies)+.000001);
fM = (float(nMaskG) - float(nZombies))/float(nMaskG)


##########################################
# Plot histograms
################################

leg = ROOT.TLegend(0.5, 0.5, 0.8, 0.8)

canHits = ROOT.TCanvas()
h_hits.Draw("HIST")
leg.AddEntry(h_hits)
f_hits = ROOT.TF1("fhits","gaus", 0,args.maximum*6 )
h_zombies.SetLineColor(ROOT.kRed)
h_hits.Fit("fhits","r+")
f_hits.Draw("same")
h_zombies.Draw("same&HIST")
leg.AddEntry(h_zombies)
leg.Draw()
canHits.SetLogy(1)
canHits.SetLogx(1)
canHits.Print("%s/Hits.pdf" %outDir)

dZ.SetTitle("%i Dead Pixels and %i Zombies;x;y" %(nDead, nZombies))

can = ROOT.TCanvas("Occ","Occupancy and Masking",800, 800)
can.Divide(2,2)
can.cd(1)
occupancy.SetTitle("Occuapncy, SUB = %s V;x;y" %sub)
occupancy.Draw("colz")

can.cd(2)
vacancy.Draw("colz")
can.cd(3)
ghostsNmasked.Draw("colz")
can.cd(4)
dZ.Draw("colz")
can.Print("%s/Occ.pdf" %outDir)


can2 = ROOT.TCanvas("Masking","Masking and Ghosts",800,400)
can2.Divide(2,1)
can2.cd(1)
masked.Draw("colz")
can2.cd(2)
ghostsNmasked.Draw("colz")
can2.Print("%s/Masks.pdf" %outDir)


can3 = ROOT.TCanvas("Mask","Masked Pixels",400,400)
masked.Draw("colz")
can3.Print("%s/Masked.pdf" %outDir)
can3.Print("%s/Masked.png" %outDir)

can4 = ROOT.TCanvas("MaskG","Masked Pixels and Ghosts",400,400)
ghostsNmasked.Draw("colz")
can4.Print("%s/Ghosts.pdf" %outDir)
can4.Print("%s/Ghosts.png" %outDir)

can5 = ROOT.TCanvas("Occupancy","Occupancy",400,400)
occupancy.Draw("colz")
can5.Print("%s/Occupancy.pdf" %outDir)
can5.Print("%s/Occupancy.png" %outDir)

can6 = ROOT.TCanvas("Vacancy","Vacancy",400,400)
vacancy.Draw("colz")
can6.Print("%s/Vacancy.pdf" %outDir)
can6.Print("%s/Vacancy.png" %outDir)

can7 = ROOT.TCanvas("Zombies","%i Dead Pixels and %i Zombies" %(nDead, nZombies),400,400)
dZ.Draw("colz")
can7.Print("%s/Zombies.pdf" %outDir)
can7.Print("%s/Zombies.png" %outDir)



##########################################
# Write .txt file with statistics
#########################################
out = file("%s/spookyStats.txt" %outDir,"w")
 
out.write("Statisitics from source scan \n")
out.write("Input file %s \n" %inputFile)
out.write("IDB = %s\n" %idb)
out.write("Masked,%d\n"%nMask)
out.write("Masked+Ghosts,%d\n" %nMaskG)
out.write("Vacant,%d\n"%nVacant)
out.write("Dead,%d\n"%nDead)
out.write("Zombies,%d\n"%nZombies)
out.write("Mean hits in zombies,%d\n"%intZombies)
out.write("Mean hits (Gaussian fit),%d\n"%f_hits.GetParameter(1))
out.write("Total hits,%i\n" %nOcc)
out.write("Fraction successfully masked,%f\n" %fM)
out.write("Sanity check:Masked+Ghosts+Dead-Zombies = Vacant,%d+%d-%d= %d\n" %(nMaskG,nDead,nZombies,nVacant ))

out.close()




      




 
