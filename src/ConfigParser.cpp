#include "MaltaDAQ/ConfigParser.h"
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

ConfigParser::ConfigParser(){}

ConfigParser::~ConfigParser(){
  Clear();
}

void ConfigParser::Clear(){
  while(m_configs.size()){
    ReadoutConfig * config=m_configs.back();
    m_configs.pop_back();
    delete config;
  }
}

void ConfigParser::SetVerbose(bool enable){
  m_verbose=enable;
}

bool ConfigParser::Open(string file){
  if(m_verbose){cout << "ConfigParser::Open file: " << file << endl;}
  ifstream test(file.c_str());
  if(!test.good()){
    cout << "ConfigParser::Open cannot open file: " << file << endl;
    return false;
  }
  vector<string> raw = Load(file);

  return Parse(raw);
}

vector<string> ConfigParser::Load(string file){

  if(m_verbose){cout << "ConfigParser::Parse file:" << file << endl;}

  vector<string> vout;

  ifstream fr;

  fr.open(file.c_str());

  while (fr.good()){

    string buffer;
    getline(fr, buffer);

    // Ignore if is a comment
    size_t end = buffer.find_first_of('#');
    if (end==0) continue;

    // Ignore if line is empty
    if(buffer.length()==0) continue;

    // Split
    size_t separator = buffer.find_first_of(':');
    if (separator != string::npos){

      string key = ToLower(Trim(buffer.substr(0, separator)));
      string val = Trim(buffer.substr(separator+1));

      //If include call Parse again
      if(key=="include"){
	string newfile = val;
	vector<string> included = Load(newfile);
	vout.insert(vout.end(), included.begin(), included.end());
      }else{
	vout.push_back(buffer);
      }
    }else{
      vout.push_back(buffer);
    }
  }


  return vout;
}

ReadoutConfig* ConfigParser::GetConfigBySample(string sample){
  for(uint32_t i=0; i<m_configs.size(); i++){
    if(m_configs.at(i)->GetSample() == sample){ return m_configs.at(i); }
  }
  return NULL;
}

bool ConfigParser::Parse(vector<string> &raw){

  Clear();

  ReadoutConfig * config;

  for(uint32_t i=0;i<raw.size();i++){

    string buffer = raw.at(i);

    // Is it a new section?
    size_t c0 = buffer.find_first_of('[');
    size_t c1 = buffer.find_first_of(']');
    if (c0 != string::npos && c1 != string::npos){
      string header = buffer.substr(c0 + 1, c1 - c0 - 1);
      config=FindConfig(header);
      if(config==NULL){
	config=new ReadoutConfig();
	config->SetName(header);
        std::cout << "Added config with header: " << header << std::endl;
	m_configs.push_back(config);
      }else{
	//config should be always NULL
	//it it's not it's because the
	//plane was added already
	//throw warning
	cout << "ConfigParser::Parse Error duplicated plane:" << header << endl;
	return false;
      }
      continue;
    }

    if(config==NULL){
      cout << "ConfigParser::Parse Config file does not contain a section. Creating dummy PLANE_1" << endl;
      config=new ReadoutConfig();
      config->SetName("PLANE_1");
      m_configs.push_back(config);
    }

    //split
    size_t separator = buffer.find_first_of(':');
    if (separator == string::npos){
      cout << "ConfigParser::Parse cannot parse line: [" << buffer << "]" << endl;
      return false;
    }

    string key = ToLower(Trim(buffer.substr(0, separator)));
    string val = Trim(buffer.substr(separator+1));

    //parse
    if(key=="address"){
      config->SetAddress(val);
    }else if(key=="type"){
      config->SetType(val);
    }else if(key=="wafer"||key=="sample"){
      config->SetWafer(val);
    }else if(key=="classname"){
      config->SetClassname(val);
    }else if(key=="library"){
      config->SetLibrary(val);
    }else if(key=="dut"){
      config->SetDUT(val);
    }else if(key=="modulesize"){
      //Florian: reset module size if arg is actually given
      int32_t modulesize = std::stoi(val);
      std::cout << "Found modulesize config parameter!" << std::endl
                << "Parsed value is: " << modulesize << std::endl;
      config->SetModuleSize(modulesize);
    }else if(key=="config_voltage"||key=="configvoltage"){
      config->SetConfigVoltage(val);
    }else if(key=="mask"){
      config->AddMaskString(val);
      istringstream ss(val);
      string token;
      getline(ss, token, ',');
      int xpos = stoi(token);
      getline(ss, token, ',');
      int ypos = stoi(token);
      config->AddMask(xpos,ypos);
    }else{
      config->AddKey(buffer);
    }
  }
  return true;
}

void ConfigParser::WriteConfigFile(string filename){

  ofstream out(filename);

  for(uint32_t i=0;i<m_configs.size();i++){
    ReadoutConfig * config=m_configs.at(i);
    out << "[" << config->GetName() << "]" << endl;

    out << "Address: " << config->GetAddress() << endl;
    out << "Type: "    << config->GetType()    << endl;
    out << "sample: "  << config->GetWafer()   << endl;
    for(uint32_t i=0;i<config->GetKeys().size();i++){
      out << config->GetKeys()[i] << endl;
    }
    for(uint32_t j=0;j<config->GetMasks().size();j++){
      out << "MASK: " << config->GetMaskStrings()[j] << endl;
    }
  }
  out.close();
}

std::map<std::string,std::map<std::string,std::string>> ConfigParser::ConfigForROOTFile(){
  std::map<std::string,std::map<std::string,std::string>> runConfig;

  for(uint32_t i=0;i<m_configs.size();i++){
    std::string mask_dc = "";
    ReadoutConfig* config=m_configs.at(i);
    runConfig[config->GetName()]["plane"] = i;
    runConfig[config->GetName()]["address"] = config->GetAddress();
    runConfig[config->GetName()]["type"] = config->GetType();
    runConfig[config->GetName()]["sample"] = config->GetWafer();
    //write out misellaneous data
    for(uint32_t j=0;j<config->GetKeys().size();j++){
      std::string entry = config->GetKeys()[j];
      auto separatorPos = std::find(entry.begin(),entry.end(),'=');
      if(separatorPos == entry.end()){
        separatorPos = std::find(entry.begin(),entry.end(),':');
      }
      if(separatorPos == entry.end()){
        cout << "Could not find separator for key-value parsing: " << endl;
        cout << entry << endl;
        cout << "Entry will be ignored..." << endl;
        continue;
      }
      std::string key(entry.begin(),separatorPos);
      //trim whitespaces
      key.erase(std::remove_if(key.begin(), key.end(), [](unsigned char c){return std::isspace(c);}), key.end());

      std::string val(separatorPos+1,entry.end());
      //trim whitespaces
      val.erase(std::remove_if(val.begin(), val.end(), [](unsigned char c){return std::isspace(c);}), val.end());

      if(key != "MASK_DOUBLE_COLUMNS" && key != "MASK_PIXEL"){
        runConfig[config->GetName()][key] = val;
      } else { continue; }
    }
  }

  return runConfig;

  //ofstream out(filename);

  //Create ROOT file
  //Create 1 Tree per plane
  //Fill Tree with branches, each branch name is the name of the config parameter
  //Branches that contain strings, fill them with TObjString

  /*for(uint32_t i=0;i<m_configs.size();i++){
    ReadoutConfig * config=m_configs.at(i);
    out << "[" << config->GetName() << "]" << endl;

    out << "Address: " << config->GetAddress() << endl;
    out << "Type: "    << config->GetType()    << endl;
    out << "sample: "  << config->GetWafer()   << endl;
    for(uint32_t i=0;i<config->GetKeys().size();i++){
      out << config->GetKeys()[i] << endl;
    }
    for(uint32_t j=0;j<config->GetMasks().size();j++){
      out << "MASK: " << config->GetMaskStrings()[j] << endl;
    }
  }
  out.close();*/
}


void ConfigParser::Print(){
  for(uint32_t i=0;i<m_configs.size();i++){
    ReadoutConfig * config=m_configs.at(i);
    cout << "[" << config->GetName() << "]" << endl;
    cout << "Address: " << config->GetAddress() << endl;
    cout << "Type: "    << config->GetType() << endl;
    cout << "Wafer: "   << config->GetWafer() << endl;
    cout << "Module Size: " << config->GetModuleSize() << std::endl;
    for(uint32_t j=0;j<config->GetMasks().size();j++){
      cout << "MASK: " << config->GetMaskStrings()[j] << endl;
    }
    for(uint32_t j=0;j<config->GetKeys().size();j++){
      cout << config->GetKeys()[j] << endl;
    }
    cout << endl;
  }
}

void ConfigParser::Add(ReadoutConfig *config){
  m_configs.push_back(config);
}

uint32_t ConfigParser::GetN(){
  return m_configs.size();
}

ReadoutConfig* ConfigParser::GetConfig(uint32_t module){
  return m_configs.at(module);
}

ReadoutConfig* ConfigParser::FindConfig(string name){
  for(uint32_t i=0; i<m_configs.size(); i++){
    if(m_configs.at(i)->GetName() == name){ return m_configs.at(i); }
  }
  return NULL;
}

string ConfigParser::Trim(string str){
  size_t first = str.find_first_not_of(' ');
  if (first == std::string::npos) return "";
  size_t last = str.find_last_not_of(' ');
  return str.substr(first, (last-first+1));
}

string ConfigParser::ToLower(string str){
  transform(str.begin(), str.end(), str.begin(), ::tolower);
  return str;
}
