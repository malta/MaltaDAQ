#include "MaltaDAQ/ReadoutModule.h"
#include "MaltaDAQ/ModuleLoader.h"
#include "MaltaDAQ/ConfigParser.h"

#include <cmdl/cmdargs.h>

#include <iostream>
#include <string>
#include <vector>
#include <iomanip> 
#include <unistd.h>

using namespace std;

int main(int argc, char *argv[]) {
  cout << "#####################################" << endl
       << "# Welcome to MALTA Config App!      #" << endl
       << "#####################################" << endl;
  
  cout << "Parsing command line parameters" << endl;  
  
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr  cConfig('c',"config","config","config file",CmdArg::isREQ);

  CmdLine cmdl(*argv,&cConfig,&cVerbose,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  
  //Parameters
  string itkpath = getenv("ITK_PATH");
  
  ConfigParser mc;
  mc.SetVerbose(cVerbose);
  if(!mc.Open(string(cConfig))){
    cout << "Error opening config file: " << cConfig << endl;
    return 0;
  }
  
  if(cVerbose) mc.Print();

  vector<ReadoutModule*> maltas;
  
  cout << "Configure" << endl;
  for(uint i=0;i<mc.GetN();i++){
    ReadoutConfig * config=mc.GetConfig(i);
    cout << "Create Plane " << i << endl;
    ReadoutModule *mm =  ModuleLoader::newReadoutModule(config->GetLibrary(),
							config->GetClassname(),
							config->GetName(),
							config->GetAddress(),
							".");

    mm->SetVerbose(cVerbose);
    mm->SetConfig(config);
    mm->Configure();
    maltas.push_back(mm);
  }

  cout << "Configuration finished" << endl;

  cout << "Cleanup the house" << endl;
  for(uint i=0;i<maltas.size();i++){
    delete maltas[i];
  }
  
  cout << "Have a nice day" << endl;
  return 0;

}


