#include "MaltaDAQ/ConfigParser.h"
#include "MaltaDAQ/ModuleLoader.h"
#include "MaltaDAQ/MaltaSocketDAQ.h"
#include "MaltaDAQ/ReadoutModule.h"

#include <cmdl/cmdargs.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <unistd.h>

#include "TApplication.h"
#include "TSysEvtHandler.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "TStyle.h"
#include "TLine.h"
#include "TLatex.h"


using namespace std;

bool g_cont=true;

void PrintText(uint32_t count) {
  TLatex l;
  l.SetTextSize(0.05);
  l.SetNDC();
  l.SetTextColor(1);
  ostringstream val;
  val << "nTrigger= " << count;
  l.DrawLatex(0.6,0.6,val.str().c_str());
}

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

class THandler: public TSignalHandler{
public:
  THandler(ESignals sig): TSignalHandler(sig, false){}
  virtual Bool_t Notify(){
    handler(0);
    return false;
  }
};


int main(int argc, char *argv[]) {
  cout << "#####################################" << endl
       << "# Welcome to MALTA Multi DAQ App!   #" << endl
       << "#####################################" << endl;

  cout << "Parsing command line parameters" << endl;

  CmdArgInt  cRun('r',"run","runnumber","run number",CmdArg::isREQ);
  CmdArgInt  cEvts('e',"events","events","number of events. Default -1");
  CmdArgInt  cSmpl('s',"sampling","events","sample every N events. Default 100");
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr  cOutdir('o',"output","output","output directory");
  CmdArgBool cNoise('n',"noiseRun","will generate random L1a to test noise");
  CmdArgStr  cConfig('c',"config","config","config file");
  CmdArgBool cPico('p',"picoRun","include pico in the run");

  CmdLine cmdl(*argv,&cRun,&cConfig,&cVerbose,&cNoise,&cOutdir,&cSmpl,&cPico,&cEvts,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  //Parameters
  string datapath = getenv("MALTA_DATA_PATH");
  string outdir = (cOutdir.flags()&CmdArg::GIVEN?cOutdir:"./TelescopeData");

  ConfigParser mc;
  mc.SetVerbose(cVerbose);
  if(cConfig.flags()&CmdArg::GIVEN){
    if(!mc.Open(string(cConfig))){
      cout << "Error opening config file: " << cConfig << endl;
      return 0;
    }
  }else{
    ReadoutConfig * config = new ReadoutConfig();
    config->SetName("TestPlane");
    config->SetAddress("192.168.0.1:50001");
    config->SetType("MALTA");
    config->SetWafer("W2R11");
    mc.Add(config);
  }
  if(cVerbose) mc.Print();

  vector<ReadoutModule*> maltas;
  vector<ReadoutModule*> maltas_forsocket;

  cout << "Configure" << endl;
  for(uint i=0;i<mc.GetN();i++){
    ReadoutConfig * config=mc.GetConfig(i);
    cout << "Loading module: " << i << ", type: " << config->GetType() << endl;

    string Library  ="Malta";
    string ClassName="MaltaModule";
    if ( config->GetType()=="MALTA2" ) {
      Library  ="Malta2";
      ClassName="Malta2Module";      
    }

    ReadoutModule * mm = ModuleLoader::newReadoutModule(Library, //config->GetLibrary(),
							ClassName, //config->GetClassname(),
							config->GetName(),
							config->GetAddress(),
							outdir);
  
    mm->SetVerbose(cVerbose);
    mm->SetConfig(config);
    mm->Configure();
    mm->SetSampling((cSmpl.flags()&CmdArg::GIVEN)?cSmpl:100);
    maltas.push_back(mm);
    maltas_forsocket.push_back(mm);
    
  }

  cout << "I am beyond instantiation ... " << endl;

  g_cont=true;
  signal(SIGINT,handler);
  signal(SIGTERM,handler);

  THandler theHandler1(kSigInterrupt);
  THandler theHandler2(kSigTermination);
  theHandler1.Add();
  theHandler2.Add();

  TApplication theApp("myApp",0,0);

  TLine* L0=new TLine(120,140,120,420);
  TLine* L1=new TLine(400,140,400,420);
  TLine* L2=new TLine(120,140,400,140);
  TLine* L3=new TLine(120,420,400,420);
  L0->SetLineWidth(2);
  L1->SetLineWidth(2);
  L2->SetLineWidth(2);
  L3->SetLineWidth(2);
  TLine* La=new TLine(256,140,256,420);
  La->SetLineWidth(2);
  La->SetLineColor(kGray);


  vector<TH1D*> lTime;
  vector<TH2D*> lMap;
  ostringstream title;
  title << "MALTA Telescope run #" << cRun;
  TCanvas* myC=new TCanvas("Telescope",title.str().c_str(),2000,800);
  gStyle->SetPalette(77);
  gStyle->SetOptStat(111111);
  myC->Divide(maltas.size(),3);
  TCanvas* myC_projections=new TCanvas("Telescope_projections",title.str().c_str(),1000,500);
  myC_projections->Divide(2,1);
  TCanvas* myC_flipped=new TCanvas("Telescope_flipped","Telescope_flipped",2000,600);
  gStyle->SetPalette(77);
  gStyle->SetOptStat(111111);
  myC_flipped->Divide(maltas.size(),1);

    //loop for alignment canvas
  TLine* L0_clone = (TLine*)L0->Clone();
  TLine* L1_clone = (TLine*)L1->Clone();
  TLine* L2_clone = (TLine*)L2->Clone();
  TLine* L3_clone = (TLine*)L3->Clone();
  TLine* La_clone = (TLine*)La->Clone();
  
  for (unsigned int m=0; m<maltas.size(); m++) {
    bool minimalta=(maltas.at(m)->GetConfig()->GetType()=="MiniMALTA");
    myC->cd(m+1);
    myC->cd(m+1)->SetLogz();
    TH2D* tmp2D=(maltas.at(m))->GetHitMapHisto();
    if (tmp2D==0) continue;
    //lMap.push_back(tmp2D);
    ////if ( !minimalta ) tmp2D->GetXaxis()->SetRangeUser(0,350);
    //    gStyle->SetOptStat(0);
    tmp2D->Draw("COLZ");
    L0->Draw("SAME");
    //L1->Draw("SAME");
    //L2->Draw("SAME");
    //L3->Draw("SAME");
    //La->Draw("SAME");
    myC->cd(m+1+maltas.size());
    TH1D* tmp1D=(maltas.at(m))->GetTimingHisto();
    tmp1D->SetMinimum(0.);
    //lTime.push_back(tmp1D);
    if ( !minimalta ) tmp1D->GetXaxis()->SetRangeUser( 0,  500);//80,  240);  //150,400);
    else             tmp1D->GetXaxis()->SetRangeUser( 0,15000);
    gStyle->SetOptStat(111111);
    tmp1D->Draw("HIST");
    myC->cd(m+1+2*maltas.size());
    TH1D* tmp1Db=(maltas.at(m))->GetNHitHisto();
    gStyle->SetOptStat(111111);
    tmp1Db->Draw("HIST");
    if (m==0){
      myC_projections->cd(1);
      TH1F* tmp1D_px=NULL;
      tmp1D_px= (maltas.at(m))->GetProjectionX();
      tmp1D_px->Draw("hist");
      TH1F* tmp1D_py=NULL;
      tmp1D_py= (maltas.at(m))->GetProjectionY();
      myC_projections->cd(2);
      tmp1D_py->Draw("hist");
    }

    myC_flipped->cd(m+1);
    myC_flipped->cd(m+1)->SetLogz();
    gStyle->SetOptStat(111111);
    TH2D* tmp2D_sy=(maltas.at(m))->GetSymmetricHitMapHisto();
    std::cout<<"hist is there"<<std::endl;
    if (tmp2D_sy==0) continue;
    //gStyle->SetOptStat(0);
    tmp2D_sy->Draw("COLZ");
  }
  myC->Update();
  myC_projections->Update();
  myC_flipped->Update();

     
  
  ReadoutModule* /*PicoTDCModule*/ m_picoModule = NULL;
  bool withPico=( (cPico.flags() & CmdArg::GIVEN) ? cPico:false);
  TCanvas *pico = NULL;

  if (withPico==true){
    ostringstream os;
    os << setfill('0') << setw(6) << cRun;

    string pico_run= os.str();
    m_picoModule =  ModuleLoader::newReadoutModule("PicoTDC","PicoTDCModule",
							  "PicoTDC",
							  "udp://ep-ade-gw-01:60000", "pico_runs");

    //m_picoModule = new PicoTDCModule ("pico", "udp://ep-ade-gw-01:60000", "pico_runs");
    //m_picoModule = new PicoTDCModule ("pico", "udp://ep-ade-gw-01:60000", "pico_runs");
    m_picoModule->Configure();
    sleep(5);
    m_picoModule->Start(("pico_run_"+pico_run+".root").c_str(), false);
    sleep(5);
    pico = new TCanvas("pico",("pico run #" + std::to_string(cRun)).c_str(), 800, 600);
    pico->Divide(4,3);
    pico->Update();
    maltas_forsocket.push_back(m_picoModule);
  }

  ///////Malta Socket ///////
  MaltaSocketDAQ * m_socket = new MaltaSocketDAQ();
  m_socket-> ConfigureSocket(maltas_forsocket);

  cout << "Start" << endl;
  for(uint i=0;i<maltas.size();i++){
    cout << "Starting Malta " << i << endl;
    ostringstream os;
    os << setfill('0') << setw(6) << cRun
       << "_" <<  i << ".root";
    maltas[i]->EnableFastSignal();
    if((maltas[i]->GetHitMapHisto())) maltas[i]->GetHitMapHisto()->SetMinimum(0.9);
    maltas[i]->Start(os.str());
  }

  /*
  usleep(1000);
  for(uint i=0;i<maltas.size();i++){
    cout << " HELLO, I am your friendly neighbour plane: " << i << endl;
    maltas[i]->EnableFastSignal();
    (maltas.at(i))->GetHitMapHisto()->SetMinimum(0.9);
    }
  */
  int count=0;

  //m_socket->ReadSocket()maltas_forsocket);
  ///////////////////////////
  while(g_cont){
    //usleep(100);
    sleep(0.5);
    count+=1;
    if (count%5==0) { //was 10
      for (unsigned int m=0; m<maltas.size(); m++) {
	myC->cd(m+1);
	myC->cd(m+1)->SetLogz();
        if ((maltas.at(m))->GetHitMapHisto()==0) continue;
	(maltas.at(m))->GetHitMapHisto()->Draw("COLZ");
	L0->Draw("SAME");
	L1->Draw("SAME");
	L2->Draw("SAME");
	L3->Draw("SAME");
	La->Draw("SAME");
	myC->cd(m+1+maltas.size());
	(maltas.at(m))->GetTimingHisto()->GetXaxis()->SetRangeUser(50,300);
	(maltas.at(m))->GetTimingHisto()->Draw("HIST");
	PrintText((maltas.at(m))->GetNTriggers() );
	myC->cd(m+1+2*maltas.size());
        (maltas.at(m))->GetNHitHisto()->Draw("HIST");
	if(!maltas.at(m)->IsRunning()){g_cont=false;}
        if (m==0){
         myC_projections->cd(1);
         TH1F* tmp1D_px=NULL;
         tmp1D_px= (maltas.at(m))->GetProjectionX();
         tmp1D_px->Draw("hist");
         TH1F* tmp1D_py=NULL;
         tmp1D_py= (maltas.at(m))->GetProjectionY();
         myC_projections->cd(2);
         tmp1D_py->Draw("hist");
       }
	myC_flipped->cd(m+1);
	myC_flipped->cd(m+1)->SetLogz();
	gStyle->SetOptStat(111111);
	cout<<"hist flipped"<<endl;
	if ((maltas.at(m))->GetSymmetricHitMapHisto()==0) continue;
	(maltas.at(m))->GetSymmetricHitMapHisto()->Draw("COLZ");
	L0_clone->Draw("SAME");
	L1_clone->Draw("SAME");
	L2_clone->Draw("SAME");
	L3_clone->Draw("SAME");
	La_clone->Draw("SAME");
      }
      myC->Update();
      myC_projections->Update();
      myC_flipped->Update();
      sleep(5);
      if (m_picoModule!=NULL) m_picoModule->PlotCanvasRealTime(pico);
      if (m_picoModule!=NULL) pico->Update();
      //m_socket->ReadSocket(maltas_forsocket);

    }
    ///////////if ( (maltas.at(0))->GetNTriggers()==300 or (maltas.at(1))->GetNTriggers()==300  ) g_cont=false;
    //cout << "HELLO" << endl;
  }
  m_socket->Stop();
  delete m_socket;
  //close(m_sockfd);
  /*
  for(uint i=0;i<maltas.size();i++){
    maltas[i]->DisableFastSignal();
  }
  */

  cout << "Stop" << endl;

  //Let the threads finish processing
  sleep(0.1);
   for(uint i=0;i<maltas.size();i++){
    cout << "Stopping Malta " << i << endl;
    maltas[i]->Stop();
    maltas[i]->DisableFastSignal();
  }

  cout << "Cleanup the house" << endl;
  for(uint i=0;i<maltas.size();i++){
     delete maltas[i];
   }


  if(m_picoModule!=NULL){
    m_picoModule->Stop();
    delete m_picoModule;
    delete pico;
  }

  cout << "Have a nice day" << endl;
  return 0;
}
