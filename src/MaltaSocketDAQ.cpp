#include "MaltaDAQ/MaltaSocketDAQ.h"
#include <thread>



using namespace std;

void MaltaSocketDAQ::ConfigureSocket(vector<ReadoutModule*> maltas){
  //Create new socket
  m_sockfd = socket(AF_INET, SOCK_STREAM, 0);
  
  //Set reuse address
  int reuse_addr=1;
  setsockopt(m_sockfd, SOL_SOCKET, SO_REUSEADDR, (const void*)&reuse_addr, sizeof(reuse_addr));
  //Set reuse port
  int reuse_port=1;
  setsockopt(m_sockfd, SOL_SOCKET, SO_REUSEPORT, (const void*)&reuse_port, sizeof(reuse_port));
  //set linger on close
  struct linger onclose;
  onclose.l_onoff = 0;
  onclose.l_linger = 1;
  setsockopt(m_sockfd, SOL_SOCKET, SO_LINGER, (const void*)&onclose, sizeof(onclose));
  struct timeval timeout;      
  timeout.tv_sec = 10;
  timeout.tv_usec = 0;
  setsockopt (m_sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
  //bind to port
  struct sockaddr_in serv_addr;
  memset(&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(8080);

  int ret = bind(m_sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
  if(ret<0){cout << "ERROR binding Controllable to port 8080 " << endl;}
  
  listen(m_sockfd,5);
  m_maltas=maltas;
  m_thread_socket = thread(&MaltaSocketDAQ::ReadSocket,this);

}


void MaltaSocketDAQ::ReadSocket(){//vector<ReadoutModule*> maltas){
  int sendOK=1;
  //while (m_cont){
  //if(!g_cont) break;
  struct sockaddr_in cli_addr;
  socklen_t cli_len;
  while (m_cont){
  int client = accept(m_sockfd, (struct sockaddr *) &cli_addr, &cli_len);
  //cout << client << endl;
  //if(!g_cont) break;
  //if(client < 0){ continue; cout << "ERROR on accept" << endl; continue;}
  char buffer[1024] = {0};
  //if(!g_cont) break;
  read( client , buffer, 1024);
  //int valread = read( client , buffer, 1024);
  //if (valread < 0) {cout << "ERROR reading from socket" << endl;}
  //if(!g_cont) break;
  //printf("CLIENT: command %s received\n",buffer );
  if(strcmp(buffer,"RESET")==0){
    for (unsigned int i =0; i <m_maltas.size(); ++i){
      cout << "RESET MALTA PLANE: " << i <<endl;
      m_maltas.at(i)->ResetL1Counter();
    }
    do {
      sendOK=0;
      for (unsigned int i =0; i <m_maltas.size(); ++i) sendOK+=m_maltas.at(i)->RetrieveL1ResetFlag();
    }
    while (sendOK!=0); 
    
    const char *hello = "SERVER Socket: Malta L1 counter reset DONE!";
    send(client, hello, strlen(hello),0); 
    printf("reset done! \n");
         
  }
  close(client);
  usleep(500);
  }
}
void MaltaSocketDAQ::Stop(){
  m_cont=false;
  m_thread_socket.join(); 
}

MaltaSocketDAQ::~MaltaSocketDAQ(){
  //m_thread_socket.join();
  close(m_sockfd);
}
