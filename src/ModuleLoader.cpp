#include "MaltaDAQ/ModuleLoader.h"
#include <dlfcn.h>
#include <iostream>

using namespace std;

void ModuleLoaderDeleteInstance(){
  delete ModuleLoader::getInstance();
}

ModuleLoader * ModuleLoader::m_me = 0;

ModuleLoader::ModuleLoader(){
  atexit(ModuleLoaderDeleteInstance);
}

ModuleLoader::~ModuleLoader(){
  map<string,void*>::iterator it=m_libs.begin();
  for(;it!=m_libs.end();it++){
	cout << "Unloading " << it->first << endl;
	dlclose(it->second);
  }
}

ModuleLoader * ModuleLoader::getInstance(){
  if(m_me==0) m_me = new ModuleLoader();
  return m_me;
}

void * ModuleLoader::getHandle(string library){
  
  #ifdef __APPLE__
  string suffix(".dylib");
  #else 
  string suffix(".so");
  #endif
  
  void * handle = NULL;
  map<string,void*>::const_iterator it=m_libs.find(library);
  if(it==m_libs.end()) {
	string libname="lib" + library + suffix;
	cout << " Loading library " << libname;
	handle=dlopen(libname.c_str(), RTLD_LAZY|RTLD_GLOBAL);
	char * error = dlerror();
	if (error!=0){cout << " Error: " << error << endl; return NULL;}
	cout << " Success" << endl;
	m_libs[library]=handle; 
  }else{
	handle=it->second;
  }
  return handle;
}

std::string ModuleLoader::getLibName(string name){
  int state=1;
  for(unsigned int i = 0; i<name.length(); i++){
    if(isupper(name[i])==true && state==0){
      return name.substr(0,i);
    }else if(isupper(name[i])==false){state=0;}
  }
  return name;
}

ReadoutModule * ModuleLoader::newReadoutModule(string library, string type, string name, string address, string outdir){
  string sSym = "create" + type;
  void * handle = 0;
  ReadoutModule * obj = 0;
  
  //Check if exists in main process
  ReadoutModuleBuilder constructor = (ReadoutModuleBuilder) dlsym(handle,sSym.c_str());
  
  if(constructor==0){
    //Look for it in loaded libraries
    handle = getInstance()->getHandle(library);
    if(handle==NULL){return NULL;}
    ReadoutModuleBuilder constructor2 = (ReadoutModuleBuilder) dlsym(handle,sSym.c_str());
    if(constructor2==0){return NULL;}
    //create new object
    try{obj = constructor2(name, address, outdir);}catch(...){cout << "Error loading ReadoutModule" << endl;}
  }else{
    //create new object
    try{obj = constructor(name, address, outdir);}catch(...){cout << "Error loading ReadoutModule" << endl;}
  }
  return obj;
}

