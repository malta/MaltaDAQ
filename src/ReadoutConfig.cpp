#include "MaltaDAQ/ReadoutConfig.h"

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cstdint>
using namespace std;

ReadoutConfig::ReadoutConfig(){
  //Florian: initialize to single chip module by default
  m_modulesize = 1;
}

ReadoutConfig::~ReadoutConfig(){
  m_keys.clear();
  m_masks.clear();
  m_pixmasks.clear();
}

void ReadoutConfig::SetName(string name){
  m_name=name;
}

void ReadoutConfig::SetAddress(string addr){
  m_address=addr;
}

void ReadoutConfig::SetType(string typ){
  m_type=typ;
}

void ReadoutConfig::SetWafer(string wafer){
  m_wafer=wafer;
}

void ReadoutConfig::SetClassname(string classname){
  m_classname=classname;
}

void ReadoutConfig::SetLibrary(string library){
  m_library=library;
}

void ReadoutConfig::SetDUT(string dut){
  m_dut=dut;
}

void ReadoutConfig::SetConfigVoltage(string value){
  m_configvoltage=value;
}

void ReadoutConfig::SetModuleSize(int modulesize){
  m_modulesize = modulesize;
}

void ReadoutConfig::AddMaskString(string mask){
  m_masks.push_back(mask);
}

void ReadoutConfig::AddMask(int pixx, int pixy){
  m_pixmasks.push_back(pair<int,int>(pixx,pixy));
}

void ReadoutConfig::AddKey(string key){
  m_keys.push_back(key);
}

string ReadoutConfig::GetName(){
  return m_name;
}

string ReadoutConfig::GetAddress(){
  return m_address;
}

string ReadoutConfig::GetType(){
  return m_type;
}

string ReadoutConfig::GetWafer(){
  return m_wafer;
}

string ReadoutConfig::GetSample(){
  return m_wafer;
}

string ReadoutConfig::GetLibrary(){
  return m_library;
}

string ReadoutConfig::GetClassname(){
  return m_classname;
}

string ReadoutConfig::GetDUT(){
  return m_dut;
}

int32_t ReadoutConfig::GetModuleSize(){
  return m_modulesize;
}

int32_t ReadoutConfig::IsDUT(){
  return str2int(m_dut);
}

string ReadoutConfig::GetConfigVoltage(){
  return m_configvoltage;
}

float ReadoutConfig::ConfigVoltage(float defValue){
  return str2float(m_configvoltage,defValue);
}

vector<string> ReadoutConfig::GetKeys(){
  return m_keys;
}

vector<string> ReadoutConfig::GetMaskStrings(){
  return m_masks;
}

vector<pair<int32_t,int32_t> > ReadoutConfig::GetMasks(){
  return m_pixmasks;
}

std::map<std::string,std::string> ReadoutConfig::ConfigForROOTFile(){
  std::map<std::string,std::string> runConfig;

  runConfig["address"] = GetAddress();
  runConfig["type"] = GetType();
  runConfig["sample"] = GetWafer();
  runConfig["dut"] = GetDUT();
  runConfig["configvoltage"] = GetConfigVoltage();
  //write out misellaneous data
  for(uint32_t j=0;j<m_keys.size();j++){
    std::string entry = m_keys[j];
    //cout << "Parsing config entry: " << endl;
    //cout << entry << endl;
    auto separatorPos = std::find(entry.begin(),entry.end(),'=');
    if(separatorPos == entry.end()){
      separatorPos = std::find(entry.begin(),entry.end(),':');
    }
    if(separatorPos == entry.end()){
      cout << "Could not find separator for key-value parsing: " << endl;
      cout << entry << endl;
      cout << "Entry will be ignored..." << endl;
      continue;
    }
    std::string key(entry.begin(),separatorPos);
    //trim whitespaces
    key.erase(std::remove_if(key.begin(), key.end(), [](unsigned char c){return std::isspace(c);}), key.end());

    std::string val(separatorPos+1,entry.end());
    //trim whitespaces
    val.erase(std::remove_if(val.begin(), val.end(), [](unsigned char c){return std::isspace(c);}), val.end());

    //cout << "key: " << key << endl;
    //cout << "val: " << val << endl;

    if(key != "MASK_DOUBLE_COLUMNS" && key != "MASK_PIXEL"){
      runConfig[key] = val;
    } else { continue; }
  }

  return runConfig;
}

int32_t ReadoutConfig::str2int(string str){
  int32_t value=0;
  if(str.find("0x") != std::string::npos){
    value = std::stoi(str, 0, 16);
  }else if(  str.find("true") != string::npos ||
	     str.find("True") != string::npos ||
	     str.find("TRUE") != string::npos
	     ){
    value = 1;
  }else if(  str.find("false") != string::npos ||
	     str.find("False") != string::npos ||
	     str.find("FALSE") != string::npos
	     ){
    value = 0;
  }else if(!str.empty()){
    value = std::stoi(str);
  }
  return value;
}

float ReadoutConfig::str2float(string str, float def){
  float value=def;
  try{value=std::stof(str);}catch(...){}
  return value;
}

std::map<std::string,std::string> & ReadoutConfig::GetKeyMap(){
  if(m_keymap.empty()){m_keymap=ConfigForROOTFile();}
  return m_keymap;
}

