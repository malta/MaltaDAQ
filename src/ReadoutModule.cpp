#include "MaltaDAQ/ReadoutModule.h"
#include <string>
#include "TH2D.h"

using namespace std;

ReadoutModule::ReadoutModule(string name, string address, string outdir){  
  m_name     = name;
  m_outdir   = outdir;
  m_debug    = false;
  m_verbose  = false;
  m_sampling = 100;
  m_internalTrigger = false;
  m_config   = 0;
  m_running  = false;
  m_maxEvt   = 0;  
  m_Htime    = 0;
  m_Hsize    = 0;
  m_Hmap     = 0;
  m_pX       = 0;
  m_pY       = 0;
  m_Hmap_swapped =0;
  m_Hmap_symmetric = 0;  
}

ReadoutModule::~ReadoutModule(){
  //  if(m_Hmap_swapped) delete m_Hmap_swapped;
}  

void ReadoutModule::SetConfig(ReadoutConfig* config){
  m_config=config;
}

ReadoutConfig* ReadoutModule::GetConfig(){
  return m_config;
}

void ReadoutModule::SetVerbose(bool enable){
  m_verbose=enable;
}

void ReadoutModule::SetName(string name){
  m_name=name;
}

void ReadoutModule::SetInternalTrigger(bool enable){
  m_internalTrigger=enable;
}

void ReadoutModule::SetMaxEvents(uint32_t maxEvents){
  m_maxEvt=maxEvents;
}

void ReadoutModule::SetSampling(uint32_t sampling){
  m_sampling=sampling;
}

bool ReadoutModule::IsRunning(){
  return m_running;
}

string ReadoutModule::GetName(){
  return m_name;
}

TH1D* ReadoutModule::GetTimingHisto(){
  return m_Htime;
}

TH1D* ReadoutModule::GetNHitHisto(){
  return m_Hsize;
}

TH2D* ReadoutModule::GetHitMapHisto(){
  return m_Hmap;
}


TH2D* ReadoutModule::GetSwappedHitMapHisto(){
  return m_Hmap_swapped;
}


TH2D* ReadoutModule::GetSymmetricHitMapHisto(){
  return m_Hmap_symmetric;
}


TH1F* ReadoutModule::GetProjectionX(){
  return m_pX;
}

TH1F* ReadoutModule::GetProjectionY(){
  return m_pY;
}

void ReadoutModule::PlotCanvasRealTime(TCanvas *c){}

void ReadoutModule::ResetL1Counter(){ 
  m_resetL1Counters = true; 
}

bool ReadoutModule::RetrieveL1ResetFlag(){ 
  return m_resetL1Counters; 
}
