#!/usr/bin/env python
####################################
# Daq script for MALTA read-out
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# December 2017
####################################

import os
import argparse
import array
import sys
import ROOT
ROOT.gStyle.SetPalette(1)
ROOT.gROOT.SetBatch(1)

parser=argparse.ArgumentParser()
parser.add_argument('-f' ,'--file',help='output file',default="output.root")
parser.add_argument('-dr','--duplicateRemoval',help='remove duplicates',type=bool,default=True)
parser.add_argument('-t' ,'--applyTimingCut',help='apply timing cut [10,170]',type=bool,default=False)
parser.add_argument('-a' ,'--applyFlorescenceCut',help='apply noise cut for florescence setup',type=bool,default=False)
args=parser.parse_args()


class Tuple(dict):
    def __init__(self):
        self.treenumber=-1
    def LoadBranches(self,t):
        if self.treenumber == t.GetTreeNumber(): return
        self.treenumber = t.GetTreeNumber()
        for i in range(t.GetListOfLeaves().GetEntries()):
            self.__setattr__(t.GetListOfLeaves().At(i).GetName(),
                             t.GetListOfLeaves().At(i))
            pass
        pass

ROOT.gStyle.SetPadLeftMargin(0.134)
ROOT.gStyle.SetPadBottomMargin(0.146)
ROOT.gStyle.SetPadRightMargin(0.035)
ROOT.gStyle.SetPadTopMargin(0.078)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetLabelSize(0.05,"x")
ROOT.gStyle.SetLabelSize(0.05,"y")
ROOT.gStyle.SetTitleSize(0.05,"t")
ROOT.gStyle.SetTitleSize(0.05,"x")
ROOT.gStyle.SetTitleSize(0.05,"y")
ROOT.gStyle.SetTitleOffset(1.40,"y")
#ROOT.gStyle.SetOptTitle(0);
ROOT.gStyle.SetOptStat(0);

chain = ROOT.TChain("MALTA")
chain.AddFile(args.file)
tree = Tuple()

refbit  = []
pixel   = []
group   = []
parity  = []
delay   = []
dcolumn = []
bcid    = []
chipid  = []
phase   = []
evtid   = []
word1   = []
word2   = []
l1id    = []
timer   = []
isDuplicate   = []


entry = 0
while entry < 1000000:
#while True:
    chain.GetEntry(entry)
    if chain.LoadTree(entry)<0:
        break
    entry+=1
    if entry%100000==0: print( entry)
    tree.LoadBranches(chain)
    ####refbit.append(tree.refbit.GetValue())
    pixel.append(tree.pixel.GetValue())
    group.append(tree.group.GetValue())
    parity.append(tree.parity.GetValue())
    #delay.append(tree.delay.GetValue())
    dcolumn.append(tree.dcolumn.GetValue())
    bcid.append(tree.bcid.GetValue())
    #chipid.append(tree.chipid.GetValue())
    phase.append(tree.phase.GetValue())
    evtid.append(tree.winid.GetValue())
    l1id.append(tree.l1id.GetValue())
    #print( tree.word1.GetValue())
    #print( tree.word2.GetValue())
    #word1.append(tree.word1.GetValue())
    #word2.append(tree.word2.GetValue())
    ###########timer.append(tree.timer.GetValue())
    isDuplicate.append(tree.isDuplicate.GetValue())
    pass


'''
cGroup = ROOT.TCanvas("cGroup","Group",800,600)
hGroup = ROOT.TH1D("hGroup","Group",32,0,32)

for i in range(len(group)):
    hGroup.Fill(group[i])
    pass

hGroup.GetXaxis().SetNdivisions(20)
hGroup.Draw("")
cGroup.Update()
cGroup.SaveAs("Plots/Group_%s.root"%args.file[:-5])
cGroup.SaveAs("Plots/Group_%s.pdf"%args.file[:-5])
cGroup.SaveAs("Plots/Group_%s.c"%args.file[:-5])


cParity = ROOT.TCanvas("cParity","Parity",800,600)
hParity = ROOT.TH1D("hParity","Parity",3,0,2)

for i in range(len(parity)):
    hParity.Fill(parity[i])
    pass

hParity.GetXaxis().SetNdivisions(3)
hParity.Draw("")
cParity.Update()
cParity.SaveAs("Plots/Parity_%s.root"%args.file[:-5])
cParity.SaveAs("Plots/Parity_%s.pdf"%args.file[:-5])
cParity.SaveAs("Plots/Parity_%s.c"%args.file[:-5])


cDelay = ROOT.TCanvas("cDelay","Delay",800,600)
hDelay = ROOT.TH1D("hDelay","Delay",3,6,8)

for i in range(len(delay)):
    hDelay.Fill(delay[i])
    pass

hDelay.GetXaxis().SetNdivisions(3)
hDelay.Draw("")
cDelay.Update()
cDelay.SaveAs("Plots/Delay_%s.root"%args.file[:-5])
cDelay.SaveAs("Plots/Delay_%s.pdf"%args.file[:-5])
cDelay.SaveAs("Plots/Delay_%s.c"%args.file[:-5])


cDColumn = ROOT.TCanvas("cDColumn","DColumn",800,600)
hDColumn = ROOT.TH1D("hDColumn","DColumn",256,0,256)

for i in range(len(dcolumn)):
    hDColumn.Fill(dcolumn[i])
    pass

hDColumn.Draw("")
cDColumn.Update()
cDColumn.SaveAs("Plots/DoubleColumn_%s.root"%args.file[:-5])
cDColumn.SaveAs("Plots/DoubleColumn_%s.pdf"%args.file[:-5])
cDColumn.SaveAs("Plots/DoubleColumn_%s.c"%args.file[:-5])


cBCID = ROOT.TCanvas("cBCID","BCID",800,600)
hBCID = ROOT.TH1D("hBCID","BCID",3,0,3)

for i in range(len(bcid)):
    hBCID.Fill(bcid[i])
    pass

hBCID.GetXaxis().SetNdivisions(4)
hBCID.Draw("")
cBCID.Update()
cBCID.SaveAs("Plots/BCID_%s.root"%args.file[:-5])
cBCID.SaveAs("Plots/BCID_%s.pdf"%args.file[:-5])
cBCID.SaveAs("Plots/BCID_%s.c"%args.file[:-5])


cChipID = ROOT.TCanvas("cChipID","ChipID",800,600)
hChipID = ROOT.TH1D("hChipID","ChipID",4,0,4)

for i in range(len(chipid)):
    hChipID.Fill(chipid[i])
    pass

hChipID.GetXaxis().SetNdivisions(4)
hChipID.Draw("")
cChipID.Update()
cChipID.SaveAs("Plots/ChipID_%s.root"%args.file[:-5])
cChipID.SaveAs("Plots/ChipID_%s.pdf"%args.file[:-5])
cChipID.SaveAs("Plots/ChipID_%s.c"%args.file[:-5])


cPhase = ROOT.TCanvas("cPhase","Phase",800,600)
hPhase = ROOT.TH1D("hPhase","Phase",8,0,8)

for i in range(len(phase)):
    hPhase.Fill(phase[i])
    pass

hPhase.GetXaxis().SetNdivisions(10)
hPhase.Draw("")
cPhase.Update()
cPhase.SaveAs("Plots/Phase_%s.root"%args.file[:-5])
cPhase.SaveAs("Plots/Phase_%s.pdf"%args.file[:-5])
cPhase.SaveAs("Plots/Phase_%s.c"%args.file[:-5])


cEvtID = ROOT.TCanvas("cEvtID","EvtID",800,600)
hEvtID = ROOT.TH1D("hEvtID","EvtID",16,0,16)

for i in range(len(evtid)):
    hEvtID.Fill(evtid[i])
    pass

hEvtID.GetXaxis().SetNdivisions(4)
hEvtID.Draw("")
cEvtID.Update()
cEvtID.SaveAs("Plots/EvtID_%s.root"%args.file[:-5])
cEvtID.SaveAs("Plots/EvtID_%s.pdf"%args.file[:-5])
cEvtID.SaveAs("Plots/EvtID_%s.c"%args.file[:-5])



cPixels = ROOT.TCanvas("cPixels","Pixels",800,600)
hPixels = ROOT.TH1I("hPixels","Pixels",16,0,16)

sum = 0
pix = 0
for pix in pixel:
    pix = "%s"%format(int(pix),"016b")
    #print( pix)
    for bit in range(16):
        #print( bit)
        if pix[15-bit] == "1":
            sum += 1
            hPixels.Fill(bit) 

print( "SUM: ",sum)
print( "diff" ,16000000-sum)
hPixels.Draw("")
cPixels.Update()
cPixels.SaveAs("Plots/Pixels_%s.root"%args.file[:-5])
cPixels.SaveAs("Plots/Pixels_%s.pdf"%args.file[:-5])
cPixels.SaveAs("Plots/Pixels_%s.c"%args.file[:-5])
'''


xmin= -0.5
xmax=511.5
ymin= -0.5
ymax=511.5

cPixelHitMap = ROOT.TCanvas("cPixelHitMap","PixelHitMap",600,600)
cPixelHitMap.SetRightMargin(0.11)
hPixelHitMap = ROOT.TH2D("hPixelHitMap","PixelHitMap",512,xmin,xmax,512,ymin,ymax)

allEvents=0
uniqueEvents=0
a = 0
eventsPassed=0
for event in range(len(dcolumn)):
    if isDuplicate[event] == 1: continue #add args
    if phase[event]==7        : continue ##VD: noise protection
    
    #if (l1id[event]==0    and timer[event]<4   ): continue
    #if (l1id[event]==1168 and timer[event]>1053): continue

    fullTime=bcid[event]*25+evtid[event]*3.125+phase[event]*0.39
    ###print( fullTime)
    if args.applyTimingCut:
        if fullTime>170: continue
        if fullTime< 10: continue
    ##if fullTime>200: continue
    #############################if fullTime<450: continue

    #md = MaltaData.MaltaData()
    #md.setWord1(int(word1[event]))
    #md.setWord2(int(word2[event]))
    #md.getInfo()

    ##if bcid[event]!=0: continue ##VALERIO!!!!!!!!!!!!!!11 AAAAAAARRRRRHHHHHHH
    FUCKpix = int(pixel[event])
    eventsPassed+=1
    #pixelStr = format(int("%.f"%(pixel[event])),'016b')
    for pix in range(16):
        if ((FUCKpix>>pix)&1)==0: 
            #print( "fuck fuck fuck")
            continue
        
        xpos = dcolumn[event]*2
        if pix>7: xpos+=1
        
        ypos = group[event]*16
        if parity[event]==1: ypos += 8
        
        if (pix == 0) or (pix == 8):  ypos+=0
        if (pix == 1) or (pix == 9):  ypos+=1
        if (pix == 2) or (pix == 10): ypos+=2
        if (pix == 3) or (pix == 11): ypos+=3
        if (pix == 4) or (pix == 12): ypos+=4
        if (pix == 5) or (pix == 13): ypos+=5
        if (pix == 6) or (pix == 14): ypos+=6
        if (pix == 7) or (pix == 15): ypos+=7
        #print( " XPOS: "+str(xpos)+" -- YPOS: "+str(ypos))

        #some on-the-fly pixel removal
       
        '''
        if args.applyFlorescenceCut:
            if xpos== 51 and ypos==240: continue
            if xpos== 51 and ypos==328: continue
            if xpos== 73 and ypos==195: continue
            if xpos== 90 and ypos==253: continue
            if xpos==125 and ypos==241: continue
            if xpos==152 and ypos==208: continue
            if xpos==177 and ypos==161: continue
            if xpos==185 and ypos==120: continue
            if xpos==226 and ypos==173: continue
            if xpos==0   and ypos== 85: continue
            if xpos==66  and ypos==104: continue
            if xpos==128 and ypos==141: continue
            if xpos==238 and ypos==126: continue
            if xpos== 74 and ypos== 21: continue
            if xpos== 98 and ypos==173: continue
            if xpos== 99 and ypos==111: continue
        '''    
        hPixelHitMap.Fill(xpos,ypos)

        pass
    pass

fname=os.path.basename(args.file)
if "." in fname: fname=fname.split(".")[0]

oFile=ROOT.TFile("Plots/PixelHitMap_%s.root"%fname,"RECREATE")
print( "Creating new file: Plots/PixelHitMap_%s.root"%fname)
print( "Total event number: %s"%len(dcolumn))
print( "Number of events after duplication: %s"%(eventsPassed))
print( "Number of pixel hits after duplication: %s"%(hPixelHitMap.Integral()))
hPixelHitMap.SetTitle("Frequency;Pixel PosX;Pixel PosY")
hPixelHitMap.SetMinimum(0.)
###hPixelHitMap.SetMaximum(1.)

hPixelHitMap.Draw("COLZ") ###TEXT
AllHit=float(hPixelHitMap.Integral())
print( " " )
sumV=0
seen=0

tomask=[]
for x in range(0,516):
    for y in range(0,516):
        cont=hPixelHitMap.GetBinContent(x,y)
        if cont!=0: seen+=1
        ####if cont!=0: ###IVAN  and hPixelHitMap.GetXaxis().GetBinCenter(x)==222 and hPixelHitMap.GetXaxis().GetBinCenter(y)==267: ###IVAN and 
        if float(cont)/AllHit>0.001:
            theString=""
            theString+="(x,y)= %2i :: %2i"%(hPixelHitMap.GetXaxis().GetBinCenter(x),hPixelHitMap.GetXaxis().GetBinCenter(y))
            ###IVAN 
            #print( (" %3i, %3i"%(hPixelHitMap.GetXaxis().GetBinCenter(x),hPixelHitMap.GetYaxis().GetBinCenter(y)))+("  entries: %7i "%cont))
            ##print( ""+str(int(hPixelHitMap.GetXaxis().GetBinCenter(x)))+","+str(int(hPixelHitMap.GetYaxis().GetBinCenter(y)))+","+str(int(cont)))
            #print( theString)
            tomask.append({'x':hPixelHitMap.GetXaxis().GetBinCenter(x),
                           'y':hPixelHitMap.GetXaxis().GetBinCenter(y),
                           'z':cont})
            
            sumV+=cont
            pass
        pass
    pass
    
for entry in sorted(tomask, key = lambda v: v['z']):
    print (" %3i, %3i  entries %7i"%(entry['x'],entry['y'],entry['z']))
    pass

print( " ")

for entry in sorted(tomask, key = lambda v: v['z']):
    print ("MASK_PIXEL: %3i, %3i #entries %7i"%(entry['x'],entry['y'],entry['z']))
    pass

print( " ")
for x in range(0,516):
    totCol=0
    for y in range(0,516):
        cont=hPixelHitMap.GetBinContent(x,y)
        totCol+=cont
    ###if totCol!=0: print( "Col: "+str(x-1)+"  saw "+str(int(totCol))+" hits!")

print( " ")
print( " Represented fraction is: "+str( float(sumV)/AllHit*100 )+" %")
print( " number of pixels with hits: "+str(seen))
print( " ")
#hPixelHitMap.GetZaxis().SetRangeUser(5000,1000)
hPixelHitMap.SetMaximum(hPixelHitMap.GetMaximum()*0.03)
#hPixelHitMap.RebinX(2)
#hPixelHitMap.RebinY(2)
cPixelHitMap.SetLogz()
cPixelHitMap.Update()
oFile.WriteObject(hPixelHitMap,"Map2D")
cPixelHitMap.Write()
oFile.Close()
oName="Plots/PixelHitMap_%s.pdf"%fname
cPixelHitMap.SaveAs(oName)
oNameroot="Plots/PixelHitMap_%s.root"%fname
cPixelHitMap.SaveAs(oNameroot)
###IVAN 
os.system("evince "+oName+" &")
#cPixelHitMap.SaveAs("Plots/PixelHitMap_%s.root"%args.file[:-5])
#cPixelHitMap.SaveAs("Plots/PixelHitMap_%s.c"%args.file[:-5])

